<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_occ extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function home() {
        //get status today
        //date_default_timezone_set('Asia/Jakarta');
        $today = get_dateotp('0 days');
        $stn = 'CGK';

        //ALL Station
        $data['nSchedule'] = get_schedule('count', $today);
        $data['nCancelPlan'] = get_cancelPlan('count', $today);
        $data['nCancel'] = get_cancel('count', $today);
        $data['nOnschedule'] = get_onschedule('count', $today);
        $data['nDeparted'] = get_departed('count', $today);
        $data['nDepartedArr'] = get_departedArr('count', $today);
        $data['nDelay'] = get_delay('count', $today);
        $data['nDelayArrival'] = get_delayArrival('count', $today);
        $data['nOntime'] = get_onTime('count', $today);
        $data['nOntimeArrival'] = get_onTimeArrival('count', $today);
        //Penyesuaian perhitungan OTP
        //$data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], $data['nCancel']);
        $data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], 0);
        $data['pOTPArrival'] = get_percentOTP($data['nOntimeArrival'], $data['nDepartedArr'], 0);
        $data['pOnschedule'] = get_percentage($data['nOnschedule'], $data['nSchedule'], 100);
        $data['pDeparted'] = get_percentage($data['nDeparted'], $data['nOnschedule'], 100);
        $data['pCancelPlan'] = get_percentage($data['nCancelPlan'], $data['nSchedule'], 100);
        $data['pCancel'] = get_percentage($data['nCancel'], $data['nSchedule'], 100);
        $data['pDelay'] = get_percentage($data['nDelay'], $data['nDeparted'], 100);
        $data['pDelayArrival'] = get_percentage($data['nDelayArrival'], $data['nDepartedArr'], 100);

        //CGK Station
        $data['nScheduleCgk'] = get_schedule('count', $today, NULL, $stn);
        $data['nCancelPlanCgk'] = get_cancelPlan('count', $today, NULL, $stn);
        $data['nCancelCgk'] = get_cancel('count', $today, NULL, $stn);
        $data['nOnscheduleCgk'] = get_onschedule('count', $today, NULL, $stn);
        $data['nDepartedCgk'] = get_departed('count', $today, NULL, $stn);
        $data['nDepartedCgkArr'] = get_departedArr('count', $today, NULL, NULL, $stn);
        $data['nDelayCgk'] = get_delay('count', $today, NULL, $stn);
        $data['nDelayCgkArrival'] = get_delayArrival('count', $today, NULL, NULL, $stn);
        $data['nOntimeCgk'] = get_onTime('count', $today, NULL, $stn);
        $data['nOntimeCgkArrival'] = get_onTimeArrival('count', $today, NULL, NULL, $stn);
        //penyesuaian perhitungan OTP CGK
        //$data['pOTPCgk'] = get_percentOTP($data['nOntimeCgk'], $data['nDepartedCgk'], $data['nCancelCgk']);
        $data['pOTPCgk'] = get_percentOTP($data['nOntimeCgk'], $data['nDepartedCgk'], 0);
        $data['pOTPCgkArrival'] = get_percentOTP($data['nOntimeCgkArrival'], $data['nDepartedCgkArr'], 0);
        $data['pOnscheduleCgk'] = get_percentage($data['nOnscheduleCgk'], $data['nScheduleCgk'], 100);
        $data['pDepartedCgk'] = get_percentage($data['nDepartedCgk'], $data['nOnscheduleCgk'], 100);
        $data['pCancelPlanCgk'] = get_percentage($data['nCancelPlanCgk'], $data['nScheduleCgk'], 100);
        $data['pCancelCgk'] = get_percentage($data['nCancelCgk'], $data['nScheduleCgk'], 100);
        $data['pDelayCgk'] = get_percentage($data['nDelayCgk'], $data['nDepartedCgk'], 100);
        $data['pDelayCgkArrival'] = get_percentage($data['nDelayCgkArrival'], $data['nDepartedCgkArr'], 100);

        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = ' | last update:' . date('H:i', $time);
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_occ_home';

        return $data;
    }

    function stat() {
        $data['arrStation'] = get_station('count');
        $data['stn'] = 'all';
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'data statistics';
        $data['view'] = 'v_occ_stat';

        return $data;
    }

    function daily() {
        if ($this->uri->segment(3))
            $data['stn'] = $this->uri->segment(3);
        else
            $data['stn'] = 'all';
        if ($this->uri->segment(4))
            $data['status'] = $this->uri->segment(4);
        else
            $data['status'] = 'all';
        $data['arrStation'] = get_station();
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'daily statistics';
        $data['view'] = 'v_occ_daily';

        return $data;
    }

    function tabledata($typeData, $sDate) {
        if ($typeData == 'scheduled')
            $data['arrRecords'] = get_recScheduleByDate($sDate);
        if ($typeData == 'cancelplan')
            $data['arrRecords'] = get_recCancelPlanByDate($sDate);
        if ($typeData == 'onschedule')
            $data['arrRecords'] = get_recOnScheduleByDate($sDate);
        if ($typeData == 'cancel')
            $data['arrRecords'] = get_recCancelByDate($sDate);
        if ($typeData == 'departure')
            $data['arrRecords'] = get_recDepartureByDate($sDate);
        if ($typeData == 'ontime')
            $data['arrRecords'] = get_recOnTimeByDate($sDate);
        if ($typeData == 'delay')
            $data['arrRecords'] = get_recDelayByDate($sDate);

        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = $typeData . ' data flight | ' . date('d F Y ');
        $data['breadcrumb'] = ucwords($typeData);
        $data['view'] = 'v_occ_tabledata';

        return $data;
    }

    function record() {
        if ($this->uri->segment(3))
            $data['stn'] = $this->uri->segment(3);
        else
            $data['stn'] = 'all';
        if ($this->uri->segment(4))
            $data['status'] = $this->uri->segment(4);
        else
            $data['status'] = 'all';
        $data['stnto'] = 'all';

        $data['arrStation'] = get_station();
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'flight record';
        $data['view'] = 'v_occ_record';

        return $data;
    }

    function viewrec($flightRef) {
        $data['arrRecords'] = get_dFlightOps($flightRef);
        $data['arrPax'] = get_dFlightPax($flightRef);
        $data['idflight'] = $flightRef;
        $data['view'] = 'v_occ_record_view';

        return $data;
    }

    /* Table Flight Number Begin */

    function dataflight($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $statuspp = NULL, $delay = NULL) {
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP per Day';
        $data['daily_record_flight'] = get_daily_record_flight($date1, $date2, $stnService, $org, $dest, $statuspp);
        //$data['daily_record_cod'] = get_daily_record_cod($date1, $date2, $stnService, $org, $dest, $statuspp);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        $data['delay'] = $delay;
        $data['org'] = $org;
        $data['dest'] = $dest;
        $data['statuspp'] = $statuspp;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_flightday';

        return $data;
    }

    //v2 update from dataflight
    function dailyotp_v2($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $statuspp = NULL, $delay = NULL) {
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP per Day';
        $data['daily_record_flight'] = get_daily_record_flight_otp_v2($date1, $date2, $stnService, $org, $dest, $statuspp);
        //$data['daily_record_cod'] = get_daily_record_cod($date1, $date2, $stnService, $org, $dest, $statuspp);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        $data['delay'] = $delay;
        $data['org'] = $org;
        $data['dest'] = $dest;
        $data['statuspp'] = $statuspp;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_flightday_v2';

        return $data;
    }

    function data_flight_by_station($date1 = NULL, $date2 = NULL, $stnService = NULL, $aptMgmt = NULL) {
        ini_set('max_execution_time', 60);

        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP per Station';
        $data['daily_record_flight'] = data_flight_by_station($today, $date1, $date2, $stnService, $aptMgmt);
        $data['daily_record_cod'] = data_flight_by_station_cod($today, $date1, $date2, $stnService, $aptMgmt);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_station';

        return $data;
    }

    function data_per_flight($date1 = NULL, $date2 = NULL, $station = NULL, $stationto = NULL, $flighttype = NULL) {
        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'flight record';
        $data['daily_record_per_flight'] = daily_record_per_flight($today, $date1, $date2, $station, $stationto, $flighttype);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if ($this->uri->segment(3))
            $data['stn'] = $this->uri->segment(3);
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_flight';

        return $data;
    }

    function data_per_flight_v2($date1 = NULL, $date2 = NULL, $station = NULL, $stationto = NULL, $flighttype = NULL) {
        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'flight record';
        $data['daily_record_per_flight'] = daily_record_per_flight($today, $date1, $date2, $station, $stationto, $flighttype);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if ($this->uri->segment(3))
            $data['stn'] = $this->uri->segment(3);
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_flight';

        return $data;
    }

    
    function data_dephub_report($date1 = NULL, $date2 = NULL, $stnService = NULL) {
        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP Dephub Report';
        $data['daily_record_per_flight'] = daily_dephub_report($today, $date1, $date2, $stnService);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_dephub';

        return $data;
    }

    function data_montly_report($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $statuspp = NULL) {

        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP Monthly';
        $data['monthly_record_flight'] = data_montly_report($date1, $date2, $stnService, $org, $dest, $statuspp);
        $data['monthly_record_cod'] = data_montly_report_cod($date1, $date2, $stnService, $org, $dest, $statuspp);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();
        //$data['datefrom'] = $date1;
        //$data['dateto'] = $date2;
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_flightmonth';

        return $data;
    }

    function data_delaycode_report($date1 = NULL, $date2 = NULL, $delayContributor = NULL) {
        // $data['title'] = 'OTP Dashboard';
        // $data['title_small'] = 'Daily Delay Contributor';
        $data['delay_code_contributor'] = data_delaycode_report($date1, $date2, $delayContributor);
        $data['ndelaycd'] = get_ndelay($date1, $date2, $delayContributor);
        $data['nCOD'] = get_causeOfDelay('count', $date1, $date2);
        $data['totalDep'] = get_departed('count', $date1, $date2);
        $data['totalDelay'] = get_delay('count', $date1, $date2);
        $data['pTotalDelay'] = get_percentage($data['totalDelay'], $data['totalDep'], 100, 2);
        $data['pdelaycd'] = get_percentage($data['ndelaycd'], $data['nCOD'], $data['pTotalDelay']);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        $data['delayContributor'] = $delayContributor;
        // $data['view'] = 'v_occ_record_delaycode';
        return $data;
    }

    function data_delaycode_report_v2($date1 = NULL, $date2 = NULL, $delayContributor = NULL, $sFleet = NULL) {
        // $data['title'] = 'OTP Dashboard';
        // $data['title_small'] = 'Daily Delay Contributor';
        $data['delay_code_contributor'] = data_delaycode_report_v2($date1, $date2, $delayContributor, $sFleet);
        $data['ndelaycd'] = get_ndelay($date1, $date2, $delayContributor);
        $data['nCOD'] = get_causeOfDelay('count', $date1, $date2);
        $data['totalDep'] = get_departed('count', $date1, $date2);
        $data['totalDelay'] = get_delay('count', $date1, $date2);
        $data['pTotalDelay'] = get_percentage($data['totalDelay'], $data['totalDep'], 100, 2);
        $data['pdelaycd'] = get_percentage($data['ndelaycd'], $data['nCOD'], $data['pTotalDelay']);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        $data['delayContributor'] = $delayContributor;
        // $data['view'] = 'v_occ_record_delaycode';
        return $data;
    }

    function details_delay($type_cod, $cod_date, $service, $org, $dest, $delay, $statuspp) {
        $today = get_dateotp('0 days');

        $data['detail_cod'] = get_detail_cod($type_cod, $cod_date, $service, $org, $dest, $delay, $statuspp);
        $data['typeCOD'] = $type_cod;
        $data['view'] = 'v_occ_detaildelay';

        return $data;
    }

    function login_page($username, $password) {
        if ($username == "occ" && $password == "adminOcc") {

            $data = true;
        } else {
            $data = false;
        }
        return $data;
    }

    function aboutotpdashboard() {
        $data["title"] = "OTP Dashboard";
        $data["title_small"] = "About and FAQ";
        $data['view'] = 'v_occ_about_otp';
        return $data;
    }

    function occreport($dOtp = NULL, $timeFilter = NULL) {
        $today = get_dateotp('0 days');
        $stn = 'CGK';
        $sTime = $dOtp . 'T00:00:00';
        if ($dOtp == date("Y-m-d")) {
            if ($timeFilter < 23 && $timeFilter > 0) {
                $eTime = $dOtp . 'T' . $timeFilter . ':00:00';
            } else if ($timeFilter == 0) {
                $eTime = $dOtp . 'T' . date("H:i:s");
            } else
                $eTime = $dOtp . 'T' . $timeFilter . ':59:00';
        }
        else if ($dOtp < date("Y-m-d")) {
            if ($timeFilter < 23 && $timeFilter > 0) {
                $eTime = $dOtp . 'T' . $timeFilter . ':00:00';
            } else if ($timeFilter == 0) {
                $eTime = $dOtp . 'T' . date("H:i:s");
            } else
                $eTime = $dOtp . 'T' . $timeFilter . ':59:00';
        }
        //ALL Station
        $data['nSchedule'] = get_schedule('count', $dOtp);
        $data['nCancelPlan'] = get_cancelPlan('count', $dOtp);
        $data['nCancel'] = get_cancelByRangeTime('count', $sTime, $eTime);
        $data['nOnschedule'] = get_onschedule('count', $dOtp);
        $data['nDeparted'] = get_departByRangeTime('count', $sTime, $eTime);
        $data['nDepartedArr'] = get_departArrByRangeTime('count', $sTime, $eTime);
        $data['nDelay'] = get_delayByRangeTime('count', $sTime, $eTime);
        $data['nDelayArrival'] = get_delayArrByRangeTime('count', $sTime, $eTime);
        $data['nOntime'] = get_ontimeByRangeTime('count', $sTime, $eTime);
        $data['nOntimeArrival'] = get_ontimeArrByRangeTime('count', $sTime, $eTime);
        //Penyesuaian perhitungan OTP
        //$data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], $data['nCancel']);
        $data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], 0);
        $data['pOTPArrival'] = get_percentOTP($data['nOntimeArrival'], $data['nDepartedArr'], 0);
        $data['pOnschedule'] = get_percentage($data['nOnschedule'], $data['nSchedule'], 100);
        $data['pDeparted'] = get_percentage($data['nDeparted'], $data['nOnschedule'], 100);
        $data['pCancelPlan'] = get_percentage($data['nCancelPlan'], $data['nSchedule'], 100);
        $data['pCancel'] = get_percentage($data['nCancel'], $data['nSchedule'], 100);
        $data['pDelay'] = get_percentage($data['nDelay'], $data['nDeparted'], 100);
        $data['pDelayArrival'] = get_percentage($data['nDelayArrival'], $data['nDepartedArr'], 100);

        //CGK Station
        $data['nScheduleCgk'] = get_schedule('count', $dOtp, NULL, $stn);
        $data['nCancelPlanCgk'] = get_cancelPlan('count', $dOtp, NULL, $stn);
        $data['nCancelCgk'] = get_cancel('count', $dOtp, NULL, $stn);
        $data['nOnscheduleCgk'] = get_onschedule('count', $dOtp, NULL, $stn);
        $data['nDepartedCgk'] = get_departByRangeTime('count', $sTime, $eTime, $stn);
        $data['nDepartedCgkArr'] = get_departArrByRangeTime('count', $sTime, $eTime, $stn);
        $data['nDelayCgk'] = get_delayByRangeTime('count', $sTime, $eTime, $stn);
        $data['nDelayCgkArrival'] = get_delayArrByRangeTime('count', $sTime, $eTime, $stn);
        $data['nOntimeCgk'] = get_ontimeByRangeTime('count', $sTime, $eTime, $stn);
        $data['nOntimeCgkArrival'] = get_ontimeArrByRangeTime('count', $sTime, $eTime, $stn);
        //penyesuaian perhitungan OTP CGK
        //$data['pOTPCgk'] = get_percentOTP($data['nOntimeCgk'], $data['nDepartedCgk'], $data['nCancelCgk']);
        $data['pOTPCgk'] = get_percentOTP($data['nOntimeCgk'], $data['nDepartedCgk'], 0);
        $data['pOTPCgkArrival'] = get_percentOTP($data['nOntimeCgkArrival'], $data['nDepartedCgkArr'], 0);
        $data['pOnscheduleCgk'] = get_percentage($data['nOnscheduleCgk'], $data['nScheduleCgk'], 100);
        $data['pDepartedCgk'] = get_percentage($data['nDepartedCgk'], $data['nOnscheduleCgk'], 100);
        $data['pCancelPlanCgk'] = get_percentage($data['nCancelPlanCgk'], $data['nScheduleCgk'], 100);
        $data['pCancelCgk'] = get_percentage($data['nCancelCgk'], $data['nScheduleCgk'], 100);
        $data['pDelayCgk'] = get_percentage($data['nDelayCgk'], $data['nDepartedCgk'], 100);
        $data['pDelayCgkArrival'] = get_percentage($data['nDelayCgkArrival'], $data['nDepartedCgkArr'], 100);

        //$data['nAvgDelay'] = get_averagedelay($data['nDelayCgkArrival'], $data['nDepartedCgkArr'], 100);
        //$data['nDelayFourHours'] = get_delayfourhours($data['nDelayCgkArrival'], $data['nDepartedCgkArr'], 100);
        //Delay ALL
        $totalDelay = get_delayByRangeTime('count', $sTime, $eTime);
        if ($totalDelay <> 0) {
            $sDate = get_dateotp('0 days');
            $nCOD = get_causeOfDelayRangeTime('count', $sTime, $eTime);
            $recCOD = get_causeOfDelayRangeTime('rec', $sTime, $eTime);
            $totalDep = get_departByRangeTime('count', $sTime, $eTime);
            $pTotalDelay = get_percentage($totalDelay, $totalDep, 100);
            $nAPTF = 0;
            $nCOMC = 0;
            $nFLOP = 0;
            $nOTHR = 0;
            $nSTNH = 0;
            $nSYST = 0;
            $nTECH = 0;
            $nWTHR = 0;

            if ($pTotalDelay <> 0) {
                foreach ($recCOD as $key => $row) {
                    $sCODType = get_causeOfDelayText($row['REASONCODE']);
                    if ($sCODType == 'APTF')
                        $nAPTF = $nAPTF + $row['NCD'];
                    if ($sCODType == 'COMC')
                        $nCOMC = $nCOMC + $row['NCD'];
                    if ($sCODType == 'FLOP')
                        $nFLOP = $nFLOP + $row['NCD'];
                    if ($sCODType == 'OTHR')
                        $nOTHR = $nOTHR + $row['NCD'];
                    if ($sCODType == 'STNH')
                        $nSTNH = $nSTNH + $row['NCD'];
                    if ($sCODType == 'SYST')
                        $nSYST = $nSYST + $row['NCD'];
                    if ($sCODType == 'TECH')
                        $nTECH = $nTECH + $row['NCD'];
                    if ($sCODType == 'WTHR')
                        $nWTHR = $nWTHR + $row['NCD'];
                    $data["paptf"] = get_percentage($nAPTF, $nCOD, $pTotalDelay);
                    $data["pcomc"] = get_percentage($nCOMC, $nCOD, $pTotalDelay);
                    $data["pflop"] = get_percentage($nFLOP, $nCOD, $pTotalDelay);
                    $data["pothr"] = get_percentage($nOTHR, $nCOD, $pTotalDelay);
                    $data["pstnh"] = get_percentage($nSTNH, $nCOD, $pTotalDelay);
                    $data["psyst"] = get_percentage($nSYST, $nCOD, $pTotalDelay);
                    $data["ptech"] = get_percentage($nTECH, $nCOD, $pTotalDelay);
                    $data["pwthr"] = get_percentage($nWTHR, $nCOD, $pTotalDelay);

                    $data["naptf"] = $nAPTF;
                    $data["ncomc"] = $nCOMC;
                    $data["nflop"] = $nFLOP;
                    $data["nothr"] = $nOTHR;
                    $data["nstnh"] = $nSTNH;
                    $data["nsyst"] = $nSYST;
                    $data["ntech"] = $nTECH;
                    $data["nwthr"] = $nWTHR;
                }
            }
        }
        //Delay CGK
        $totalDelaycgk = get_delayByRangeTime('count', $sTime, $eTime, $stn);
        if ($totalDelaycgk <> 0) {
            $sDate = get_dateotp('0 days');
            $nCODcgk = get_causeOfDelayRangeTime('count', $sTime, $eTime, 'CGK');
            $recCODcgk = get_causeOfDelayRangeTime('rec', $sTime, $eTime, 'CGK');
            $totalDepcgk = get_departByRangeTime('count', $sTime, $eTime, $stn);
            $pTotalDelaycgk = get_percentage($totalDelaycgk, $totalDepcgk, 100);
            $nAPTFcgk = 0;
            $nCOMCcgk = 0;
            $nFLOPcgk = 0;
            $nOTHRcgk = 0;
            $nSTNHcgk = 0;
            $nSYSTcgk = 0;
            $nTECHcgk = 0;
            $nWTHRcgk = 0;

            if ($pTotalDelaycgk <> 0) {
                foreach ($recCODcgk as $key => $row) {
                    $sCODType = get_causeOfDelayText($row['REASONCODE']);
                    if ($sCODType == 'APTF')
                        $nAPTFcgk = $nAPTFcgk + $row['NCD'];
                    if ($sCODType == 'COMC')
                        $nCOMCcgk = $nCOMCcgk + $row['NCD'];
                    if ($sCODType == 'FLOP')
                        $nFLOPcgk = $nFLOPcgk + $row['NCD'];
                    if ($sCODType == 'OTHR')
                        $nOTHRcgk = $nOTHRcgk + $row['NCD'];
                    if ($sCODType == 'STNH')
                        $nSTNHcgk = $nSTNHcgk + $row['NCD'];
                    if ($sCODType == 'SYST')
                        $nSYSTcgk = $nSYSTcgk + $row['NCD'];
                    if ($sCODType == 'TECH')
                        $nTECHcgk = $nTECHcgk + $row['NCD'];
                    if ($sCODType == 'WTHR')
                        $nWTHRcgk = $nWTHRcgk + $row['NCD'];
                    $data["paptfcgk"] = get_percentage($nAPTFcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["pcomccgk"] = get_percentage($nCOMCcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["pflopcgk"] = get_percentage($nFLOPcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["pothrcgk"] = get_percentage($nOTHRcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["pstnhcgk"] = get_percentage($nSTNHcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["psystcgk"] = get_percentage($nSYSTcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["ptechcgk"] = get_percentage($nTECHcgk, $nCODcgk, $pTotalDelaycgk);
                    $data["pwthrcgk"] = get_percentage($nWTHRcgk, $nCODcgk, $pTotalDelaycgk);

                    $data["naptfcgk"] = $nAPTFcgk;
                    $data["ncomccgk"] = $nCOMCcgk;
                    $data["nflopcgk"] = $nFLOPcgk;
                    $data["nothrcgk"] = $nOTHRcgk;
                    $data["nstnhcgk"] = $nSTNHcgk;
                    $data["nsystcgk"] = $nSYSTcgk;
                    $data["ntechcgk"] = $nTECHcgk;
                    $data["nwthrcgk"] = $nWTHRcgk;
                }
            }
        }

        //OTP SPECIAL ROUTE
        $data["get_otp_special_route"] = getotpspecialroute($sTime, $eTime);
        $data["get_info_delay"] = getcurrentinfodelay($sTime, $eTime);

        $data["get_average_delay"] = get_averagedelay(1, $sTime, $eTime);
        $data["get_delay_four"] = get_averagedelay(2, $sTime, $eTime);

        $data["get_average_delay_cgk"] = get_averagedelay(1, $sTime, $eTime, "CGK");
        $data["get_delay_four_cgk"] = get_averagedelay(2, $sTime, $eTime, "CGK");

        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = ' | last update:' . date('H:i', $time);
        $data['jam'] = date('H:i', $time);
        //$data['today'] = $today;
        $data["timeFilter"] = $timeFilter;
        $data["dOtp"] = $dOtp;
        $data['view'] = 'v_occ_record_occreport';

        return $data;
    }

    public function cis_crew($fnum = NULL, $stn = NULL) {
        if ($fnum != NULL) {
            $data['org'] = get_std($fnum, 2);
            $data['std'] = get_std($fnum, 1);
            $data['dest'] = get_std($fnum, 3);
            $data['getcis'] = get_cis_crew($data['org']);
            foreach ($data['getcis'] as $key => $value) {
                $data["hotel_calling"] = $value["cis_hotel_calling"];
                $data["hotel_pickup"] = $value["cis_hotel_pickup"];
                $data["gm_name"] = $value["cis_gm_name"];
                $data["gm_phone1"] = $value["cis_gm_phone1"];
                $data["gm_phone2"] = $value["cis_gm_phone2"];
                $data["kk_name"] = $value["cis_kk_name"];
                $data["kk_phone1"] = $value["cis_kk_phone1"];
                $data["kk_phone2"] = $value["cis_kk_phone2"];
                $data["ko_name"] = $value["cis_ko_name"];
                $data["ko_phone1"] = $value["cis_ko_phone1"];
                $data["ko_phone2"] = $value["cis_ko_phone2"];
                $data["gmf_name"] = $value["cis_mm_name"];
                $data["gmf_phone1"] = $value["cis_mm_phone1"];
                $data["gmf_phone2"] = $value["cis_mm_phone2"];
                $data["ks_name"] = $value["cis_ks_name"];
                $data["ks_phone1"] = $value["cis_ks_phone1"];
                $data["ks_phone2"] = $value["cis_ks_phone2"];
                $data["hotel_name"] = $value["cis_hotel_name"];
                $data["hotel_phone1"] = $value["cis_hotel_phone1"];
                $data["hotel_phone2"] = $value["cis_hotel_phone2"];
                $data["hotel_address"] = $value["cis_hotel_address"];
                $data["transport_name"] = $value["cis_transport_name"];
                $data["transport_phone1"] = $value["cis_transport_phone1"];
                $data["transport_phone2"] = $value["cis_transport_phone2"];
                $data["transport_address"] = $value["cis_transport_address"];
                $data["hospital_name"] = $value["cis_hospital_name"];
                $data["hospital_phone1"] = $value["cis_hospital_phone1"];
                $data["hospital_phone2"] = $value["cis_hospital_phone2"];
                $data["hospital_address"] = $value["cis_hospital_address"];
                $data["noted"] = $value["cis_noted"];
            }
        }
        else
        {
            $data['org'] = $stn;
            $data['getcis'] = get_cis_crew($data['org']);
            foreach ($data['getcis'] as $key => $value) {
                $data["hotel_calling"] = $value["cis_hotel_calling"];
                $data["hotel_pickup"] = $value["cis_hotel_pickup"];
                $data["gm_name"] = $value["cis_gm_name"];
                $data["gm_phone1"] = $value["cis_gm_phone1"];
                $data["gm_phone2"] = $value["cis_gm_phone2"];
                $data["kk_name"] = $value["cis_kk_name"];
                $data["kk_phone1"] = $value["cis_kk_phone1"];
                $data["kk_phone2"] = $value["cis_kk_phone2"];
                $data["ko_name"] = $value["cis_ko_name"];
                $data["ko_phone1"] = $value["cis_ko_phone1"];
                $data["ko_phone2"] = $value["cis_ko_phone2"];
                $data["gmf_name"] = $value["cis_mm_name"];
                $data["gmf_phone1"] = $value["cis_mm_phone1"];
                $data["gmf_phone2"] = $value["cis_mm_phone2"];
                $data["ks_name"] = $value["cis_ks_name"];
                $data["ks_phone1"] = $value["cis_ks_phone1"];
                $data["ks_phone2"] = $value["cis_ks_phone2"];
                $data["hotel_name"] = $value["cis_hotel_name"];
                $data["hotel_phone1"] = $value["cis_hotel_phone1"];
                $data["hotel_phone2"] = $value["cis_hotel_phone2"];
                $data["hotel_address"] = $value["cis_hotel_address"];
                $data["transport_name"] = $value["cis_transport_name"];
                $data["transport_phone1"] = $value["cis_transport_phone1"];
                $data["transport_phone2"] = $value["cis_transport_phone2"];
                $data["transport_address"] = $value["cis_transport_address"];
                $data["hospital_name"] = $value["cis_hospital_name"];
                $data["hospital_phone1"] = $value["cis_hospital_phone1"];
                $data["hospital_phone2"] = $value["cis_hospital_phone2"];
                $data["hospital_address"] = $value["cis_hospital_address"];
                $data["noted"] = $value["cis_noted"];
            }
        }
        $data['title'] = 'Crew Information Sheet';
        $data['title_small'] = 'Crew Page';
        $data['view'] = 'v_occ_record_cis_crew';

        $data['arrStation'] = get_station();
        //$data['fltnumber'] = get_flight_number($this->session->userdata('station'));
        $data['fltnumber'] = get_flight_number('CGK');
        return $data;
    }

    public function cis_admin($stn) {
        $data['title'] = 'Crew Information Sheet';
        $data['title_small'] = 'Station Page';
        $data['view'] = 'v_occ_cis_admin';
        $data['getcisadmin'] = get_cis_admin($stn);
        foreach ($data['getcisadmin'] as $key => $value) {
                $data["hotel_calling"] = $value["cis_hotel_calling"];
                $data["hotel_pickup"] = $value["cis_hotel_pickup"];
                $data["gm_name"] = $value["cis_gm_name"];
                $data["gm_phone1"] = $value["cis_gm_phone1"];
                $data["gm_phone2"] = $value["cis_gm_phone2"];
                $data["kk_name"] = $value["cis_kk_name"];
                $data["kk_phone1"] = $value["cis_kk_phone1"];
                $data["kk_phone2"] = $value["cis_kk_phone2"];
                $data["ko_name"] = $value["cis_ko_name"];
                $data["ko_phone1"] = $value["cis_ko_phone1"];
                $data["ko_phone2"] = $value["cis_ko_phone2"];
                $data["gmf_name"] = $value["cis_mm_name"];
                $data["gmf_phone1"] = $value["cis_mm_phone1"];
                $data["gmf_phone2"] = $value["cis_mm_phone2"];
                $data["ks_name"] = $value["cis_ks_name"];
                $data["ks_phone1"] = $value["cis_ks_phone1"];
                $data["ks_phone2"] = $value["cis_ks_phone2"];
                $data["hotel_name"] = $value["cis_hotel_name"];
                $data["hotel_phone1"] = $value["cis_hotel_phone1"];
                $data["hotel_phone2"] = $value["cis_hotel_phone2"];
                $data["hotel_address"] = $value["cis_hotel_address"];
                $data["transport_name"] = $value["cis_transport_name"];
                $data["transport_phone1"] = $value["cis_transport_phone1"];
                $data["transport_phone2"] = $value["cis_transport_phone2"];
                $data["transport_address"] = $value["cis_transport_address"];
                $data["hospital_name"] = $value["cis_hospital_name"];
                $data["hospital_phone1"] = $value["cis_hospital_phone1"];
                $data["hospital_phone2"] = $value["cis_hospital_phone2"];
                $data["hospital_address"] = $value["cis_hospital_address"];
                $data["noted"] = $value["cis_noted"];
            }
        return $data;
    }

    public function insert_cis_admin($stn, $hotel_calling, $hotel_pickup, $gm_name, $gm_phone1, $gm_phone2, $kk_name, $kk_phone1, $kk_phone2, $ko_name, $ko_phone1, $ko_phone2, $gmf_name, $gmf_phone1, $gmf_phone2, $ks_name, $ks_phone1, $ks_phone2, $hotel_name, $hotel_phone1, $hotel_phone2, $hotel_address, $transport_name, $transport_phone1, $transport_phone2, $transport_address, $hospital_name, $hospital_phone1, $hospital_phone2, $hospital_address, $noted) {
        $data['insertcis'] = insert_cis_admin($stn, $hotel_calling, $hotel_pickup, $gm_name, $gm_phone1, $gm_phone2, $kk_name, $kk_phone1, $kk_phone2, $ko_name, $ko_phone1, $ko_phone2, $gmf_name, $gmf_phone1, $gmf_phone2, $ks_name, $ks_phone1, $ks_phone2, $hotel_name, $hotel_phone1, $hotel_phone2, $hotel_address, $transport_name, $transport_phone1, $transport_phone2, $transport_address, $hospital_name, $hospital_phone1, $hospital_phone2, $hospital_address, $noted);
        $data['msg'] = "Data updated succesfully";
        return $data;
    }

    function gamovementap($date1 = NULL, $date2 = NULL, $station = NULL, $stationto = NULL, $flighttype = NULL) {
        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'flight record';
        $data['daily_record_per_flight'] = gamovementap($today, $date1, $date2, $station, $stationto, $flighttype);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if ($this->uri->segment(3))
            $data['stn'] = $this->uri->segment(3);
        else
            $data['stn'] = 'all';
        $data['stnto'] = 'all';
        $data['arrStation'] = get_station();

        $data['view'] = 'v_occ_record_flight_AP';

        return $data;
    }

   	function data_dephub_report_new($date1 = NULL, $date2 = NULL, $stnService = NULL) {
        $today = get_dateotp('0 days');
        $data['title'] = 'OTP Dashboard';
        $data['title_small'] = 'OTP Dephub Report';
        $data['daily_record_per_flight'] = daily_dephub_report_new($today, $date1, $date2, $stnService);
        $data['datefrom'] = $date1;
        $data['dateto'] = $date2;
        if (!is_null($stnService))
            $data['stn'] = $stnService;
        else
            $data['stn'] = 'all';
        /* $data['totalDailySchedule'] = get_totalSchedule('count',$today);
          $data['total_ontime'] = get_totalOnTime($today);
          $data['total_arr_ontime'] = get_totalArrivalOnTime($today);
          $data['total_technic'] = get_delay_technic($today);
          $data['total_stnh'] = get_delay_stnh($today);
          $data['total_comm'] = get_delay_comm($today);
          $data['total_syst'] = get_delay_syst($today);
          $data['total_flops'] = get_delay_flops($today);
          $data['total_aptf'] = get_delay_aptf($today);
          $data['total_weather'] = get_delay_weather($today);
          $data['total_misc'] = get_delay_misc($today);
          $data['total_cod'] = get_total_cod($today); */
        $data['view'] = 'v_occ_record_dephub_new';
        return $data;
    }

}
