<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function getDailyFH($sDate, $eDate) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);

    $CI->db->select('STAFFNUMBER, "DATE", VALUE');
    $CI->db->from('DBSABRECM.CREWDAILYAGGREGATE');
    $CI->db->where('"DATE" >= TO_DATE('.$sDate.', \'YYYY-MM-DD\')'); 
    $CI->db->where('"DATE" < TO_DATE('.$eDate.', \'YYYY-MM-DD\')'); 
    $CI->db->where('NAME', 'DAILY_FH');
    $CI->db->order_by('STAFFNUMBER, "DATE"');

    $q = $CI->db->get();
    $CI->db->close();

    $result = $q->result_array();
    return $result;
}

function getFH($sDay, $hmin, $hmax) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);

    $CI->db->select("STAFFNUMBER, SUM(VALUE) AS TOT_VAL");
    $CI->db->from("DBSABRECM.CREWDAILYAGGREGATE");
    $CI->db->where("\"DATE\" = TO_DATE('".$sDay."', 'YYYY-MM-DD')"); 
    $CI->db->where("VALUE BETWEEN '".$hmin."' AND '".$hmax."'");
    $CI->db->where("NAME", "DAILY_FH");
    $CI->db->group_by("STAFFNUMBER");

    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->num_rows();
    else return 0;
}

function get_record($sDate, $eDate, $fhStart = NULL, $fhEnd = NULL, $acType = NULL, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    if ($acType) $sacType = "AND TBL_TMP.EQTKEY_CODE = '".$acType."'"; else $sacType = "";
    if ($act) $sact = "AND TBL_TMP.ACTIVITY_TYPE = '".$act."'"; else $sact = "";
    /* 
    $CI->db->select("CREWDAILYAGGREGATE.STAFFNUMBER, SUM(VALUE) AS TOT_VAL");
    $CI->db->from("DBSABRECM.CREWDAILYAGGREGATE");
    $CI->db->where("\"DATE\" >= TO_DATE('".$sDate."', 'YYYY-MM-DD')"); 
    $CI->db->where("\"DATE\" <= TO_DATE('".$eDate."', 'YYYY-MM-DD')"); 
    $CI->db->where("NAME", "DAILY_FH");
    $CI->db->group_by("CREWDAILYAGGREGATE.STAFFNUMBER");
    $CI->db->order_by("TOT_VAL", "DESC");
    */

    $sQuery = "SELECT * FROM (
                SELECT
                    TBL_TMP.STAFFNUMBER,
                    TBL_TMP.FIRSTNAME,
                    TBL_TMP.LASTNAME,
                    TBL_TMP.EQTKEY_CODE,
                    SUM(ROUND((CAST(TO_TIMESTAMP(TBL_TMP.ARR_TIME,'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE)  - CAST(TO_TIMESTAMP(TBL_TMP.DEP_TIME,'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE))*1440)) as VALUEMINS        
                FROM (
                    SELECT
                        CREWROSTER.CREATEDATE,
                        CREWROSTER.\"VERSION\",
                        CREWROSTER.PAIRINGID,
                        FLIGHTLEG.FLIGHTLEGID,
                        FLIGHTLEG.EQTKEY_CODE,
                        CREWROSTER.STAFFNUMBER,
                        CREW.FIRSTNAME,
                        CREW.LASTNAME,
                        CREWROSTER.PATTERNLABEL,
                        CREWROSTER.POSITIONCODE,
                        FLIGHTLEG.OPERATIONALSUFFIX,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE = 'DEADHEAD' THEN 'DEADHEAD'
                            WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' AND PAIRING_DUTYACTIVITY.AIRPORTCODE != PAIRING.BASECODE THEN 'LAYOVER'
                            ELSE PAIRING_DUTYACTIVITY.TYPE END
                        ) AS ACTIVITY_TYPE,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('FLY') THEN (FLIGHTLEG.AIRLINECODE || FLIGHTLEG.FLIGHTNUMBER)
                            ELSE PAIRING_DUTYACTIVITY.ACTIVITYTYPECODE END
                        ) AS ACTIVITY_CODE,
                        FLIGHTLEG.FLIGHTNUMBER,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY') THEN FLIGHTLEG.DEPAIRPORTCODE
                            WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN PAIRING_DUTYACTIVITY.AIRPORTCODE
                            ELSE PAIRING_DUTYACTIVITY.STARTAIRPORTCODE END
                        ) AS DEP_STATION,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY')
                            THEN TO_CHAR(FLIGHTLEG.LATESTDEPTIMEUTC, 'YYYY-MM-DD HH24:MI')
                            WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN TO_CHAR(PAIRING_DUTYACTIVITY.STARTTIMEUTC, 'YYYY-MM-DD HH24:MI')
                            ELSE TO_CHAR(PAIRING_DUTYACTIVITY.ACTUALSTARTTIMEUTC, 'YYYY-MM-DD HH24:MI') END
                        ) AS DEP_TIME,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY') THEN FLIGHTLEG.ARRAIRPORTCODE
                            WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN PAIRING_DUTYACTIVITY.AIRPORTCODE
                            ELSE PAIRING_DUTYACTIVITY.ENDAIRPORTCODE END
                        ) AS ARR_STATION,
                        (
                            CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY')
                            THEN TO_CHAR(FLIGHTLEG.LATESTARRTIMEUTC, 'YYYY-MM-DD HH24:MI')
                            WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN TO_CHAR(PAIRING_DUTYACTIVITY.ENDTIMEUTC, 'YYYY-MM-DD HH24:MI')
                            ELSE TO_CHAR(PAIRING_DUTYACTIVITY.ACTUALENDTIMEUTC, 'YYYY-MM-DD HH24:MI') END
                        ) AS ARR_TIME,
                        FLIGHTLEG.STATUS
                    FROM DBSABRECM.PAIRING_DUTYACTIVITY
                    JOIN DBSABRECM.CREWROSTER
                        ON CREWROSTER.PAIRINGID = PAIRING_DUTYACTIVITY.PAIRINGID
                    JOIN DBSABRECM.PAIRING
                        ON CREWROSTER.PAIRINGID = PAIRING.PAIRINGID
                    LEFT JOIN DBSABRECM.FLIGHTLEG
                        ON PAIRING_DUTYACTIVITY.FLIGHTLEGID = FLIGHTLEG.FLIGHTLEGID
                    LEFT JOIN DBSABRECM.CREW
                        ON CREW.STAFFNUMBER = CREWROSTER.STAFFNUMBER
                    WHERE PAIRING_DUTYACTIVITY.\"TYPE\" != 'IN'
                    AND (
                        (
                            CREWROSTER.STARTDATETIMEUTC >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
                            AND CREWROSTER.STARTDATETIMEUTC < TO_DATE('".$eDate."', 'YYYY-MM-DD')
                        ) OR
                        (
                            CREWROSTER.ENDDATETIMEUTC >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
                            AND CREWROSTER.ENDDATETIMEUTC < TO_DATE('".$eDate."', 'YYYY-MM-DD')
                        )
                    )
                    AND CREWROSTER.POSITIONCODE <> 'CP'        
                    ORDER BY DEP_TIME ASC
                ) TBL_TMP
            WHERE TO_DATE(TBL_TMP.DEP_TIME, 'YYYY-MM-DD HH24:MI') >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
            AND TO_DATE(TBL_TMP.DEP_TIME, 'YYYY-MM-DD HH24:MI') <= TO_DATE('".$eDate."', 'YYYY-MM-DD')
            ".$sacType."
            ".$sact."
            GROUP BY TBL_TMP.STAFFNUMBER, TBL_TMP.FIRSTNAME, TBL_TMP.LASTNAME, TBL_TMP.EQTKEY_CODE
            ) TBL_TMP
            ORDER BY VALUEMINS DESC";

    $q = $CI->db->query($sQuery);
    //echo $CI->db->last_query(); exit;
    $CI->db->close();

    if ($q->num_rows() > 0) {
        $result = $q->result_array();
        //print_r($result); exit;

        return $result;
    } else return FALSE;
}

function getCrewFH($fhStart, $fhEnd, $sDate, $eDate) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);

    $CI->db->select("COUNT(STAFFNUMBER) AS CREWCOUNT");
    $CI->db->from("DBSABRECM.CREWDAILYAGGREGATE");
    $CI->db->where("\"DATE\" >= TO_DATE('".$sDate."', 'YYYY-MM-DD')"); 
    $CI->db->where("\"DATE\" <= TO_DATE('".$eDate."', 'YYYY-MM-DD')"); 
    $CI->db->where("VALUE BETWEEN '".$fhStart."' AND '".$fhEnd."'");
    $CI->db->where("NAME", "DAILY_FH");

    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->CREWCOUNT;
    else return 0;
}

function countFlightHour($sDate, $eDate, $fhStart, $fhEnd, $acType = NULL, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    if ($acType) $sacType = "AND TBL_TMP.EQTKEY_CODE = '".$acType."'"; else $sacType = "";
    if ($act) $sact = "AND TBL_TMP.ACTIVITY_TYPE = '".$act."'"; else $sact = "";
    $sQuery = "SELECT * FROM (
                    SELECT
                        TBL_TMP.STAFFNUMBER,        
                        SUM(ROUND((CAST(TO_TIMESTAMP(TBL_TMP.ARR_TIME,'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE)  - CAST(TO_TIMESTAMP(TBL_TMP.DEP_TIME,'YYYY-MM-DD HH24:MI:SS.FF1') AS DATE))*1440)) as VALUEMINS        
                    FROM (
                        SELECT
                            CREWROSTER.CREATEDATE,
                            CREWROSTER.\"VERSION\",
                            CREWROSTER.PAIRINGID,
                            FLIGHTLEG.FLIGHTLEGID,
                            FLIGHTLEG.EQTKEY_CODE,
                            CREWROSTER.STAFFNUMBER,
                            CREWROSTER.PATTERNLABEL,
                            CREWROSTER.POSITIONCODE,
                            FLIGHTLEG.OPERATIONALSUFFIX,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE     = 'DEADHEAD' THEN 'DEADHEAD'
                                WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' AND PAIRING_DUTYACTIVITY.AIRPORTCODE != PAIRING.BASECODE THEN 'LAYOVER'
                                ELSE PAIRING_DUTYACTIVITY.TYPE END
                            ) AS ACTIVITY_TYPE,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('FLY') THEN (FLIGHTLEG.AIRLINECODE || FLIGHTLEG.FLIGHTNUMBER)
                                ELSE PAIRING_DUTYACTIVITY.ACTIVITYTYPECODE END
                            ) AS ACTIVITY_CODE,
                            FLIGHTLEG.FLIGHTNUMBER,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY') THEN FLIGHTLEG.DEPAIRPORTCODE
                                WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN PAIRING_DUTYACTIVITY.AIRPORTCODE
                                ELSE PAIRING_DUTYACTIVITY.STARTAIRPORTCODE END
                            ) AS DEP_STATION,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY')
                                THEN TO_CHAR(FLIGHTLEG.LATESTDEPTIMEUTC, 'YYYY-MM-DD HH24:MI')
                                WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN TO_CHAR(PAIRING_DUTYACTIVITY.STARTTIMEUTC, 'YYYY-MM-DD HH24:MI')
                                ELSE TO_CHAR(PAIRING_DUTYACTIVITY.ACTUALSTARTTIMEUTC, 'YYYY-MM-DD HH24:MI') END
                            ) AS DEP_TIME,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY') THEN FLIGHTLEG.ARRAIRPORTCODE
                                WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN PAIRING_DUTYACTIVITY.AIRPORTCODE
                                ELSE PAIRING_DUTYACTIVITY.ENDAIRPORTCODE END
                            ) AS ARR_STATION,
                            (
                                CASE WHEN PAIRING_DUTYACTIVITY.TYPE IN ('DEADHEAD', 'FLY')
                                THEN TO_CHAR(FLIGHTLEG.LATESTARRTIMEUTC, 'YYYY-MM-DD HH24:MI')
                                WHEN PAIRING_DUTYACTIVITY.TYPE = 'OUT' THEN TO_CHAR(PAIRING_DUTYACTIVITY.ENDTIMEUTC, 'YYYY-MM-DD HH24:MI')
                                ELSE TO_CHAR(PAIRING_DUTYACTIVITY.ACTUALENDTIMEUTC, 'YYYY-MM-DD HH24:MI') END
                            ) AS ARR_TIME,
                            FLIGHTLEG.STATUS
                        FROM DBSABRECM.PAIRING_DUTYACTIVITY
                        JOIN DBSABRECM.CREWROSTER
                            ON CREWROSTER.PAIRINGID = PAIRING_DUTYACTIVITY.PAIRINGID
                        JOIN DBSABRECM.PAIRING
                            ON CREWROSTER.PAIRINGID = PAIRING.PAIRINGID
                        LEFT JOIN DBSABRECM.FLIGHTLEG
                            ON PAIRING_DUTYACTIVITY.FLIGHTLEGID = FLIGHTLEG.FLIGHTLEGID
                        WHERE PAIRING_DUTYACTIVITY.\"TYPE\" != 'IN'
                        AND (
                            (
                                CREWROSTER.STARTDATETIMEUTC >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
                                AND CREWROSTER.STARTDATETIMEUTC <= TO_DATE('".$eDate."', 'YYYY-MM-DD')
                            ) OR
                            (
                                CREWROSTER.ENDDATETIMEUTC >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
                                AND CREWROSTER.ENDDATETIMEUTC <= TO_DATE('".$eDate."', 'YYYY-MM-DD')
                            )
                        )
                        AND CREWROSTER.POSITIONCODE <> 'CP'
                        ORDER BY DEP_TIME ASC
                    ) TBL_TMP
                JOIN DBSABRECM.CREW
                ON CREW.STAFFNUMBER = TBL_TMP.STAFFNUMBER
                WHERE TO_DATE(TBL_TMP.DEP_TIME, 'YYYY-MM-DD HH24:MI') >= TO_DATE('".$sDate."', 'YYYY-MM-DD')
                AND TO_DATE(TBL_TMP.DEP_TIME, 'YYYY-MM-DD HH24:MI') <= TO_DATE('".$eDate."', 'YYYY-MM-DD')
                ".$sacType."
                ".$sact."
                GROUP BY TBL_TMP.STAFFNUMBER
                ) TBL_TMP
                WHERE VALUEMINS BETWEEN ".$fhStart." AND ".$fhEnd."
                ";
    $q = $CI->db->query($sQuery);
    //echo $CI->db->last_query(); exit;
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->num_rows();
    else return false;
}

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    
    return sprintf($format, $hours, $minutes);
} 

function getListMonth() {
    $formattedMonthArray = array(
        "1" => "January", "2" => "February", "3" => "March", "4" => "April",
        "5" => "May", "6" => "June", "7" => "July", "8" => "August",
        "9" => "September", "10" => "October", "11" => "November", "12" => "December",
    );

    return $formattedMonthArray;
}

function getlistAct() {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $CI->db->select("DISTINCT(TYPE) AS ACTYPE");
    $CI->db->from("DBSABRECM.PAIRING_DUTYACTIVITY");
    $CI->db->order_by('TYPE');
    $q = $CI->db->get();
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function getListAcType() {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $CI->db->select("DISTINCT(EQTTYPECODE) AS EQT");
    $CI->db->from("DBSABRECM.AIRCRAFT_EQTTYPE");
    $CI->db->order_by('EQTTYPECODE');
    $q = $CI->db->get();
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function getlistActLocal() {
    $CI = & get_instance();
    $actType = array('daily flying hours', 'DAILY_DO', 'Daily duty hours', 'DAILY_ABSC');
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("DISTINCT UPPER(type) AS ACTLOCALTYPE");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->where_in('type', $actType);
    $CI->db->order_by('UPPER(type)', 'DESC');
    $q = $CI->db->get();    
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function countFHlocal($sDate, $eDate, $fhStart, $fhEnd, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("crew_daily_statistics.staffnumber, sum(value) as fh");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);    
    $CI->db->group_by('crew_daily_statistics.staffnumber');
    if ($fhStart == '6001') $CI->db->having("fh > '".$fhStart."'");
    else $CI->db->having("fh between '".$fhStart."' and '".$fhEnd."'");
    $CI->db->order_by('fh', 'desc');

    $q = $CI->db->get(); 
    //echo $CI->db->last_query(); 
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $q->num_rows();
    else return FALSE;
}

function getRecordFhLocal($sDate, $eDate, $rangeFH = NULL, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    //$CI->db->select("FIRSTNAME, LASTNAME, crew_daily_statistics.staffnumber, sum(value) as fh");
    $CI->db->select("FIRSTNAME, crew_daily_statistics.staffnumber, sum(value) as fh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    //$CI->db->join('crew', 'crew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);  
    $CI->db->group_by('crew_daily_statistics.staffnumber');
    if ($rangeFH=='6001') $CI->db->having("fh > '".$rangeFH."'");
    else if ($rangeFH<>'') $CI->db->having("fh between '".$rangeFH."'");    
    $CI->db->order_by('fh', 'desc');

    $q = $CI->db->get(); 
    //echo $CI->db->last_query();
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function getRecordFhAvgLocal($nYear) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    //$CI->db->select("FIRSTNAME, LASTNAME, crew_daily_statistics.staffnumber, sum(value) as fh");
    $CI->db->select("FIRSTNAME, crew_daily_statistics.staffnumber, sum(value) as fh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    //$CI->db->join('crew', 'crew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("year(tdate) = '".$nYear."'");
    $actType = array('daily flying hours', 'DAILY_FH');
    $CI->db->where_in('type', $actType);
    $CI->db->group_by('crew_daily_statistics.staffnumber');
    $CI->db->order_by('fh', 'desc');

    $q = $CI->db->get(); 
    //echo $CI->db->last_query();
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function getAvg($sDate, $eDate, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("sum(value) as sumfh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    //if ($act<>'') $CI->db->where('type', $act);
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);  
    
    $q = $CI->db->get(); 
    $sumval = $q->row()->sumfh;
    //echo $CI->db->last_query();
    //$CI->db->close();    

    //$CI->db->select("count(value) as countfh");
    $CI->db->select("COUNT(DISTINCT(crew_daily_statistics.staffnumber)) as countfh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    //if ($act<>'') $CI->db->where('type', $act);
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);  

    $q2 = $CI->db->get(); 
    $nval = $q2->row()->countfh;
    //echo $CI->db->last_query();
    $CI->db->close();    

    $avg = $sumval / $nval;

    return $avg;
}

function getAvgMonth($nMonth, $nYear) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("sum(value) as sumfh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("month(tdate) = '".$nMonth."' and year(tdate) = '".$nYear."'");
    $actType = array('daily flying hours', 'DAILY_FH');
    $CI->db->where_in('type', $actType);
    
    $q = $CI->db->get(); 
    $sumval = $q->row()->sumfh;
    //echo $CI->db->last_query();
    //$CI->db->close();    

    //$CI->db->select("count(value) as countfh");
    $CI->db->select("COUNT(DISTINCT(crew_daily_statistics.staffnumber)) as countfh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("month(tdate) = '".$nMonth."' and year(tdate) = '".$nYear."'");
    $actType = array('daily flying hours', 'DAILY_FH');
    $CI->db->where_in('type', $actType);

    $q2 = $CI->db->get(); 
    $nval = $q2->row()->countfh;
    //echo $CI->db->last_query();
    $CI->db->close();    

    $avg = $sumval / $nval;

    return $avg;
}

function getMax($sDate, $eDate, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("sum(value) as maxfh");    
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    //if ($act<>'') $CI->db->where('type', $act);
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);  
    $CI->db->group_by('crew_daily_statistics.staffnumber');
    $CI->db->order_by('maxfh', 'desc');
    $CI->db->limit(1);
    
    $q = $CI->db->get(); 
    //echo $CI->db->last_query();
    $CI->db->close();    

    if ($q->num_rows() > 0) return $q->row()->maxfh;
    else return FALSE;
}

function getMin($sDate, $eDate, $act = NULL) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("sum(value) as minfh");
    $CI->db->from("crewflighthour_db.crew_daily_statistics");
    $CI->db->join('cabincrew', 'cabincrew.STAFFNUMBER = crew_daily_statistics.staffnumber');
    $CI->db->where("tdate between '".$sDate."' and '".$eDate."'");
    //if ($act<>'') $CI->db->where('type', $act);
    if ($act == 'DAILY FLYING HOURS') {
        $actType = array('daily flying hours', 'DAILY_FH');
        $CI->db->where_in('type', $actType);
    } else 
        $CI->db->where('type', $act);  
    $CI->db->group_by('crew_daily_statistics.staffnumber');
    $CI->db->order_by('minfh', 'asc');
    $CI->db->limit(1);
    
    $q = $CI->db->get(); 
    //echo $CI->db->last_query();
    $CI->db->close();    

    if ($q->num_rows() > 0) return $q->row()->minfh;
    else return FALSE;
}

function getName($nopeg) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);
    $CI->db->select("FIRSTNAME");
    $CI->db->from("crewflighthour_db.cabincrew");
    $CI->db->where('STAFFNUMBER', $nopeg);
    
    $q = $CI->db->get(); 
    $CI->db->close();    

    if ($q->num_rows() > 0) return $q->row()->FIRSTNAME;
    else return FALSE;
}

function getHistFH($sDay, $nopeg) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('cfh', TRUE);

    $CI->db->select("value");
    $CI->db->from("crew_daily_statistics");
    $CI->db->where("tdate", $sDay);
    $CI->db->where("staffnumber", $nopeg);
    $CI->db->where_in('type', array('daily flying hours','DAILY_FH'));

    $q = $CI->db->get();
    //echo $CI->db->last_query();exit;
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->value;
    else return 0;
}
