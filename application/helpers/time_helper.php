<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_hourotp($interval) {
	$date = date_create();
	date_add($date, date_interval_create_from_date_string($interval));

	//$h = date_format($date, 'H:i');

	
	if (date_format($date, 'H') % 2 == 0) 
		$h = date_format($date, 'H');
	else {
		date_add($date, date_interval_create_from_date_string('1 hours'));
		$h = date_format($date, 'H');
	}

	return $h;
}

function get_dateotp($interval) {
	$date = date_create();
	date_add($date, date_interval_create_from_date_string($interval));

	$h = date_format($date, 'Y-m-d');

	return $h;
}


function check_delay($depTime, $blockOff) {	
	if (date_create($blockOff) > date_create($depTime)) {
		$diff = date_diff(date_create($depTime), date_create($blockOff)); 
		if (($diff->i > 15) || ($diff->h > 0))
			return $diff->format("%H:%I");
	} else
		return FALSE;
}