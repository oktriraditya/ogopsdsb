<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_login() {
	$CI =& get_instance();	

	if ($CI->session->userdata('sNopeg')) {
		auth_log($CI->session->userdata('sNopeg'));
		return TRUE;
	} else {
		return FALSE;
	}
}

function auth_log($sNopeg) {
	$CI =& get_instance();	
	$CI->db = $CI->load->database('dm', TRUE);
	$data = array(
				'last_login_time' => date('Y-m-d H:i:s'),
				'last_login_from' => get_ip_number()
			);
	
	$CI->db->where('user_id', $sNopeg);
	$CI->db->update('user', $data); 
	$CI->db->close();
}

function is_auth($sNopeg) {
	$CI =& get_instance();	
	$CI->db = $CI->load->database('dm', TRUE);
	$CI->db->select('user_id, role_id')->from('user');
	$CI->db->where('user_id', $sNopeg);		
	$CI->db->where('is_active', 1);
	$q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row();
    else return FALSE;
}

function is_allow($roleid, $url) {
	$CI =& get_instance();	
	$CI->db = $CI->load->database('dm', TRUE);
	$CI->db->select('menu.menu_id')->from('menu');
	$CI->db->join('role_privilege', 'role_privilege.menu_id = menu.menu_id');
	$CI->db->where('role_privilege.role_id', $roleid);
	$CI->db->where('menu.menu_url', $url);		
	$q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return TRUE;
    else return FALSE;
}

function get_sess() {
	$CI =& get_instance();	
	if ($CI->session->userdata('sNopeg')) {
		return $CI->session->userdata('sNopeg');
	} else {
		return FALSE;
	}
}

function get_nopeg() {
	$CI =& get_instance();	
	$sNopeg = $CI->session->userdata('sNopeg');

	return $sNopeg;
}

function auth_sso($data) {		
	$sUserName = $data['username'];
	$sPassword = $data['password'];		
	$userID = explode("@",$sUserName);
	if ($userID[0]) $userName = $userID[0];
	else $userName = $data['username'];
			
	/**************************************************
	Bind to an Active Directory LDAP server and look something up.
	***************************************************/
	$SearchFor = $userName."@garuda-indonesia.com";       		//What string do you want to find?
	$SearchField = "mail";   									//In what Active Directory field do you want to search for the string?

	$LDAPHost = "192.168.31.228";       					//Your LDAP server DNS Name or IP Address (Second)
	//$LDAPHost = "corp.garuda-indonesia.com";					//Your LDAP server DNS Name or IP Address (First)
	//$LDAPHost = "192.168.31.229";       					//Your LDAP server DNS Name or IP Address (Third)
	$dn = "DC=corp,DC=garuda-indonesia,DC=com"; 				//Put your Base DN here
	$LDAPUserDomain = "";  										//Needs the @, but not always the same as the LDAP server domain
	$LDAPUser = "CORP\\$userName";	        					//A valid Active Directory login
	$LDAPUserPassword = $sPassword;
	$LDAPFieldsToFind = array("department","description","company","employeeID");
   
	$cnx = ldap_connect($LDAPHost) or die("Could not connect to LDAP");
	ldap_set_option($cnx, LDAP_OPT_PROTOCOL_VERSION, 3);  		//Set the LDAP Protocol used by your AD service
	ldap_set_option($cnx, LDAP_OPT_REFERRALS, 0);         		//This was necessary for my AD to do anything
	//ldap_bind($cnx,$LDAPUser.$LDAPUserDomain,$LDAPUserPassword) or die("Could not bind to LDAP");
	$isAuth = @ldap_bind($cnx,$LDAPUser.$LDAPUserDomain,$LDAPUserPassword);
	if ($isAuth) {					
			error_reporting (E_ALL ^ E_NOTICE);   						//Suppress some unnecessary messages
			$filter="($SearchField=$SearchFor*)"; 						//Wildcard is * Remove it if you want an exact match
			$sr=ldap_search($cnx, $dn, $filter, $LDAPFieldsToFind);
			$info = ldap_get_entries($cnx, $sr);

			$emp['nopeg'] = $info[0]["employeeid"][0];
			$emp['unit'] = $info[0]["department"][0];
			$emp['unitname'] = $info[0]["description"][0];		
			$emp['name'] = get_name($info[0]["dn"]);			

			return $emp;
	} else
		return FALSE;
}

function auth_ext($data) {
	$sUserName = $data['username'];
	$sPassword = $data['password'];		
	$CI =& get_instance();	
	$CI->db = $CI->load->database('dm', TRUE);
	$CI->db->select('user_id')->from('user');
	$CI->db->where('user_id', $sUserName);
	$CI->db->where('password', md5('dash'.$sPassword.'board'));
	$q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->user_id;
    else return FALSE;
}

function get_name($sName) {
	$name = explode(',',$sName);
	$empname = explode('=',$name[0]);

	return $empname[1];
}

function get_ip_number() {
	if ( function_exists( 'apache_request_headers' ) ) {
		$headers = apache_request_headers();
	} else {
		$headers = $_SERVER;
	}
	//Get the forwarded IP if it exists
	if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
		$the_ip = $headers['X-Forwarded-For'];
	} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )) {
		$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
	} else {
		$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
	}
	
	return $the_ip; 
}

function getMenu($role, $level) {
	$CI =& get_instance();	
	$CI->db = $CI->load->database('dm', TRUE);
	$CI->db->select('menu.menu_id, menu.menu_title, menu.menu_level, menu.menu_url, menu.parent_id')->from('menu');
	$CI->db->join('role_privilege', 'role_privilege.menu_id = menu.menu_id');
	$CI->db->where('role_privilege.role_id', $role);
	$CI->db->where('menu.parent_id', $level);
	$CI->db->order_by('menu_sort');
	
	$q = $CI->db->get();
	$CI->db->close();

    if ($q->num_rows() > 0) return $q->result_array();
    else return FALSE;
}
