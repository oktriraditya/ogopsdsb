<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function list_fleet() {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $CI->db->select("DISTINCT(fleet)");
    $CI->db->from("db_crewstrength.temp_crew_list");
    $CI->db->order_by('fleet');
    $q = $CI->db->get();
    $CI->db->close();
    $result = $q->result_array();

    if ($q->num_rows() > 0) return $result;
    else return FALSE;
}

function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    foreach($period as $date) {
        $array[] = $date->format($format);
    }

    return $array;
}

function list_year() {
    $year = range( date("Y") , 2017);
    return $year;
}

function list_month() {
    for ($m=1; $m<=12; $m++) {
        $month[] = date('F', mktime(0,0,0,$m, 1, date('Y')));
    }
    return $month;
}

function list_DatebyMonth($month) {
    $year = date('Y');
    $start_date = "01-".$month."-".$year;
    $start_time = strtotime($start_date);
    $end_time = strtotime("+1 month", $start_time);

    for($i=$start_time; $i<$end_time; $i+=86400) {
        $list[] = date('d', $i);
    }
    return $list;
}

function get_default_ssim($sFleet){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "Select value from info where flag = 'default-ssim-".$sFleet."'";
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['value'];
}

function list_olReg($sDate, $sRank, $sFleet, $ssim, $mode=null) {
    if (($ssim == null) || ($ssim == 'all')) $ssim = get_default_ssim($sFleet);

    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    if ($mode == 2) {
        $CI->db->select("(or.ol_count * (1 + f.standby) / (1 - f.total_nonops)) + f.office + ta.tr_count_tq as ol_count");
        $CI->db->from("online_req or");
        $CI->db->join("training_allocation ta", "or.ol_date = ta.tr_date and or.ol_fleet = ta.tr_fleet and or.ol_rank = ta.tr_rank",'left');
        $CI->db->join("formula f", "or.ol_rank = f.fr_rank and or.ol_fleet = f.fr_fleet and f.eff_date <='".$sDate."' and f.exp_date >= '".$sDate."' ");
        $CI->db->where('or.ol_date', $sDate);
        $CI->db->where('or.ol_rank', $sRank);
        $CI->db->where('or.ol_fleet', $sFleet);
        $CI->db->where('file_scenario', $ssim);
        //if (($ssim <> null) && ($ssim <> 'all')) $CI->db->where('or.file_scenario', $ssim);
    } else {
        $CI->db->select("ol_count");
        $CI->db->from("db_crewstrength.online_req");
        $CI->db->where('ol_date', $sDate);
        $CI->db->where('ol_rank', $sRank);
        $CI->db->where('ol_fleet', $sFleet);
        $CI->db->where('file_scenario', $ssim);
        //if (($ssim <> null) && ($ssim <> 'all')) $CI->db->where('file_scenario', $ssim);
    }

    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->ol_count;
    else return 0;
}

function list_pilotReq($sDate, $sRank, $sFleet, $ssim) {
    if (($ssim == null) || ($ssim == 'all')) $ssim = get_default_ssim($sFleet);

    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select("(or.ol_count * (1 + f.standby) / (1 - f.total_nonops)) + f.office + ta.tr_count_tq as ol_count");
    $CI->db->from("online_req or");
    $CI->db->join("training_allocation ta", "or.ol_date = ta.tr_date and or.ol_fleet = ta.tr_fleet and or.ol_rank = ta.tr_rank",'left');
    $CI->db->join("formula f", "or.ol_rank = f.fr_rank and or.ol_fleet = f.fr_fleet and f.eff_date <='".$sDate."' and f.exp_date >= '".$sDate."' ");
    $CI->db->where('or.ol_date', $sDate);
    $CI->db->where('or.ol_rank', $sRank);
    $CI->db->where('or.ol_fleet', $sFleet);
    $CI->db->where('file_scenario', $ssim);
    //if (($ssim <> null) && ($ssim <> 'all')) $CI->db->where('or.file_scenario', $ssim);
    $q = $CI->db->get();
    $CI->db->close();

    if ($q->num_rows() > 0) return $q->row()->ol_count;
    else return 0;
}


function list_onLeave($sDate, $sRank, $sFleet) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select("lv_count");
    $CI->db->from("db_crewstrength.leave_allocation");
    $CI->db->where('lv_date', $sDate);
    $CI->db->where('lv_rank', $sRank);
    $CI->db->where('lv_fleet', $sFleet);

    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->lv_count;
    else return 0;
}

function list_trainAlloc($sDate, $sRank, $sFleet) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select("count_route as train_count");
    $CI->db->from("db_crewstrength.crew_strength");
    $CI->db->where('date', $sDate);
    $CI->db->where('rank', $sRank);
    $CI->db->where('fleet', $sFleet);

    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->train_count;
    else return 0;
}

/*
function list_avail($sDate, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther, $ssim=null) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    switch ($dataMode) {
        case 2:
            if($incTraining == 1 ) {
      				$trainInc = '- IFNULL(ta.tr_count_tq, 0)';
      				$trainFrm = '';
            } else {
      				$trainInc = '';
      				$trainFrm = '- IFNULL(f.tr_pc, 0) - IFNULL(f.tr_ground_rec, 0) - IFNULL(f.tr_ground_mand, 0) - IFNULL(f.tr_teach, 0)';
    			  }

			      ($incOffice == 1 ? $officeInc = '- IFNULL(f.office, 0)' : $officeInc = '');
            ($incOther == null ? $otherInc = '- IFNULL(f.otd, 0)' : $otherInc = '');
            $crewNet = "(IFNULL(cs.count_qual, 0) ".$officeInc." ".$trainInc.")";
            ($DO == 6 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.do, 0) + (IFNULL(f.do, 0) * 0.75) ".$otherInc.$trainFrm.")" :
            ($DO == 7 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.do, 0) + (IFNULL(f.do, 0) * 0.875) ".$otherInc.$trainFrm.")" :
            $totFormula = "* (IFNULL(f.total_nonops, 0) ".$otherInc. $trainFrm.")"));
            break;
        case 3:
            ($incTraining == 1 ? $trainInc = '- IFNULL(ta.tr_count_tq, 0) - IFNULL(ta.tr_count_recurrent, 0)' : $trainInc = '');
            ($incOffice == 1 ? $officeInc = '- IFNULL(f.office, 0)' : $officeInc = '');
            ($incOther == null ? $otherInc = '- IFNULL(f.otd, 0)' : $otherInc = '');
            $crewNet = "(IFNULL(cs.count_qual, 0) ".$officeInc." ".$trainInc." - IFNULL(la.lv_count, 0))";
            ($DO == 6 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.tr_pc, 0) - IFNULL(f.tr_ground_rec, 0) - IFNULL(f.tr_ground_mand, 0) - IFNULL(f.tr_teach, 0) - IFNULL(f.annual_leave, 0) - IFNULL(f.do, 0) + (IFNULL(f.do, 0) * 0.75) ".$otherInc.")" :
            ($DO == 7 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.tr_pc, 0) - IFNULL(f.tr_ground_rec, 0) - IFNULL(f.tr_ground_mand, 0) - IFNULL(f.tr_teach, 0) - IFNULL(f.annual_leave, 0) - IFNULL(f.do, 0) + (IFNULL(f.do, 0) * 0.875) ".$otherInc.")" :
            $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.tr_pc, 0) - IFNULL(f.tr_ground_rec, 0) - IFNULL(f.tr_ground_mand, 0) - IFNULL(f.tr_teach, 0) - IFNULL(f.annual_leave, 0) ".$otherInc.")"));
            break;
        case 4:
            ($incTraining == 1 ? $trainInc = '- IFNULL(ta.tr_count_tq, 0)' : $trainInc = '');
            ($incOffice == 1 ? $officeInc = '- IFNULL(f.office, 0)' : $officeInc = '');
            ($incOther == null ? $otherInc = '- IFNULL(f.otd, 0)' : $otherInc = '');
            $crewNet = "(IFNULL(cs.count_qual, 0) ".$officeInc." ".$trainInc." - IFNULL(la.lv_count, 0))";
            ($DO == 6 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.annual_leave, 0) - IFNULL(f.do, 0) - (IFNULL(f.do, 0) * 0.75) ".$otherInc.")" :
            ($DO == 7 ? $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.annual_leave, 0) - IFNULL(f.do, 0) - (IFNULL(f.do, 0) * 0.875) ".$otherInc.")" :
            $totFormula = "* (IFNULL(f.total_nonops, 0) - IFNULL(f.annual_leave, 0) ".$otherInc.")" ));
            break;
        default:
            $totFormula = "";

    }

    $CI->db->select("(".$crewNet." - (".$crewNet." ".$totFormula."))/(1 + f.standby) as c_avail");
    $CI->db->from("crew_strength cs");
    $CI->db->join("training_allocation ta", "cs.date = ta.tr_date AND ta.tr_rank = cs.rank and ta.tr_fleet = cs.fleet", "left");
    $CI->db->join("formula f", "cs.rank = f.fr_rank AND cs.fleet = f.fr_fleet");
    if ($dataMode <> 2) $CI->db->join("leave_allocation la", "cs.date = la.lv_date AND la.lv_rank = cs.rank AND la.lv_fleet = cs.fleet", "left");
    $CI->db->where("cs.date", $sDate);
    $CI->db->where("cs.rank", $sRank);
    $CI->db->where("cs.fleet", $sFleet);
    $CI->db->where("f.eff_date <=", $sDate);
    $CI->db->where("f.exp_date >=", $sDate);
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->c_avail;
    else return 0;
}
*/
function get_act_breakdown($sDate, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther, $ssim=null){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  if($incOffice == 1 ){
    $office = ", IFNULL(f.office, 0) as office";
    $ofc = "- IFNULL(f.office, 0)";
  } else { $office = ""; $ofc = ""; }

  switch ($dataMode) {
      case 2:
          if($incTraining == 1 ) {
            $tr_alloc = ", (IFNULL(cs.count_qual,0) ".$ofc."-IFNULL(ta.tr_count_tq,0))*(f.tr_ground_rec+f.tr_pc+f.tr_teach+f.tr_ground_mand) as tr_recurrent";;
            $tr_tq = ", IFNULL(ta.tr_count_tq, 0) as tr_typequal";
            $tr = "- IFNULL(ta.tr_count_tq, 0)";
          } else {
            $tr_alloc = "";
            $tr_tq = "";
            $tr = "";
          }
          $crewNet = "(IFNULL(cs.count_qual, 0) ".$ofc." ".$tr." ) ";
          $an_leave = ", ".$crewNet." * IFNULL(f.annual_leave, 0) as annual_leave";
          break;
      case 3:
          if($incTraining == 1 ) {
            $tr_alloc = ", IFNULL(ta.tr_count_recurrent,0) as tr_recurrent";;
            $tr_tq = ", IFNULL(ta.tr_count_tq, 0) as tr_typequal";
            $tr = "- IFNULL(ta.tr_count_tq, 0)";
          } else {
            $tr_alloc = "";
            $tr_tq = "";
            $tr = "";
          }
          $crewNet = "(IFNULL(cs.count_qual, 0) ".$ofc." ".$tr." ) ";
          $an_leave = ", IFNULL(la.lv_count, 0) as annual_leave";
          break;
      default: //case 4
          if($incTraining == 1 ) {
            $tr_alloc = ", (IFNULL(cs.count_qual,0) ".$ofc."-IFNULL(ta.tr_count_tq,0))*(f.tr_ground_rec+f.tr_pc+f.tr_teach+f.tr_ground_mand) as tr_recurrent";;
            $tr_tq = ", IFNULL(ta.tr_count_tq, 0) as tr_typequal";
            $tr = "- IFNULL(ta.tr_count_tq, 0)";
          } else {
            $tr_alloc = "";
            $tr_tq = "";
            $tr = "";
          }
          $crewNet = "(IFNULL(cs.count_qual, 0) ".$ofc." ".$tr." ) ";
          $an_leave = ", IFNULL(la.lv_count, 0) as annual_leave";
          break;
      }

  if ($incOther == 1 ? $otd = ", ".$crewNet." * IFNULL(f.otd, 0) as other" : $otd = '');
  if ($DO == 8) {
    $do = ", ".$crewNet." * IFNULL(f.do, 0) as day_off";
  } else if ($DO == 7) {
    $do = ", ".$crewNet." * (IFNULL(f.do, 0) * 0.875) as day_off";
  } else { // DO 6
    $do = ", ".$crewNet." * (IFNULL(f.do, 0) * 0.75) as day_off";
  }

  //$stb    = ", (".$crewNet."-(".$crewNet."* IFNULL(f.total_nonops,0))) * (f.standby/(1+f.standby)) as standby";
  //$ol     = ", (".$crewNet."-(".$crewNet."* IFNULL(f.total_nonops,0))) * (1/(1+f.standby)) as online_cap";
  $stb    = ", f.standby as standby";
  $medlic = ", ".$crewNet." * (IFNULL(f.medex,0)+IFNULL(f.license,0)) as medex_lic";
  $sick   = ", ".$crewNet." * IFNULL(f.non_prod, 0) as margin_sick";
  $all_columns = "cs.count_qual as qual ".$stb." ".$office." ".$tr_tq." ".$tr_alloc." ".$an_leave." ".$do." ".$medlic." ".$otd." ".$sick."";

  $CI->db->select($all_columns);
  $CI->db->from("crew_strength cs");
  $CI->db->join("training_allocation ta", "cs.date = ta.tr_date AND ta.tr_rank = cs.rank and ta.tr_fleet = cs.fleet", "left");
  $CI->db->join("formula f", "cs.rank = f.fr_rank AND cs.fleet = f.fr_fleet");
  if ($dataMode <> 2) $CI->db->join("leave_allocation la", "cs.date = la.lv_date AND la.lv_rank = cs.rank AND la.lv_fleet = cs.fleet", "left");
  $CI->db->where("cs.date", $sDate);
  $CI->db->where("cs.rank", $sRank);
  $CI->db->where("cs.fleet", $sFleet);
  $CI->db->where("f.eff_date <=", $sDate);
  $CI->db->where("f.exp_date >=", $sDate);
  $q = $CI->db->get();
  $result = $q->result_array();

  $tot_nonops = (!isset($result[0]['office']) ? 0 : $result[0]['office'] )+(!isset($result[0]['tr_typequal']) ? 0 : $result[0]['tr_typequal'] )+(!isset($result[0]['tr_recurrent'])? 0: $result[0]['tr_recurrent'])
  + $result[0]['annual_leave'] + $result[0]['day_off']+$result[0]['medex_lic']+(!isset($result[0]['other'])? 0 : $result[0]['other'])+$result[0]['margin_sick'];

  $result[0]['online_cap'] = ( $result[0]['qual'] - $tot_nonops ) * (1/($result[0]['standby']+1));
  $result[0]['standby'] = ( $result[0]['qual'] - $tot_nonops ) * ($result[0]['standby']/($result[0]['standby']+1));
  //$result[0]['sum'] = $tot_nonops + $result[0]['online_cap'] + $result[0]['standby'];
  $CI->db->close();
  if ($q->num_rows() > 0) return $result;
  else return 0;
}


function get_ssim($sDate, $eDate, $flt) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select("DISTINCT(O.file_scenario), I.value");
    $CI->db->from("db_crewstrength.online_req O");
    $CI->db->join("db_crewstrength.info I", "O.file_scenario = I.value AND I.flag = 'default-ssim-".$flt." '", 'left');
    $CI->db->where('ol_date BETWEEN "'. date('Y-m-d', strtotime($sDate)). '" and "'. date('Y-m-d', strtotime($eDate)).'"');
    $CI->db->where('O.ol_fleet', $flt);
    $CI->db->order_by("I.value DESC, O.file_scenario DESC");
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->result();
    else return false;
}

/*
function list_DutyCode($dt1,$dt2,$flt,$rk) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select("DISTINCT(duty_code)");
    $CI->db->from("db_crewstrength.crew_assignment");
    $CI->db->where('date >=', $dt1);
    $CI->db->where('date <=', $dt2);
    $CI->db->where('rank', $rk);
    $CI->db->where('fleet', $flt);
    $CI->db->order_by('duty_code');
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->result_array();
    else return false;
}*/

function list_DutyCode($dt1,$dt2,$flt,$rk) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $CI->db->select("DISTINCT(duty_code)");
    $CI->db->from("db_crewstrength.crew_assignment");
    $CI->db->where("date between '".$dt1."' and '".$dt2."'");
    $CI->db->where('rank', $rk);
    $CI->db->where('fleet', $flt);
    $CI->db->order_by('duty_code');
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->result_array();
    else return false;
}

function get_valDutyCode($duty_code, $sDate, $sRank, $sFleet) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $CI->db->select("count_duty");
    $CI->db->from("db_crewstrength.crew_assignment");
    $CI->db->where('date', $sDate);
    $CI->db->where('rank', $sRank);
    $CI->db->where('fleet', $sFleet);
    $CI->db->where('duty_code', $duty_code);
    $q = $CI->db->get();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->count_duty;
    else return 0;
}

function list_pilot($sDay, $sRank, $sFleet, $type) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $CI->db->select($type);
    $CI->db->from("db_crewstrength.crew_strength");
    $CI->db->where('date', $sDay);
    $CI->db->where('rank', $sRank);
    $CI->db->where('fleet', $sFleet);

    $q = $CI->db->get();
    //echo $CI->db->last_query();
    $CI->db->close();
    if ($q->num_rows() > 0) return $q->row()->$type;
    else return 0;
}

function query_number_pilot_assignment($dt, $flt, $rk, $status){
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $complexQuery = 0;
    if ($status == "All") {
      $complexQuery = "select CREW_LIST.FLEET, CREW_LIST.RANK_,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'PRE/POST_TRAVEL'
      ELSE Nested_Duty.Duty END
      as ACTIVITIES_GROUP,
      count(CREW_LIST.IDPEG) as TOTAL_CREW
      from
      (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
      f.fleetcode as FLEET, cr.rankcode as RANK_, e.status
      from DBSABRECM.CREW C
      INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' )  and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
      INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
      INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
      group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status) CREW_LIST
      LEFT JOIN
      (select crs.staffnumber as IDPilot,
      listagg (pda.activitytypecode, ',')
      within group (order by crs.startdatetimeutc) as Duty
      from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
      where
      crs.pairingid  = p.pairingid
      and p.pairingid = pd.pairingid
      and pd.dutyid = pda.dutyid
      and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
      and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
      group by crs.staffnumber
      ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG
      group by CREW_LIST.FLEET, CREW_LIST.RANK_,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'BLANK'
      ELSE Nested_Duty.Duty END";
    } else if ($status == 'Qualified') {
      if ($rk== "CP") {
        $rk2= "GACPT";
      } else { //FO
        $rk2= "GAFOT";
      }
      $complexQuery = "select CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'BLANK'
      ELSE Nested_Duty.Duty END
      as ACTIVITIES_GROUP,
      count(CREW_LIST.IDPEG) as TOTAL_CREW
      from
      (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
      f.fleetcode as FLEET, cr.rankcode as RANK_, e.status, 'Qualified' as QUALSTAT
      from DBSABRECM.CREW C left join
      (select q.staffnumber as ID_Q, q.name as qual
      from DBSABRECM.CREW_QUALIFICATION Q
      where
      (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
      and (q.name like '".$rk2."')
      ) Nested_Q on c.staffnumber = Nested_Q.ID_Q and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
      INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' ) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
      INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
      INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
      where qual is null
      group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status, Nested_Q.qual) CREW_LIST
      LEFT JOIN
      (select crs.staffnumber as IDPilot,
      listagg (pda.activitytypecode, ',')
      within group (order by crs.startdatetimeutc) as Duty
      from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
      where
      crs.pairingid  = p.pairingid
      and p.pairingid = pd.pairingid
      and pd.dutyid = pda.dutyid
      and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
      and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
      group by crs.staffnumber
      ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG
      group by CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'BLANK'
      ELSE Nested_Duty.Duty END";
    } else { //Training
      if ($rk== "CP") {
        $rk2= "GACPT";
      } else {$rk2="GAFOT";}
      $complexQuery = "select CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'BLANK'
      ELSE Nested_Duty.Duty END
      as ACTIVITIES_GROUP,
      count(CREW_LIST.IDPEG) as TOTAL_CREW
      from
      (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
      f.fleetcode as FLEET, cr.rankcode as RANK_, e.status, 'Training' as QUALSTAT
      from DBSABRECM.CREW C inner join
      (select q.staffnumber as ID_Q, q.name as qual
      from DBSABRECM.CREW_QUALIFICATION Q
      where
      (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
      and (q.name like '".$rk2."')
      ) Nested_Q on Nested_Q.ID_Q = c.staffnumber and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
      INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' ) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
      INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
      INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
      group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status, Nested_Q.qual) CREW_LIST
      LEFT JOIN
      (select crs.staffnumber as IDPilot,
      listagg (pda.activitytypecode, ',')
      within group (order by crs.startdatetimeutc) as Duty
      from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
      where
      crs.pairingid  = p.pairingid
      and p.pairingid = pd.pairingid
      and pd.dutyid = pda.dutyid
      and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
      and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
      group by crs.staffnumber
      ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG
      group by CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT,
      Case WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%FLY%') THEN 'FLY'
      WHEN Nested_Duty.Duty LIKE ('%,%') and Nested_Duty.Duty LIKE ('%DHD%') THEN 'DHD'
      WHEN Nested_Duty.Duty LIKE ('%SIM%') THEN 'SIM'
      WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
      WHEN Nested_Duty.Duty is null THEN 'BLANK'
      ELSE Nested_Duty.Duty END ";
    }
    $q = $CI->db->query($complexQuery);
    $result = $q->result_array();
    return $result;
  }

  function query_numberofpilot($dt){

    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $complexQuery = 0;
    $complexQuery = "select CREW_LIST.FLEET as FLEET, CREW_LIST.RANK_ as RANK, CREW_LIST.QUALSTAT as QUAL, count(CREW_LIST.IDPEG) as TOTAL from
    (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
    f.fleetcode as FLEET, cr.rankcode as RANK_,
    Case Nested_Q.qual
    WHEN 'Training' THEN 'Training'
    ELSE 'Qualified' END
    as QUALSTAT
    from DBSABRECM.CREW C left join
    (select q.staffnumber as ID_Q, CASE
    WHEN (q.name = 'GAFOT') or (q.name = 'GACPT') THEN 'Training'
    ELSE 'Qualified' END
    as qual
    from DBSABRECM.CREW_QUALIFICATION Q
    where
    (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
    and (q.name like 'GACPT%' or q.name like 'GAFOT%')
    ) Nested_Q on Nested_Q.ID_Q = c.staffnumber and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
    INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
    INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
    INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
    group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, Nested_Q.qual
    )CREW_LIST
    group by CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT
    order by CREW_LIST.FLEET, CREW_LIST.RANK_, CREW_LIST.QUALSTAT";
    $q = $CI->db->query($complexQuery);
    $result = $q->result_array();
    return $result;
  }


  function get_pilotpersonel($dt,$flt,$rk,$status){

    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $complexQuery = 0;
    $complexQuery = "select max(to_number(c.staffnumber)) as IDPEG, c.firstname as FIRSTNAME, c.lastname as LASTNAME, c.gender as GENDER, c.birthdate as BIRTHDATE,
    f.fleetcode as FLEET, cr.rankcode as RANK_,
    Case Nested_Q.qual
    WHEN 'Training' THEN 'Training'
    ELSE 'Qualified' END
    as QUALSTAT
    from DBSABRECM.CREW C left join
    (select q.staffnumber as ID_Q, CASE
    WHEN (q.name = 'GAFOT') or (q.name = 'GACPT') THEN 'Training'
    ELSE 'Qualified' END
    as qual
    from DBSABRECM.CREW_QUALIFICATION Q
    where
    (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
    and (q.name like 'GACPT%' or q.name like 'GAFOT%')
    ) Nested_Q on Nested_Q.ID_Q = c.staffnumber and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
    INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
    INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
    INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
    group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, Nested_Q.qual";

    $q = $CI->db->query($complexQuery);
    $result = $q->result_array();
    return $result;
  }

  function get_allpilotpersonel($dt){

    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $complexQuery = 0;
    $complexQuery = "select max(to_number(c.staffnumber)) as IDPEG, c.firstname as FIRSTNAME, c.lastname as LASTNAME, c.gender as GENDER, c.birthdate as BIRTHDATE,
    f.fleetcode as FLEET, cr.rankcode as RANK_,
    Case Nested_Q.qual
    WHEN 'Training' THEN 'Training'
    ELSE 'Qualified' END
    as QUALSTAT
    from DBSABRECM.CREW C left join
    (select q.staffnumber as ID_Q, CASE
    WHEN (q.name = 'GAFOT') or (q.name = 'GACPT') THEN 'Training'
    ELSE 'Qualified' END
    as qual
    from DBSABRECM.CREW_QUALIFICATION Q
    where
    (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
    and (q.name like 'GACPT%' or q.name like 'GAFOT%')
    ) Nested_Q on Nested_Q.ID_Q = c.staffnumber and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
    INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
    INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
    INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
    group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, Nested_Q.qual";

    $q = $CI->db->query($complexQuery);
    $result = $q->result_array();
    return $result;
  }


  function get_detail_who_are_assigned($key, $dt, $rk, $flt, $status){

    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $Query = 0;
    if ($key=="SIM"){$persen='%';}else{$persen='';}
    if ($key<>"BLANK") {
      if ($status == "All") {
        $Query = "select CREW_LIST.IDPEG, CONCAT(CONCAT(CREW_LIST.FirstName,' '), CREW_LIST.LastName) as FULLNAME,
        Case WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
        ELSE Nested_Duty.Duty END
        as ACTIVITIES_GROUP
        from
        (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
        f.fleetcode as FLEET, cr.rankcode as RANK_, e.status
        from DBSABRECM.CREW C
        INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' )  and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
        INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
        INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
        group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status) CREW_LIST
        INNER JOIN
        (select crs.staffnumber as IDPilot,
        listagg (pda.activitytypecode, ',')
        within group (order by crs.startdatetimeutc) as Duty
        from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
        where
        crs.pairingid  = p.pairingid
        and p.pairingid = pd.pairingid
        and pd.dutyid = pda.dutyid
        and pda.activitytypecode like '".$key.$persen."' and (pda.activitytypecode is not null)
        and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
        and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
        group by crs.staffnumber
        ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG and (Nested_Duty.Duty is not null)";
      } else if ($status == "Qualified"){
        if ($rk == "CP") {
          $rk2= "GACPT";
        } else {$rk2="GAFOT";}
        $Query = "select CREW_LIST.IDPEG, CONCAT(CONCAT(CREW_LIST.FirstName,' '), CREW_LIST.LastName) as FULLNAME,
        Case WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
        ELSE Nested_Duty.Duty END
        as ACTIVITIES_GROUP
        from
        (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
        f.fleetcode as FLEET, cr.rankcode as RANK_, e.status, 'Qualified' as QUALSTAT
        from DBSABRECM.CREW C left join
        (select q.staffnumber as ID_Q, q.name as qual
        from DBSABRECM.CREW_QUALIFICATION Q
        where
        (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
        and (q.name like '".$rk2."')
        ) Nested_Q on c.staffnumber = Nested_Q.ID_Q and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
        INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' ) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
        INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
        INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
        where qual is null
        group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status, Nested_Q.qual) CREW_LIST
        LEFT JOIN
        (select crs.staffnumber as IDPilot,
        listagg (pda.activitytypecode, ',')
        within group (order by crs.startdatetimeutc) as Duty
        from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
        where
        crs.pairingid  = p.pairingid
        and p.pairingid = pd.pairingid
        and pd.dutyid = pda.dutyid
        and pda.activitytypecode like '".$key.$persen."' and (pda.activitytypecode is not null)
        and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
        and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
        group by crs.staffnumber
        ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG and (Nested_Duty.Duty is not null)";
      } else { // Training
        if ($rk == "CP") {
          $rk2= "GACPT";
        } else {$rk2="GAFOT";}
        $Query="select CREW_LIST.IDPEG, CONCAT(CONCAT(CREW_LIST.FirstName,' '), CREW_LIST.LastName) as FULLNAME,
        Case WHEN Nested_Duty.Duty LIKE ('%,%')THEN SUBSTR(Nested_Duty.Duty,1,(INSTR(Nested_Duty.Duty,',',1,1)-1))
        ELSE Nested_Duty.Duty END
        as ACTIVITIES_GROUP
        from
        (select max(to_number(c.staffnumber)) as IDPEG, c.firstname, c.lastname, c.gender, c.birthdate,
        f.fleetcode as FLEET, cr.rankcode as RANK_, e.status, 'Training' as QUALSTAT
        from DBSABRECM.CREW C inner join
        (select q.staffnumber as ID_Q, q.name as qual
        from DBSABRECM.CREW_QUALIFICATION Q
        where
        (q.issuedate <= to_date('".$dt."','yyyy-mm-dd') and (q.expirydate > to_date('".$dt."','yyyy-mm-dd') or q.expirydate is null))
        and (q.name like '".$rk2."')
        ) Nested_Q on Nested_Q.ID_Q = c.staffnumber and (c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null))
        INNER JOIN DBSABRECM.CREW_RANK CR on c.staffnumber = cr.staffnumber and (cr.rankcode = '".$rk."' ) and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
        INNER JOIN DBSABRECM.CREW_FLEET F on c.staffnumber = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
        INNER JOIN DBSABRECM.CREW_EMPLOYMENT E on c.staffnumber = e.staffnumber and (e.startdate <= to_date('".$dt."','yyyy-mm-dd') and (e.enddate > to_date('".$dt."','yyyy-mm-dd') or e.enddate is null)) and (e.status = 'ACTIVE' or e.status = 'SUSPEND')
        group by c.firstname, c.lastname, c.gender, c.birthdate, f.fleetcode, cr.rankcode, e.status, Nested_Q.qual) CREW_LIST
        LEFT JOIN
        (select crs.staffnumber as IDPilot,
        listagg (pda.activitytypecode, ',')
        within group (order by crs.startdatetimeutc) as Duty
        from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
        where
        crs.pairingid  = p.pairingid
        and p.pairingid = pd.pairingid
        and pd.dutyid = pda.dutyid
        and to_date(to_char(crs.startdatetimeutc + interval '7' hour,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date('".$dt."','yyyy-mm-dd')
        and to_date(to_char(crs.enddatetimeutc + interval '6' hour + interval '59' minute,'yyyy-mm-dd'),'yyyy-mm-dd')   >= to_date('".$dt."','yyyy-mm-dd')
        group by crs.staffnumber
        ) Nested_Duty on Nested_Duty.IDPilot = CREW_LIST.IDPEG and (Nested_Duty.Duty is not null)";
      }
    }
    //echo '<script type="text/javascript">alert("'. $Query .'")</script>';
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }


  function get_all_in_progress_training($dt){

    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $rkstat = "'GACPT' or A.name = 'GAFOT'";
    $Query = "Select
    distinct(QUAL.IDPilotQual) as StaffID,
    QUAL.IDQual as QualID,
    QUAL.FULLNAME as FULLNAME,
    QUAL.RANKS as RANKS,
    to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,
    to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
    to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
    from
    (select A.STAFFNUMBER as IDPilotQual, CONCAT(CONCAT(C.FirstName,' '), C.LastName) as FULLNAME, SUBSTR(A.name,-3,3) as RANKS, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
    from DBSABRECM.CREW_QUALIFICATION A, DBSABRECM.CREW C
    where (A.name = ".$rkstat.") and A.STAFFNUMBER = C.STAFFNUMBER
    and A.ISSUEDATE <= to_date('".$dt."','YYYY-MM-DD') and A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD')
    order by A.ISSUEDATE asc) QUAL
    LEFT JOIN
    (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
    from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
    where
    crs.pairingid  = p.pairingid
    and p.pairingid = pd.pairingid
    and pd.dutyid = pda.dutyid
    and pda.activitytypecode = 'FLY'
    ) ROUTE_TR
    on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
    and ROUTE_TR.StartFly > qual.startqual
    and ROUTE_TR.StartFly < qual.endqual
    group by
    (QUAL.IDPilotQual), QUAL.IDQual, QUAL.FULLNAME, QUAL.RANKS, FLEET.CodeFleet, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
    order by StartTraining asc";

    //echo '<script type="text/javascript">alert("'. $Query .'")</script>';
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }

  // INVALID, CEK LATER, MODE START DATE HINGGA END DATE
  /*function get_progress_training($flt, $rk, $status, $dt1, $dt2){

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($rk = 'all') { $rkstat = "'GACPT' or A.name = 'GAFOT'"; } else { $rkstat= "'GA".$rk."T'";}
  $Query = "Select
  distinct(QUAL.IDPilotQual) as StaffID,
  QUAL.IDQual as QualID,
  QUAL.FULLNAME as FULLNAME,
  QUAL.RANKS as RANKS,
  FLEET.CodeFleet as FleetType,
  to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,				to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
  to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
  from
  (select A.STAFFNUMBER as IDPilotQual, CONCAT(CONCAT(C.FirstName,' '), C.LastName) as FULLNAME, SUBSTR(A.name,-3,3) as RANKS, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
  from DBSABRECM.CREW_QUALIFICATION A, DBSABRECM.CREW C
  where (A.name = ".$rkstat.") and A.STAFFNUMBER = C.STAFFNUMBER
  and A.ISSUEDATE > to_date('".$dt1."','YYYY-MM-DD') and A.ISSUEDATE < to_date('".$dt2."','YYYY-MM-DD')
  order by A.ISSUEDATE asc) QUAL
  INNER JOIN
  (select B.STAFFNUMBER as IDPilotFleet, B.FleetCode as CodeFleet, B.EFFECTIVEDATE as StartFleet, B.EXPIRYDATE as EndFleet
  from DBSABRECM.CREW_FLEET B
  where B.FleetCode = '".$flt."'
  order by StartFleet desc
  )FLEET
  ON QUAL.IDPilotQual = FLEET.IDPilotFleet
  and Qual.StartQual >= Fleet.StartFleet and Fleet.EndFleet >= Qual.StartQual
  LEFT JOIN
  (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
  from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
  where
  crs.pairingid  = p.pairingid
  and p.pairingid = pd.pairingid
  and pd.dutyid = pda.dutyid
  and pda.activitytypecode = 'FLY'
  ) ROUTE_TR
  on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
  and ROUTE_TR.StartFly > qual.startqual
  and ROUTE_TR.StartFly < qual.endqual
  group by
  (QUAL.IDPilotQual), QUAL.IDQual, QUAL.FULLNAME, QUAL.RANKS, FLEET.CodeFleet, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
  order by StartTraining asc";

  //echo '<script type="text/javascript">alert("'. $Query .'")</script>';
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  return $result;
  }*/

  function get_training_in_progress($flt, $rk,$dt, $dt2){
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    if ($rk == 'all') { $rkstat = "(TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT')"; } else { $rkstat= "TR.QUALNAME ='GA".$rk."T'";}
    $Query = "SELECT C.EMP_ID as STAFFID, C.FULLNAME, C.GENDER, to_char(C.BIRTHDATE,'YYYY-MM-DD') AS BIRTHDATE, CR.RANKCODE AS RANKS, F.FLEETCODE AS FLEETTYPE, to_char(ADD_MONTHS(C.BIRTHDATE,780),'YYYY-MM-DD') as END_EMPLOYEE_DATE,
    CASE
    WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is not null) THEN 'RT'
    WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is null) THEN 'GT'
    ELSE 'Q' END
    as QUALSTAT,
    TR.QUALNAME, TR.StartTraining, TR.StartRouteTraining, TR.EndTraining
    FROM
    (select max(to_number(c.staffnumber)) as EMP_ID, CONCAT(CONCAT(c.firstname,' '), c.lastname) as FULLNAME, c.gender as GENDER, c.birthdate, c.employmentdate
    from DBSABRECM.CREW C
    where c.employmentdate <= to_date('".$dt2."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)
    group by c.firstname, c.middlename, c.lastname, c.gender, c.birthdate, c.employmentdate
  ) C
  INNER JOIN DBSABRECM.CREW C2 ON c.EMP_ID = C2.staffnumber
  INNER JOIN DBSABRECM.CREW_RANK CR on c.EMP_ID = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') -- Pilot only
  and (cr.effectivedate <= to_date('".$dt2."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
  INNER JOIN DBSABRECM.CREW_FLEET F on c.EMP_ID = f.staffnumber and (f.effectivedate <= to_date('".$dt2."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '".$flt."'
  LEFT JOIN
  (Select
    distinct(QUAL.IDPilotQual) as StaffID,
    QUAL.QUALNAME as QUALNAME,
    QUAL.IDQual as QualID,
    to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,
    to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
    to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
    from
    (select A.STAFFNUMBER as IDPilotQual, a.name as QUALNAME, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
      from DBSABRECM.CREW_QUALIFICATION A
      where (A.name = 'GACPT' or A.name = 'GAFOT')
      and A.ISSUEDATE <= to_date('".$dt2."','YYYY-MM-DD') and (A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD') OR A.EXPIRYDATE IS NULL)
      order by A.ISSUEDATE asc) QUAL
      LEFT JOIN
      (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
        from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
        where
        crs.pairingid  = p.pairingid
        and p.pairingid = pd.pairingid
        and pd.dutyid = pda.dutyid
        and pda.activitytypecode = 'FLY'
      ) ROUTE_TR
      on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
      and ROUTE_TR.StartFly > qual.startqual
      and ROUTE_TR.StartFly < qual.endqual
      group by
      (QUAL.IDPilotQual), QUAL.QUALNAME, QUAL.IDQual, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
      order by StartTraining asc
    ) TR ON TR.StaffID = C.EMP_ID
    where ".$rkstat;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }

  function get_registered_training($flt, $status, $dt1, $dt2){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT rt.registered_id,rt.start_training, rt.qual_est, rt.route_est,rt.emp_id,rt.batch_id,batch.batch_id, batch.batch_name, batch.tr_name, batch.tr_program_id
    FROM registered_trainee rt
    left JOIN (SELECT tb.batch_id, tb.batch_name, tp.tr_program_id, tp.tr_name, tp.tr_fleet
      FROM training_batch tb
      JOIN training_program tp ON tb.tr_program_id = tp.tr_program_id)
      batch ON rt.batch_id = batch.batch_id
      WHERE rt.start_training > '".$dt1."' and rt.start_training < '".$dt2."'";
      $q = $CI->db->query($Query);
      $result = $q->result_array();
      return $result;
    }

    function get_single_registered_training($id, $dt){
      $CI = & get_instance();
      $CI->db = $CI->load->database('mysql', TRUE);
      $Query = "select rt.registered_id, rt.emp_id, rt.start_training, rt.batch_id, Batch.batch_name, Batch.tr_name, rt.route_est, rt.qual_est
      from registered_trainee rt
      left join (select tb.batch_id, tb.batch_name, tr.tr_name
      from training_batch tb, training_program tr
      where tr.tr_program_id = tb.tr_program_id) Batch
      on Batch.batch_id = rt.batch_id
      where rt.emp_id = ".$id." and rt.start_training = '".$dt."'";
      $q = $CI->db->query($Query);
      $result = $q->result_array();
      $CI->db->close();
      return $result;
    }

    function get_trainee_n_register_inprogress($flt,$rk,$dt){
      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);
      ($rk == "all" ? $rank = "" : ($rk == "CP" ? $rank = "and cr.rankcode = 'CP'" : $rank = "and cr.rankcode = 'FO'"));
      $Query = "SELECT C.EMP_ID, C.FULLNAME, C.GENDER, to_char(C.BIRTHDATE,'YYYY-MM-DD') AS BIRTHDATE, CR.RANKCODE AS RANK_, F.FLEETCODE AS FLEET, to_char(ADD_MONTHS(C.BIRTHDATE,780),'YYYY-MM-DD') as END_EMPLOYEE_DATE,
      CASE
      WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is not null) THEN 'RT'
      WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is null) THEN 'GT'
      ELSE 'Q' END
      as QUALSTAT,
      TR.QUALNAME, TR.StartTraining, TR.StartRouteTraining, TR.EndTraining
      FROM
      (select max(to_number(c.staffnumber)) as EMP_ID, CONCAT(CONCAT(c.firstname,' '), c.lastname) as FULLNAME, c.gender as GENDER, c.birthdate, c.employmentdate
      from DBSABRECM.CREW C
      where c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)
      group by c.firstname, c.middlename, c.lastname, c.gender, c.birthdate, c.employmentdate
    ) C
    INNER JOIN DBSABRECM.CREW C2 ON c.EMP_ID = C2.staffnumber
    INNER JOIN DBSABRECM.CREW_RANK CR on c.EMP_ID = cr.staffnumber
    and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
    INNER JOIN DBSABRECM.CREW_FLEET F on c.EMP_ID = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
    INNER JOIN
    (Select
      distinct(QUAL.IDPilotQual) as StaffID,
      QUAL.QUALNAME as QUALNAME,
      QUAL.IDQual as QualID,
      to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,
      to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
      to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
      from
      (select A.STAFFNUMBER as IDPilotQual, a.name as QUALNAME, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
        from DBSABRECM.CREW_QUALIFICATION A
        where (A.name = 'GACPT' or A.name = 'GAFOT')
        and A.ISSUEDATE <= to_date('".$dt."','YYYY-MM-DD') and (A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD') OR A.EXPIRYDATE IS NULL)
        order by A.ISSUEDATE asc) QUAL
        LEFT JOIN
        (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
          from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
          where
          crs.pairingid  = p.pairingid
          and p.pairingid = pd.pairingid
          and pd.dutyid = pda.dutyid
          and pda.activitytypecode = 'FLY'
        ) ROUTE_TR
        on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
        and ROUTE_TR.StartFly > qual.startqual
        --and ROUTE_TR.StartFly < qual.endqual
        --and ROUTE_TR.StartFly < nvl(qual.endqual,to_char(ADD_MONTHS('".$dt."',780),'YYYY-MM-DD'))
        group by
        (QUAL.IDPilotQual), QUAL.QUALNAME, QUAL.IDQual, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
        order by StartTraining asc
      ) TR ON TR.StaffID = C.EMP_ID
      where F.FLEETCODE = '".$flt."' ".$rank." ";
      $q = $CI->db->query($Query);
      $CI->db->close();
      $trainee = $q->result_array();
      $CI2 = & get_instance();
      $CI2->db = $CI2->load->database('mysql', TRUE);
      foreach ($trainee as $key => $value) {
        $register = get_single_registered_training($value['EMP_ID'],$value['STARTTRAINING']);
        (isset($register[0]['batch_name']) ? $trainee[$key]['batch_name']= $register[0]['batch_name']  : $trainee[$key]['batch_name']= null  );
        (isset($register[0]['tr_name']) ? $trainee[$key]['tr_name']= $register[0]['tr_name']  : $trainee[$key]['tr_name']= null  );
        (isset($register[0]['route_est']) ? $trainee[$key]['route_est']= $register[0]['route_est']  : $trainee[$key]['route_est']= null  );
        (isset($register[0]['qual_est']) ? $trainee[$key]['qual_est']= $register[0]['qual_est']  : $trainee[$key]['qual_est']= null  );
      }
      $CI2->db->close();
      return $trainee;
    }

    function get_single_tr_estimation($id,$dt){
      //$CI = & get_instance();
      //$CI->db = $CI->load->database('mysql', TRUE);
      if ($flt = 'All') {$flt2="";} else {$flt2="and tr.tr_fleet = '".$flt."'";}
      $Query = "select rt.registered_id, rt.emp_id, rt.start_training, rt.batch_id, rt.route_est, rt.qual_est
      from registered_trainee rt
      where rt.emp_id = '".$id."' and rt.start_training = str_to_date('".$dt."','%Y-%m-%d')";
      $q = $CI->db->query($Query);
      $result = $q->result_array();
      return $result;
      //return $Query;
    }

    function get_tr_program_batch($flt){
      $CI = & get_instance();
      $CI->db = $CI->load->database('mysql', TRUE);
      $Query = "SELECT tb.batch_id, tr_name, batch_name FROM training_program tr, training_batch tb
      where tr.tr_program_id = tb.tr_program_id and tr.tr_fleet = '".$flt."'";
      $q = $CI->db->query($Query);
      $CI->db->close();
      $result = $q->result_array();
      return $result;
      //return $Query;
    }

    function update_register($btn,$id, $startdate, $date= NULL, $batch = NULL){
      $CI = & get_instance();
      $CI->db = $CI->load->database('mysql', TRUE);
      $batch_id = "";
      if($btn == "ql"){
        $Query = "UPDATE registered_trainee SET qual_est = '".$date."' WHERE emp_id = '".$id."' and start_training = '".$startdate."'";
      }
      if($btn == "rt"){
        $Query = "UPDATE registered_trainee SET route_est = '".$date."' WHERE emp_id = '".$id."' and start_training = '".$startdate."'";
      }
      else if($btn == "tr"){
        $batch_id = explode("/",$batch);
        $Query = "UPDATE registered_trainee SET batch_id = '".$batch_id[0]."' WHERE emp_id = '".$id."' and start_training = '".$startdate."'";
      }
      $q = $CI->db->query($Query);
    }

    function insert_register($btn,$id, $startdate, $date= NULL, $batch_id = NULL){
      $CI = & get_instance();
      $CI->db = $CI->load->database('mysql', TRUE);
      if($btn == "ql"){
        $Query = "INSERT INTO registered_trainee(emp_id, start_training, qual_est) VALUES ('".$id."', '".$startdate."', '".$date."')";
      }
      if($btn == "rt"){
        $Query = "INSERT INTO registered_trainee(emp_id, start_training, route_est) VALUES ('".$id."', '".$startdate."', '".$date."')";
      }
      else if($btn == "tr"){
        $Query = "INSERT INTO registered_trainee(emp_id, start_training, batch_id) VALUES ('".$id."', '".$startdate."', '".$batch_id."')";
      }
      $q = $CI->db->query($Query);
      //return $CI>db->affected_rows()
    }

    function check_register($id, $start){
      $CI = & get_instance();
      $CI->db = $CI->load->database('mysql', TRUE);
      $Query = "select count(registered_id) AS count from registered_trainee where emp_id = '".$id."' and start_training = '".$start."' ";
      $q = $CI->db->query($Query);
      $result = $q->result_array();
      return $result[0]['count'];
      //return $Query;
    }

  function fleet_list(){
    //FLEET YANG ACTIVE
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    $Query = "SELECT distinct(fleet) as fleet FROM temp_crew_list";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }

  function get_pilot_and_progress($dt) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $Query = "SELECT C.EMP_ID, C.FULLNAME, C.GENDER, to_char(C.BIRTHDATE,'YYYY-MM-DD') AS BIRTHDATE, CR.RANKCODE AS RANK_, F.FLEETCODE AS FLEET, to_char(ADD_MONTHS(C2.BIRTHDATE,780),'YYYY-MM-DD') as END_EMPLOYEE_DATE,
    CASE
    WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is not null) THEN 'RT'
    WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is null) THEN 'GT'
    ELSE 'Q' END
    as QUALSTAT,
    TR.QUALNAME, TR.StartTraining, TR.StartRouteTraining, TR.EndTraining,
    LIC.LIC, LIC.LIC_NO, LIC.LIC_ISSUEDATE AS LIC_ISSUE_DATE, LIC.LIC_EXPIRYDATE AS LIC_EXP_DATE, INS.QUAL_NAME AS FLT_INS
    FROM
    (select max(to_number(c.staffnumber)) as EMP_ID, CONCAT(CONCAT(c.firstname,' '), c.lastname) as FULLNAME, c.gender as GENDER, c.birthdate
    from DBSABRECM.CREW C
    where c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)
    group by c.firstname, c.middlename, c.lastname, c.gender, c.birthdate
  ) C
  INNER JOIN DBSABRECM.CREW C2 ON c.EMP_ID = C2.staffnumber
  INNER JOIN DBSABRECM.CREW_RANK CR on c.EMP_ID = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') -- Pilot only
  and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
  INNER JOIN DBSABRECM.CREW_FLEET F on c.EMP_ID = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
  LEFT JOIN
  (Select
    distinct(QUAL.IDPilotQual) as StaffID,
    QUAL.QUALNAME as QUALNAME,
    QUAL.IDQual as QualID,
    to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,
    to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
    to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
    from
    (select A.STAFFNUMBER as IDPilotQual, a.name as QUALNAME, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
      from DBSABRECM.CREW_QUALIFICATION A
      where (A.name = 'GACPT' or A.name = 'GAFOT')
      and A.ISSUEDATE <= to_date('".$dt."','YYYY-MM-DD') and (A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD') OR A.EXPIRYDATE IS NULL)
      order by A.ISSUEDATE asc) QUAL
      LEFT JOIN
      (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
        from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
        where
        crs.pairingid  = p.pairingid
        and p.pairingid = pd.pairingid
        and pd.dutyid = pda.dutyid
        and pda.activitytypecode = 'FLY'
      ) ROUTE_TR
      on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
      and ROUTE_TR.StartFly > qual.startqual
      group by
      (QUAL.IDPilotQual), QUAL.QUALNAME, QUAL.IDQual, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
      order by StartTraining asc
    ) TR ON TR.StaffID = C.EMP_ID
    LEFT JOIN
        (SELECT CD.STAFFNUMBER,
        listagg (CD.DOCUMENTTYPE, ',')
          within group (order by CD.ISSUEDATE) as LIC,
        listagg (CD.DOCNUMBER, ',')
            within group (order by CD.ISSUEDATE) as LIC_NO,
        listagg (to_char(CD.ISSUEDATE,'YYYY-MM-DD'), ',')
            within group (order by CD.ISSUEDATE) as LIC_ISSUEDATE,
        listagg (to_char(CD.EXPIRYDATE,'YYYY-MM-DD'), ',')
            within group (order by CD.ISSUEDATE) as LIC_EXPIRYDATE
        FROM DBSABRECM.CREW_DOCUMENT cd
        WHERE cd.DOCUMENTTYPE = 'ATPL' OR cd.DOCUMENTTYPE = 'CPL'
        group by CD.STAFFNUMBER) LIC ON c.EMP_ID = LIC.STAFFNUMBER
    LEFT JOIN
        (SELECT CQ.STAFFNUMBER,
        listagg (CQ.NAME, ',')
            within group (order by CQ.ISSUEDATE) as QUAL_NAME
        FROM DBSABRECM.CREW_QUALIFICATION CQ
        WHERE (CQ.NAME like 'GAFIA%' or CQ.NAME like 'GACAS%') and (to_date(to_char(CQ.ISSUEDATE,'YYYY-MM-DD'),'YYYY-MM-DD') <=  to_date('".$dt."','YYYY-MM-DD') and to_date(to_char(CQ.EXPIRYDATE,'YYYY-MM-DD'),'YYYY-MM-DD') >  to_date('".$dt."','YYYY-MM-DD'))
        GROUP BY CQ.STAFFNUMBER) INS ON c.EMP_ID = INS.STAFFNUMBER
    WHERE to_date(to_char(ADD_MONTHS(C2.BIRTHDATE,780),'YYYY-MM-DD'),'YYYY-MM-DD')> to_date('".$dt."','YYYY-MM-DD')";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }

  function clean_temp_crew_list(){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "DELETE FROM temp_crew_list";
    $q = $CI->db->query($Query);
    return $q;
  }

  //function get_pilot_out_plan($id){
  function get_pilot_out_plan($dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    //$Query = "SELECT * from pilot_out_estimation where emp_id = '".$id."'";
    $dt2 = new DateTime ($dt);
    $dt2 = $dt2->modify('-60 day');
    $Query = "SELECT * from pilot_out_estimation where out_date > '".$dt2->format('Y-m-d')."'";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    return $result;
  }

  function insert_temp_crew_list($data) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "INSERT INTO temp_crew_list (id, name, gender, birthdate, rank, fleet, emp_end_date, qualstat, start_training, start_route, end_training, tr_name, batch_name) VALUES ('".$data['EMP_ID']."', '".$data['FULLNAME']."', '".$data['GENDER']."', '".$data['BIRTHDATE']."', '".$data['RANK_']."', '".$data['FLEET']."', '".$data['END_EMPLOYEE_DATE']."', '".$data['QUALSTAT']."', '".$data['STARTTRAINING']."', '".$data['STARTROUTETRAINING']."', '".$data['ENDTRAINING']."', '".$data['TR_NAME']."', '".$data['BATCH_NAME']."')";
    $q = $CI->db->query($Query);
    $CI->db->close();
    //return $CI>db->affected_rows()
    }

  // function insert_temp_crew_list($data) {
  //   $CI = & get_instance();
  //   $CI->db = $CI->load->database('mysql', TRUE);
  //   $Query = "INSERT INTO temp_crew_list (id, name, gender, birthdate, rank, fleet, emp_end_date, qualstat, start_training, start_route, end_training, tr_name, batch_name, lic, lic_no, lic_issue_date, lic_exp_date, flt_ins) VALUES ('".$data['EMP_ID']."', '".$data['FULLNAME']."', '".$data['GENDER']."', '".$data['BIRTHDATE']."', '".$data['RANK_']."', '".$data['FLEET']."', '".$data['END_EMPLOYEE_DATE']."', '".$data['QUALSTAT']."', '".$data['STARTTRAINING']."', '".$data['STARTROUTETRAINING']."', '".$data['ENDTRAINING']."', '".$data['TR_NAME']."', '".$data['BATCH_NAME']."', '".$data['LIC']."', '".$data['LIC_NO']."', '".$data['LIC_ISSUE_DATE']."', '".$data['LIC_EXP_DATE']."', '".$data['FLT_INS']."')";
  //   print_r($Query);
  //   $q = $CI->db->query($Query);
  //   $CI->db->close();
  //   //return $CI->db->affected_rows();
  // }

  function count_temp_crew_list() {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT fleet, rank, qualstat, count(id) as count from temp_crew_list
    group by fleet, rank, qualstat";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function insert_crew_strength($dt,$flt,$rk,$q,$rt,$gt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "INSERT INTO crew_strength (fleet, rank, date, count_qual, count_route, count_ground)
    VALUES ('".$flt."', '".$rk."', '".$dt."', '".$q."', '".$rt."', '".$gt."')";
    $q = $CI->db->query($Query);
    $CI->db->close();
    //$result = $q->result
  }

  function clean_crew_strength($start,$end=NULL){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    is_null($end) ? $Query = "DELETE FROM crew_strength where date >= '".$start."'" : $Query = "DELETE FROM crew_strength where date >= '".$start."' and date < '".$end."'";
    $q = $CI->db->query($Query);
    $CI->db->close();
    return $Query;
  }

  // movement crew

  function get_qual_prev_route_temp_crew($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where end_training = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and (qualstat = 'RT' or qualstat = 'GT') and (start_route is not null or start_route <> '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_qual_prev_ground_temp_crew($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where end_training = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and qualstat = 'GT' and (start_route is null or start_route = '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_route_prev_ground_temp_crew($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where start_route = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and qualstat = 'GT'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_qual_prev_route_course($flt,$rk,$dt,$current){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT SUM(tr_count_trainee) as count FROM course_plan where tr_start >'".$current."' and est_qualified = '".$dt."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is not null or est_route <> '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_qual_prev_ground_course($flt,$rk,$dt,$current){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT SUM(tr_count_trainee) as count FROM course_plan where tr_start >'".$current."' and est_qualified = '".$dt."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is null or est_route = '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_route_prev_ground_course($flt,$rk,$dt,$current){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT SUM(tr_count_trainee) as count FROM course_plan where tr_start >'".$current."' and est_route = '".$dt."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_ground_course($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT SUM(tr_count_trainee) as count FROM course_plan where tr_start = '".$dt."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_ground_course_prev($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT SUM(tr_count_trainee) as count FROM course_plan where tr_start = '".$dt."' and tr_fleet_prev = '".$flt."' and tr_rank_previous = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_end_date_q($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where emp_end_date = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and (end_training <= emp_end_date or qualstat = 'Q')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_end_date_rt($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where emp_end_date = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and start_route <= emp_end_date and end_training > emp_end_date and start_route <> '0000-00-00' and (qualstat = 'GT' or qualstat = 'RT')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function get_end_date_gt($flt,$rk,$dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT count(*) as count FROM temp_crew_list where emp_end_date = '".$dt."' and fleet = '".$flt."' and rank = '".$rk."' and start_route > emp_end_date and end_training > emp_end_date and (start_route = '0000-00-00' or end_training = '0000-00-00') and qualstat = 'GT'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count'];
  }

  function insert_crew_assignment($code,$count,$flt,$rk,$dt,$qual){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "INSERT INTO crew_assignment (fleet, rank, date, qual, duty_code, count_duty)
    VALUES ('".$flt."', '".$rk."', '".$dt."', '".$qual."', '".$code."', '".$count."')" ;
    $q = $CI->db->query($Query);
    $CI->db->close();
    return $Query;
  }

  function query_table_crewstrength($flt,$rk,$sdt,$edt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);

    if ($flt == 'All Fleets') $FLT =""; else $FLT = "AND fleet = '".$flt."'";
    if ($rk == 'All Ranks') $RK =""; else $RK = "AND rank = '".$rk."'";


    $Query = "SELECT  DATE, sum(count_qual) AS Q, sum(count_route) AS RT, sum(count_ground) AS GT
    FROM crew_strength
    WHERE DATE >= '".$sdt."' AND DATE <= '".$edt."' ".$FLT." ".$RK." GROUP BY DATE";

    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  //movement with detail

  function get_person_qual_prev_route_temp_crew($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.end_training AS date, 'Qual from route tr.' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE
    (tc.end_training > '".$start."' AND tc.end_training <= '".$end."') AND tc.fleet = '".$flt."' AND tc.rank = '".$rk."' AND (tc.qualstat = 'RT' OR tc.qualstat = 'GT') AND (tc.start_route IS NOT NULL OR tc.start_route <> '0000-00-00')";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_qual_prev_ground_temp_crew($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.end_training AS date, 'Qual from ground tr.' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE
    (tc.end_training > '".$start."' AND tc.end_training <= '".$end."') AND tc.fleet = '".$flt."' AND tc.rank = '".$rk."' AND tc.qualstat = 'GT' AND (tc.start_route IS NULL OR tc.start_route = '0000-00-00')";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_route_prev_ground_temp_crew($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.start_route AS date, 'Route tr from ground tr.' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE
    (tc.start_route > '".$start."' AND tc.start_route <= '".$end."') AND tc.fleet = '".$flt."' AND tc.rank = '".$rk."' AND tc.qualstat = 'GT'";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_qual_prev_route_course($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT cp.est_qualified as date, 'qual from next course plan' as movement, cp.tr_count_trainee as count_of_movement,  cp.tr_name, cp.batch_name, cp.tr_start as start_training, cp.est_route as start_route, cp.est_qualified as end_training FROM course_plan cp where tr_start >'".$start."' and est_qualified > '".$start."' and est_qualified <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is not null or est_route <> '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_qual_prev_ground_course($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT cp.est_qualified as date, 'qual from next course plan' as movement, cp.tr_count_trainee as count_of_movement,  cp.tr_name, cp.batch_name, cp.tr_start as start_training, cp.est_route as start_route, cp.est_qualified as end_training FROM course_plan cp where tr_start >'".$start."' and est_qualified > '".$start."' and est_qualified <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is null or est_route = '0000-00-00')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_route_prev_ground_course($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT cp.est_route as date, 'start route on next course plan' as movement, cp.tr_count_trainee as count_of_movement,  cp.tr_name, cp.batch_name, cp.tr_start as start_training, cp.est_route as start_route, cp.est_qualified as end_training FROM course_plan cp where tr_start >'".$start."' and est_route > '".$start."' and est_route <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_course_ground_course($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT cp.tr_start as date, 'start ground on next course plan' as movement, cp.tr_count_trainee as count_of_movement,  cp.tr_name, cp.batch_name, cp.tr_start as start_training, cp.est_route as start_route, cp.est_qualified as end_training FROM course_plan cp where tr_start > '".$start."' and tr_start <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_course_ground_course_prev($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT cp.tr_start as date, 'mutated/promoted to other rank/fleet' as movement, cp.tr_count_trainee as count_of_movement,  cp.tr_name, cp.batch_name, cp.tr_start as start_training, cp.est_route as start_route, cp.est_qualified as end_training FROM course_plan cp where tr_start > '".$start."' and tr_start <= '".$end."' and tr_fleet_prev = '".$flt."' and tr_rank_previous = '".$rk."'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_end_date_q($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.emp_end_date AS date, 'End of Employment' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE tc.emp_end_date > '".$start."' and tc.emp_end_date <= '".$end."' and tc.fleet = '".$flt."' and tc.rank = '".$rk."' and (tc.end_training <= tc.emp_end_date or tc.qualstat = 'Q')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_person_end_date_rt($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.emp_end_date AS date, 'End of Employment' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE tc.emp_end_date > '".$start."' and tc.emp_end_date <= '".$end."' and tc.fleet = '".$flt."' and tc.rank = '".$rk."' and tc.start_route <= tc.emp_end_date and tc.end_training > tc.emp_end_date and tc.start_route <> '0000-00-00' and (tc.qualstat = 'GT' or tc.qualstat = 'RT')" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
    }

  function get_person_end_date_gt($flt,$rk,$start,$end){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tc.emp_end_date AS date, 'End of Employment' AS movement, 1 as count_of_movement, tc.id, tc.name, tc.fleet, tc.rank, tc.qualstat, tp.tr_name, tb.batch_name, tc.start_training, tc.start_route, tc.end_training, tc.emp_end_date
    FROM temp_crew_list tc
    LEFT JOIN registered_trainee rt ON tc.id = rt.emp_id
    LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
    LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
    WHERE tc.emp_end_date > '".$start."' and tc.emp_end_date <= '".$end."' and tc.fleet = '".$flt."' and tc.rank = '".$rk."' and tc.start_route > tc.emp_end_date and tc.end_training > tc.emp_end_date and (tc.start_route = '0000-00-00' or tc.end_training = '0000-00-00') and tc.qualstat = 'GT'" ;
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
    }

  function check_trmax($tr_name){
    if (strpos(strtoupper($tr_name), "MAX") === false && strpos(strtoupper($tr_name), "7M8") === false) return 0;
    else return 1;
  }

  function retrieve_crewlist($flt, $rk){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $state_flt = "";
    $state_rk = "";
    $state_where = "";
    $state_and = "";

    if ($flt != "ALL" || $rk != "ALL") {
      $state_where = "WHERE";
      ($flt == "ALL" ? $state_flt = "" : $state_flt = "tc.fleet = '".$flt."'");
      ($rk == "ALL" ? $state_rk = "" : $state_rk = "tc.rank = '".$rk."'");
      (($rk != "ALL") && ($flt != "ALL") ? $state_and = "and" : $state_and = "");
    }

    $Query = "SELECT tc.id, tc.name, tc.gender, tc.birthdate, tc.rank, tc.fleet, tc.emp_end_date, tc.qualstat, tc.start_training, tc.start_route, tc.end_training, tc.tr_name, tc.batch_name
    FROM temp_crew_list tc ".$state_where." ".$state_flt." ".$state_and." ".$state_rk."";

    // $Query = "SELECT tc.id, tc.name, tc.gender, tc.birthdate, tc.rank, tc.fleet, tc.emp_end_date, tc.qualstat, tc.start_training, tc.start_route, tc.end_training, tc.tr_name, tc.batch_name, tc.lic, tc.lic_no, tc.lic_issue_date, tc.lic_exp_date, tc.flt_ins
    // FROM temp_crew_list tc ".$state_where." ".$state_flt." ".$state_and." ".$state_rk."";


    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function get_ods_crewstrength($dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $Query = "SELECT cs.fleet as fleet, cs.rank_ as rank_, cs.qualstat as qualstat, count(cs.emp_id) as count_id FROM
        (SELECT C.EMP_ID, C.FULLNAME, C.GENDER, to_char(C.BIRTHDATE,'YYYY-MM-DD') AS BIRTHDATE, CR.RANKCODE AS RANK_, F.FLEETCODE AS FLEET,
            CASE
            WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is not null) THEN 'RT'
            WHEN (TR.QUALNAME = 'GACPT' or TR.QUALNAME = 'GAFOT') and (TR.StartRouteTraining is null) THEN 'GT'
            ELSE 'Q' END
            as QUALSTAT
            FROM
            (select max(to_number(c.staffnumber)) as EMP_ID, CONCAT(CONCAT(c.firstname,' '), c.lastname) as FULLNAME, c.gender as GENDER, c.birthdate
            from DBSABRECM.CREW C
            where c.employmentdate <= to_date('".$dt."','yyyy-mm-dd') and (c.terminationdate > to_date('".$dt."','yyyy-mm-dd') or c.terminationdate is null)
            group by c.firstname, c.middlename, c.lastname, c.gender, c.birthdate
          ) C
          INNER JOIN DBSABRECM.CREW C2 ON c.EMP_ID = C2.staffnumber
          INNER JOIN DBSABRECM.CREW_RANK CR on c.EMP_ID = cr.staffnumber and (cr.rankcode = 'CP' or cr.rankcode = 'FO') -- Pilot only
          and (cr.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (cr.expirydate > to_date('".$dt."','yyyy-mm-dd') or cr.expirydate is null))
          INNER JOIN DBSABRECM.CREW_FLEET F on c.EMP_ID = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null))
          LEFT JOIN
          (Select
            distinct(QUAL.IDPilotQual) as StaffID,
            QUAL.QUALNAME as QUALNAME,
            QUAL.IDQual as QualID,
            to_char(QUAL.StartQual,'YYYY-MM-DD') as StartTraining,
            to_char(min(ROUTE_TR.StartFly),'YYYY-MM-DD') as StartRouteTraining,
            to_char(QUAL.EndQual,'YYYY-MM-DD') as EndTraining
            from
            (select A.STAFFNUMBER as IDPilotQual, a.name as QUALNAME, a.qualificationid as IDQual, A.ISSUEDATE as StartQual, A.EXPIRYDATE as EndQual
              from DBSABRECM.CREW_QUALIFICATION A
              where (A.name = 'GACPT' or A.name = 'GAFOT')
              and A.ISSUEDATE <= to_date('".$dt."','YYYY-MM-DD') and (A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD') OR A.EXPIRYDATE IS NULL)
              order by A.ISSUEDATE asc) QUAL
              LEFT JOIN
              (select crs.staffnumber as IDPilotRoute, crs.startdatetimeloc as StartFly, pda.activitytypecode as Duty
                from DBSABRECM.CREWROSTER CRS, DBSABRECM.PAIRING P, DBSABRECM.PAIRING_DUTY PD, DBSABRECM.PAIRING_DUTYACTIVITY PDA
                where
                crs.pairingid  = p.pairingid
                and p.pairingid = pd.pairingid
                and pd.dutyid = pda.dutyid
                and pda.activitytypecode = 'FLY'
              ) ROUTE_TR
              on  QUAL.IDPilotQual = ROUTE_TR.IDPilotRoute
              and ROUTE_TR.StartFly > qual.startqual
              group by
              (QUAL.IDPilotQual), QUAL.QUALNAME, QUAL.IDQual, to_char(QUAL.StartQual,'YYYY-MM-DD'), to_char(QUAL.EndQual,'YYYY-MM-DD')
              order by StartTraining asc
            ) TR ON TR.StaffID = C.EMP_ID
            WHERE to_date(to_char(ADD_MONTHS(C2.BIRTHDATE,780),'YYYY-MM-DD'),'YYYY-MM-DD')> to_date('".$dt."','YYYY-MM-DD')
        ) CS
    group by cs.fleet, cs.rank_, cs.qualstat";
    $q = $CI->db->query($Query);
    $CI->db->close();
    $result = $q->result_array();
    return $result;
  }

  function get_cpt_fot($dt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $Query = "SELECT A.STAFFNUMBER as EMPID, a.name as QUALNAME, to_char(A.ISSUEDATE,'YYYY-MM-DD') as StartQual, F.FLEETCODE AS FLEET
      from DBSABRECM.CREW_QUALIFICATION A
      INNER JOIN DBSABRECM.CREW_FLEET F on A.STAFFNUMBER = f.staffnumber and (f.effectivedate <= to_date('".$dt."','yyyy-mm-dd') and (f.expirydate > to_date('".$dt."','yyyy-mm-dd') or f.expirydate is null)) and f.fleetcode = '737'
      where (A.name = 'GACPT' or A.name = 'GAFOT')
      and A.ISSUEDATE <= to_date('".$dt."','YYYY-MM-DD') and (A.EXPIRYDATE > to_date('".$dt."','YYYY-MM-DD') OR A.EXPIRYDATE IS NULL)
      order by A.ISSUEDATE asc";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result;
  }

  function check_cpt_fot_max($id,$sdt){
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    $Query = "SELECT tp.tr_name, tb.batch_name FROM
      registered_trainee rt
      LEFT JOIN training_batch tb ON rt.batch_id = tb.batch_id
      LEFT JOIN training_program tp ON tp.tr_program_id = tb.tr_program_id
      WHERE rt.start_training = '".$sdt."' AND rt.emp_id = '".$id."' AND (UPPER(tp.tr_name) LIKE '7M8%' OR UPPER(tb.batch_name) LIKE '%MAX%')";
    $q = $CI->db->query($Query);
    $CI->db->close();
    if ($q->num_rows() > 0) return TRUE;
    else return FALSE;
  }

function get_max_cs($dt1,$dt2,$flt,$rk,$field = "cs.count_qual+cs.count_route"){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT MAX( ".$field. ") as MAX
   FROM crew_strength cs
   WHERE cs.date BETWEEN '".$dt1."' AND '".$dt2."'
   AND cs.fleet = '".$flt."'
   AND cs.rank = '".$rk."'";
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  if ($q->num_rows() > 0) return $result[0]['MAX'];
  else return FALSE;
}

function clean_crew_assign($dt1,$dt2,$qual){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "DELETE from crew_assignment WHERE date between '".$dt1."' and '".$dt2."' and qual = '".$qual."' ";
  $q = $CI->db->query($Query);
  return TRUE;
}

// Query Number of Pilot
function get_ods_flight_count($dt1,$dt2,$flt){
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($flt == '330') { $fFlt = "and A.AIRCRAFTTYPE LIKE '33%'";}
  else if ($flt == '737') {$fFlt = "and ((A.AIRCRAFTTYPE LIKE '73%') or (A.AIRCRAFTTYPE = '7M8'))"; }
  else if ($flt == 'ATR7') {$fFlt = "and A.AIRCRAFTTYPE LIKE 'AT%'";}
  else if ($flt == '777') {$fFlt = "and A.AIRCRAFTTYPE LIKE '77%'";}
  else if ($flt == 'CRJ') {$fFlt = "and A.AIRCRAFTTYPE LIKE 'CR%'";}
  else {$fFlt = "";}
  $Query = "SELECT TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD') as DATE_CGK, A.SERVICETYPE, COUNT(A.FLIGHTLEGREF) as COUNT
  from DBODSXML4OPS.XML4OPS A
  where to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '".$dt1."' and '".$dt2."' and A.status = 'Scheduled'
  and ((A.SERVICETYPE = 'J') or (A.SERVICETYPE = 'G') or (A.SERVICETYPE = 'C') or (A.SERVICETYPE = 'P') or (A.SERVICETYPE = 'O')) ".$fFlt."
  group by TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD'), A.SERVICETYPE
  order by DATE_CGK";
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  if ($q->num_rows() > 0) return $result;
  else return 0;
}

function clean_flight_count($dt1,$dt2,$stat=NULL,$scen=NULL){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "DELETE from flight_count WHERE date between '".$dt1."' and '".$dt2."' and stat = '".$stat."'";
  $q = $CI->db->query($Query);
  return TRUE;
}

function insert_fligth_count($count,$fleet,$dt,$svc,$stat){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "INSERT INTO flight_count (flightcount, fleet, date, service, stat)
  VALUES ('".$count."', '".$fleet."', '".$dt."', '".$svc."', '".$stat."')";
  $q = $CI->db->query($Query);
  $CI->db->close();
}

function get_flight_count($dt,$flt,$svc=NULL){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  if ($svc == NULL ) {$svc_stat = "";}
  else {$svc_stat = "and service = '".$svc."'";}

  $Query = "SELECT flightcount, service FROM flight_count
  WHERE fleet = '".$flt."' AND date = '".$dt."' AND file_scenario = 'default' ".$svc_stat;
  $q = $CI->db->query($Query);
  $CI->db->close();
  if ($q->num_rows() > 0) {return $q->row()->flightcount;}
  else {return 0;}
}

/// REMAKE
function get_course_ground_course_count($flt,$rk,$start,$end){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT sum(cp.tr_count_trainee) as count_of_movement FROM course_plan cp where tr_start > '".$start."' and tr_start <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'";
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['count_of_movement'];
}

function get_course_ground_course_prev_count($flt,$rk,$start,$end){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT sum(cp.tr_count_trainee) as count_of_movement FROM course_plan cp where tr_start > '".$start."' and tr_start <= '".$end."' and tr_fleet_prev = '".$flt."' and tr_rank_previous = '".$rk."'";
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['count_of_movement'];
}

function get_person_qual_prev_route_course_count($flt,$rk,$start,$end){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT sum(cp.tr_count_trainee) as count_of_movement FROM course_plan cp where tr_start >'".$start."' and est_qualified > '".$start."' and est_qualified <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is not null or est_route <> '0000-00-00')" ;
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['count_of_movement'];
}

function get_person_qual_prev_ground_course_count($flt,$rk,$start,$end){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT sum(cp.tr_count_trainee) as count_of_movement FROM course_plan cp where tr_start >'".$start."' and est_qualified > '".$start."' and est_qualified <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."' and (est_route is null or est_route = '0000-00-00')" ;
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['count_of_movement'];
}

function get_person_route_prev_ground_course_count($flt,$rk,$start,$end){
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $Query = "SELECT sum(cp.tr_count_trainee) as count_of_movement FROM course_plan cp where tr_start >'".$start."' and est_route > '".$start."' and est_route <= '".$end."' and tr_fleet = '".$flt."' and tr_rank = '".$rk."'" ;
  $q = $CI->db->query($Query);
  $result = $q->result_array();
  $CI->db->close();
  return $result[0]['count_of_movement'];
}

function check_pilot_must_not_include($id, $dt) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('mysql', TRUE);
    //$Query = "SELECT * from pilot_out_estimation where emp_id = '".$id."'";
    $dt_i = new DateTime ($dt);
    $Query = "SELECT count(emp_id) as count_id from pilot_out_estimation where emp_id = '".$id."' and out_date <= '".$dt_i->format('Y-m-d')."'";
    $q = $CI->db->query($Query);
    $result = $q->result_array();
    $CI->db->close();
    return $result[0]['count_id'];
}
