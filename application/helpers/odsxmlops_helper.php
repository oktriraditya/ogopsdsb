<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

function divertStatus() {
  return "(SUFFIX != 'R' or SUFFIX IS NULL)";
}

function domstn() {
  $domstn = "'AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
  ,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
  ,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
  ,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR','SRI','KJT','AAP','YIA','HLP'";

  return $domstn;
}

function intstn() {

  $intstn = "'DVO','PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO','NGO'";
  return $intstn;
}

function get_departByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select("COUNT(FLTNUM) as NFDEPART");
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'HH24:MI') as ACTUAL_BLOCKOFF_LC");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->where($timeLimit);
  $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
  $CI->db->where_in("SERVICETYPE", $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  //$q = $CI->db->get();
  //echo $CI->db->last_query();

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDEPART;
  if ($reqType == 'rec') {
    $result = $q->result_array();
    return $result;
    $q = $CI->db->get();
  }
}

function get_departArrByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select("COUNT(FLTNUM) as NFDEPART");
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'HH24:MI') as ACTUAL_BLOCKOFF_LC");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->where($timeLimit);
  $CI->db->where("ACTUAL_BLOCKON IS NOT NULL");
  $CI->db->where_in("SERVICETYPE", $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  //$q = $CI->db->get();
  //echo $CI->db->last_query();

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDEPART;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_delayArrByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDELAY');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where("ACTUAL_BLOCKON IS NOT NULL");
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDELAY;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_delayByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDELAY');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDELAY;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_ontimeArrByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NF');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKON IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NF;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_ontimeByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NF');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NF;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_cancelByRangeTime($reqType, $sTime, $eTime, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
  $modDateLimit = "MODIFIEDDATE BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFCANCEL');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where($modDateLimit);
  $CI->db->where('STATUS', 'Cancelled');
  $CI->db->where_in('SERVICETYPE', $arrST);
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFCANCEL;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_schedule($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS");
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFSCHED');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  //$q=$CI->db->get();
  //echo $CI->db->last_query();

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFSCHED;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_cancelPlan($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFCANCEL');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where('STATUS', 'Cancelled');
  $CI->db->where_in('SERVICETYPE', $arrST);
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFCANCEL;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_departed($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDEPART');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDEPART;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_departedArr($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDEPART');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where('ACTUAL_BLOCKON IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDEPART;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_delay($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDELAY');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDELAY;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_delayArrival($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }
  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFDELAY');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKON IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFDELAY;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_onschedule($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFONSCHED');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where('STATUS', 'Scheduled');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('SCHED_DEPARTUREAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFONSCHED;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_onTime($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NF');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NF;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_onTimeArrival($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if ($eDate)
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }

  //delay tolerance
  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NF');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where("((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKON IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('SCHED_ARRIVALAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NF;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_cancel($reqType, $sDate, $eDate = NULL, $stn = NULL, $stnto = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');

  //datelimit
  if ($sDate) {
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    $sModDateLimit = "to_char(MODIFIEDDATE, 'yyyy-mm-dd') = '" . $sDate . "'";
  }

  if ($eDate) {
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";
    $sModDateLimit = "to_char(MODIFIEDDATE, 'yyyy-mm-dd') = '" . $eDate . "'";
  }

  if ($sDate && $eDate) {
    if ($sDate == $eDate)
    $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
    else {
      $eDate .= 'T23:59:59';
      $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
      $sModDateLimit = "MODIFIEDDATE BETWEEN to_date('" . $sDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eDate . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
    }
  }


  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'count')
  $CI->db->select('COUNT(FLTNUM) as NFCANCEL');
  if ($reqType == 'rec')
  $CI->db->select("FLIGHTLEGREF, FLTNUM, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as DEPTIME, DEPAIRPORT, to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ARRTIME, SCHED_ARRIVALAIRPORT, to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as OFF_BLOCK, STATUS, AIRCRAFTREG, AIRCRAFTTYPE, REMARKS, CANCELREASONCODE");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where($timeLimit);
  $CI->db->where($sModDateLimit);
  $CI->db->where('STATUS', 'Cancelled');
  $CI->db->where_in('SERVICETYPE', $arrST);
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($stnto)
  $CI->db->where('DEPAIRPORT', $stnto);
  if ($reqType == 'rec')
  $CI->db->order_by('DEPTIME');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NFCANCEL;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_causeOfDelay($reqType, $sDate, $eDate = NULL, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $reasoncode = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89',
  '09', '14', '16', '25', '30', '91', '92',
  '01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96',
  '51', '52', '90', '93', '97', '98', '99',
  '10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39',
  '50', '55', '56', '57', '58',
  '40', '41', '42', '43', '44', '45', '46', '47', '48',
  '70', '71', '72', '73', '75', '76', '77');
  //datelimit
  $sDateLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  $eDateLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";
  //delay tolerance
  $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM-DD') between '" . $sDate . "' and '" . $eDate . "'";

  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select('REASONCODE, COUNT(REASONCODE) as NCD');
  if ($reqType == 'count')
  $CI->db->select('COUNT(REASONCODE) as NCD');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY', 'DBODSXML4OPS.XML4OPS.FLIGHTLEGREF = DBODSXML4OPS.XML4OPS_DELAY.FLIGHTLEGREF');
  if ($sDate && $eDate) {
    $CI->db->where($timeLimit);
  } else {
    $CI->db->where($sDateLimit);
    if ($eDate)
    $CI->db->where($eDateLimit);
  }
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where_in("DELAYSEQ", array(1, 2));
  $CI->db->where_in("REASONCODE", $reasoncode);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->group_by('REASONCODE');
  if ($reqType == 'rec')
  $CI->db->order_by('NCD', 'DESC');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NCD;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_causeOfDelayRangeTime($reqType, $sTime, $eTime = NULL, $stn = NULL) {
  //service type included
  $arrST = array('J', 'G', 'A');
  $reasoncode = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89',
  '09', '14', '16', '25', '30', '91', '92',
  '01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96',
  '51', '52', '90', '93', '97', '98', '99',
  '10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39',
  '50', '55', '56', '57', '58',
  '40', '41', '42', '43', '44', '45', '46', '47', '48',
  '70', '71', '72', '73', '75', '76', '77');
  //datelimit
  //$sDateLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  //$eDateLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') = '" . $eDate . "'";
  //delay tolerance
  $timeLimit = "SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";

  $sDiff = '+000000015 00:00:00.000000000';

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  if ($reqType == 'rec')
  $CI->db->select('REASONCODE, COUNT(REASONCODE) as NCD');
  if ($reqType == 'count')
  $CI->db->select('COUNT(REASONCODE) as NCD');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY', 'DBODSXML4OPS.XML4OPS.FLIGHTLEGREF = DBODSXML4OPS.XML4OPS_DELAY.FLIGHTLEGREF');
  if ($sTime && $eTime) {
    $CI->db->where($timeLimit);
  } /* else {
    $CI->db->where($sDateLimit);
    if ($eTime)
    $CI->db->where($eDateLimit);
  } */
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where('ACTUAL_BLOCKOFF IS NOT NULL');
  $CI->db->where_in('SERVICETYPE', $arrST);
  $CI->db->where_in("DELAYSEQ", array(1, 2));
  $CI->db->where_in("REASONCODE", $reasoncode);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stn)
  $CI->db->where('DEPAIRPORT', $stn);
  if ($reqType == 'rec')
  $CI->db->group_by('REASONCODE');
  if ($reqType == 'rec')
  $CI->db->order_by('NCD', 'DESC');

  if ($reqType == 'count')
  return $CI->db->get()->row()->NCD;
  if ($reqType == 'rec') {
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }
}

function get_causeOfDelayText($rCode) {
  $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
  $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
  $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
  $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
  $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
  $arrSYST = array('50', '55', '56', '57', '58');
  $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
  $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');

  switch ($rCode) {
    case (in_array($rCode, $arrAPTF)):
    $rCodeName = 'APTF';
    break;
    case (in_array($rCode, $arrCOMC)):
    $rCodeName = 'COMC';
    break;
    case (in_array($rCode, $arrFLOP)):
    $rCodeName = 'FLOP';
    break;
    case (in_array($rCode, $arrOTHR)):
    $rCodeName = 'OTHR';
    break;
    case (in_array($rCode, $arrSTNH)):
    $rCodeName = 'STNH';
    break;
    case (in_array($rCode, $arrSYST)):
    $rCodeName = 'SYST';
    break;
    case (in_array($rCode, $arrTECH)):
    $rCodeName = 'TECH';
    break;
    case (in_array($rCode, $arrWTHR)):
    $rCodeName = 'WTHR';
    break;
    default:
    $rCodeName = 'DLRA';
    break;
  }

  return $rCodeName;
}

//moktri nambahin
function get_flight_number($stn = NULL, $date = NULL) {
  $CI = & get_instance();
  $serviceType = array('J', 'G', 'A');
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select('DISTINCT(FLTNUM) as FNUM');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where_in('XML4OPS.SERVICETYPE', $serviceType);
  $CI->db->where('DEPAIRPORT ', $stn);
  $CI->db->where('STATUS =', "Scheduled");
  $CI->db->where("to_char(SCHEDULED_DEPDT_LC,'YYYY-MM-DD')", $date);
  $CI->db->order_by('FNUM');

  $q = $CI->db->get();
  $result = $q->result_array();
  return $result;
}

function get_station() {
  $CI = & get_instance();
  $serviceType = array('J', 'G', 'A');
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select('DISTINCT(DEPAIRPORT) as STN');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where_in('XML4OPS.SERVICETYPE', $serviceType);
  $CI->db->where('DEPAIRPORT !=', "000");
  $CI->db->order_by('STN');

  $q = $CI->db->get();
  $result = $q->result_array();
  return $result;
}



//moktri nambahin
function cek_station($stn = NULL) {
  $CI = & get_instance();
  $serviceType = array('J', 'G', 'A');
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select('COUNT(DEPAIRPORT) as STN');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where_in('XML4OPS.SERVICETYPE', $serviceType);
  $CI->db->where('DEPAIRPORT', strtoupper($stn));
  //$CI->db->order_by('STN');

  $q = $CI->db->get();
  $result = $q->row()->STN;
  return $result;
}

function insert_otp_verification($otp) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $startTime = date("d-m-Y H:i:s");
  $query = "insert into dbcis.tb_login_otp (otp_number,expired_date,is_expired,created_date) values ('".$otp."','".date("d-m-Y H:i:s",strtotime('+5 minutes',strtotime($startTime)))."',0,'".date("d-m-Y H:i:s")."')";

  $q = $CI->db->query($query);
  return $q;
}

function update_otp_verification($otp) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $startTime = date("d-m-Y H:i:s");
  $query = "Update dbcis.tb_login_otp set is_expired = 1 where otp_number = '".$otp."'";

  $q = $CI->db->query($query);
  return $q;
}

function check_otp_verification($otp) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $query = "select count(otp_number) as otp from dbcis.tb_login_otp where otp_number = '".$otp."' and is_expired = 0 and expired_date >= '".date("d-m-Y H:i:s")."'";

  $q = $CI->db->query($query);
  $result = $q->row()->otp;
  return $result;
}

function check_registered_email($email) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $query = "select count(email) as email from dbcis.tb_register_user where email = '".$email."'";

  $q = $CI->db->query($query);
  $result = $q->row()->email;
  return $result;
}

function insert_registered_email($email,$role,$station = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);

  $query = "insert into dbcis.tb_register_user (email,role,station,is_disabled,last_login) values ('".$email."','".$role."','".$station."',0,'".date("d-m-Y H:i:s")."')";

  $q = $CI->db->query($query);
  return $q;
}

function update_registered_email($email, $station = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $startTime = date("d-m-Y H:i:s");
  if(is_null($station) || $station == "")
  $query = "Update dbcis.tb_register_user set last_login = '".date("d-m-Y H:i:s")."' where email = '".$email."'";
  else
  $query = "Update dbcis.tb_register_user set station = '".$station."' where email = '".$email."'";
  $q = $CI->db->query($query);
  return $q;
}

function get_std($fltnum, $option) {
  $CI = & get_instance();
  $serviceType = array('J', 'G', 'A');
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("SCHED_DEPARTUREAIRPORT, SCHED_ARRIVALAIRPORT, to_char(SCHEDULED_DEPDT+ interval '7' hour, 'HH24:MI') AS SCHEDULED_DEPDT,
  to_char(SCHEDULED_ARRDT+ interval '7' hour, 'HH24:MI') AS SCHEDULED_ARRDT,
  to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'HH24:MI') AS ACTUAL_BLOCKOFF,
  to_char(ACTUAL_BLOCKON+ interval '7' hour, 'HH24:MI') AS ACTUAL_BLOCKON");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where_in('XML4OPS.SERVICETYPE', $serviceType);
  $CI->db->where('DEPAIRPORT !=', "000");
  $CI->db->where("to_char(SCHEDULED_DEPDT_LC,'DD-MM-YYYY')", date("d-m-Y"));
  $CI->db->where('FLTNUM =', $fltnum);
  $q = $CI->db->get();

  if ($option == 1)
  return $q->row()->SCHEDULED_DEPDT;
  else if ($option == 2)
  return $q->row()->SCHED_DEPARTUREAIRPORT;
  else if ($option == 3)
  return $q->row()->SCHED_ARRIVALAIRPORT;
}

function get_percentOTP($nOnTime, $nDepart, $nCancel) {
  $pOtp = $nDepart > 0 ? round(($nOnTime / ($nDepart + $nCancel)) * 100, 2) : 0;

  return $pOtp;
}

function get_percentage($val1, $val2, $percent) {
  $p = $val2 > 0 ? round(($val1 / $val2) * $percent, 2) : 0;

  return $p;
}

function get_delaycode($flightref, $fltnum) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select('REASONCODE');
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY', 'DBODSXML4OPS.XML4OPS.FLIGHTLEGREF = DBODSXML4OPS.XML4OPS_DELAY.FLIGHTLEGREF');
  $CI->db->where('XML4OPS_DELAY.FLIGHTLEGREF', $flightref);
  $CI->db->where('FLTNUM', $fltnum);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  $CI->db->order_by('FLTNUM');

  $q = $CI->db->get();
  //echo $CI->db->last_query();
  $result = $q->result_array();

  return $result;
}

function get_dFlightOps($idFlight) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("
  FLIGHTLEGREF,
  FLTNUM,
  DEPAIRPORT,
  AIRCRAFTTYPE,
  AIRCRAFTREG,
  SCHED_ARRIVALAIRPORT,
  to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS SCHEDULED_DEPDT,
  to_char(SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS SCHEDULED_ARRDT,
  to_char(ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS ACTUAL_BLOCKOFF,
  to_char(ACTUAL_TAKEOFF+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS ACTUAL_TAKEOFF,
  to_char(ACTUAL_TOUCHDOWN+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS ACTUAL_TOUCHDOWN,
  to_char(ACTUAL_BLOCKON+ interval '7' hour, 'DD-MM-YYYY, HH24:MI') AS ACTUAL_BLOCKON,
  to_char(MODIFIEDDATE, 'DD-MM-YYYY, HH24:MI') AS LAST_UPDATE,
  REMARKS,
  ");
  $CI->db->from('DBODSXML4OPS.XML4OPS');
  $CI->db->where('XML4OPS.FLIGHTLEGREF', $idFlight);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

function get_dDelay($idFlight) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("*");
  $CI->db->from('DBODSXML4OPS.XML4OPS_DELAY');
  $CI->db->where('FLIGHTLEGREF', $idFlight);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

function get_dFlightPax($idFlight) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("*");
  $CI->db->from('DBODSXML4OPS.XML4OPS_PAXCOUNT');
  $CI->db->where('FLIGHTLEGREF', $idFlight);
  $CI->db->order_by('MODE', 'DESC');
  $CI->db->order_by('CLASS', 'ASC');

  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

/* FUNCTION DAILY FLIGHT OPERATIONAL BEGIN */

//function get_FlightNum(){
//        $CI =& get_instance();
//	$CI->db = $CI->load->database('default',TRUE);
//
//	$CI->db->select("
//                            ORIGINAL_DEPDT
//                        ");
//	$CI->db->from('DBODSXML4OPS.XML4OPS');
//        $CI->db->limit('2000');
//
//        $q = $CI->db->get();
//        $result = $q->result_array();
//
//        return $result;
//}

function get_totalSchedule($reqType, $sDate) {

  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') <= '" . $sDate . "'";
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);

  if ($reqType == 'rec')
  $CI->db->select("to_char(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY') AS DEPDATE");
  if ($reqType == 'count')
  $CI->db->select(" TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY') as TANGGAL, COUNT(*) AS SCHEDULE");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->where($timeLimit);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  $CI->db->where("ACTUAL_BLOCKOFF is NOT NULL");
  //$CI->db->where("ACTUAL_BLOCKON_LC is NOT NULL");
  $CI->db->where("SCHEDULED_DEPDT is NOT NULL");
  if ($reqType == 'count')
  $CI->db->group_by("TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY')");
  //if($reqType == 'rec')
  $CI->db->order_by("TANGGAL DESC");

  //if($reqType == 'count') return $CI->db->get()->row()->SCHEDULE;
  if ($reqType == 'count') {
    $q = $CI->db->get();
    $result = $q->result_array();

    return $result;
  }
}

function get_totalOnTime($sDate) {
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') <= '" . $sDate . "'";
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  //if($reqType == 'rec') $CI->db->select("to_char(PUBLISHED_DEPDT, 'DD-MM-YYYY') AS DEPDATE");
  //if($reqType == 'count')
  $CI->db->select(" TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY') as TANGGAL, COUNT(*) AS ONTIME");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->where($timeLimit);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  $CI->db->where("ACTUAL_BLOCKOFF is NOT NULL");
  //$CI->db->where("SCHEDULED_DEPDT+ interval '7' hour is NOT NULL");
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  //if($reqType == 'count')
  $CI->db->group_by("TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY')");
  //if($reqType == 'rec')
  $CI->db->order_by("TANGGAL DESC");

  //if($reqType == 'count') return $CI->db->get()->row()->SCHEDULE;
  //if($reqType == 'count') {
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
  //}
}

function get_totalArrivalOnTime($sDate) {
  $timeLimit = "to_char(SCHEDULED_DEPDT+ interval '7' hour, 'yyyy-mm-dd') <= '" . $sDate . "'";
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  //if($reqType == 'rec') $CI->db->select("to_char(PUBLISHED_DEPDT, 'DD-MM-YYYY') AS DEPDATE");
  //if($reqType == 'count')
  $CI->db->select(" TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY') as TANGGAL, COUNT(*) AS ARRONTIME");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->where($timeLimit);
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  //$CI->db->where("SCHEDULED_DEPDT_LC is NOT NULL");
  $CI->db->where("((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour))*24*60 <=", "'" . $sDiff . "'", FALSE);
  //if($reqType == 'count')
  $CI->db->group_by("TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY')");
  //if($reqType == 'rec')
  $CI->db->order_by("TANGGAL DESC");

  //if($reqType == 'count') return $CI->db->get()->row()->SCHEDULE;
  //if($reqType == 'count') {
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
  //}
}

function get_detail_cod($type_cod, $cod_date, $stnService, $org, $dest, $delay, $pp) {

  $ruteInt = "((XML4OPS.DEPAIRPORT IN (" . domstn() . ")
  AND XML4OPS.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (XML4OPS.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND XML4OPS.DEPAIRPORT IN (" . intstn() . ")) OR (XML4OPS.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND XML4OPS.DEPAIRPORT IN (" . intstn() . ")))";
  $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
  $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
  $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
  $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
  $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
  $arrSYST = array('50', '55', '56', '57', '58');
  $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
  $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');

  $arrAPTFPREV = array('80');
  $arrCOMCPREV = array('30');
  $arrFLOPPREV = array('60');
  $arrOTHRPREV = array('90');
  $arrSTNHPREV = array('10', '20');
  $arrSYSTPREV = array('50');
  $arrTECHPREV = array('40');
  $arrWTHRPREV = array('70');

  $arrAPTFORG = array('81', '82', '83', '84', '85', '86', '87', '88', '89');
  $arrCOMCORG = array('09', '14', '16', '25', '91', '92');
  $arrFLOPORG = array('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
  $arrOTHRORG = array('51', '52', '93', '97', '98', '99');
  $arrSTNHORG = array('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
  $arrSYSTORG = array('55', '56', '57', '58');
  $arrTECHORG = array('41', '42', '43', '44', '45', '46', '47', '48');
  $arrWTHRORG = array('71', '72', '73', '75', '76', '77');

  $sDiff = '+000000015 00:00:00.000000000';
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $serviceType = array('J', 'G', 'A');
  $statusPP = "(XML4OPS.SCHED_DEPARTUREAIRPORT = '" . $org . "' and XML4OPS.SCHED_ARRIVALAIRPORT = '" . $dest . "'
  OR XML4OPS.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and XML4OPS.SCHED_ARRIVALAIRPORT = '" . $org . "')";

  $CI->db->select("XML4OPS.FLTNUM,XML4OPS.AIRCRAFTREG, XML4OPS.SCHED_DEPARTUREAIRPORT,XML4OPS.SCHED_ARRIVALAIRPORT,
  XML4OPS.SCHED_AIRCRAFTTYPE, XML4OPS.REMARKS,to_char(XML4OPS.SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as SCHEDULED_DEPDT_LC,
  XML4OPS_DELAY.REASONCODE,XML4OPS_DELAY.DELAYSEQ,
  to_char(XML4OPS.SCHEDULED_ARRDT+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as SCHEDULED_ARRDT_LC,
  to_char(XML4OPS.ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC,
  to_char(XML4OPS.ACTUAL_BLOCKON+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ACTUAL_BLOCKON_LC,
  XML4OPS_DELAY.DURATION, XML4OPS.ONWARD_FLTNUM,
  to_char(XML4OPS.ONWARD_DATE+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ONWARD_DATE,
  to_char(XML4OPS.ACTUAL_BLOCKOFF+ interval '7' hour, 'DD-MM-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY', 'DBODSXML4OPS.XML4OPS.FLIGHTLEGREF = DBODSXML4OPS.XML4OPS_DELAY.FLIGHTLEGREF');
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  //$CI->db->where($timeLimit);
  if ($stnService == "dom") {
    $CI->db->where("XML4OPS.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
    $CI->db->where("XML4OPS.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
  } else if ($stnService == "int") {
    $CI->db->where($ruteInt);
  }
  $CI->db->where("SCHEDULED_DEPDT is NOT NULL");
  $CI->db->where("((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour))*24*60 >", "'" . $sDiff . "'", FALSE);
  $CI->db->where("TO_DATE(TRIM(CAST(SCHEDULED_DEPDT+ interval '7' hour AS DATE)),'DD-MM-YY') =", $cod_date);
  $CI->db->where_in("XML4OPS.SERVICETYPE", $serviceType);
  $CI->db->where_in("XML4OPS_DELAY.DELAYSEQ", array(1, 2));
  if ($pp == "checked" && $org != 'all' && $dest != 'all') {
    $CI->db->where($statusPP);
  }
  if ($pp == "unchecked" && $org != 'all' && $dest != 'all') {
    $CI->db->where("XML4OPS.SCHED_DEPARTUREAIRPORT", $org);
    $CI->db->where("XML4OPS.SCHED_ARRIVALAIRPORT", $dest);
  }
  if ($org != 'all' && $dest == 'all') {
    $CI->db->where("XML4OPS.SCHED_DEPARTUREAIRPORT", $org);
  }
  if ($org == 'all' && $dest != 'all') {
    $CI->db->where("XML4OPS.SCHED_ARRIVALAIRPORT", $dest);
  }
  if ($type_cod == 1 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrTECH);
  } else if ($type_cod == 2 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSTNH);
  } else if ($type_cod == 3 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrCOMC);
  } else if ($type_cod == 4 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSYST);
  } else if ($type_cod == 5 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrFLOP);
  } else if ($type_cod == 6 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrAPTF);
  } else if ($type_cod == 7 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrWTHR);
  } else if ($type_cod == 8 && $delay == "all") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrOTHR);
  }
  if ($type_cod == 1 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrTECHPREV);
  } else if ($type_cod == 2 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSTNHPREV);
  } else if ($type_cod == 3 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrCOMCPREV);
  } else if ($type_cod == 4 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSYSTPREV);
  } else if ($type_cod == 5 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrFLOPPREV);
  } else if ($type_cod == 6 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrAPTFPREV);
  } else if ($type_cod == 7 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrWTHRPREV);
  } else if ($type_cod == 8 && $delay == "previous") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrOTHRPREV);
  }
  if ($type_cod == 1 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrTECHORG);
  } else if ($type_cod == 2 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSTNHORG);
  } else if ($type_cod == 3 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrCOMCORG);
  } else if ($type_cod == 4 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSYSTORG);
  } else if ($type_cod == 5 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrFLOPORG);
  } else if ($type_cod == 6 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrAPTFORG);
  } else if ($type_cod == 7 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrWTHRORG);
  } else if ($type_cod == 8 && $delay == "origin") {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrOTHRORG);
  }


  $CI->db->order_by("XML4OPS.AIRCRAFTREG ASC");
  $CI->db->order_by("SCHEDULED_DEPDT_LC ASC");
  $CI->db->order_by("XML4OPS_DELAY.DELAYSEQ ASC");

  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

function filterMonthlyReport($stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {
  $a = "";
  $b = "";
  $c = "";


  $statusPP = " and (A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
  OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
  $ruteInt = " and ((A.DEPAIRPORT IN (" . domstn() . ")
  AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")))";
  if ($stnService == "dom") {
    $a = " and A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") and A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")";
  } else if ($stnService == "int") {
    $a = $ruteInt;
  }
  if ($pp == "checked" && !is_null($org) && !is_null($dest)) {
    $b = $statusPP;
  } else if ($pp == "unchecked" && !is_null($org) && !is_null($dest)) {
    $b = " and A.SCHED_DEPARTUREAIRPORT ='" . $org . "' and A.SCHED_ARRIVALAIRPORT ='" . $dest . "'";
  }
  if (!is_null($org) && is_null($dest)) {
    $c = " and A.SCHED_DEPARTUREAIRPORT = '" . $org . "'";
  } else if (is_null($org) && !is_null($dest)) {
    $c = " and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'";
  }
  return $a . $b . $c;
}

function get_daily_record_flight($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {

  $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
  AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")))";
  $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
  OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
  $serviceType = array('J', 'G', 'A');

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  $CI->db->select("TANGGAL,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(case when FIRSTFLIGHT = 1 and ONTIME = 1 then 1 else NULL END) as OTPFIRSTFLIGHT, "
  . "SUM(case when FIRSTFLIGHT = 1 and ZMDFIRSTFLIGHT = 1 then 1 else NULL END) as ZMDFIRSTFLIGHT,"
  . "COUNT(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR, "
  . "AVG(avgdelay) as AVGDELAY, AVG(avgdelayarr) as AVGDELAYARR, "
  . "SUM(TECH) as TECH,SUM(STNH) as STNH,SUM(COMM) as COMM,SUM(SYST) as SYST,SUM(FLOPS) as FLOPS,SUM(APTF) as APTF,SUM(WEATHER) as WEATHER,SUM(MISC) as MISC,"
  . "SUM(ORGTECH) as ORGTECH,SUM(ORGSTNH) as ORGSTNH,SUM(ORGCOMM) as ORGCOMM,SUM(ORGSYST) as ORGSYST,SUM(ORGFLOPS) as ORGFLOPS,SUM(ORGAPTF) as ORGAPTF,SUM(ORGWEATHER) as ORGWEATHER,SUM(ORGMISC) as ORGMISC,"
  . "SUM(PREVTECH) as PREVTECH,SUM(PREVSTNH) as PREVSTNH,SUM(PREVCOMM) as PREVCOMM,SUM(PREVSYST) as PREVSYST,SUM(PREVFLOPS) as PREVFLOPS,SUM(PREVAPTF) as PREVAPTF,SUM(PREVWEATHER) as PREVWEATHER,SUM(PREVMISC) as PREVMISC,");
  $CI->db->from("(select TO_DATE(TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL,
  MAX(case when A.STATUS = 'Scheduled' and
  A.ACTUAL_BLOCKOFF is not null then 1 else NULL END) as SCHEDULED,
  MAX(case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKOFF is not null then 1 else NULL END) as ONTIME,
  MAX(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END) as ARRONTIME,
  MAX(case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else NULL END) as SCHEDARR,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) and A.ACTUAL_BLOCKOFF is not null THEN 1 ELSE 0 END AS FIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
  THEN 1 ELSE 0 END AS OTPFIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
  THEN 1 ELSE 0 END AS ZMDFIRSTFLIGHT,
  case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKOFF is not null then
  EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END as avgdelay,
  case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKON is not null then
  EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 ELSE NULL END as AVGDELAYARR,
  COUNT(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as TECH,
  COUNT(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as STNH,
  COUNT(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as COMM,
  COUNT(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as SYST,
  COUNT(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as FLOPS,
  COUNT(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and B.DELAYSEQ IN (1,2) then 1 else NULL END) as APTF,
  COUNT(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as WEATHER,
  COUNT(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as MISC,
  COUNT(case when B.REASONCODE IN ('40') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as PREVTECH,
  COUNT(case when B.REASONCODE IN ('10','20')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVSTNH,
  COUNT(case when B.REASONCODE IN ('30')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVCOMM,
  COUNT(case when B.REASONCODE IN ('50')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVSYST,
  COUNT(case when B.REASONCODE IN ('60')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVFLOPS,
  COUNT(case when B.REASONCODE IN ('80') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVAPTF,
  COUNT(case when B.REASONCODE IN ('70')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVWEATHER,
  COUNT(case when B.REASONCODE IN ('90')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVMISC,
  COUNT(case when B.REASONCODE IN ('41', '42', '43', '44', '45', '46', '47', '48') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as ORGTECH,
  COUNT(case when B.REASONCODE IN ('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGSTNH,
  COUNT(case when B.REASONCODE IN ('09', '14', '16', '25', '91', '92')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGCOMM,
  COUNT(case when B.REASONCODE IN ('55', '56', '57', '58')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGSYST,
  COUNT(case when B.REASONCODE IN ('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGFLOPS,
  COUNT(case when B.REASONCODE IN ('81', '82', '83', '84', '85', '86', '87', '88', '89') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGAPTF,
  COUNT(case when B.REASONCODE IN ('71', '72', '73', '75', '76', '77')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGWEATHER,
  COUNT(case when B.REASONCODE IN ('51', '52', '93', '97', '98', '99')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGMISC
  FROM DBODSXML4OPS.XML4OPS A left join DBODSXML4OPS.XML4OPS_DELAY B ON A.FLIGHTLEGREF = B.FLIGHTLEGREF");
  //$CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF','left');
  $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT  and A.SERVICETYPE IN ('J','G', 'A') and A.STATUS = 'Scheduled'");
  $CI->db->where(divertStatus());

  if ($stnService == "dom") {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
    $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
  } else if ($stnService == "int") {
    $CI->db->where($ruteInt);
  }
  if ($pp == "checked" && $org != 'all' && $dest != 'all') {
    $CI->db->where($statusPP);
  }
  if ($pp == "unchecked" && $org != 'all' && $dest != 'all') {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }
  if ($org != 'all' && $dest == 'all') {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
  }
  if ($org == 'all' && $dest != 'all') {
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }
  if (!is_null($date1) && !is_null($date2))
  $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");


  $CI->db->group_by("AIRCRAFTREG,SCHEDULED_DEPDT,ACTUAL_BLOCKOFF,ACTUAL_BLOCKON,AIRCRAFTTYPE,SCHEDULED_ARRDT) GROUP BY TANGGAL");
  //$CI->db->group_by("A.ACTUAL_BLOCKOFF) GROUP BY TANGGAL");
  //$CI->db->group_by("TANGGAL");
  //$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY')");
  //$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
  $CI->db->order_by("TANGGAL DESC");
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

function get_daily_record_flight_otp_v2($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {

  $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
  AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")))";
  $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
  OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
  $serviceType = array('J', 'G', 'A');
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  $CI->db->select("TANGGAL,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(case when FIRSTFLIGHT = 1 and ONTIME = 1 then 1 else NULL END) as OTPFIRSTFLIGHT, "
  . "SUM(case when FIRSTFLIGHT = 1 and ZMDFIRSTFLIGHT = 1 then 1 else NULL END) as ZMDFIRSTFLIGHT,"
  . "COUNT(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR, "
  . "AVG(avgdelay) as AVGDELAY, AVG(avgdelayarr) as AVGDELAYARR, "
  . "SUM(TECH) as TECH,SUM(RMP) as RMP,SUM(PAXBAG) as PAXBAG,SUM(SYST) as SYST,SUM(FLOPS) as FLOPS,SUM(APTF) as APTF,SUM(WEATHER) as WEATHER,SUM(MISC) as MISC, SUM(CGM) as CGM, SUM(FUEL) as FUEL, SUM(CATR) as CATR, SUM(REAC) as REAC");
  $CI->db->from("(select TO_DATE(TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL,
  MAX(case when A.STATUS = 'Scheduled' and
  A.ACTUAL_BLOCKOFF is not null then 1 else NULL END) as SCHEDULED,
  MAX(case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKOFF is not null then 1 else NULL END) as ONTIME,
  MAX(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END) as ARRONTIME,
  MAX(case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else NULL END) as SCHEDARR,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) and A.ACTUAL_BLOCKOFF is not null THEN 1 ELSE 0 END AS FIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
  THEN 1 ELSE 0 END AS OTPFIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
  THEN 1 ELSE 0 END AS ZMDFIRSTFLIGHT,
  case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKOFF is not null then
  EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END as avgdelay,
  case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKON is not null then
  EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 ELSE NULL END as AVGDELAYARR,
  COUNT(case when B.REASONCODE IN ('41', '42', '43', '44', '45', '46', '47', '48') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as TECH,
  COUNT(case when B.REASONCODE IN ('31', '32', '33', '34', '35', '38', '39') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as RMP,
  COUNT(case when B.REASONCODE IN ('11', '12', '13', '14', '15', '16', '17', '18', '19', '91', '92') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PAXBAG,
  COUNT(case when B.REASONCODE IN ('55', '56', '57', '58') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as SYST,
  COUNT(case when B.REASONCODE IN ('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as FLOPS,
  COUNT(case when B.REASONCODE IN ('81', '82', '83', '84', '85', '86', '87', '88', '89') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and B.DELAYSEQ IN (1,2) then 1 else NULL END) as APTF,
  COUNT(case when B.REASONCODE IN ('71', '72', '73', '75', '76', '77')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as WEATHER,
  COUNT(case when B.REASONCODE IN ('06', '09', '51', '52', '93', '97', '98', '99')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as MISC,
  COUNT(case when B.REASONCODE IN ('21', '22', '23', '24', '25', '27', '28', '29')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as CGM,
  COUNT(case when B.REASONCODE IN ('36') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as FUEL,
  COUNT(case when B.REASONCODE IN ('37')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as CATR,
  COUNT(case when B.REASONCODE IN ('10','20','30','40','50','60','70','80','90') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as REAC
  FROM DBODSXML4OPS.XML4OPS A left join DBODSXML4OPS.XML4OPS_DELAY B ON A.FLIGHTLEGREF = B.FLIGHTLEGREF");
  //$CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF','left');
  $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT  and A.SERVICETYPE IN ('J','G', 'A') and A.STATUS = 'Scheduled'");
  $CI->db->where(divertStatus());

  if ($stnService == "dom") {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
    $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
  } else if ($stnService == "int") {
    $CI->db->where($ruteInt);
  }
  if ($pp == "checked" && $org != 'all' && $dest != 'all') {
    $CI->db->where($statusPP);
  }
  if ($pp == "unchecked" && $org != 'all' && $dest != 'all') {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }
  if ($org != 'all' && $dest == 'all') {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
  }
  if ($org == 'all' && $dest != 'all') {
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }
  if (!is_null($date1) && !is_null($date2))
  $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");

  $CI->db->group_by("AIRCRAFTREG,SCHEDULED_DEPDT,ACTUAL_BLOCKOFF,ACTUAL_BLOCKON,AIRCRAFTTYPE,SCHEDULED_ARRDT) GROUP BY TANGGAL");
  //$CI->db->group_by("A.ACTUAL_BLOCKOFF) GROUP BY TANGGAL");
  //$CI->db->group_by("TANGGAL");
  //$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY')");
  //$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
  $CI->db->order_by("TANGGAL DESC");
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}
//LUPA BUAT APA
/* function get_daily_record_flight($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {
$domstn = "'AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR','SRI'";
$intstn = "'PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO','NGO'";
$ruteInt = "((A.DEPAIRPORT IN (" . $domstn . ")
AND A.SCHED_ARRIVALAIRPORT IN (" . $intstn . "))
OR (A.SCHED_ARRIVALAIRPORT IN (" . $domstn . ")
AND A.DEPAIRPORT IN (" . $intstn . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . $intstn . ")
AND A.DEPAIRPORT IN (" . $intstn . ")))";
$statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
$serviceType = array('J', 'G', 'A');

$CI = & get_instance();
$CI->db = $CI->load->database('default', TRUE);
$sDiff = '+000000015 00:00:00.000000000';
$CI->db->select("TANGGAL,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(case when FIRSTFLIGHT = 1 and ONTIME = 1 then 1 else NULL END) as OTPFIRSTFLIGHT, "
. "SUM(case when FIRSTFLIGHT = 1 and ZMDFIRSTFLIGHT = 1 then 1 else NULL END) as ZMDFIRSTFLIGHT,"
. "COUNT(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR, AVG(avgdelay) as AVGDELAY,AVG(avgdelayarr) as AVGDELAYARR,"
. "SUM(TECH) as TECH,SUM(STNH) as STNH,SUM(COMM) as COMM,SUM(SYST) as SYST,SUM(FLOPS) as FLOPS,SUM(APTF) as APTF,SUM(WEATHER) as WEATHER,SUM(MISC) as MISC,"
. "SUM(ORGTECH) as ORGTECH,SUM(ORGSTNH) as ORGSTNH,SUM(ORGCOMM) as ORGCOMM,SUM(ORGSYST) as ORGSYST,SUM(ORGFLOPS) as ORGFLOPS,SUM(ORGAPTF) as ORGAPTF,SUM(ORGWEATHER) as ORGWEATHER,SUM(ORGMISC) as ORGMISC,"
. "SUM(PREVTECH) as PREVTECH,SUM(PREVSTNH) as PREVSTNH,SUM(PREVCOMM) as PREVCOMM,SUM(PREVSYST) as PREVSYST,SUM(PREVFLOPS) as PREVFLOPS,SUM(PREVAPTF) as PREVAPTF,SUM(PREVWEATHER) as PREVWEATHER,SUM(PREVMISC) as PREVMISC,");
$CI->db->from("(select TO_DATE(TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL,
MAX(case when A.STATUS = 'Scheduled' and
A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END) as SCHEDULED,
MAX(case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END) as ONTIME,
MAX(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END) as ARRONTIME,
MAX(case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END) as SCHEDARR,
CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) and A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE 0 END AS FIRSTFLIGHT,
CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
" . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE 0 END AS OTPFIRSTFLIGHT,
CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
" . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE 0 END AS ZMDFIRSTFLIGHT,
case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then
EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END as avgdelay,
case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKON is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then
EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 ELSE NULL END as AVGDELAYARR,
COUNT(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as TECH,
COUNT(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as STNH,
COUNT(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as COMM,
COUNT(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as SYST,
COUNT(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as FLOPS,
COUNT(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and B.DELAYSEQ IN (1,2) then 1 else NULL END) as APTF,
COUNT(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as WEATHER,
COUNT(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as MISC,
COUNT(case when B.REASONCODE IN ('40') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as PREVTECH,
COUNT(case when B.REASONCODE IN ('10','20')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVSTNH,
COUNT(case when B.REASONCODE IN ('30')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVCOMM,
COUNT(case when B.REASONCODE IN ('50')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVSYST,
COUNT(case when B.REASONCODE IN ('60')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVFLOPS,
COUNT(case when B.REASONCODE IN ('80') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVAPTF,
COUNT(case when B.REASONCODE IN ('70')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVWEATHER,
COUNT(case when B.REASONCODE IN ('90')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as PREVMISC,
COUNT(case when B.REASONCODE IN ('41', '42', '43', '44', '45', '46', '47', '48') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2)  then 1 else NULL END) as ORGTECH,
COUNT(case when B.REASONCODE IN ('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGSTNH,
COUNT(case when B.REASONCODE IN ('09', '14', '16', '25', '91', '92')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGCOMM,
COUNT(case when B.REASONCODE IN ('55', '56', '57', '58')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGSYST,
COUNT(case when B.REASONCODE IN ('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGFLOPS,
COUNT(case when B.REASONCODE IN ('81', '82', '83', '84', '85', '86', '87', '88', '89') and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGAPTF,
COUNT(case when B.REASONCODE IN ('71', '72', '73', '75', '76', '77')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGWEATHER,
COUNT(case when B.REASONCODE IN ('51', '52', '93', '97', '98', '99')and
((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and  B.DELAYSEQ IN (1,2) then 1 else NULL END) as ORGMISC
FROM DBODSXML4OPS.XML4OPS A left join DBODSXML4OPS.XML4OPS_DELAY B ON A.FLIGHTLEGREF = B.FLIGHTLEGREF");
//$CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF','left');
$CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT  and A.SERVICETYPE IN ('J','G', 'A') and A.STATUS = 'Scheduled'");
$CI->db->where(divertStatus());

//    if ($stnService == "dom"){
//    $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (".$domstn.")");
//    $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (".$domstn.")");
//    }
//    else if ($stnService == "int"){
//    $CI->db->where($ruteInt);
//    }
//    if ($pp == "checked" && $org!='all' && $dest!='all'){
//        $CI->db->where($statusPP);
//    }
//    if ($pp == "unchecked" && $org!='all' && $dest!='all'){
//        $CI->db->where("A.SCHED_DEPARTUREAIRPORT",$org);
//        $CI->db->where("A.SCHED_ARRIVALAIRPORT",$dest);
//    }
//    if ($org!='all' && $dest=='all'){
//        $CI->db->where("A.SCHED_DEPARTUREAIRPORT",$org);
//    }
//    if ($org=='all' && $dest!='all'){
//        $CI->db->where("A.SCHED_ARRIVALAIRPORT",$dest);
//    }
if (!is_null($date1) && !is_null($date2))
$CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");


$CI->db->group_by("AIRCRAFTREG,SCHEDULED_DEPDT,ACTUAL_BLOCKOFF,ACTUAL_BLOCKON,AIRCRAFTTYPE,SCHEDULED_ARRDT, SCHED_DEPARTUREAIRPORT, SCHED_ARRIVALAIRPORT, DEPAIRPORT) GROUP BY TANGGAL");
//$CI->db->group_by("A.ACTUAL_BLOCKOFF) GROUP BY TANGGAL");
//$CI->db->group_by("TANGGAL");
//$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY')");
//$CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
$CI->db->order_by("TANGGAL DESC");
$q = $CI->db->get();
$result = $q->result_array();

return $result;
} */

function get_daily_record_cod($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {


  $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
  AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")))";

  $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
  OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
  $serviceType = array('J', 'G', 'A');

  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  $CI->db->select("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL,
  AVG(case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then
  EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END) as AVGDELAY,
  AVG(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then
  EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
  EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 else NULL END) as AVGDELAYARR,
  count(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as TECH,
  count(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as STNH,
  count(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as COMM,
  count(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as SYST,
  count(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as FLOPS,
  count(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as APTF,
  count(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as WEATHER,
  count(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as MISC,
  count(case when B.REASONCODE IN ('40') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as PREVTECH,
  count(case when B.REASONCODE IN ('10','20')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVSTNH,
  count(case when B.REASONCODE IN ('30')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVCOMM,
  count(case when B.REASONCODE IN ('50')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVSYST,
  count(case when B.REASONCODE IN ('60')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVFLOPS,
  count(case when B.REASONCODE IN ('80') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVAPTF,
  count(case when B.REASONCODE IN ('70')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVWEATHER,
  count(case when B.REASONCODE IN ('90')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as PREVMISC,
  count(case when B.REASONCODE IN ('41', '42', '43', '44', '45', '46', '47', '48') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as ORGTECH,
  count(case when B.REASONCODE IN ('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGSTNH,
  count(case when B.REASONCODE IN ('09', '14', '16', '25', '91', '92')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGCOMM,
  count(case when B.REASONCODE IN ('55', '56', '57', '58')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGSYST,
  count(case when B.REASONCODE IN ('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGFLOPS,
  count(case when B.REASONCODE IN ('81', '82', '83', '84', '85', '86', '87', '88', '89') and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGAPTF,
  count(case when B.REASONCODE IN ('71', '72', '73', '75', '76', '77')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGWEATHER,
  count(case when B.REASONCODE IN ('51', '52', '93', '97', '98', '99')and
  ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as ORGMISC");
  $CI->db->from("DBODSXML4OPS.XML4OPS A");
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
  $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($stnService == "dom") {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
    $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
  }
  if ($stnService == "int") {
    $CI->db->where($ruteInt);
  }
  if ($pp == "checked" && !is_null($org) && !is_null($dest)) {
    $CI->db->where($statusPP);
  }
  if ($pp == "unchecked" && !is_null($org) && !is_null($dest)) {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }

  if ($org != 'all' && $dest == 'all') {
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
  }
  if ($org == 'all' && $dest != 'all') {
    $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
  }
  if (!is_null($date1) && !is_null($date2))
  $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");

  $CI->db->where_in("A.SERVICETYPE", $serviceType);
  $CI->db->where_in("B.DELAYSEQ", array(1, 2));
  $CI->db->where("A.ACTUAL_BLOCKOFF IS NOT NULL");
  $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY')");
  $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY')");
  $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
  $CI->db->order_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY'), to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY'), TANGGAL DESC");
  $q = $CI->db->get();
  $result = $q->result_array();

  return $result;
}

function cekService($stnService, $arrival = NULL) {


  if ($stnService == "dom" && $arrival == NULL) {
    return " DEP.DEPAIRPORT IN (" . domstn() . ")";
  } else if ($stnService == "int" && $arrival == NULL) {
    return " DEP.DEPAIRPORT IN (" . intstn() . ")";
  }
  if ($stnService == "dom" && $arrival != NULL) {
    return " ARR.AIRPORT IN (" . domstn() . ")";
  } else if ($stnService == "int" && $arrival != NULL) {
    return " ARR.AIRPORT IN (" . intstn() . ")";
  } else
  return "";
}

function cekAP($aptMgmt, $arrival = NULL) {
  $AP1 = "'AMQ','BDJ','BIK','BPN','DPS','JOG','KOE','LOP','MDC','SOC','SRG','SUB','UPG'";
  $AP2 = "'BDO','BTJ','CGK','DJB','DTB','KNO','PDG','PGK','PKU','PLM','PNK','TNJ'";
  if ($aptMgmt == "ap1" && $arrival == NULL) {
    return "and A.SCHED_DEPARTUREAIRPORT IN (" . $AP1 . ")";
  } else if ($aptMgmt == "ap2" && $arrival == NULL) {
    return "and A.SCHED_DEPARTUREAIRPORT IN (" . $AP2 . ")";
  }
  if ($aptMgmt == "ap1" && $arrival != NULL) {
    return "and A.LATEST_ARRIVALAIRPORT IN (" . $AP1 . ")";
  } else if ($aptMgmt == "ap2" && $arrival != NULL) {
    return "and A.LATEST_ARRIVALAIRPORT IN (" . $AP2 . ")";
  } else
  return "";
}

function cekAPFirst($aptMgmt = NULL, $stnService = NULL) {
  $AP1 = "'AMQ','BDJ','BIK','BPN','DPS','JOG','KOE','LOP','MDC','SOC','SRG','SUB','UPG'";
  $AP2 = "'BDO','BTJ','CGK','DJB','DTB','KNO','PDG','PGK','PKU','PLM','PNK','TNJ'";



  if ($aptMgmt == "ap1" && ($stnService == NULL || $stnService == "dom")) {
    return "where DEP.DEPAIRPORT IN (" . $AP1 . ")";
  } else if ($aptMgmt == "ap2" && ($stnService == NULL || $stnService == "dom")) {
    return "where DEP.DEPAIRPORT IN (" . $AP2 . ")";
  } else if ($aptMgmt == NULL && $stnService == "dom") {
    return "where DEP.DEPAIRPORT IN (" . domstn() . ")";
  } else if ($aptMgmt == NULL && $stnService == "int") {
    return "where DEP.DEPAIRPORT IN (" . intstn() . ")";
  } else
  return "";
}

function cekDate($date1, $date2, $sDate) {
  //$timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  if (!is_null($date1) && !is_null($date2))
  return "and to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'";
  //else return "and to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
}

function data_flight_by_station($sDate, $date1 = NULL, $date2 = NULL, $stnService = NULL, $aptMgmt = NULL) {


  $serviceType = array('J', 'G', 'A');
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  $complexQuery = "SELECT DEP.DEPAIRPORT   AS DEPAIRPORT,
  ARR.AIRPORT   AS ARRAIRPORT,
  DEP.scheduled AS SCHEDULED,
  DEP.ontime    AS ONTIME,
  DEP.ONTIME30    AS ONTIME30,
  ARR.SCHEDARR    AS SCHEDARR,
  ARR.arrontime AS ARRONTIME,
  ARR.AVGDELAYARR AS AVGDELAYARR,
  DEP.FIRSTFLIGHT AS FIRSTFLIGHT,
  DEP.OTPFIRSTFLIGHT AS OTPFIRSTFLIGHT,
  DEP.ZMDFIRSTFLIGHT AS ZMDFIRSTFLIGHT,
  DEP.AVGGROUNDTIME AS AVGGROUNDTIME,
  DEP.GTP AS GTP,
  DEP.GTPSTRICT AS GTPSTRICT,
  DEP.GTPSTNH AS GTPSTNH,
  DEP.QUICKHANDLINGCHANCE AS QUICKHANDLINGCHANCE,
  DEP.QUICKHANDLING AS QUICKHANDLING,
  DEP.TECH AS TECH,
  DEP.STNH AS STNH,
  DEP.COMM AS COMM,
  DEP.SYST AS SYST,
  DEP.FLOPS AS FLOPS,
  DEP.APTF AS APTF,
  DEP.WEATHER AS WEATHER,
  DEP.MISC AS MISC,
  DEP.AVGDELAY AS AVGDELAY,
  DEP.A80 AS A80,
  DEP.A81 AS A81,
  DEP.A82 AS A82,
  DEP.A83 AS A83,
  DEP.A84 AS A84,
  DEP.A85 AS A85,
  DEP.A86 AS A86,
  DEP.A87 AS A87,
  DEP.A88 AS A88,
  DEP.A89 AS A89 FROM (SELECT DEPAIRPORT,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(OTPFIRSTFLIGHT) as OTPFIRSTFLIGHT, SUM(ZMDFIRSTFLIGHT) as ZMDFIRSTFLIGHT,"
  . "SUM(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(SCHEDARR) AS SCHEDARR, AVG(GROUNDTIME) AVGGROUNDTIME,"
  . "SUM(ONTIME30) AS ONTIME30,"
  . "SUM(case when ONTIME = 1 OR (GROUNDTIME != 0 and GROUNDTIME <= (STDGROUNDTIME)) OR (GROUNDTIME = 0 and ZMDFIRSTFLIGHT = 1) "
  . "then 1 else 0 END) as GTP,"
  . "SUM(case when (GROUNDTIME != 0 and ((GROUNDTIME <= (STDGROUNDTIME)) OR (GROUNDTIME <= (GROUNDTIMEEXP)))) OR (GROUNDTIME = 0 and ZMDFIRSTFLIGHT = 1) "
  . "then 1 else 0 END) as GTPSTRICT,"
  . "SUM(case when (STNH = 1 and GROUNDTIME != 0 and ((GROUNDTIME <= (STDGROUNDTIME)) OR (GROUNDTIME <= (GROUNDTIMEEXP)))) OR (STNH = 1 and GROUNDTIME = 0 and ZMDFIRSTFLIGHT = 1) "
  . "then 1 else 0 END) as GTPSTNH,"
  . "SUM(case when QUICKHANDLING = 1 and ((GROUNDTIME != 0 and GROUNDTIME <= (STDGROUNDTIME-5)) OR (GROUNDTIME = 0 and ZMDFIRSTFLIGHT = 1)) then 1 else NULL END ) as QUICKHANDLING,"
  . "SUM(QUICKHANDLING) as QUICKHANDLINGCHANCE,"
  . "SUM(TECH) as TECH,SUM(STNH) as STNH,SUM(COMM) as COMM,SUM(SYST) as SYST,SUM(FLOPS) as FLOPS,SUM(APTF) as APTF,SUM(WEATHER) as WEATHER,SUM(MISC) as MISC,"
  . "AVG(AVGDELAY) as AVGDELAY,SUM(A80) as A80,SUM(A81) as A81,SUM(A82) as A82,SUM(A83) as A83,SUM(A84) as A84,SUM(A85) as A85,SUM(A86) as A86,SUM(A87) as A87,SUM(A88) as A88,SUM(A89) as A89 FROM
  (SELECT DEPAIRPORT as DEPAIRPORT,
    MAX(case when A.STATUS = 'Scheduled' and
      A.ACTUAL_BLOCKOFF is not null then 1 else 0 END) as SCHEDULED,
      MAX(case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
      A.ACTUAL_BLOCKOFF is not null then 1 else 0 END) as ONTIME,
      MAX(case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000030 00:00:00.000000000' and
      A.ACTUAL_BLOCKOFF is not null then 1 else 0 END) as ONTIME30,
      MAX(case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else 0 END) as SCHEDARR,
      CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) and A.ACTUAL_BLOCKOFF is not null THEN 1 ELSE 0 END AS FIRSTFLIGHT,
      CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
      and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
      THEN 1 ELSE 0 END AS OTPFIRSTFLIGHT,
      CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
      and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
      THEN 1 ELSE 0 END AS ZMDFIRSTFLIGHT,
      CASE WHEN
      TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) THEN 0 ELSE
      EXTRACT(minute from (actual_blockoff+ interval '7' hour) - lag((actual_blockon+ interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC))+
      EXTRACT(hour from (actual_blockoff+ interval '7' hour) - lag((actual_blockon+ interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC))*60 END AS GROUNDTIME,
      CASE WHEN
      TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) THEN 0 ELSE
      EXTRACT(minute from (SCHEDULED_DEPDT+ interval '7' hour) - lag((actual_blockon+ interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC))+
      EXTRACT(hour from (SCHEDULED_DEPDT+ interval '7' hour) - lag((actual_blockon+ interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC))*60 END AS GROUNDTIMEEXP,
      case when AIRCRAFTTYPE IN ('AT7') then 30 when AIRCRAFTTYPE IN ('CRK') then 40 when AIRCRAFTTYPE IN ('738','7M8') then 45 when AIRCRAFTTYPE IN ('330','332','333','777','773','744') then 60 END as STDGROUNDTIME,
      case when (lag((actual_blockon+ interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC) -
      lag((scheduled_arrdt + interval '7' hour))
      over (ORDER BY TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YYYY')), AIRCRAFTREG, TO_CHAR(actual_blockoff+ interval '7' hour, 'DD-MON-YY HH24:MI') ASC)) *24*60 > '+000000015 00:00:00.000000000'
      then 1 else 0 END AS QUICKHANDLING,
      AVG(case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'
      and A.ACTUAL_BLOCKOFF is not null then
      EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
      EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END) as avgdelay,
      count(case when B.REASONCODE IN ('80') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A80,
      count(case when B.REASONCODE IN ('81') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A81,
      count(case when B.REASONCODE IN ('82') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A82,
      count(case when B.REASONCODE IN ( '83') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A83,
      count(case when B.REASONCODE IN ('84') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A84,
      count(case when B.REASONCODE IN ('85') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A85,
      count(case when B.REASONCODE IN ('86') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A86,
      count(case when B.REASONCODE IN ('87') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A87,
      count(case when B.REASONCODE IN ('88') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A88,
      count(case when B.REASONCODE IN ('89') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A89,
      count(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as TECH,
      count(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as STNH,
      count(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as COMM,
      count(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as SYST,
      count(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as FLOPS,
      count(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as APTF,
      count(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as WEATHER,
      count(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as MISC
      FROM DBODSXML4OPS.XML4OPS A left join DBODSXML4OPS.XML4OPS_DELAY B ON A.FLIGHTLEGREF = B.FLIGHTLEGREF where A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT
      and " . divertStatus() . cekDate($date1, $date2, $sDate) . " and A.SERVICETYPE IN ('J','G', 'A') and A.status = 'Scheduled'"
      . "GROUP BY DEPAIRPORT, AIRCRAFTREG,SCHEDULED_DEPDT,ACTUAL_BLOCKOFF,ACTUAL_BLOCKON,AIRCRAFTTYPE,SCHEDULED_ARRDT) GROUP BY DEPAIRPORT) DEP "
      . "full outer join (SELECT SCHED_ARRIVALAIRPORT AS AIRPORT,
      SUM(CASE WHEN( ( A.ACTUAL_BLOCKON + interval '7' hour ) - ( A.SCHEDULED_ARRDT + interval '7' hour ) )*24*60 <= '+000000015 00:00:00.000000000' and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT THEN 1 else 0 END) AS arrontime,
      AVG(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'
      and A.ACTUAL_BLOCKON is not null then
      EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
      EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 ELSE NULL END) as AVGDELAYARR,
      SUM(case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else 0 END) as SCHEDARR
      FROM DBODSXML4OPS.XML4OPS A where A.SERVICETYPE IN ('J','G', 'A') " . cekDate($date1, $date2, $sDate) . ""
      . " and A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT  and A.status = 'Scheduled' and "
      . divertStatus() . "GROUP BY SCHED_ARRIVALAIRPORT)arr ON DEP.DEPAIRPORT = ARR.AIRPORT " . cekAPFirst($aptMgmt, $stnService) . " ORDER BY DEP.DEPAIRPORT ASC";
      $q = $CI->db->query($complexQuery);
      $result = $q->result_array();

      return $result;
    }

    function data_flight_by_station_cod($sDate, $date1 = NULL, $date2 = NULL, $stnService = NULL, $aptMgmt = NULL) {

      $AP1 = array('AMQ', 'BDJ', 'BIK', 'BPN', 'DPS', 'JOG', 'KOE', 'LOP', 'MDC', 'SOC', 'SRG', 'SUB', 'UPG');
      $AP2 = array('BDO', 'BTJ', 'CGK', 'DJB', 'DTB', 'KNO', 'PDG', 'PGK', 'PKU', 'PLM', 'PNK', 'TNJ');

      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
      $serviceType = array('J', 'G', 'A');
      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);
      $sDiff = '+000000015 00:00:00.000000000';
      $CI->db->select("DEPAIRPORT,
      AVG(case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then
      EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
      EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else NULL END) as avgdelay,
      count(case when B.REASONCODE IN ('80') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A80,
      count(case when B.REASONCODE IN ('81') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A81,
      count(case when B.REASONCODE IN ('82') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A82,
      count(case when B.REASONCODE IN ( '83') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A83,
      count(case when B.REASONCODE IN ('84') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A84,
      count(case when B.REASONCODE IN ('85') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A85,
      count(case when B.REASONCODE IN ('86') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A86,
      count(case when B.REASONCODE IN ('87') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A87,
      count(case when B.REASONCODE IN ('88') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A88,
      count(case when B.REASONCODE IN ('89') and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as A89,
      count(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as TECH,
      count(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as STNH,
      count(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as COMM,
      count(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and  B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as SYST,
      count(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as FLOPS,
      count(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as APTF,
      count(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as WEATHER,
      count(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and B.DELAYSEQ IN (1,2) and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as MISC");
      $CI->db->from("DBODSXML4OPS.XML4OPS A");
      $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
      $CI->db->where_in("A.SERVICETYPE", $serviceType);
      $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
      $CI->db->where(divertStatus());
      if ($stnService == "dom") {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
      }
      if ($stnService == "int") {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ")");
      }
      if ($aptMgmt == "ap1") {
        $CI->db->where_in("A.SCHED_DEPARTUREAIRPORT", $AP1);
      }
      if ($aptMgmt == "ap2") {
        $CI->db->where_in("A.SCHED_DEPARTUREAIRPORT", $AP2);
      }
      if (!is_null($date1) && !is_null($date2))
      $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");
      else
      $CI->db->where($timeLimit);
      $CI->db->group_by("DEPAIRPORT");
      $CI->db->order_by("DEPAIRPORT ASC");
      $q = $CI->db->get();
      $result = $q->result_array();

      return $result;
    }

    function daily_record_per_flight($sDate, $date1 = NULL, $date2 = NULL, $stnOrg = NULL, $stnDest = NULL, $flighttype = NULL) {

      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";

      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);

      $CI->db->select("A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
      SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT,LATEST_DEPARTUREAIRPORT,LATEST_ARRIVALAIRPORT,SUFFIX,SERVICETYPE,STATUS,
      TO_CHAR(SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY HH24:MI') AS SCHEDULED_DEPDT_LC,
      TO_CHAR(SCHEDULED_ARRDT + interval '7' hour,'DD-MON-YYYY HH24:MI') as SCHEDULED_ARRDT_LC,
      TO_CHAR(ACTUAL_BLOCKOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC,
      TO_CHAR(ACTUAL_BLOCKON + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKON_LC,
      TO_CHAR(A.ACTUAL_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TAKEOFF_LC,
      TO_CHAR(A.ACTUAL_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TOUCHDOWN_LC,
      TO_CHAR(A.ESTIMATED_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TAKEOFF_LC,
      TO_CHAR(A.ESTIMATED_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TOUCHDOWN_LC,
      case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPDEP,
      case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000'  and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END as OTPARR,
      case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END as OTPZMD,
      MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
      MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
      MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
      MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
      case when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'DOM'
      when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      else NULL END as ROUTE,
      A.REMARKS");
      $CI->db->from("DBODSXML4OPS.XML4OPS A");
      $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
      if ($flighttype == "reg") {
        $CI->db->where_in("A.SERVICETYPE", array('J', 'G', 'A'));
      } else if ($flighttype == "hajj") {
        $CI->db->where_in("A.SERVICETYPE", array('O'));
      }
      $CI->db->where_in("A.STATUS", array('Scheduled','Cancelled'));
      //$CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
      if (!is_null($date1) && !is_null($date2))
      $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");
      else
      $CI->db->where($timeLimit);
      if (!is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }
      if (!is_null($stnOrg) && is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
      }
      if (is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }

      $CI->db->group_by("A.FLIGHTLEGREF");
      $CI->db->group_by("A.SCHEDULED_DEPDT");
      //$CI->db->group_by("A.DEPARTURE_INDO");
      //$CI->db->group_by("A.ARRIVAL_INDO");
      $CI->db->group_by("A.FLTNUM");
      $CI->db->group_by("A.STATUS");
      $CI->db->group_by("A.AIRCRAFTREG");
      $CI->db->group_by("A.AIRCRAFTTYPE");
      $CI->db->group_by("A.SCHED_DEPARTUREAIRPORT");
      $CI->db->group_by("A.SCHED_ARRIVALAIRPORT");
      $CI->db->group_by("A.ACTUAL_BLOCKON");
      $CI->db->group_by("A.ACTUAL_BLOCKOFF");
      $CI->db->group_by("A.SCHEDULED_ARRDT");
      $CI->db->group_by("A.ACTUAL_TAKEOFF");
      $CI->db->group_by("A.ACTUAL_TOUCHDOWN");
      $CI->db->group_by("A.ESTIMATED_TAKEOFF");
      $CI->db->group_by("A.ESTIMATED_TOUCHDOWN");
      $CI->db->group_by("A.LATEST_DEPARTUREAIRPORT");
      $CI->db->group_by("A.LATEST_ARRIVALAIRPORT");
      $CI->db->group_by("A.REMARKS");
      $CI->db->group_by("A.SUFFIX");
      $CI->db->group_by("A.SERVICETYPE");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY') ASC");
      $CI->db->order_by("A.AIRCRAFTREG ASC");
      $CI->db->order_by("ACTUAL_BLOCKOFF_LC ASC");
      $q = $CI->db->get();
      $result = $q->result_array();

      return $result;
    }

    function daily_flight_schedule($sDate, $date1 = NULL, $date2 = NULL, $stnOrg = NULL, $stnDest = NULL, $flighttype = NULL) {

      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";

      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);

      $CI->db->select("A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD') as SCHED_DAY_OF_TRAVEL_CGK, TO_CHAR(A.SCHEDULED_DEPDT,'YYYY-MM-DD') as SCHED_DAY_OF_TRAVEL_UTC, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
          SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT,LATEST_DEPARTUREAIRPORT,LATEST_ARRIVALAIRPORT,SUFFIX,SERVICETYPE,STATUS,A.CANCELREASONCODE,to_char(A.CREATEDDATE,'DD-Mon-YYYY') as CREATED_DATE, to_char(A.MODIFIEDDATE,'DD-Mon-YYYY HH24:MI') as MODIFIED_DATE, TO_DATE(TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD'),'YYYY-MM-DD') - TO_DATE(TO_CHAR(A.MODIFIEDDATE + interval '7' hour,'YYYY-MM-DD'),'YYYY-MM-DD') as CANCEL_DURATION_BEFORE_FLIGHT,
          TO_CHAR(SCHEDULED_DEPDT,'YYYY-MM-DD HH24:MI') AS SCHEDULED_DEPDT_UTC,
          TO_CHAR(SCHEDULED_ARRDT,'YYYY-MM-DD HH24:MI') as SCHEDULED_ARRDT_UTC,
          TO_CHAR(ACTUAL_BLOCKOFF,'YYYY-MM-DDY HH24:MI') as ACTUAL_BLOCKOFF_UTC,
          TO_CHAR(ACTUAL_BLOCKON,'YYYY-MM-DD HH24:MI') as ACTUAL_BLOCKON_UTC,
          TO_CHAR(A.ACTUAL_TAKEOFF,'YYYY-MM-DD HH24:MI') as ACTUAL_TAKEOFF_UTC,
          TO_CHAR(A.ACTUAL_TOUCHDOWN,'YYYY-MM-DD HH24:MI') as ACTUAL_TOUCHDOWN_UTC,
          TO_CHAR(A.ESTIMATED_TAKEOFF,'YYYY-MM-DD HH24:MI') as ESTIMATED_TAKEOFF_UTC,
          TO_CHAR(A.ESTIMATED_TOUCHDOWN,'YYYY-MM-DD HH24:MI') as ESTIMATED_TOUCHDOWN_UTC,
          case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPDEP,
          case when ((A.ACTUAL_BLOCKON) - (A.SCHEDULED_ARRDT)) * 24*60 <= '+000000015 00:00:00.000000000'  and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END as OTPARR,
          case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END as OTPZMD,
          MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
          MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
          MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
          MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
          case when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'DOM'
          when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
          when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'INT'
          when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
          else NULL END as ROUTE,
          A.REMARKS,
          BOOK.BOOKPAX as BOOKEDPAX,
          ACT.ACTPAX as ACTUALPAX,
          CONFIG.CAP as SEATCAPACITY,
          CREW.COCKPIT,
          CREW.CABIN");
      $CI->db->from("DBODSXML4OPS.XML4OPS A");
      $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
      $CI->db->join('(select E.FLIGHTLEGREF, E."MODE" as PAXSTAT, sum(E.COUNT) as BOOKPAX from DBODSXML4OPS.XML4OPS_PAXCOUNT E group by E.FLIGHTLEGREF,E."MODE") BOOK', "A.FLIGHTLEGREF = BOOK.FLIGHTLEGREF and BOOK.PAXSTAT = 'BOOKED'", 'left');
      $CI->db->join('(select F.FLIGHTLEGREF, F."MODE" as PAXSTAT, sum(F.COUNT) as ACTPAX from DBODSXML4OPS.XML4OPS_PAXCOUNT F group by F.FLIGHTLEGREF,F."MODE") ACT', "A.FLIGHTLEGREF = ACT.FLIGHTLEGREF and ACT.PAXSTAT = 'ACTUAL'", 'left');
      $CI->db->join('(select D.FLIGHTLEGREF, sum(D.COUNT) as CAP from DBODSXML4OPS.XML4OPS_AIRCRAFTCONF D group by D.FLIGHTLEGREF) CONFIG', 'A.FLIGHTLEGREF = CONFIG.FLIGHTLEGREF', 'left');
      $CI->db->join("(select L.FLIGHTLEGID, L.SCHEDDEPTIMEUTC, L.FLIGHTNUMBER, L.DEPAIRPORTCODE,
                        listagg (CASE WHEN CR.RANKCODE IN ('CP', 'FO','FDT','XCUF') THEN CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(C.STAFFNUMBER,' '),CONCAT(CONCAT(CONCAT(CONCAT(C.FIRSTNAME,' '),C.MIDDLENAME),' '),C.LASTNAME)),' ('),CR.RANKCODE),') ') END, ', ')
                        within group (order by C.STAFFNUMBER) as COCKPIT,
                        listagg (CASE WHEN CR.RANKCODE IN ('PU','FA','COB','FAY','XCUC') THEN CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(C.STAFFNUMBER,' '),CONCAT(CONCAT(CONCAT(CONCAT(C.FIRSTNAME,' '),C.MIDDLENAME),' '),C.LASTNAME)),' ('),CR.RANKCODE),') ') END, ', ')
                        within group (order by C.STAFFNUMBER) as CABIN
                    from DBSABRECM.FLIGHTLEG L
                    JOIN DBSABRECM.PAIRING_DUTYACTIVITY DA ON DA.FLIGHTLEGID = L.FLIGHTLEGID
                    JOIN DBSABRECM.PAIRING_DUTY PD ON PD.DUTYID = DA.DUTYID
                    JOIN DBSABRECM.CREWROSTER CR ON CR.PAIRINGID = PD.PAIRINGID
                    JOIN DBSABRECM.CREW C ON C.STAFFNUMBER = CR.STAFFNUMBER
                    JOIN DBSABRECM.CREW_RANK CR ON CR.STAFFNUMBER = C.STAFFNUMBER AND (cr.effectivedate < L.SCHEDDEPTIMEUTC) and (cr.expirydate > L.SCHEDDEPTIMEUTC or cr.expirydate is null)
                    group by L.FLIGHTLEGID, L.SCHEDDEPTIMEUTC, L.FLIGHTNUMBER, L.DEPAIRPORTCODE) CREW", 'A.FLTNUM = CREW.FLIGHTNUMBER AND A.SCHEDULED_DEPDT = CREW.SCHEDDEPTIMEUTC AND CREW.DEPAIRPORTCODE = A.LATEST_DEPARTUREAIRPORT', 'left');
      if ($flighttype == "reg") {
        $CI->db->where_in("A.SERVICETYPE", array('J', 'G', 'A'));
      } else if ($flighttype == "hajj") {
        $CI->db->where_in("A.SERVICETYPE", array('O'));
      } else if ($flighttype == "chr") {
        $CI->db->where_in("A.SERVICETYPE", array('C'));
      }
      $CI->db->where_in("A.STATUS", array('Scheduled','Cancelled'));
      //$CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
      if (!is_null($date1) && !is_null($date2))
      $CI->db->where("to_char(SCHEDULED_DEPDT, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");
      else
      $CI->db->where($timeLimit);
      if (!is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }
      if (!is_null($stnOrg) && is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
      }
      if (is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }
      $CI->db->group_by("A.FLIGHTLEGREF");
      $CI->db->group_by("A.SCHEDULED_DEPDT");
      $CI->db->group_by("A.SCHEDULED_ARRDT");
      $CI->db->group_by("A.DEPARTURE_INDO");
      $CI->db->group_by("A.ARRIVAL_INDO");
      $CI->db->group_by("A.FLTNUM");
      $CI->db->group_by("A.AIRCRAFTREG");
      $CI->db->group_by("A.AIRCRAFTTYPE");
      $CI->db->group_by("A.SCHED_DEPARTUREAIRPORT");
      $CI->db->group_by("A.SCHED_ARRIVALAIRPORT");
      $CI->db->group_by("A.LATEST_DEPARTUREAIRPORT");
      $CI->db->group_by("A.LATEST_ARRIVALAIRPORT");
      $CI->db->group_by("A.ACTUAL_BLOCKOFF");
      $CI->db->group_by("A.ACTUAL_BLOCKON");
      $CI->db->group_by("A.ACTUAL_TAKEOFF");
      $CI->db->group_by("A.ACTUAL_TOUCHDOWN");
      $CI->db->group_by("A.ESTIMATED_TAKEOFF");
      $CI->db->group_by("A.ESTIMATED_TOUCHDOWN");
      $CI->db->group_by("A.ACTUAL_TAKEOFF");
      $CI->db->group_by("A.ACTUAL_TOUCHDOWN");
      $CI->db->group_by("A.REMARKS");
      $CI->db->group_by("A.SUFFIX");
      $CI->db->group_by("A.SERVICETYPE");
      $CI->db->group_by("A.CANCELREASONCODE");
      $CI->db->group_by("A.CREATEDDATE");
      $CI->db->group_by("A.MODIFIEDDATE");
      $CI->db->group_by("BOOK.BOOKPAX");
      $CI->db->group_by("ACT.ACTPAX");
      $CI->db->group_by("CONFIG.CAP");
      $CI->db->group_by("STATUS");
      $CI->db->group_by("CREW.COCKPIT");
      $CI->db->group_by("CREW.CABIN");
      $CI->db->group_by("case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END");
      $CI->db->group_by("case when ((A.ACTUAL_BLOCKON) - (A.SCHEDULED_ARRDT)) * 24*60 <= '+000000015 00:00:00.000000000' and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END, case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END");
      $CI->db->group_by("case when A.SCHED_DEPARTUREAIRPORT IN (".domstn().") AND A.SCHED_ARRIVALAIRPORT IN (".domstn().") then 'DOM' when A.SCHED_DEPARTUREAIRPORT IN (".intstn().") OR A.SCHED_ARRIVALAIRPORT IN (".intstn().") then 'INT' else NULL END");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY') ASC");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT,'YYYY') ASC");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT,'MM-YYYY') ASC");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT,'DD-MM-YYYY') ASC");
      $CI->db->order_by("A.AIRCRAFTREG ASC");
      $CI->db->order_by("ACTUAL_BLOCKOFF_UTC ASC");
      $q = $CI->db->get();
      //$result = $q->result_array();
      return $q;
    }

    function daily_dephub_report($sDate, $date1 = NULL, $date2 = NULL, $stnService = NULL) {
      $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
      AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
      OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")))";
      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
      $serviceType = array('J', 'G', 'A');
      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);

      $CI->db->select("A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD') as TANGGAL, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
      SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT, TO_CHAR(SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY HH24:MI') AS SCHEDULED_DEPDT_LC,
      TO_CHAR(SCHEDULED_ARRDT + interval '7' hour,'DD-MON-YYYY HH24:MI') as SCHEDULED_ARRDT_LC, TO_CHAR(ACTUAL_BLOCKOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC,
      TO_CHAR(ACTUAL_BLOCKON + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKON_LC,
      case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPDEP,
      case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPARR,
      case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END as OTPZMD,
      MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
      MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
      MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
      MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
      case when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'DOM'
      when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      else NULL END as ROUTE");
      $CI->db->from("DBODSXML4OPS.XML4OPS A");
      $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
      $CI->db->where_in("A.SERVICETYPE", $serviceType);
      $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
      $CI->db->where(divertStatus());
      if ($stnService == "dom") {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
        $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
      }
      if ($stnService == "int") {
        $CI->db->where($ruteInt);
      }
      $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
      if (!is_null($date1) && !is_null($date2))
      $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");
      else
      $CI->db->where($timeLimit);
      $CI->db->group_by("A.FLIGHTLEGREF");
      $CI->db->group_by("A.SCHEDULED_DEPDT");
      //$CI->db->group_by("A.DEPARTURE_INDO");
      //$CI->db->group_by("A.ARRIVAL_INDO");
      $CI->db->group_by("A.FLTNUM");
      $CI->db->group_by("A.AIRCRAFTREG");
      $CI->db->group_by("A.AIRCRAFTTYPE");
      $CI->db->group_by("A.SCHED_DEPARTUREAIRPORT");
      $CI->db->group_by("A.SCHED_ARRIVALAIRPORT");
      $CI->db->group_by("A.ACTUAL_BLOCKON");
      $CI->db->group_by("A.ACTUAL_BLOCKOFF");
      $CI->db->group_by("A.SCHEDULED_ARRDT");
      $CI->db->order_by("TANGGAL ASC");
      $CI->db->order_by("A.AIRCRAFTREG ASC");
      $CI->db->order_by("ACTUAL_BLOCKOFF_LC ASC");
      $q = $CI->db->get();
      $result = $q->result_array();

      return $result;
    }

    function data_montly_report($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {


      $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
      AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
      OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")))";
      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM') between '" . $date1 . "' and '" . $date2 . "'";
      $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
      OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
      $serviceType = array('J', 'G', 'A');
      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);
      $sDiff = '+000000015 00:00:00.000000000';
      $CI->db->select("BULAN,BULAN2,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(OTPFIRSTFLIGHT) as OTPFIRSTFLIGHT, SUM(ZMDFIRSTFLIGHT) as ZMDFIRSTFLIGHT,"
      . "SUM(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR");
      $CI->db->from("(SELECT to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY') as BULAN,
      to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') as BULAN2,
      case when A.STATUS = 'Scheduled' and
      A.ACTUAL_BLOCKOFF is not null then 1 else NULL END as SCHEDULED,
      case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '" . $sDiff . "' and
      A.ACTUAL_BLOCKOFF is not null then 1 else NULL END as ONTIME,
      case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '" . $sDiff . "' and
      A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END as ARRONTIME,
      case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else NULL END as SCHEDARR,
      TO_DATE(TO_CHAR(SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL, CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) THEN 1 ELSE 0 END AS FIRSTFLIGHT,
      CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
      and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
      THEN 1 ELSE 0 END AS OTPFIRSTFLIGHT,
      CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
      OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
      and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
      THEN 1 ELSE 0 END AS ZMDFIRSTFLIGHT
      FROM DBODSXML4OPS.XML4OPS A");
      $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
      $CI->db->where(divertStatus());
      if ($stnService == "dom") {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
        $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
      }
      if ($stnService == "int") {
        $CI->db->where($ruteInt);
      }
      if ($pp == "checked" && !is_null($org) && !is_null($dest)) {
        $CI->db->where($statusPP);
      }
      if ($pp == "unchecked" && !is_null($org) && !is_null($dest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
      }
      if (!is_null($org) && is_null($dest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
      }
      if (is_null($org) && !is_null($dest)) {
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
      }
      $CI->db->where($timeLimit);
      $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL AND A.SERVICETYPE IN ('J','G', 'A'))");
      //$CI->db->where_in("A.SERVICETYPE", $serviceType);
      $CI->db->group_by("BULAN");
      $CI->db->group_by("BULAN2");
      $CI->db->order_by("BULAN2 ASC");
      $q = $CI->db->get();
      $result = $q->result_array();
      return $result;
    }

    //LUPA BUAT APA
    /* function data_montly_report($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {
    $domstn = "'AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
    ,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
    ,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
    ,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR','SRI'";
    $intstn = "'PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO','NGO'";

    $ruteInt = "((A.DEPAIRPORT IN (" . $domstn . ")
    AND A.SCHED_ARRIVALAIRPORT IN (" . $intstn . "))
    OR (A.SCHED_ARRIVALAIRPORT IN (" . $domstn . ")
    AND A.DEPAIRPORT IN (" . $intstn . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . $intstn . ")
    AND A.DEPAIRPORT IN (" . $intstn . ")))";
    $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM') between '" . $date1 . "' and '" . $date2 . "'";
    $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
    OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
    $serviceType = array('J', 'G', 'A');
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $sDiff = '+000000015 00:00:00.000000000';
    $CI->db->select("BULAN,BULAN2,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(OTPFIRSTFLIGHT) as OTPFIRSTFLIGHT, SUM(ZMDFIRSTFLIGHT) as ZMDFIRSTFLIGHT,"
    . "SUM(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR");
    $CI->db->from("(SELECT to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY') as BULAN,
    to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') as BULAN2,
    case when A.STATUS = 'Scheduled' and
    A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END as SCHEDULED,
    case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '" . $sDiff . "' and
    A.ACTUAL_BLOCKOFF is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END as ONTIME,
    case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '" . $sDiff . "' and
    A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END as ARRONTIME,
    case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null " . filterMonthlyReport($stnService, $org, $dest, $pp) . " then 1 else NULL END as SCHEDARR,
    TO_DATE(TO_CHAR(SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL,
    CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
    OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) " . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE NULL END AS FIRSTFLIGHT,
    CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
    OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
    and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
    " . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE NULL END AS OTPFIRSTFLIGHT,
    CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
    OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
    and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
    " . filterMonthlyReport($stnService, $org, $dest, $pp) . " THEN 1 ELSE NULL END AS ZMDFIRSTFLIGHT
    FROM DBODSXML4OPS.XML4OPS A");
    $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
    $CI->db->where(divertStatus());

    $CI->db->where($timeLimit);
    $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL AND A.SERVICETYPE IN ('J','G', 'A'))");
    //$CI->db->where_in("A.SERVICETYPE", $serviceType);
    $CI->db->group_by("BULAN");
    $CI->db->group_by("BULAN2");
    $CI->db->order_by("BULAN2 ASC");
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  } */

  function data_montly_report_cod($date1 = NULL, $date2 = NULL, $stnService = NULL, $org = NULL, $dest = NULL, $pp = NULL) {


    $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
    AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
    OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
    AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
    AND A.DEPAIRPORT IN (" . intstn() . ")))";
    $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM') between '" . $date1 . "' and '" . $date2 . "'";
    $statusPP = "(A.SCHED_DEPARTUREAIRPORT = '" . $org . "' and A.SCHED_ARRIVALAIRPORT = '" . $dest . "'
    OR A.SCHED_DEPARTUREAIRPORT = '" . $dest . "' and A.SCHED_ARRIVALAIRPORT = '" . $org . "')";
    $serviceType = array('J', 'G', 'A');
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $sDiff = '+000000015 00:00:00.000000000';
    $CI->db->select("to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY') as BULAN,
    count(case when B.REASONCODE IN ('40', '41', '42', '43', '44', '45', '46', '47', '48') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as TECH,
    count(case when B.REASONCODE IN ('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as STNH,
    count(case when B.REASONCODE IN ('09', '14', '16', '25', '30', '91', '92')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as COMM,
    count(case when B.REASONCODE IN ('50', '55', '56', '57', '58')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as SYST,
    count(case when B.REASONCODE IN ('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as FLOPS,
    count(case when B.REASONCODE IN ('80', '81', '82', '83', '84', '85', '86', '87', '88', '89') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as APTF,
    count(case when B.REASONCODE IN ('70', '71', '72', '73', '75', '76', '77')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as WEATHER,
    count(case when B.REASONCODE IN ('51', '52', '90', '93', '97', '98', '99')and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' then 1 else NULL END) as MISC");
    $CI->db->from("DBODSXML4OPS.XML4OPS A");
    $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
    $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
    $CI->db->where(divertStatus());
    if ($stnService == "dom") {
      $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ")");
      $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")");
    }
    if ($stnService == "int") {
      $CI->db->where($ruteInt);
    }
    if ($pp == "checked" && !is_null($org) && !is_null($dest)) {
      $CI->db->where($statusPP);
    }
    if ($pp == "unchecked" && !is_null($org) && !is_null($dest)) {
      $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
      $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
    }
    if (!is_null($org) && is_null($dest)) {
      $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $org);
    }
    if (is_null($org) && !is_null($dest)) {
      $CI->db->where("A.SCHED_ARRIVALAIRPORT", $dest);
    }
    $CI->db->where($timeLimit);
    //$CI->db->where("B.DELAYSEQ != 3");
    $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
    $CI->db->where_in("A.SERVICETYPE", $serviceType);
    $CI->db->where_in("B.DELAYSEQ", array(1, 2));
    $CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY')");
    $CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM')");
    $CI->db->order_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') ASC");
    $q = $CI->db->get();
    $result = $q->result_array();
    return $result;
  }

  function data_delaycode_report($date1 = NULL, $date2 = NULL, $delayContributor = NULL) {


    $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
    AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
    OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
    AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
    AND A.DEPAIRPORT IN (" . intstn() . ")))";
    $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM-DD') between '" . $date1 . "' and '" . $date2 . "'";
    $serviceType = array('J', 'G', 'A');
    $CI = & get_instance();
    $CI->db = $CI->load->database('default', TRUE);
    $sDiff = '+000000015 00:00:00.000000000';
    $CI->db->select("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL");
    if ($delayContributor == 1) {
      $CI->db->select("count(case when B.REASONCODE IN ('40') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A40,
      count(case when B.REASONCODE IN ('41') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A41,
      count(case when B.REASONCODE IN ('42') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A42,
      count(case when B.REASONCODE IN ('43') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A43,
      count(case when B.REASONCODE IN ('44') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A44,
      count(case when B.REASONCODE IN ('45') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A45,
      count(case when B.REASONCODE IN ('46') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A46,
      count(case when B.REASONCODE IN ('47') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A47,
      count(case when B.REASONCODE IN ('48') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A48");
    }
    if ($delayContributor == 2) {
      $CI->db->select("
      count(case when B.REASONCODE IN ('10') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A10,
      count(case when B.REASONCODE IN ('11') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A11,
      count(case when B.REASONCODE IN ('12') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A12,
      count(case when B.REASONCODE IN ('13') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A13,
      count(case when B.REASONCODE IN ('15') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A15,
      count(case when B.REASONCODE IN ('17') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A17,
      count(case when B.REASONCODE IN ('18') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A18,
      count(case when B.REASONCODE IN ('20') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A20,
      count(case when B.REASONCODE IN ('21') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A21,
      count(case when B.REASONCODE IN ('22') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A22,
      count(case when B.REASONCODE IN ('23') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A23,
      count(case when B.REASONCODE IN ('24') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A24,
      count(case when B.REASONCODE IN ('26') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A26,
      count(case when B.REASONCODE IN ('27') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A27,
      count(case when B.REASONCODE IN ('28') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A28,
      count(case when B.REASONCODE IN ('29') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A29,
      count(case when B.REASONCODE IN ('31') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A31,
      count(case when B.REASONCODE IN ('32') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A32,
      count(case when B.REASONCODE IN ('33') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A33,
      count(case when B.REASONCODE IN ('34') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A34,
      count(case when B.REASONCODE IN ('35') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A35,
      count(case when B.REASONCODE IN ('36') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A36,
      count(case when B.REASONCODE IN ('37') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A37,
      count(case when B.REASONCODE IN ('38') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A38,
      count(case when B.REASONCODE IN ('39') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A39");
    }
    if ($delayContributor == 3) {
      $CI->db->select("count(case when B.REASONCODE IN ('09') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A09,
      count(case when B.REASONCODE IN ('14') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A14,
      count(case when B.REASONCODE IN ('16') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A16,
      count(case when B.REASONCODE IN ('25') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A25,
      count(case when B.REASONCODE IN ('30') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A30,
      count(case when B.REASONCODE IN ('91') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A91,
      count(case when B.REASONCODE IN ('92') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A92");
    }
    if ($delayContributor == 4) {
      $CI->db->select("count(case when B.REASONCODE IN ('50') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A50,
      count(case when B.REASONCODE IN ('55') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A55,
      count(case when B.REASONCODE IN ('56') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A56,
      count(case when B.REASONCODE IN ('57') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A57,
      count(case when B.REASONCODE IN ('58') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A58");
    }
    if ($delayContributor == 5) {
      $CI->db->select("count(case when B.REASONCODE IN ('01') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A01,
      count(case when B.REASONCODE IN ('02') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A02,
      count(case when B.REASONCODE IN ('60') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A60,
      count(case when B.REASONCODE IN ('61') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A61,
      count(case when B.REASONCODE IN ('62') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A62,
      count(case when B.REASONCODE IN ('63') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A63,
      count(case when B.REASONCODE IN ('64') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A64,
      count(case when B.REASONCODE IN ('65') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A65,
      count(case when B.REASONCODE IN ('66') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A66,
      count(case when B.REASONCODE IN ('67') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A67,
      count(case when B.REASONCODE IN ('68') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A68,
      count(case when B.REASONCODE IN ('69') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A69,
      count(case when B.REASONCODE IN ('94') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A94,
      count(case when B.REASONCODE IN ('95') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A95,
      count(case when B.REASONCODE IN ('96') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A96");
    }
    if ($delayContributor == 6) {
      $CI->db->select("count(case when B.REASONCODE IN ('80') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A80,
      count(case when B.REASONCODE IN ('81') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A81,
      count(case when B.REASONCODE IN ('82') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A82,
      count(case when B.REASONCODE IN ('83') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A83,
      count(case when B.REASONCODE IN ('84') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A84,
      count(case when B.REASONCODE IN ('85') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A85,
      count(case when B.REASONCODE IN ('86') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A86,
      count(case when B.REASONCODE IN ('87') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A87,
      count(case when B.REASONCODE IN ('88') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A88,
      count(case when B.REASONCODE IN ('89') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A89");
    }
    if ($delayContributor == 7) {
      $CI->db->select("count(case when B.REASONCODE IN ('70') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A70,
      count(case when B.REASONCODE IN ('71') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A71,
      count(case when B.REASONCODE IN ('72') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A72,
      count(case when B.REASONCODE IN ('73') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A73,
      count(case when B.REASONCODE IN ('75') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A75,
      count(case when B.REASONCODE IN ('76') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A76,
      count(case when B.REASONCODE IN ('77') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A77");
    }
    if ($delayContributor == 8) {
      $CI->db->select("count(case when B.REASONCODE IN ('51') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A51,
      count(case when B.REASONCODE IN ('52') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A52,
      count(case when B.REASONCODE IN ('90') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A90,
      count(case when B.REASONCODE IN ('93') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A93,
      count(case when B.REASONCODE IN ('97') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A97,
      count(case when B.REASONCODE IN ('98') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A98,
      count(case when B.REASONCODE IN ('99') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A99");
    }
    if ($delayContributor == "all") {
      $CI->db->select("count(case when B.REASONCODE IN ('40') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A40,
      count(case when B.REASONCODE IN ('41') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A41,
      count(case when B.REASONCODE IN ('42') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A42,
      count(case when B.REASONCODE IN ('43') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A43,
      count(case when B.REASONCODE IN ('44') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A44,
      count(case when B.REASONCODE IN ('45') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A45,
      count(case when B.REASONCODE IN ('46') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A46,
      count(case when B.REASONCODE IN ('47') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A47,
      count(case when B.REASONCODE IN ('48') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A48,
      count(case when B.REASONCODE IN ('10') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A10,
      count(case when B.REASONCODE IN ('11') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A11,
      count(case when B.REASONCODE IN ('12') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A12,
      count(case when B.REASONCODE IN ('13') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A13,
      count(case when B.REASONCODE IN ('15') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A15,
      count(case when B.REASONCODE IN ('17') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A17,
      count(case when B.REASONCODE IN ('18') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A18,
      count(case when B.REASONCODE IN ('20') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A20,
      count(case when B.REASONCODE IN ('21') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A21,
      count(case when B.REASONCODE IN ('22') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A22,
      count(case when B.REASONCODE IN ('23') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A23,
      count(case when B.REASONCODE IN ('24') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A24,
      count(case when B.REASONCODE IN ('26') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A26,
      count(case when B.REASONCODE IN ('27') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A27,
      count(case when B.REASONCODE IN ('28') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A28,
      count(case when B.REASONCODE IN ('29') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A29,
      count(case when B.REASONCODE IN ('31') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A31,
      count(case when B.REASONCODE IN ('32') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A32,
      count(case when B.REASONCODE IN ('33') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A33,
      count(case when B.REASONCODE IN ('34') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A34,
      count(case when B.REASONCODE IN ('35') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A35,
      count(case when B.REASONCODE IN ('36') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A36,
      count(case when B.REASONCODE IN ('37') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A37,
      count(case when B.REASONCODE IN ('38') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A38,
      count(case when B.REASONCODE IN ('39') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A39,
      count(case when B.REASONCODE IN ('09') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A09,
      count(case when B.REASONCODE IN ('14') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A14,
      count(case when B.REASONCODE IN ('16') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A16,
      count(case when B.REASONCODE IN ('25') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A25,
      count(case when B.REASONCODE IN ('30') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A30,
      count(case when B.REASONCODE IN ('91') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A91,
      count(case when B.REASONCODE IN ('92') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A92,
      count(case when B.REASONCODE IN ('50') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A50,
      count(case when B.REASONCODE IN ('55') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A55,
      count(case when B.REASONCODE IN ('56') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A56,
      count(case when B.REASONCODE IN ('57') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A57,
      count(case when B.REASONCODE IN ('58') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A58,
      count(case when B.REASONCODE IN ('01') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A01,
      count(case when B.REASONCODE IN ('02') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A02,
      count(case when B.REASONCODE IN ('60') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A60,
      count(case when B.REASONCODE IN ('61') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A61,
      count(case when B.REASONCODE IN ('62') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A62,
      count(case when B.REASONCODE IN ('63') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A63,
      count(case when B.REASONCODE IN ('64') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A64,
      count(case when B.REASONCODE IN ('65') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A65,
      count(case when B.REASONCODE IN ('66') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A66,
      count(case when B.REASONCODE IN ('67') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A67,
      count(case when B.REASONCODE IN ('68') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A68,
      count(case when B.REASONCODE IN ('69') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A69,
      count(case when B.REASONCODE IN ('94') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A94,
      count(case when B.REASONCODE IN ('95') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A95,
      count(case when B.REASONCODE IN ('96') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A96,
      count(case when B.REASONCODE IN ('80') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A80,
      count(case when B.REASONCODE IN ('81') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A81,
      count(case when B.REASONCODE IN ('82') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A82,
      count(case when B.REASONCODE IN ('83') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A83,
      count(case when B.REASONCODE IN ('84') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A84,
      count(case when B.REASONCODE IN ('85') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A85,
      count(case when B.REASONCODE IN ('86') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A86,
      count(case when B.REASONCODE IN ('87') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A87,
      count(case when B.REASONCODE IN ('88') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A88,
      count(case when B.REASONCODE IN ('89') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A89,
      count(case when B.REASONCODE IN ('70') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A70,
      count(case when B.REASONCODE IN ('71') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A71,
      count(case when B.REASONCODE IN ('72') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A72,
      count(case when B.REASONCODE IN ('73') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A73,
      count(case when B.REASONCODE IN ('75') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A75,
      count(case when B.REASONCODE IN ('76') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A76,
      count(case when B.REASONCODE IN ('77') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A77,
      count(case when B.REASONCODE IN ('51') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A51,
      count(case when B.REASONCODE IN ('52') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A52,
      count(case when B.REASONCODE IN ('90') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A90,
      count(case when B.REASONCODE IN ('93') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A93,
      count(case when B.REASONCODE IN ('97') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A97,
      count(case when B.REASONCODE IN ('98') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A98,
      count(case when B.REASONCODE IN ('99') and
      ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A99");
    }
    $CI->db->from("DBODSXML4OPS.XML4OPS A");
    $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
    $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
    $CI->db->where(divertStatus());
    /* if ($stnService == "dom"){
    $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (".$domstn.")");
    $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (".$domstn.")");
  }
  if ($stnService == "int"){
  $CI->db->where($ruteInt);
} */
$CI->db->where($timeLimit);
//$CI->db->where("B.DELAYSEQ != 3");
$CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
$CI->db->where_in("A.SERVICETYPE", $serviceType);
$CI->db->where_in("B.DELAYSEQ", array(1, 2));
$CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY')");
$CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM')");
$CI->db->order_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') ASC");
$q = $CI->db->get();
$result = $q->result_array();
return $result;
}

function data_delaycode_report_v2($date1 = NULL, $date2 = NULL, $delayContributor = NULL, $sFleet = NULL) {


  $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
  AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
  OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
  AND A.DEPAIRPORT IN (" . intstn() . ")))";
  $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM-DD') between '" . $date1 . "' and '" . $date2 . "'";
  $serviceType = array('J', 'G', 'A');
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $sDiff = '+000000015 00:00:00.000000000';
  $CI->db->select("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL");
  if ($delayContributor == 1) {
    $CI->db->select("count(case when B.REASONCODE IN ('40') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A40,
    count(case when B.REASONCODE IN ('41') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A41,
    count(case when B.REASONCODE IN ('42') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A42,
    count(case when B.REASONCODE IN ('43') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A43,
    count(case when B.REASONCODE IN ('44') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A44,
    count(case when B.REASONCODE IN ('45') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A45,
    count(case when B.REASONCODE IN ('46') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A46,
    count(case when B.REASONCODE IN ('47') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A47,
    count(case when B.REASONCODE IN ('48') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A48");
  }
  if ($delayContributor == 2) {
    $CI->db->select("
    count(case when B.REASONCODE IN ('10') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A10,
    count(case when B.REASONCODE IN ('11') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A11,
    count(case when B.REASONCODE IN ('12') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A12,
    count(case when B.REASONCODE IN ('13') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A13,
    count(case when B.REASONCODE IN ('15') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A15,
    count(case when B.REASONCODE IN ('17') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A17,
    count(case when B.REASONCODE IN ('18') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A18,
    count(case when B.REASONCODE IN ('20') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A20,
    count(case when B.REASONCODE IN ('21') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A21,
    count(case when B.REASONCODE IN ('22') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A22,
    count(case when B.REASONCODE IN ('23') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A23,
    count(case when B.REASONCODE IN ('24') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A24,
    count(case when B.REASONCODE IN ('26') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A26,
    count(case when B.REASONCODE IN ('27') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A27,
    count(case when B.REASONCODE IN ('28') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A28,
    count(case when B.REASONCODE IN ('29') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A29,
    count(case when B.REASONCODE IN ('31') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A31,
    count(case when B.REASONCODE IN ('32') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A32,
    count(case when B.REASONCODE IN ('33') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A33,
    count(case when B.REASONCODE IN ('34') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A34,
    count(case when B.REASONCODE IN ('35') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A35,
    count(case when B.REASONCODE IN ('36') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A36,
    count(case when B.REASONCODE IN ('37') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A37,
    count(case when B.REASONCODE IN ('38') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A38,
    count(case when B.REASONCODE IN ('39') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A39");
  }
  if ($delayContributor == 3) {
    $CI->db->select("count(case when B.REASONCODE IN ('09') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A09,
    count(case when B.REASONCODE IN ('14') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A14,
    count(case when B.REASONCODE IN ('16') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A16,
    count(case when B.REASONCODE IN ('25') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A25,
    count(case when B.REASONCODE IN ('30') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A30,
    count(case when B.REASONCODE IN ('91') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A91,
    count(case when B.REASONCODE IN ('92') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A92");
  }
  if ($delayContributor == 4) {
    $CI->db->select("count(case when B.REASONCODE IN ('50') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A50,
    count(case when B.REASONCODE IN ('55') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A55,
    count(case when B.REASONCODE IN ('56') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A56,
    count(case when B.REASONCODE IN ('57') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A57,
    count(case when B.REASONCODE IN ('58') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A58");
  }
  if ($delayContributor == 5) {
    $CI->db->select("count(case when B.REASONCODE IN ('01') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A01,
    count(case when B.REASONCODE IN ('02') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A02,
    count(case when B.REASONCODE IN ('60') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A60,
    count(case when B.REASONCODE IN ('61') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A61,
    count(case when B.REASONCODE IN ('62') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A62,
    count(case when B.REASONCODE IN ('63') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A63,
    count(case when B.REASONCODE IN ('64') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A64,
    count(case when B.REASONCODE IN ('65') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A65,
    count(case when B.REASONCODE IN ('66') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A66,
    count(case when B.REASONCODE IN ('67') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A67,
    count(case when B.REASONCODE IN ('68') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A68,
    count(case when B.REASONCODE IN ('69') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A69,
    count(case when B.REASONCODE IN ('94') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A94,
    count(case when B.REASONCODE IN ('95') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A95,
    count(case when B.REASONCODE IN ('96') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A96");
  }
  if ($delayContributor == 6) {
    $CI->db->select("count(case when B.REASONCODE IN ('80') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A80,
    count(case when B.REASONCODE IN ('81') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A81,
    count(case when B.REASONCODE IN ('82') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A82,
    count(case when B.REASONCODE IN ('83') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A83,
    count(case when B.REASONCODE IN ('84') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A84,
    count(case when B.REASONCODE IN ('85') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A85,
    count(case when B.REASONCODE IN ('86') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A86,
    count(case when B.REASONCODE IN ('87') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A87,
    count(case when B.REASONCODE IN ('88') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A88,
    count(case when B.REASONCODE IN ('89') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A89");
  }
  if ($delayContributor == 7) {
    $CI->db->select("count(case when B.REASONCODE IN ('70') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A70,
    count(case when B.REASONCODE IN ('71') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A71,
    count(case when B.REASONCODE IN ('72') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A72,
    count(case when B.REASONCODE IN ('73') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A73,
    count(case when B.REASONCODE IN ('75') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A75,
    count(case when B.REASONCODE IN ('76') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A76,
    count(case when B.REASONCODE IN ('77') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A77");
  }
  if ($delayContributor == 8) {
    $CI->db->select("count(case when B.REASONCODE IN ('51') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A51,
    count(case when B.REASONCODE IN ('52') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A52,
    count(case when B.REASONCODE IN ('90') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A90,
    count(case when B.REASONCODE IN ('93') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A93,
    count(case when B.REASONCODE IN ('97') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A97,
    count(case when B.REASONCODE IN ('98') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A98,
    count(case when B.REASONCODE IN ('99') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A99");
  }
  if ($delayContributor == "all") {
    $CI->db->select("count(case when B.REASONCODE IN ('40') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A40,
    count(case when B.REASONCODE IN ('41') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A41,
    count(case when B.REASONCODE IN ('42') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A42,
    count(case when B.REASONCODE IN ('43') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A43,
    count(case when B.REASONCODE IN ('44') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A44,
    count(case when B.REASONCODE IN ('45') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A45,
    count(case when B.REASONCODE IN ('46') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A46,
    count(case when B.REASONCODE IN ('47') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A47,
    count(case when B.REASONCODE IN ('48') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A48,
    count(case when B.REASONCODE IN ('10') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A10,
    count(case when B.REASONCODE IN ('11') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A11,
    count(case when B.REASONCODE IN ('12') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A12,
    count(case when B.REASONCODE IN ('13') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A13,
    count(case when B.REASONCODE IN ('15') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A15,
    count(case when B.REASONCODE IN ('17') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A17,
    count(case when B.REASONCODE IN ('18') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A18,
    count(case when B.REASONCODE IN ('20') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A20,
    count(case when B.REASONCODE IN ('21') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A21,
    count(case when B.REASONCODE IN ('22') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A22,
    count(case when B.REASONCODE IN ('23') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A23,
    count(case when B.REASONCODE IN ('24') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A24,
    count(case when B.REASONCODE IN ('26') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A26,
    count(case when B.REASONCODE IN ('27') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A27,
    count(case when B.REASONCODE IN ('28') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A28,
    count(case when B.REASONCODE IN ('29') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A29,
    count(case when B.REASONCODE IN ('31') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A31,
    count(case when B.REASONCODE IN ('32') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A32,
    count(case when B.REASONCODE IN ('33') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A33,
    count(case when B.REASONCODE IN ('34') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A34,
    count(case when B.REASONCODE IN ('35') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A35,
    count(case when B.REASONCODE IN ('36') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A36,
    count(case when B.REASONCODE IN ('37') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A37,
    count(case when B.REASONCODE IN ('38') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A38,
    count(case when B.REASONCODE IN ('39') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A39,
    count(case when B.REASONCODE IN ('09') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A09,
    count(case when B.REASONCODE IN ('14') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A14,
    count(case when B.REASONCODE IN ('16') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A16,
    count(case when B.REASONCODE IN ('25') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A25,
    count(case when B.REASONCODE IN ('30') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A30,
    count(case when B.REASONCODE IN ('91') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A91,
    count(case when B.REASONCODE IN ('92') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A92,
    count(case when B.REASONCODE IN ('50') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A50,
    count(case when B.REASONCODE IN ('55') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A55,
    count(case when B.REASONCODE IN ('56') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A56,
    count(case when B.REASONCODE IN ('57') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A57,
    count(case when B.REASONCODE IN ('58') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A58,
    count(case when B.REASONCODE IN ('01') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A01,
    count(case when B.REASONCODE IN ('02') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A02,
    count(case when B.REASONCODE IN ('60') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A60,
    count(case when B.REASONCODE IN ('61') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A61,
    count(case when B.REASONCODE IN ('62') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A62,
    count(case when B.REASONCODE IN ('63') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A63,
    count(case when B.REASONCODE IN ('64') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A64,
    count(case when B.REASONCODE IN ('65') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A65,
    count(case when B.REASONCODE IN ('66') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A66,
    count(case when B.REASONCODE IN ('67') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A67,
    count(case when B.REASONCODE IN ('68') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A68,
    count(case when B.REASONCODE IN ('69') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A69,
    count(case when B.REASONCODE IN ('94') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A94,
    count(case when B.REASONCODE IN ('95') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A95,
    count(case when B.REASONCODE IN ('96') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A96,
    count(case when B.REASONCODE IN ('80') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A80,
    count(case when B.REASONCODE IN ('81') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A81,
    count(case when B.REASONCODE IN ('82') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A82,
    count(case when B.REASONCODE IN ('83') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A83,
    count(case when B.REASONCODE IN ('84') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A84,
    count(case when B.REASONCODE IN ('85') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A85,
    count(case when B.REASONCODE IN ('86') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A86,
    count(case when B.REASONCODE IN ('87') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A87,
    count(case when B.REASONCODE IN ('88') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A88,
    count(case when B.REASONCODE IN ('89') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A89,
    count(case when B.REASONCODE IN ('70') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A70,
    count(case when B.REASONCODE IN ('71') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A71,
    count(case when B.REASONCODE IN ('72') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A72,
    count(case when B.REASONCODE IN ('73') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A73,
    count(case when B.REASONCODE IN ('75') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A75,
    count(case when B.REASONCODE IN ('76') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A76,
    count(case when B.REASONCODE IN ('77') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A77,
    count(case when B.REASONCODE IN ('51') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A51,
    count(case when B.REASONCODE IN ('52') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A52,
    count(case when B.REASONCODE IN ('90') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A90,
    count(case when B.REASONCODE IN ('93') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A93,
    count(case when B.REASONCODE IN ('97') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A97,
    count(case when B.REASONCODE IN ('98') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A98,
    count(case when B.REASONCODE IN ('99') and
    ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'  then 1 else NULL END) as A99");
  }
  $CI->db->from("DBODSXML4OPS.XML4OPS A");
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
  $CI->db->where("A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  /* if ($stnService == "dom"){
  $CI->db->where("A.SCHED_DEPARTUREAIRPORT IN (".$domstn.")");
  $CI->db->where("A.SCHED_ARRIVALAIRPORT IN (".$domstn.")");
}
if ($stnService == "int"){
$CI->db->where($ruteInt);
} */
$CI->db->where($timeLimit);
$CI->db->where_in("AIRCRAFTTYPE",$sFleet);
//$CI->db->where("B.DELAYSEQ != 3");
$CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
$CI->db->where_in("A.SERVICETYPE", $serviceType);
$CI->db->where_in("B.DELAYSEQ", array(1, 2));
$CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY')");
$CI->db->group_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM')");
$CI->db->order_by("to_char(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') ASC");
$q = $CI->db->get();
$result = $q->result_array();
return $result;
}

function list_fleet_MM() {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("DISTINCT(AIRCRAFTTYPE)");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->order_by('AIRCRAFTTYPE');
  $q = $CI->db->get();
  $CI->db->close();
  $result = $q->result_array();

  if ($q->num_rows() > 0) return $result;
  else return FALSE;
}

function get_ndelay($date1 = NULL, $date2 = NULL, $type_cod = NULL) {
  $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
  $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
  $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
  $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
  $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
  $arrSYST = array('50', '55', '56', '57', '58');
  $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
  $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');
  $sDiff = '+000000015 00:00:00.000000000';
  $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'YYYY-MM-DD') between '" . $date1 . "' and '" . $date2 . "'";
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $serviceType = array('J', 'G', 'A');

  $CI->db->select("count(XML4OPS_DELAY.REASONCODE) as NDELAY");
  $CI->db->from("DBODSXML4OPS.XML4OPS");
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY', 'DBODSXML4OPS.XML4OPS.FLIGHTLEGREF = DBODSXML4OPS.XML4OPS_DELAY.FLIGHTLEGREF');
  //$CI->db->where($timeLimit);

  $CI->db->where("ACTUAL_BLOCKOFF is NOT NULL");
  $CI->db->where($timeLimit);
  $CI->db->where("((XML4OPS.ACTUAL_BLOCKOFF + interval '7' hour) - (XML4OPS.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000'");
  $CI->db->where_in("XML4OPS.SERVICETYPE", $serviceType);
  $CI->db->where_in("XML4OPS_DELAY.DELAYSEQ", array(1, 2));
  $CI->db->where("XML4OPS.LATEST_ARRIVALAIRPORT != XML4OPS.SCHED_DEPARTUREAIRPORT");
  $CI->db->where(divertStatus());
  if ($type_cod == 1) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrTECH);
  } else if ($type_cod == 2) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSTNH);
  } else if ($type_cod == 3) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrCOMC);
  } else if ($type_cod == 4) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrSYST);
  } else if ($type_cod == 5) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrFLOP);
  } else if ($type_cod == 6) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrAPTF);
  } else if ($type_cod == 7) {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrWTHR);
  } else {
    $CI->db->where_in("XML4OPS_DELAY.REASONCODE", $arrOTHR);
  }

  return $CI->db->get()->row()->NDELAY;
}

function getotpspecialroute($sTime = NULL, $eTime = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $complexQuery = "select ORG, DEST, BULAN,sum(FIRSTFLIGHT) AS FIRSTFLIGHT, SUM(OTPFIRSTFLIGHT) as OTPFIRSTFLIGHT, SUM(ZMDFIRSTFLIGHT) as ZMDFIRSTFLIGHT,
  SUM(SCHEDULED) AS SCHEDULED,SUM(ONTIME) AS ONTIME,SUM(ARRONTIME) AS ARRONTIME, SUM(SCHEDARR) AS SCHEDARR
  from(SELECT A.DEPAIRPORT as ORG, A.SCHED_ARRIVALAIRPORT as DEST, to_char(SCHEDULED_DEPDT + interval '7' hour,'MON-YYYY') as BULAN,
  to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM') as BULAN2,
  case when A.STATUS = 'Scheduled' and
  A.ACTUAL_BLOCKOFF is not null  then 1 else NULL END as SCHEDULED,
  case when ((A.ACTUAL_BLOCKOFF+ interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKOFF is not null  then 1 else NULL END as ONTIME,
  case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000' and
  A.ACTUAL_BLOCKON is not null and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT  then 1 else NULL END as ARRONTIME,
  case when  A.STATUS = 'Scheduled' and A.ACTUAL_BLOCKON is not null then 1 else NULL END as SCHEDARR,
  TO_DATE(TO_CHAR(SCHEDULED_DEPDT + interval '7' hour, 'DD-MON-YY')) AS TANGGAL,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY'))) THEN 1 ELSE NULL END AS FIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000015 00:00:00.000000000'
  THEN 1 ELSE NULL END AS OTPFIRSTFLIGHT,
  CASE WHEN TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI') = MIN(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MM-YY HH24:MI'))
  OVER (PARTITION BY AIRCRAFTREG, TO_DATE(TO_CHAR(SCHEDULED_DEPDT+ interval '7' hour, 'DD-MON-YY')))
  and ((ACTUAL_BLOCKOFF + interval '7' hour) - (SCHEDULED_DEPDT + interval '7' hour))*24*60 <= '+000000000 00:00:00.000000000'
  THEN 1 ELSE NULL END AS ZMDFIRSTFLIGHT
  FROM DBODSXML4OPS.XML4OPS A where A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT and (SUFFIX != 'R' or SUFFIX IS NULL)
  and ((A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'SIN' OR A.DEPAIRPORT = 'SIN' and A.SCHED_ARRIVALAIRPORT = 'CGK')
  OR (A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'KNO' OR A.DEPAIRPORT = 'KNO' and A.SCHED_ARRIVALAIRPORT = 'CGK')
  OR (A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'YIA' OR A.DEPAIRPORT = 'YIA' and A.SCHED_ARRIVALAIRPORT = 'CGK')
  OR (A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'SUB' OR A.DEPAIRPORT = 'SUB' and A.SCHED_ARRIVALAIRPORT = 'CGK')
  OR (A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'DPS' OR A.DEPAIRPORT = 'DPS' and A.SCHED_ARRIVALAIRPORT = 'CGK')
  OR (A.DEPAIRPORT = 'CGK' and A.SCHED_ARRIVALAIRPORT = 'UPG' OR A.DEPAIRPORT = 'UPG' and A.SCHED_ARRIVALAIRPORT = 'CGK'))
  and ACTUAL_BLOCKOFF IS NOT NULL AND A.SERVICETYPE IN ('J','G', 'A') and SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS'))
  group by BULAN, BULAN2, ORG, DEST order by BULAN2 ASC";

  $q = $CI->db->query($complexQuery);
  $result = $q->result_array();

  return $result;
}

function getcurrentinfodelay($sTime = NULL, $eTime = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);

  $complexQuery = "select A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
  SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT,LATEST_DEPARTUREAIRPORT,LATEST_ARRIVALAIRPORT,SUFFIX,SERVICETYPE,STATUS,
  TO_CHAR(SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY HH24:MI') AS SCHEDULED_DEPDT_LC,
  TO_CHAR(SCHEDULED_ARRDT + interval '7' hour,'DD-MON-YYYY HH24:MI') as SCHEDULED_ARRDT_LC,
  TO_CHAR(ACTUAL_BLOCKOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC,
  TO_CHAR(ACTUAL_BLOCKON + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKON_LC,
  TO_CHAR(A.ACTUAL_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TAKEOFF_LC,
  TO_CHAR(A.ACTUAL_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TOUCHDOWN_LC,
  TO_CHAR(A.ESTIMATED_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TAKEOFF_LC,
  TO_CHAR(A.ESTIMATED_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TOUCHDOWN_LC,
  MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
  MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
  MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
  MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
  A.REMARKS from DBODSXML4OPS.XML4OPS A left join DBODSXML4OPS.XML4OPS_DELAY B on A.FLIGHTLEGREF = B.FLIGHTLEGREF
  where A.SERVICETYPE IN ('J', 'G', 'A') and ACTUAL_BLOCKOFF IS NOT NULL and
  A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT and (SUFFIX != 'R' or SUFFIX IS NULL)
  and ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 > '+000000015 00:00:00.000000000'
  and SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')
  group by A.FLIGHTLEGREF, A.SCHEDULED_DEPDT
  ,A.FLTNUM
  ,A.STATUS
  ,A.AIRCRAFTREG
  ,A.AIRCRAFTTYPE
  ,A.SCHED_DEPARTUREAIRPORT
  ,A.SCHED_ARRIVALAIRPORT
  ,A.ACTUAL_BLOCKON
  ,A.ACTUAL_BLOCKOFF
  ,A.SCHEDULED_ARRDT
  ,A.ACTUAL_TAKEOFF
  ,A.ACTUAL_TOUCHDOWN
  ,A.ESTIMATED_TAKEOFF
  ,A.ESTIMATED_TOUCHDOWN
  ,A.LATEST_DEPARTUREAIRPORT
  ,A.LATEST_ARRIVALAIRPORT
  ,A.REMARKS
  ,A.SUFFIX
  ,A.SERVICETYPE
  ,to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY')
  ,to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY')
  ,to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')
  order by to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY'),
  to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY'),A.AIRCRAFTREG,ACTUAL_BLOCKOFF_LC ASC";

  $q = $CI->db->query($complexQuery);
  $result = $q->result_array();

  return $result;
}

function get_averagedelay($option, $sTime = NULL, $eTime = NULL, $stn = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);

  if ($option == 1) {
    $complexQuery = "select to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd'), sum(case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKOFF is not null then
    EXTRACT(minute from (select((ACTUAL_BLOCKOFF + interval '7' hour) -(SCHEDULED_DEPDT+ interval '7' hour)) from dual))+
    EXTRACT(hour from (select((ACTUAL_BLOCKOFF+ interval '7' hour)-(SCHEDULED_DEPDT+ interval '7' hour)) from dual))*60 else 0 END)
    / NULLIF(sum(case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKOFF is not null then
    1 else 0 END),0) as AVGDELAY,
    sum(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKON is not null then
    EXTRACT(minute from (select((ACTUAL_BLOCKON + interval '7' hour) -(SCHEDULED_ARRDT+ interval '7' hour)) from dual))+
    EXTRACT(hour from (select((ACTUAL_BLOCKON+ interval '7' hour)-(SCHEDULED_ARRDT+ interval '7' hour)) from dual))*60 ELSE 0 END)
    / NULLIF(sum(case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT+ interval '7' hour))*24*60 > '+000000015 00:00:00.000000000' and A.ACTUAL_BLOCKON is not null then
    1 else 0 END),0) as AVGDELAYARR
    from DBODSXML4OPS.XML4OPS A
    where A.SERVICETYPE IN ('J', 'G', 'A') and ACTUAL_BLOCKOFF IS NOT NULL and
    A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT and (SUFFIX != 'R' or SUFFIX IS NULL)
    and SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')
    " . ($stn != NULL ? " and DEPAIRPORT = '" . $stn . "'" : "") . " group by to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd')";
    $q = $CI->db->query($complexQuery);
    if ($q->num_rows() > 0)
    return $q->row()->AVGDELAY;
    else
    return 0;
  }
  else {
    $complexQuery = "SELECT count(*) as NDELAY
    from DBODSXML4OPS.XML4OPS A
    where A.SERVICETYPE IN ('J', 'G', 'A') and ACTUAL_BLOCKOFF IS NOT NULL and
    A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT and (SUFFIX != 'R' or SUFFIX IS NULL)
    and ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 >= '+000000240 00:00:00.000000000'
    and SCHEDULED_DEPDT+ interval '7' hour BETWEEN to_date('" . $sTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS') and to_date('" . $eTime . "', 'YYYY-MM-DD\"T\"HH24:MI:SS')
    " . ($stn != NULL ? " and DEPAIRPORT = '" . $stn . "'" : "");
    $q = $CI->db->query($complexQuery);
    return $q->row()->NDELAY;
  }
}

function get_cis_crew($stn = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);
  $complexQuery = "select * from dbcis.tb_cis where cis_station = '" . $stn . "'";
  $q = $CI->db->query($complexQuery);
  $result = $q->result_array();

  return $result;
}

function get_cis_admin($stn = NULL) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);

  $complexQuery = "select * from dbcis.tb_cis where cis_station = '" . $stn . "'";
  $q = $CI->db->query($complexQuery);
  $result = $q->result_array();

  return $result;
}

function insert_cis_admin($stn, $hotel_calling, $hotel_pickup, $gm_name, $gm_phone1, $gm_phone2, $kk_name, $kk_phone1, $kk_phone2, $ko_name, $ko_phone1, $ko_phone2, $gmf_name, $gmf_phone1, $gmf_phone2, $ks_name, $ks_phone1, $ks_phone2, $hotel_name, $hotel_phone1, $hotel_phone2, $hotel_address, $transport_name, $transport_phone1, $transport_phone2, $transport_address, $hospital_name, $hospital_phone1, $hospital_phone2, $hospital_address, $noted) {
  $CI = & get_instance();
  $CI->db = $CI->load->database('mysql', TRUE);



  $countQuery = "select count(*) as JUMLAH from dbcis.tb_cis where cis_station = '" . $stn . "'";

  $q1 = $CI->db->query($countQuery);
  $cekCis = $q1->row()->JUMLAH;
  //alert($cekCis);

  if ($cekCis <= 0) {
    $query = "insert into dbcis.tb_cis (cis_hotel_calling,
    cis_hotel_pickup,
    cis_station,
    cis_gm_name,
    cis_gm_phone1,
    cis_gm_phone2,
    cis_kk_name,
    cis_kk_phone1,
    cis_kk_phone2,
    cis_ko_name,
    cis_ko_phone1,
    cis_ko_phone2,
    cis_mm_name,
    cis_mm_phone1,
    cis_mm_phone2,
    cis_ks_name,
    cis_ks_phone1,
    cis_ks_phone2,
    cis_hotel_name,
    cis_hotel_phone1,
    cis_hotel_phone2,
    cis_hotel_address,
    cis_transport_name,
    cis_transport_phone1,
    cis_transport_phone2,
    cis_transport_address,
    cis_hospital_name,
    cis_hospital_phone1,
    cis_hospital_phone2,
    cis_hospital_address,
    cis_noted) values(" . $hotel_calling . "," . $hotel_pickup . ",'" . $stn . "','" . $gm_name . "',	'" . $gm_phone1 . "',	'" . $gm_phone2 . "',	'" . $kk_name . "',	'" . $kk_phone1 . "',	'" . $kk_phone2 . "',	'" . $ko_name . "',	'" . $ko_phone1 . "',	'" . $ko_phone2 . "',	'" . $gmf_name . "',	'" . $gmf_phone1 . "',	'" . $gmf_phone2 . "',	'" . $ks_name . "',	'" . $ks_phone1 . "',	'" . $ks_phone2 . "',	'" . $hotel_name . "',	'" . $hotel_phone1 . "',	'" . $hotel_phone2 . "',	'" . $hotel_address . "',	'" . $transport_name . "',	'" . $transport_phone1 . "',	'" . $transport_phone2 . "','" . $transport_address . "','" . $hospital_name . "',	'" . $hospital_phone1 . "',	'" . $hospital_phone2 . "',	'" . $hospital_address . "',	'" . $noted . "')";
  } else {
    $query = "update dbcis.tb_cis set cis_hotel_calling=" . $hotel_calling . ",	cis_hotel_pickup=" . $hotel_pickup . ",	cis_gm_name='" . $gm_name . "',"
    . "cis_gm_phone1='" . $gm_phone1 . "',	cis_gm_phone2='" . $gm_phone2 . "',	cis_kk_name='" . $kk_name . "',	cis_kk_phone1='" . $kk_phone1 . "',	"
    . "cis_kk_phone2='" . $kk_phone2 . "',	cis_ko_name='" . $ko_name . "',	cis_ko_phone1='" . $ko_phone1 . "',	cis_ko_phone2='" . $ko_phone2 . "',	"
    . "cis_mm_name='" . $gmf_name . "',	cis_mm_phone1='" . $gmf_phone1 . "',	cis_mm_phone2='" . $gmf_phone2 . "',	cis_ks_name='" . $ks_name . "',	"
    . "cis_ks_phone1='" . $ks_phone1 . "',	cis_ks_phone2='" . $ks_phone2 . "',	cis_hotel_name='" . $hotel_name . "',	cis_hotel_phone1='" . $hotel_phone1 . "',	"
    . "cis_hotel_phone2='" . $hotel_phone2 . "',	cis_hotel_address='" . $hotel_address . "',	cis_transport_name='" . $transport_name . "',	"
    . "cis_transport_phone1='" . $transport_phone1 . "',	cis_transport_phone2='" . $transport_phone2 . "',	cis_transport_address='" . $transport_address . "',	"
    . "cis_hospital_name='" . $hospital_name . "',	cis_hospital_phone1='" . $hospital_phone1 . "',	cis_hospital_phone2='" . $hospital_phone2 . "',	"
    . "cis_hospital_address='" . $hospital_address . "',	cis_noted='" . $noted . "' where cis_station = '" . $stn . "'";
  }

  $q2 = $CI->db->query($query);
  //$result = $q2->result_array();

  //return $result;
}

function gamovementap($sDate, $date1 = NULL, $date2 = NULL, $stnOrg = NULL, $stnDest = NULL, $flighttype = NULL) {

  $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
  $CI = & get_instance();
  $CI->db = $CI->load->database('default', TRUE);
  $CI->db->select("A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY') as TANGGAL, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
  SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT,LATEST_DEPARTUREAIRPORT,LATEST_ARRIVALAIRPORT,SUFFIX,SERVICETYPE,STATUS,
  TO_CHAR(SCHEDULED_DEPDT + interval '7' hour,'DD-MON-YYYY HH24:MI') AS SCHEDULED_DEPDT_LC,
  TO_CHAR(SCHEDULED_ARRDT + interval '7' hour,'DD-MON-YYYY HH24:MI') as SCHEDULED_ARRDT_LC,
  TO_CHAR(ACTUAL_BLOCKOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKOFF_LC,
  TO_CHAR(ACTUAL_BLOCKON + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKON_LC,
  TO_CHAR(A.ACTUAL_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TAKEOFF_LC,
  TO_CHAR(A.ACTUAL_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ACTUAL_TOUCHDOWN_LC,
  TO_CHAR(A.ESTIMATED_TAKEOFF + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TAKEOFF_LC,
  TO_CHAR(A.ESTIMATED_TOUCHDOWN + interval '7' hour,'DD-MON-YYYY HH24:MI') as ESTIMATED_TOUCHDOWN_LC,
  case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPDEP,
  case when ((A.ACTUAL_BLOCKON + interval '7' hour) - (A.SCHEDULED_ARRDT + interval '7' hour)) * 24*60 <= '+000000015 00:00:00.000000000'  and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END as OTPARR,
  case when ((A.ACTUAL_BLOCKOFF + interval '7' hour) - (A.SCHEDULED_DEPDT + interval '7' hour)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END as OTPZMD,
  MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
  MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
  MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
  MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
  case when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'DOM'
  when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
  when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'INT'
  when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
  else NULL END as ROUTE,
  A.REMARKS");
  $CI->db->from("DBODSXML4OPS.XML4OPS A");
  $CI->db->join('DBODSXML4OPS.XML4OPS_DELAY B', 'A.FLIGHTLEGREF = B.FLIGHTLEGREF', 'left');
  //if ($flighttype == "reg") {
    $CI->db->where_in("A.SERVICETYPE", array('J', 'G', 'A'));
    //} else if ($flighttype == "hajj") {
      //$CI->db->where_in("A.SERVICETYPE", array('O'));
      //}
      $CI->db->where("STATUS IN ('Scheduled')");
      //$CI->db->where_in("A.SERVICETYPE", $serviceType);
      $CI->db->where("ACTUAL_BLOCKOFF IS NOT NULL");
      if (!is_null($date1) && !is_null($date2))
      $CI->db->where("to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '" . $date1 . "' and '" . $date2 . "'");
      else
      $CI->db->where($timeLimit);
      if (!is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }
      if (!is_null($stnOrg) && is_null($stnDest)) {
        $CI->db->where("A.SCHED_DEPARTUREAIRPORT", $stnOrg);
      }
      if (is_null($stnOrg) && !is_null($stnDest)) {
        $CI->db->where("A.SCHED_ARRIVALAIRPORT", $stnDest);
      }

      $CI->db->group_by("A.FLIGHTLEGREF");
      $CI->db->group_by("A.SCHEDULED_DEPDT");
      //$CI->db->group_by("A.DEPARTURE_INDO");
      //$CI->db->group_by("A.ARRIVAL_INDO");
      $CI->db->group_by("A.FLTNUM");
      $CI->db->group_by("A.STATUS");
      $CI->db->group_by("A.AIRCRAFTREG");
      $CI->db->group_by("A.AIRCRAFTTYPE");
      $CI->db->group_by("A.SCHED_DEPARTUREAIRPORT");
      $CI->db->group_by("A.SCHED_ARRIVALAIRPORT");
      $CI->db->group_by("A.ACTUAL_BLOCKON");
      $CI->db->group_by("A.ACTUAL_BLOCKOFF");
      $CI->db->group_by("A.SCHEDULED_ARRDT");
      $CI->db->group_by("A.ACTUAL_TAKEOFF");
      $CI->db->group_by("A.ACTUAL_TOUCHDOWN");
      $CI->db->group_by("A.ESTIMATED_TAKEOFF");
      $CI->db->group_by("A.ESTIMATED_TOUCHDOWN");
      $CI->db->group_by("A.LATEST_DEPARTUREAIRPORT");
      $CI->db->group_by("A.LATEST_ARRIVALAIRPORT");
      $CI->db->group_by("A.REMARKS");
      $CI->db->group_by("A.SUFFIX");
      $CI->db->group_by("A.SERVICETYPE");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY')");
      $CI->db->group_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY')");
      $CI->db->order_by("to_char(SCHEDULED_DEPDT + interval '7' hour,'YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'MM-YYYY'),to_char(SCHEDULED_DEPDT + interval '7' hour,'DD-MM-YYYY') ASC");
      $CI->db->order_by("A.AIRCRAFTREG ASC");
      $CI->db->order_by("ACTUAL_BLOCKOFF_LC ASC");
      $q = $CI->db->get();
      $result = $q->result_array();

      return $result;
    }

    function daily_dephub_report_new($sDate, $date1 = NULL, $date2 = NULL, $stnService = NULL) {

      $ruteInt = "((A.DEPAIRPORT IN (" . domstn() . ")
      AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
      OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
      AND A.DEPAIRPORT IN (" . intstn() . ")))";
      $timeLimit = "to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') = '" . $sDate . "'";
      $serviceType = array('J', 'G', 'A');

      $CI = & get_instance();
      $CI->db = $CI->load->database('default', TRUE);
      $query = "SELECT A.FLIGHTLEGREF, TO_CHAR(A.SCHEDULED_DEPDT + interval '7' hour,'YYYY-MM-DD') as TANGGAL, FLTNUM, AIRCRAFTREG,AIRCRAFTTYPE,
      SCHED_DEPARTUREAIRPORT,SCHED_ARRIVALAIRPORT,LATEST_DEPARTUREAIRPORT,LATEST_ARRIVALAIRPORT,SUFFIX,SERVICETYPE,STATUS,CANCELREASONCODE,
      TO_CHAR(SCHEDULED_DEPDT,'DD-MON-YYYY HH24:MI') AS SCHEDULED_DEPDT_UTC,
      TO_CHAR(SCHEDULED_ARRDT,'DD-MON-YYYY HH24:MI') as SCHEDULED_ARRDT_UTC,
      TO_CHAR(ACTUAL_BLOCKOFF,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKOFF_UTC,
      TO_CHAR(ACTUAL_BLOCKON,'DD-MON-YYYY HH24:MI') as ACTUAL_BLOCKON_UTC,
      case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000015 00:00:00.000000000' then 1 else NULL END as OTPDEP,
      case when ((A.ACTUAL_BLOCKON) - (A.SCHEDULED_ARRDT)) * 24*60 <= '+000000015 00:00:00.000000000'  and LATEST_ARRIVALAIRPORT = SCHED_ARRIVALAIRPORT then 1 else NULL END as OTPARR,
      case when ((A.ACTUAL_BLOCKOFF) - (A.SCHEDULED_DEPDT)) * 24*60 <= '+000000000 00:00:00.000000000' then 1 else NULL END as OTPZMD,
      MAX(case when B.DELAYSEQ = 1 then B.REASONCODE else NULL END) as cd1,
      MAX(case when B.DELAYSEQ = 1 then B.DURATION else NULL END) as delaylength1,
      MAX(case when B.DELAYSEQ = 2 then B.REASONCODE else NULL END) as cd2,
      MAX(case when B.DELAYSEQ = 2 then B.DURATION else NULL END) as delaylength2,
      case when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'DOM'
      when A.SCHED_DEPARTUREAIRPORT IN (" . domstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") then 'INT'
      when A.SCHED_DEPARTUREAIRPORT IN (" . intstn() . ") AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ") then 'INT'
      else NULL END as ROUTE,
      A.REMARKS,
      PAXA.ACT_PAX as ACT_PAX,
      CONFIG.CAP as SEATCAPACITY
      from DBODSXML4OPS.XML4OPS A
      LEFT JOIN DBODSXML4OPS.XML4OPS_DELAY B ON A.FLIGHTLEGREF = B.FLIGHTLEGREF
      LEFT JOIN (select C.FLIGHTLEGREF, sum(C.count) as ACT_PAX from DBODSXML4OPS.XML4OPS_PAXCOUNT C WHERE C.".'"MODE"'." = 'ACTUAL' group by C.FLIGHTLEGREF ) PAXA ON A.FLIGHTLEGREF = PAXA.FLIGHTLEGREF
      LEFT JOIN (select D.FLIGHTLEGREF, sum(D.COUNT) as CAP from DBODSXML4OPS.XML4OPS_AIRCRAFTCONF D group by D.FLIGHTLEGREF) CONFIG ON A.FLIGHTLEGREF = CONFIG.FLIGHTLEGREF
      where ".route($stnService)."
      to_char(SCHEDULED_DEPDT + interval '7' hour, 'yyyy-mm-dd') between '".$date1."' AND '".$date2."'
      AND (SUFFIX != 'R' or SUFFIX IS NULL)
      AND A.LATEST_ARRIVALAIRPORT != A.SCHED_DEPARTUREAIRPORT
      AND ((A.STATUS = 'Scheduled' AND  (A.SERVICETYPE = 'G' OR A.SERVICETYPE = 'J'  OR A.SERVICETYPE = 'A')) OR (A.STATUS = 'Cancelled' AND  A.SERVICETYPE = 'J'))

      group by A.FLIGHTLEGREF,A.SCHEDULED_DEPDT,
      A.DEPARTURE_INDO,A.ARRIVAL_INDO,A.FLTNUM,A.STATUS,A.AIRCRAFTREG,A.AIRCRAFTTYPE
      ,A.SCHED_DEPARTUREAIRPORT
      ,A.SCHED_ARRIVALAIRPORT
      ,A.ACTUAL_BLOCKON
      ,A.ACTUAL_BLOCKOFF
      ,A.SCHEDULED_ARRDT
      ,A.ACTUAL_TAKEOFF
      ,A.ACTUAL_TOUCHDOWN
      ,A.ESTIMATED_TAKEOFF
      ,A.ESTIMATED_TOUCHDOWN
      ,A.LATEST_DEPARTUREAIRPORT
      ,A.LATEST_ARRIVALAIRPORT
      ,A.REMARKS
      ,A.SUFFIX
      ,A.SERVICETYPE
      ,A.CANCELREASONCODE
      ,PAXA.ACT_PAX, CONFIG.CAP
      order by SCHEDULED_DEPDT, A.AIRCRAFTREG, ACTUAL_BLOCKOFF_UTC ASC";
      $q = $CI->db->query($query);
      $result = $q->result_array();
      $CI->db->close();
      return $result;
    }

    function route($stnService = "all") {
      $domstn = "'AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
      ,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
      ,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
      ,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR','SRI','KJT','AAP','YIA','HLP'";
      $intstn = "'PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO','NGO'";

      if ($stnService == "int") {
        $result = "((A.DEPAIRPORT IN (" . domstn() . ")
        AND A.SCHED_ARRIVALAIRPORT IN (" . intstn() . "))
        OR (A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ")
        AND A.DEPAIRPORT IN (" . intstn() . ")) OR (A.SCHED_ARRIVALAIRPORT IN (" . intstn() . ")
        AND A.DEPAIRPORT IN (" . intstn() . "))) AND";
      }
      else if ($stnService == "dom") {
        $result = "A.DEPAIRPORT IN (" . domstn() . ") and A.SCHED_ARRIVALAIRPORT IN (" . domstn() . ") AND";
      }
      else {
        $result = "";
      }
      return $result;
    }





    /*FUNCTION DAILY FLIGHT OPERATIONAL END*/
