<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

function get_stations() {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);
    $CI->db->select('DISTINCT(dep_station) as STN');
    $CI->db->where('dep_station is not null');
    $CI->db->where('CHAR_LENGTH(dep_station) >= 3');
    $CI->db->from('exbaggage');
    $CI->db->order_by('STN');

    $q = $CI->db->get();
    $CI->db->close();
    $result = $q->result_array();
    return $result;
}

function listExcess($sDay, $depStn, $type) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);
    if ($type == 'WD') $rec = 'sum(IFNULL(excess_weight_wd,0)) as wd';
    else if ($type == 'PD') $rec = 'sum(IFNULL(excess_weight_pd,0)) as pd';
    $CI->db->select($rec);
    $CI->db->from('exbaggage');
    if ($depStn <> 'all') $CI->db->where('dep_station', $depStn);
    $CI->db->where('is_trt is null');
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y")', $sDay);    

    $q = $CI->db->get();
    $CI->db->close();
    if ($type == 'WD') $result = $q->row()->wd;
    else if ($type == 'PD') $result = $q->row()->pd;
    
    return ($result == null) ? 0 : $result;
}

function getExcBagRec($sDate, $eDate, $depStn, $status) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);
    if ($status == 'WD') $count_bag = 'sum(IFNULL(excess_weight_wd,0))';
    else if ($status == 'PD') $count_bag = 'sum(IFNULL(excess_weight_pd,0))';
    else if ($status == 'ALL') $count_bag = 'sum(IFNULL(excess_weight_wd,0))+sum(IFNULL(excess_weight_pd,0))';
    $CI->db->select('dep_station, count(excess_status_wd) as total_wd, count(excess_status_pd) as total_pd, sum(IFNULL(excess_weight_wd,0)) as total_bag_w, sum(IFNULL(excess_weight_pd,0)) as total_bag_p');
    $CI->db->from('exbaggage');
    if ($depStn <> 'all') $CI->db->where('dep_station', $depStn);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") >=', $sDate);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") <=', $eDate);
    $CI->db->where('is_trt is null');
    $CI->db->where('dep_station is not null');
    $CI->db->where('CHAR_LENGTH(dep_station) >= 3');
    if ($status == 'WD') $CI->db->where('excess_status_wd', 'WD');
    if ($status == 'PD') $CI->db->where('excess_status_pd', 'PD');
    $CI->db->group_by('dep_station'); 
    $q = $CI->db->get();    
    $CI->db->close();

    return ($q->num_rows() > 0) ? $q->result_array() : false;        
}

function getAllRec($sDate, $eDate, $depStn, $arrStn, $status) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);
    if ($status == 'WD') $count_bag = 'IFNULL(excess_weight_wd,0)';
    else if ($status == 'PD') $count_bag = 'IFNULL(excess_weight_pd,0)';
    else if ($status == 'ALL') $count_bag = '(IFNULL(excess_weight_wd,0) + IFNULL(excess_weight_pd,0))';
    $CI->db->select('flight_date, flight_number, dep_station, arr_station, pass_name, pass_type, ticket_no, cabin_class, sub_class, seat_no, '.$count_bag.' as total_bag, fqtv_card, fqtv_number');
    $CI->db->from('exbaggage');
    if ($depStn <> 'all') $CI->db->where('dep_station', $depStn);
    if ($arrStn <> 'all') $CI->db->where('arr_station', $arrStn);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") >=', $sDate);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") <=', $eDate);
    $CI->db->where('is_trt is null');     
    if ($status == 'WD') $CI->db->where('excess_status_wd', 'WD');
    if ($status == 'PD') $CI->db->where('excess_status_pd', 'PD');   
    $q = $CI->db->get();
    $CI->db->close();

    return ($q->num_rows() > 0) ? $q->result_array() : false;    
}

function get_xbaggage($station, $perstatus, $sDate, $eDate) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);

    if ($perstatus == 'ALL') $rec = "sum(IFNULL(excess_weight_wd,0))+sum(IFNULL(excess_weight_pd,0)) as total";
    else if ($perstatus == 'WD') $rec = "count(excess_status_wd) as total_pax, sum(IFNULL(excess_weight_wd,0)) as total";
    else if ($perstatus == 'PD') $rec = "count(excess_status_pd) as total_pax, sum(IFNULL(excess_weight_pd,0)) as total";
    $CI->db->select($rec);
    $CI->db->from('exbaggage');
    if ($station <> 'all') $CI->db->where('dep_station', $station);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") >=', $sDate);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") <=', $eDate);
    $CI->db->where('is_trt is null');
    $CI->db->where('dep_station is not null');
    $CI->db->where('CHAR_LENGTH(dep_station) >= 3');
    $q = $CI->db->get();
    $CI->db->close();    
    $result['total'] = $q->row()->total;
    $result['totalpax'] = $q->row()->total_pax;

    return ($result == null) ? 0 : $result;
    //return ($q->num_rows() > 0) ? $q->result_array() : false;
}

function getNumPass($station, $sDate, $eDate) {
    $CI = & get_instance();
    $CI->db = $CI->load->database('xb', TRUE);
    $CI->db->select('count(pass_name) as total_pax');
    $CI->db->from('exbaggage');
    if ($station <> 'all') $CI->db->where('dep_station', $station);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") >=', $sDate);
    $CI->db->where('STR_TO_DATE(flight_date,"%d%b%Y") <=', $eDate);
    $CI->db->where('is_trt is null');
    $CI->db->where('dep_station is not null');
    $CI->db->where('CHAR_LENGTH(dep_station) >= 3');
    $CI->db->group_by('dep_station'); 
    $q = $CI->db->get();
    $CI->db->close();

    return ($q->num_rows() > 0) ? $q->row()->total_pax : 0 ;    
}