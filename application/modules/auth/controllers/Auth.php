<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_auth');        
    }

    function index() {
        $isLogon = is_login();
        if ($isLogon)            
            redirect('main','refresh');                
        else 
            $this->login();
    }

    function login() {
        if ($this->input->post()) {			
            //check auth
            $a_post = $this->input->post();
            $s_nopeg = auth_sso($a_post);            
            
            if ($s_nopeg['nopeg']) {
                //check app user
                $i_isAuth = is_auth($s_nopeg['nopeg']);                
                if ($i_isAuth) {					                    
                    $a_sess = array(
                            'sNopeg'  => $i_isAuth->user_id,
                            'iGrp'     => $i_isAuth->role_id,
                            'aProfile' => $s_nopeg		      
                    );
                    $this->session->set_userdata($a_sess);		
                    //UPDATE LOG USER
                    auth_log($this->session->userdata('sNopeg'));
                    redirect('main','refresh');
                } else {                    
                    $data = $this->M_auth->login();        
                    $data['msg'] = 'You not authorized to access this system';
                    $this->load->view('v_login', $data);
                }
            } else {
                //check other user
                $s_nopeg = auth_ext($a_post);
                if ($s_nopeg) {
                    $i_isAuth = is_auth($s_nopeg);
                    if ($i_isAuth) {					                    
                        $a_sess = array(
                                'sNopeg'  => $i_isAuth->user_id,
                                'iGrp'     => $i_isAuth->role_id,
                                'aProfile' => $s_nopeg		      
                        );
                        $this->session->set_userdata($a_sess);		
                        //UPDATE LOG USER
                        auth_log($this->session->userdata('sNopeg'));
                        redirect('main','refresh');
                    } else {                    
                        $data = $this->M_auth->login();        
                        $data['msg'] = 'You not authorized to access this system';
                        $this->load->view('v_login', $data);
                    }
                } else {                
                    $data = $this->M_auth->login();        
                    $data['msg'] = 'Invalid username or password';
                    $this->load->view('v_login', $data);
                }
            } 
        } else {
            $data = $this->M_auth->login();        
            $this->load->view('v_login', $data);		
        }
    }

    function logout() {
        $i_isLogon = is_login();
		
		if ($i_isLogon) {
			$this->session->unset_userdata('sNopeg');
			$this->session->sess_destroy();
			
			redirect ('auth', 'refresh');
		} else
			redirect ('auth', 'refresh');
    }

    function error404() {
        $this->load->view('v_404');
    }

}