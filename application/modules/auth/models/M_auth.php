<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function index() {
		$data['title'] = 'Flight Operation Dashboard';
        $data['notes'] = 'Enter your username and password to access';
		
		return $data;
	}

    function login() {
        $data['title'] = 'Flight Operation Dashboard';
        $data['notes'] = 'Enter your username and password to access';
        
        return $data;
    }

}