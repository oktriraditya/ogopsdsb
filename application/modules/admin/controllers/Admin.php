<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_admin');        

        $i_isLogon = is_login();
        if (!$i_isLogon) redirect('auth', 'refresh');             
    }

    function userman() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'admin/userman');
        if ($isAuth) {
            $data = $this->M_admin->userman();
            $this->load->view('v_main', $data);
        } else {        
            redirect('main','refresh');            
        }
    }
    
    function usergrp() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'admin/userman');
        if ($isAuth) {
            $data = $this->M_admin->usergrp();
            $this->load->view('v_main', $data);
        } else {        
            redirect('main','refresh');            
        }
    }

    function grppriv() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'admin/userman');
        if ($isAuth) {
            $data = $this->M_admin->grppriv();
            $this->load->view('v_main', $data);
        } else {        
            redirect('main','refresh');            
        }
    }

    function menulist() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'admin/userman');
        if ($isAuth) {
            $data = $this->M_admin->menulist();
            $this->load->view('v_main', $data);
        } else {        
            redirect('main','refresh');            
        }
    }

}