<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function userman() {
		$data['title'] = 'Administrator';
		$data['title_small'] = 'user management';

		$this->db = $this->load->database('dm', TRUE);
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('user')
			->set_subject('User')
			->set_relation('role_id', 'role', 'role_name')			
			->columns(array('user_id','role_id','is_active','last_login_time','last_login_from'))
			->display_as('user_id','Employee ID')->display_as('role_id','Role Name')
			->fields('user_id', 'role_id', 'is_active', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('is_active','dropdown', array('1' => 'active', '2' => 'inactive'))
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->required_fields('user_id','role_id', 'is_active')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
	    $data['output']= $crud->render();

		$data['view'] = 'v_admin';

		$this->db->close();
		
		return $data;
	}

	function usergrp() {
		$data['title'] = 'Administrator';
		$data['title_small'] = 'user group';

		$this->db = $this->load->database('dm', TRUE);
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('role')
			->set_subject('User Group')
			->columns(array('role_name', 'role_shortname'))
			->display_as('role_name','Role Name')->display_as('role_shortname','Shortname')
			->fields('role_name', 'role_shortname', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('role_id', 'hidden')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->order_by('role_name', 'asc')
			->required_fields('role_id')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
	    $data['output']= $crud->render();

		$data['view'] = 'v_admin';

		$this->db->close();
		
		return $data;
	}

	function grppriv() {
		$data['title'] = 'Administrator';
		$data['title_small'] = 'group privilege';

		$this->db = $this->load->database('dm', TRUE);
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('role')
			->set_subject('Group Privilege')
			->set_relation_n_n('menus', 'role_privilege', 'menu', 'role_id', 'menu_id', 'menu_title')
			->columns(array('role_name', 'menus'))
			->display_as('role_name','Role Name')->display_as('menus','Menu Name')
			->fields('role_id', 'role_name', 'menus', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('role_name', 'readonly')
			->field_type('role_id', 'hidden')
			->field_type('role_id', 'hidden')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->required_fields('role_id', 'menus')
			->order_by('role_name', 'asc')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
	    $data['output']= $crud->render();

		$data['view'] = 'v_admin';

		$this->db->close();
		
		return $data;
	}

	function menulist() {
		$data['title'] = 'Administrator';
		$data['title_small'] = 'menu list';

		$this->db = $this->load->database('dm', TRUE);
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap-v4');
		$crud->set_table('menu')
			->set_subject('Menu')
			->set_relation('parent_id', 'menu', 'menu_title')			
			->columns(array('menu_title','menu_level','menu_url','menu_sort', 'parent_id'))
			->display_as('menu_title','Menu Title')
			->display_as('menu_level','Level')
			->display_as('menu_url','URL')
			->display_as('menu_sort','Sort Order')
			->display_as('parent_id','Parent Menu')
			->fields('menu_id','menu_title','menu_level','menu_url','menu_sort', 'parent_id', 'create_date', 'create_by', 'mod_date', 'mod_by')
			->field_type('menu_id', 'hidden')
			->field_type('create_date', 'hidden')
			->field_type('create_by', 'hidden')
			->field_type('mod_date', 'hidden')
			->field_type('mod_by', 'hidden')
			->required_fields('menu_title','menu_level','menu_url','menu_sort', 'parent_id')
			->callback_before_insert(array($this,'add_action_log'))
			->callback_before_update(array($this,'edit_action_log'));	
	    $data['output']= $crud->render();

		$data['view'] = 'v_admin';

		$this->db->close();
		
		return $data;
	}

	function add_action_log($post_array) {
		//session
		$sNopeg = get_sess();

		$post_array['create_date'] = date('Y-m-d H:i:s');
		$post_array['create_by'] = $sNopeg;

		return $post_array;
	}

	function edit_action_log($post_array) {
		//session
		$sNopeg = get_sess();		

		$post_array['mod_date'] = date('Y-m-d H:i:s');
		$post_array['mod_by'] = $sNopeg;

		return $post_array;
	}

}