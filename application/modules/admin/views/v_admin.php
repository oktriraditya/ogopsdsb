<?php foreach($output->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<?php foreach($output->js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">       
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->     
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>              
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Administrator</span>    
                <i class="fa fa-circle"></i>            
            </li>
            <li>
                <span class="active"><?php echo $title_small;?></span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB --> 
        <!-- BEGIN PAGE BASE CONTENT -->        
        <div class="portlet light bordered">                      
            <div class="portlet-body">                    
                <?php echo $output->output; ?>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT