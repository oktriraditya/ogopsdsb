<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_cis extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function home() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['title'] = 'Crew Information Dashboard';
        $data['title_small'] = 'flight hour ODS - by date';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_home';
        
        return $data;
    }

    function flighthour() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $actType = getlistAct();
        $eqtType = getListAcType();
        $data['title'] = 'Crew Information Dashboard';
        $data['title_small'] = 'flight hour ODS - by hour range';
        $data['jam'] = date('H:i', $time);
        $data['acttype'] = $actType;        
        $data['eqtType'] = $eqtType;        
        $data['view'] = 'v_flighthour';
        
        return $data;
    }

    function fhour() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $actLocalType = getlistActLocal();
        $data['title'] = 'Crew Information Dashboard';
        $data['title_small'] = 'Flight Hour 2SG - by hour range';
        $data['jam'] = date('H:i', $time);
        $data['actlocaltype'] = $actLocalType;        
        $data['view'] = 'v_fhour';
        
        return $data;
    }

    function fhavg() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $yearRange = 0;
        $data['thisYear'] = date('Y');
        $data['startYear'] = $data['thisYear'] - $yearRange;    
        $data['title'] = 'Crew Information Dashboard';
        $data['title_small'] = 'Flight Hour Average';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_fhavg';
        
        return $data;
    }

    function viewhistory($nopeg, $sDate, $eDate) {
        $data['nopeg'] = $nopeg;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $name = getName($nopeg);        
        $data['name'] = $name;
        $data['view'] = 'v_history';

        return $data;
    }

}