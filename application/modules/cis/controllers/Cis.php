<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cis extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_cis');

        $i_isLogon = is_login();
        if (!$i_isLogon) redirect('auth', 'refresh');  
    }

    function index() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cis/home');
        if ($isAuth) {
            redirect('cis/home', 'refresh');
        } else {
            
            redirect('main','refresh');
            
        }
    }

    function home() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cis/home');
        if ($isAuth) {
            $data = $this->M_cis->home();
            $this->load->view('v_main', $data);
        } else {
            
            redirect('main','refresh');
            
        }
    }

    function flighthour() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cis/flighthour');
        if ($isAuth) {
            $data = $this->M_cis->flighthour();
            $this->load->view('v_main', $data);
        } else {
            
            redirect('main','refresh');
            
        }
    }
    
    function fhstats() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;

        $this->load->view('v_fhstats', $data);        
    }

    function getfh() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $acType = $this->input->get('acType');
        $act = $this->input->get('act');
        
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['acType'] = $acType;
        $data['act'] = $act;

        $this->load->view('v_fhcrew', $data);        
    }

    function tabledata() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $acType = $this->input->get('acType');
        $act = $this->input->get('act');
        $fhStart = NULL;
        $fhEnd = NULL;

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['acType'] = $acType;
        $data['act'] = $act;
        $data['fhStart'] = $fhStart;
        $data['fhEnd'] = $fhEnd;
        
        $data['arrRecords'] = get_record($sDate, $eDate, $fhStart, $fhEnd, $acType, $act);

        $this->load->view('v_home_tabledata', $data);
    }

    function fhour() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cis/fhour');
        if ($isAuth) {
            $data = $this->M_cis->fhour();
            $this->load->view('v_main', $data);
        } else {
            
            redirect('main','refresh');
            
        }
    }

    function getfhlocal() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $act = $this->input->get('act');
        
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['act'] = $act;

        $this->load->view('v_fhlocal', $data);        
    }

    function tabledatalocal() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $act = $this->input->get('act');
        if ($this->input->get('fh')) $rangeFH = $this->input->get('fh'); else $rangeFH = NULL;
        
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['act'] = $act;
        $data['fh'] = $rangeFH;
        
        $data['arrRecords'] = getRecordFhLocal($sDate, $eDate, $rangeFH, $act);

        $this->load->view('v_home_tabledatalocal', $data);
    }

    function stats() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $act = $this->input->get('act');
        
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['act'] = $act;
        $data['avgfh'] = getAvg($sDate, $eDate, $act);
        $data['maxfh'] = getMax($sDate, $eDate, $act);
        $data['minfh'] = getMin($sDate, $eDate, $act);

        $this->load->view('v_stats', $data);    
    }

    function viewhistory() {
        $sDate = $this->uri->segment(3);
        $eDate = $this->uri->segment(4);
        $nopeg = $this->uri->segment(5);
        $data = $this->M_cis->viewhistory($nopeg, $sDate, $eDate);
        $this->load->view('cis/v_main_modal', $data);
    }

    function gethistfh() {
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $nopeg = $this->input->get('nopeg');

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['nopeg'] = $nopeg;

        $this->load->view('v_histcrew', $data);
    }

    function fhavg() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cis/fhavg');
        if ($isAuth) {
            $data = $this->M_cis->fhavg();
            $this->load->view('v_main', $data);
        } else {
            
            redirect('main','refresh');
            
        }
    }

    function getfhavg() {
        $sYear = $this->input->get('sYear');        
        $data['sYear'] = $sYear;

        $this->load->view('v_fhavglocal', $data);  
    }

    function tableavglocal() {
        $sYear = $this->input->get('sYear');        
        $data['sYear'] = $sYear;
        
        $data['arrRecords'] = getRecordFhAvgLocal($sYear);

        $this->load->view('v_home_tableavglocal', $data);
    }

}