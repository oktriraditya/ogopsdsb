<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript">
jQuery(document).ready(function() { 
    $('#fhBox').change(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var eDate = $("#eDate").val();      
        var fh = $("#fhBox").val();
        var act = '<?php echo $act;?>';
        
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>cis/tabledatalocal",
            data    : {"sDate": sDate, "eDate": eDate, "fh": fh, "act": act,},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);   
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $("#sample_1").on('click', '.view', function(){
        var id = this.id;
        var target_url = "<?php echo base_url().'cis/viewhistory/'.$sDate.'/'.$eDate.'/';?>"+id;
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
    });
});
</script>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> Crew Flight Hour Record Average</span>            
        </div>                                       
    </div>
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="helper"><?php echo 'Year : '. $sYear; ?></span>            
        </div> 
    </div>
    <div class="portlet-body">    
        
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th width="1%"> No. </th>
                        <th width="5%"> Nopeg </th>
                        <th width="10%"> Name </th>
                        <th width="5%"> Total Flight Hour (1 Year)</th>                                           
                        <th width="5%"> Average (1 Year)</th>  
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>                        
                        <th> </th>                          
                        <th> </th>                        
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    if ($arrRecords) {
                    foreach (@$arrRecords as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td> <?php echo $i;?>. </td>
                        <td> <?php echo $row['staffnumber'];?> </td>                        
                        <!--<td> <?php //echo $row['FIRSTNAME'].' '.$row['LASTNAME'];?> </td>-->
                        <td> <?php echo $row['FIRSTNAME'];?> </td>
                        <td> <?php echo convertToHoursMins($row['fh'], '%02d:%02d');?> </td>   
                        <td> <?php echo convertToHoursMins($row['fh']/12, '%02d:%02d');?> </td>   
                    </tr>
                    <?php $i++; } } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View History Crew Flight Hour</h4>
        <small>based on <i>Daily Flying Hours </i> Activity</small>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>
