<script>
jQuery(function(){
    var chart = AmCharts.makeChart("chartFHstats", {
        "type": "serial",
        "theme": "none",
        "marginRight": 80,
        "legend": {
            "horizontalGap": 70,
            "maxColumns": 70,
            "position": "bottom",
            "useGraphSettings": false,
            "markerSize": 10
        },
        "dataProvider": [
            <?php
            $arrDay = getDatesFromRange($sDate, $eDate);
            foreach($arrDay as $sDay) {
                $arrFH = getHistFH($sDay, $nopeg);
                echo "{";
                echo "'date': '".$sDay."',";
                echo "fh: ".$arrFH.",";
                echo "},";
            }
            ?>
        ],
        "balloon": 
        {
            "cornerRadius": 6,
            "horizontalPadding": 15,
            "verticalPadding": 10
        },
        "valueAxes": [{
            "duration": "mm",
            "durationUnits": {
                "hh": "h ",
                "mm": "min"
            },
            "axisAlpha": 0
        }],
        "graphs": [{
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "fillAlphas": 0.3,
            "fillColorsField": "lineColor",
            "legendValueText": "[[value]]",
            "lineColorField": "lineColor",
            "title": "flight hours",
            "valueField": "fh"
        }],
        "chartScrollbar": {

        },
        "chartCursor": {
            "categoryBalloonDateFormat": "YYYY MMM DD",
            "cursorAlpha": 0,
            "fullWidth": true
        },
        "dataDateFormat": "YYYY-MM-DD",
        "categoryField": "date",
        "categoryAxis": {
            "dateFormats": [{
                "period": "DD",
                "format": "DD"
            }, {
                "period": "WW",
                "format": "MMM DD"
            }, {
                "period": "MM",
                "format": "MMM"
            }, {
                "period": "YYYY",
                "format": "YYYY"
            }],
            "parseDates": true,
            "autoGridCount": false,
            "axisColor": "#555555",
            "gridAlpha": 0,
            "gridCount": 50
        },
        "export": {
            "enabled": true
        }

    });
});
</script>