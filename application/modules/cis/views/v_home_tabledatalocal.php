<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript">
jQuery(document).ready(function() { 
    $('#fhBox').change(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var eDate = $("#eDate").val();      
        var fh = $("#fhBox").val();
        var act = '<?php echo $act;?>';
        
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>cis/tabledatalocal",
            data    : {"sDate": sDate, "eDate": eDate, "fh": fh, "act": act,},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);   
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $("#sample_1").on('click', '.view', function(){
        var id = this.id;
        var target_url = "<?php echo base_url().'cis/viewhistory/'.$sDate.'/'.$eDate.'/';?>"+id;
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
    });
});
</script>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> Crew Flight Hour Record</span>            
        </div>        
        <div class="actions pull-left">
            <div class="table-actions-wrapper">
                <span>&nbsp;</span><span>&nbsp;</span>
                <select class="table-group-action-input form-control input-inline input-small input-sm" name="fh" id="fhBox">
                    <option value="" <?php if ($fh == "") echo 'selected=selected';?>>All</option>
                    <option value="0' and '300" <?php if ($fh == "0' and '300") echo 'selected=selected';?>>0 - 5 Hours</option>
                    <option value="301' and '600" <?php if ($fh == "301' and '600") echo 'selected=selected';?>>5 - 10 Hours</option>
                    <option value="601' and '900" <?php if ($fh == "601' and '900") echo 'selected=selected';?>>10 - 15 Hours</option>
                    <option value="901' and '1200" <?php if ($fh == "901' and '1200") echo 'selected=selected';?>>15 - 20 Hours</option>
                    <option value="1201' and '1500" <?php if ($fh == "1201' and '1500") echo 'selected=selected';?>>20 - 25 Hours</option>
                    <option value="1501' and '1800" <?php if ($fh == "1501' and '1500") echo 'selected=selected';?>>25 - 30 Hours</option>
                    <option value="1801' and '2100" <?php if ($fh == "1801' and '2100") echo 'selected=selected';?>>30 - 35 Hours</option>
                    <option value="2101' and '2400" <?php if ($fh == "2101' and '2400") echo 'selected=selected';?>>35 - 40 Hours</option>
                    <option value="2401' and '2700" <?php if ($fh == "2401' and '2700") echo 'selected=selected';?>>40 - 45 Hours</option>
                    <option value="2701' and '3000" <?php if ($fh == "2701' and '3000") echo 'selected=selected';?>>45 - 50 Hours</option>
                    <option value="3001' and '3300" <?php if ($fh == "3001' and '3300") echo 'selected=selected';?>>50 - 55 Hours</option>
                    <option value="3301' and '3600" <?php if ($fh == "3301' and '3600") echo 'selected=selected';?>>55 - 60 Hours</option>
                    <option value="3601' and '3900" <?php if ($fh == "3601' and '3900") echo 'selected=selected';?>>60 - 65 Hours</option>
                    <option value="3901' and '4200" <?php if ($fh == "3901' and '4200") echo 'selected=selected';?>>65 - 70 Hours</option>
                    <option value="4201' and '4500" <?php if ($fh == "4201' and '4500") echo 'selected=selected';?>>70 - 75 Hours</option>
                    <option value="4501' and '4800" <?php if ($fh == "4501' and '4800") echo 'selected=selected';?>>75 - 80 Hours</option>
                    <option value="4801' and '5100" <?php if ($fh == "4801' and '5100") echo 'selected=selected';?>>80 - 85 Hours</option>
                    <option value="5101' and '5400" <?php if ($fh == "5101' and '5400") echo 'selected=selected';?>>85 - 90 Hours</option>
                    <option value="5401' and '5700" <?php if ($fh == "5401' and '5700") echo 'selected=selected';?>>90 - 95 Hours</option>
                    <option value="5701' and '6000" <?php if ($fh == "5701' and '6000") echo 'selected=selected';?>>95 - 100 Hours</option>                    
                    <option value="6001" <?php if ($fh == "6001") echo 'selected=selected';?>>&gt; 100 Hours</option>                    
                </select>           
            </div> 
        </div>                          
    </div>
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="helper"><?php echo date('dMY', strtotime($sDate));?> - <?php echo date('dMY', strtotime($eDate));?> <?php if ($act<>'') echo ' , '.$act;?></span>            
        </div> 
    </div>
    <div class="portlet-body">    
        
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th width="1%"> No. </th>
                        <th width="5%"> Nopeg </th>
                        <th width="10%"> Name </th>
                        <th width="5%"> Flight Hour</th>                                                  
                        <th width="1%"></th>                        
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>                        
                        <th> </th>                         
                        <th> </th>                    
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    if ($arrRecords) {
                    foreach (@$arrRecords as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td> <?php echo $i;?>. </td>
                        <td> <?php echo $row['staffnumber'];?> </td>                        
                        <!--<td> <?php //echo $row['FIRSTNAME'].' '.$row['LASTNAME'];?> </td>-->
                        <td> <?php echo $row['FIRSTNAME'];?> </td>
                        <td> <?php echo convertToHoursMins($row['fh'], '%02d:%02d');?> </td>                           
                        <td>
                            <a class="btn btn-sm btn-outline grey-salsa view" data-target="#view" data-toggle="modal" id="<?php echo $row['staffnumber'];?>"><i class="fa fa-search"></i> Detail </a>
                        </td>          
                    </tr>
                    <?php $i++; } } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View History Crew Flight Hour</h4>
        <small>based on <i>Daily Flying Hours </i> Activity</small>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>
