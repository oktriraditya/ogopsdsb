<script>
jQuery(function(){
    var chart = AmCharts.makeChart("chartFHstats", {
        "type": "serial",
        "theme": "none",
        "startDuration": 2,
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": [
            <?php
            $arrMonth = getListMonth();
            foreach($arrMonth as $key => $sMonth) {                
                $avgfh = getAvgMonth($key, $sYear);
                $avg = convertToHoursMins($avgfh, '%02d.%02d');
                echo "{";
                echo "'month': '".$sMonth."',";
                echo "'avg': '".$avg."',";               
                echo "},";
            }
            ?>
        ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0,
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "title": "Average Flight Hour",
            "type": "column",
            "color": "#000000",
            "valueField": "avg",
            "colorField": "#FFFF33",
        }
        ],
        "depth3D": 20,
	    "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillColor": "#FFFF33",
            "gridAlpha": 0.1,
            "dashLength": 1,
            "minorGridEnabled": true,
        },        
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "Year : <?php echo $sYear;?>",
			"bold": true,
			"x": 0,
			"y": 0
		}
        ]
    });
});
</script>