
<div class="col-lg-4 col-xs-4 col-sm-4">
    <div class="note note-info">
        <h4 align="center"><strong>Average : <?php echo convertToHoursMins($avgfh, '%02d:%02d');?></strong></h4>
    </div>
</div>
<div class="col-lg-4 col-xs-4 col-sm-4">
    <div class="note note-info font-dark">
        <h4 align="center"><strong>Max : <?php echo convertToHoursMins($maxfh, '%02d:%02d');?></strong></h4>
    </div>
</div>
<div class="col-lg-4 col-xs-4 col-sm-4">
    <div class="note note-info font-dark">
    <h4 align="center"><strong>Min : <?php echo convertToHoursMins($minfh, '%02d:%02d');?></strong></h4>
    </div>
</div>
