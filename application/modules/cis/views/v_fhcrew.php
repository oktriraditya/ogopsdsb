<script>
jQuery(function(){
    var chart = AmCharts.makeChart("chartFHstats", {
        "type": "serial",
        "theme": "none",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": [
            <?php
            for($i=0;$i<=10;$i++) { 
                if ($i==0) {
                    $j=1; 
                    $range = $j." - ".$j*10;
                    $fhStart = 0;
                    $fhEnd = 60*10;
                } else {
                    if ($i==10) {
                        $iStart = $i*10; 
                        $iEnd = ($i*10)+10;
                        $range = "> 100";
                        $fhStart = ($iStart*60)+1;
                        $fhEnd = $iEnd*60;
                    } else { 
                        $iStart = $i*10; 
                        $iEnd = ($i*10)+10;
                        $range = $iStart." - ".$iEnd;
                        $fhStart = ($iStart*60)+1;
                        $fhEnd = $iEnd*60;
                    }
                }
                
                $countCrewFH = countFlightHour($sDate, $eDate, $fhStart, $fhEnd, $acType, $act);
                //echo convertToHoursMins($crewFlightMinutes[$i]['VALUEMINS'], '%02dH:%02dm');
                echo "{";
                echo "'hour': '".$range." hour',";
                echo "crew: '".$countCrewFH."',";
                echo "},";                                          
            }
            ?>
        ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "title": "Number of Crew",
            "type": "column",
            "color": "#000000",
            "valueField": "crew"
        }
        ],
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left",
            "dashLength": 1,
            "minorGridEnabled": true
        },        
        "export": {
            "enabled": true
        }

    });
});
</script>