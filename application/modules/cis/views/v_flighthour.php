<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}

function loadFHstat() {
    blockUI(FHStats);
    var sDate = $("#sDate").val();
    var eDate = $("#eDate").val();    
    var acType = $("#actype").val();
    var act = $("#act").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cis/getfh",
        data    : {"sDate": sDate, "eDate": eDate, "acType": acType, "act": act,},
        success :
            function(msg) {
                $("#chartFHstats").html(msg);
                unblockUI(FHStats);
            },
        error   :
            function(){
                unblockUI(FHStats);
                $("#chartFHstats").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function loadRec() {
    blockUI(boxTable);
    var sDate = $("#sDate").val();      
    var eDate = $("#eDate").val();    
    var acType = $("#actype").val();
    var act = $("#act").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cis/tabledata",
        data    : {"sDate": sDate, "eDate": eDate, "acType": acType, "act": act,},
        success :
            function(msg) {
                $("#boxTable").html(msg);
                unblockUI(boxTable);            
            },
        error   : 
            function(){
                unblockUI(boxTable);
                $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

jQuery(document).ready(function() {
    loadFHstat();
    loadRec();

    $('#submit').click(function(){
        loadFHstat();
        loadRec();
    });

});
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>              
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Crew Flight Hour</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">ODS by flight hour range</span>                
            </li>           
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">               
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Start Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+0y">
                                <input type="text" class="form-control" value="<?php echo date("Y-m-d", strtotime("-7 day"));?>" name="date" id="sDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">End Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+1y">
                                <input type="text" class="form-control" value="<?php echo date("Y-m-d", strtotime("+0 day"));?>" name="date" id="eDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">AC/TYPE</span>
                                </button>
                            </span>
                            <select class="form-control input-small selectAcType" name="actype" id="actype">
                                <option value="">ALL</option>
                                <?php foreach ($eqtType as $key => $row) {?>                                
                                <option value="<?php echo $row['EQT'];?>"><?php echo $row['EQT'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>   
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Activity</span>
                                </button>
                            </span>
                            <select class="form-control input-small selectAct" name="act" id="act">
                                <option value="">ALL</option>
                                <option value="FLY">FLY</option>
                                <option value="OFF">OFF</option>
                                <option value="STANDBY">STANDBY</option>
                                <!--<?php //foreach ($acttype as $key => $row) {?>                                
                                <option value="<?php //echo $row['ACTYPE'];?>"><?php echo $row['ACTYPE'];?></option>
                                <?php //} ?>-->
                            </select>
                        </div>
                    </div>
                </div>          
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light bordered" id="FHStats">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark" >Crew Flight Hour</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                             &nbsp;
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartFHstats" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div>        
        <div id="boxTable"></div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT
