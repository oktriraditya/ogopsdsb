<script>
jQuery(function(){
    var chart = AmCharts.makeChart("chartFHstats", {
        "type": "serial",
        "theme": "none",
        "startDuration": 2,
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": [
            <?php            
            $red = array(0,1,10);
            $orange = array(2,3,18,19);
            $yellow = array(4,5,6,7,8,9,10,11);
            $green = array(12,13,14,15,16,17);
            for($i=0;$i<=20;$i++) { 
                if (in_array($i, $red)) $color = "#FF0000";
                if (in_array($i, $orange)) $color = "#FF8C00";
                if (in_array($i, $yellow)) $color = "#FFFF33";
                if (in_array($i, $green)) $color = "#32CD32";

                if ($i==0) {
                    $j=1; 
                    $range = "0 - ".$j*5;
                    $fhStart = 0;
                    $fhEnd = 60*5;
                } else {
                    if ($i==20) {
                        $iStart = $i*5; 
                        $iEnd = ($i*5)+5;
                        $range = "> 100";
                        $fhStart = ($iStart*60)+1;
                        $fhEnd = 525960;
                    } else { 
                        $iStart = $i*5; 
                        $iEnd = ($i*5)+5;
                        $range = $iStart." - ".$iEnd;
                        $fhStart = ($iStart*60)+1;
                        $fhEnd = $iEnd*60;
                    }
                }
                
                $countCrewFH = countFHlocal($sDate, $eDate, $fhStart, $fhEnd, $act);
                //echo convertToHoursMins($crewFlightMinutes[$i]['VALUEMINS'], '%02dH:%02dm');
                echo "{";
                //echo "'hour': '".$range." hour',";
                echo "'hour': '".$range."',";
                echo "crew: '".$countCrewFH."',";
                echo "'color': '".$color."',";
                echo "},";                                          
            }
            //exit;
            ?>
        ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "title": "Number of Crew",
            "type": "column",
            "color": "#000000",
            "valueField": "crew",
            "colorField": "color",
        }
        ],
        "depth3D": 20,
	    "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillColor": "#FF0000",
            "gridAlpha": 0.1,
            "dashLength": 1,
            "minorGridEnabled": true,
        },        
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "<?php echo date('dFY', strtotime($sDate));?> - <?php echo date('dFY', strtotime($eDate));?> <?php if ($act<>'') echo ' , '.$act;?>",
			"bold": true,
			"x": 0,
			"y": 0
		}
        ]
    });
});
</script>