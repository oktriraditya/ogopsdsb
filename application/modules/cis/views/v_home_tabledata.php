<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> Crew Flight Hour Record</span>
            <small><?php echo $sDate;?> to <?php echo $eDate;?>, AC/Type: <strong><?php echo $acType;?></strong>, Activity: <strong><?php echo $act;?></strong></small>
        </div>
        <!--<br/>      
        <div class="actions pull-left">
            <div class="table-actions-wrapper">
                <span>&nbsp;</span><span>&nbsp;</span>
                <select class="table-group-action-input form-control input-inline input-small input-sm" name="fh" id="statusBox">
                    <option value="ALL">All</option>
                    <option value="010">0 - 10 Hours</option>
                    <option value="1020">10 - 20 Hours</option>
                    <option value="2030">20 - 30 Hours</option>
                    <option value="3040">30 - 40 Hours</option>
                    <option value="4050">40 - 50 Hours</option>
                    <option value="5060">50 - 60 Hours</option>
                    <option value="6070">60 - 70 Hours</option>
                    <option value="7080">70 - 80 Hours</option>
                    <option value="8090">80 - 90 Hours</option>
                    <option value="9010">90 - 100 Hours</option>                    
                    <option value="100">&lt; 100 Hours</option>                    
                </select>           
            </div> 
        </div>-->                      
    </div>
    <div class="portlet-body">    
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th width="1%"> No. </th>
                        <th width="10%"> Nopeg </th>
                        <th width="10%"> Name </th>
                        <th width="10%"> AC/Type</th>                        
                        <th width="10%"> Flight Hour</th>                        
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>                        
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    if ($arrRecords) {
                    foreach (@$arrRecords as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td> <?php echo $i;?>. </td>
                        <td> <?php echo $row['STAFFNUMBER'];?> </td>                        
                        <td> <?php echo $row['FIRSTNAME'].' '.$row['LASTNAME'];?> </td>                        
                        <td> <?php echo $row['EQTKEY_CODE'];?> </td>
                        <td> <?php echo convertToHoursMins($row['VALUEMINS'], '%02d:%02d');;?> </td>                        
                    </tr>
                    <?php $i++; } } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>