<script>
jQuery(function(){
    var chart = AmCharts.makeChart("chartFHstats", {
        "type": "serial",
        "theme": "none",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": [
            <?php
            $arrDay = getDatesFromRange($sDate, $eDate);
            foreach($arrDay as $sDay) {
                $arr010 = getFH($sDay, 0, 600);
                $arr020 = getFH($sDay, 601, 1200);
                $arr030 = getFH($sDay, 1201, 1800);
                $arr040 = getFH($sDay, 1801, 2400);
                $arr050 = getFH($sDay, 2401, 3000);
                $arr060 = getFH($sDay, 3001, 3600);
                $arr070 = getFH($sDay, 3601, 4200);
                $arr080 = getFH($sDay, 4201, 4800);
                $arr090 = getFH($sDay, 4801, 5400);
                $arr0100 = getFH($sDay, 5401, 6000);
                echo "{";
                echo "'date': '".$sDay."',";
                echo "10: ".$arr010.",";
                echo "20: ".$arr020.",";
                echo "30: ".$arr030.",";
                echo "40: ".$arr040.",";
                echo "50: ".$arr050.",";
                echo "60: ".$arr060.",";
                echo "70: ".$arr070.",";
                echo "80: ".$arr080.",";
                echo "90: ".$arr090.",";
                echo "100: ".$arr0100.",";
                echo "},";
            }
            ?>
        ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.3,
            "title": "0 - 10 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "10"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "10 - 20 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "20"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "20 - 30 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "30"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "30 - 40 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "40"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "40 - 50 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "50"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "50 - 60 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "60"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "60 - 70 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "70"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "70 - 80 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "80"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "80 - 90 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "90"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            
            "lineAlpha": 0.3,
            "title": "> 90 Hours",
            "type": "column",
            "color": "#000000",
            "valueField": "100"
        }
        ],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left",
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },        
        "export": {
            "enabled": true
        }

    });
});
</script>