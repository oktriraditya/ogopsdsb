<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_main');

        $i_isLogon = is_login();
        if (!$i_isLogon) redirect('auth', 'refresh');   
    }

    function index() {        
        $data = $this->M_main->index();
        $this->load->view('v_main', $data);
    }

    function profile() {
        $data = $this->M_main->profile();
        $this->load->view('v_main', $data);
    }

}