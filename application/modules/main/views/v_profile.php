    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url();?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->  
             <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><?php echo $title;?>
                        <small><?php echo $title_small;?></small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->                              
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <div class="pull-right tooltips btn btn-fit-height red">
                        <i class="fa fa-plane"></i>
                    </div>
                    <div class="pull-right tooltips btn btn-fit-height blue">
                        <i class="icon-calendar"></i>&nbsp;
                        <span id="date_time"></span>
                        <script type="text/javascript">window.onload = date_time('date_time');</script>
                    </div>
                    <div class="pull-right tooltips btn btn-fit-height green">                    
                        <i class="icon-calendar"></i>&nbsp;
                        <span id="date_time_utc"></span>
                        <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                    </div>  
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo base_url();?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">My Profile</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->   
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="<?php echo base_url();?>assets/pages/media/profile/NoProfile.jpg" class="img-responsive" alt=""> </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> <?php echo isset($this->session->userdata('aProfile')['name']) ? $this->session->userdata('aProfile')['name'] : $this->session->userdata('sNopeg');?>  </div>
                                <div class="profile-usertitle-job"> <?php echo isset($this->session->userdata('aProfile')['unit']) ? $this->session->userdata('aProfile')['unit'] : '';?>  </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->                       
                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane active" id="tab_1_1">
                                                <form role="form" action="#">
                                                    <div class="form-group">
                                                        <label class="control-label">Name</label>
                                                        <input type="text" placeholder="<?php echo isset($this->session->userdata('aProfile')['name']) ? $this->session->userdata('aProfile')['name'] : $this->session->userdata('sNopeg');?>" class="form-control" disabled /> </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Nopeg</label>
                                                        <input type="text" placeholder="<?php echo isset($this->session->userdata('aProfile')['nopeg']) ? $this->session->userdata('aProfile')['nopeg'] : '';?>" class="form-control" disabled/> </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Unit Code</label>
                                                        <input type="text" placeholder="<?php echo isset($this->session->userdata('aProfile')['unit']) ? $this->session->userdata('aProfile')['unit'] : '';?>" class="form-control" disabled /> </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Unit Name</label>
                                                        <input type="text" placeholder="<?php echo isset($this->session->userdata('aProfile')['unitname']) ? $this->session->userdata('aProfile')['unitname'] : '';?>" class="form-control" disabled /> </div>                  
                                                </form>
                                            </div>
                                            <!-- END PERSONAL INFO TAB -->                                 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
                