<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="heading">
                <h3 class="uppercase"></h3>
            </li>

            <?php
            $mainMenu = getMenu($this->session->userdata('iGrp'), 0);
            foreach ($mainMenu as $main) {
                $sub_menu = getMenu($this->session->userdata('iGrp'), $main['menu_id']);
                if ($sub_menu) {
                ?>
                <li class="nav-item">
                    <a href="<?php echo base_url().$main['menu_url']; ?>" class="nav-link nav-toggle">
                        <i class="fa fa-folder-o"></i>
                        <span class="title"><?php echo $main['menu_title']?></span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                    <?php foreach ($sub_menu as $sub) { ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url().$sub['menu_url'];?>" class="nav-link ">
                            <i class="fa fa-folder-open-o"></i>
                            <span class="title"><?php echo $sub['menu_title']?></span>
                            </a>
                        </li>
                    <?php } ?>
                    </ul>
                </li>
                <?php
                }
            }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
