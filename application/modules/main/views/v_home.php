<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>  
            </div>
            <!-- END PAGE TOOLBAR -->            
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>occ">Home</a>        
                <i class="fa fa-circle"></i>            
            </li>         
            <li>
                <span class="active">Main</span>
            </li>       
        </ul>
        <!-- END PAGE BREADCRUMB -->   
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="blog-page blog-content-1">
            <div class="row">
                <div class="col-lg-12">                                        
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="blog-post-sm bordered blog-container">
                                <div class="blog-img-thumb">
                                    <a href="<?php echo base_url();?>occ">
                                        <img src="<?php echo base_url();?>assets/pages/img/page_general_search/1.jpg" />
                                    </a>
                                </div>
                                <div class="blog-post-content">
                                    <h2 class="blog-title blog-post-title">
                                        <a href="<?php echo base_url();?>occ">On Time Performance (OTP)</a>
                                    </h2>
                                    <p class="blog-post-desc"> Flight on time performance reporting and dashboard </p>            
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="blog-post-sm bordered blog-container">
                                <div class="blog-img-thumb">
                                    <a href="<?php echo base_url();?>cstrength">
                                        <img src="<?php echo base_url();?>assets/pages/img/page_general_search/02.jpg" />
                                    </a>
                                </div>
                                <div class="blog-post-content">
                                    <h2 class="blog-title blog-post-title">
                                        <a href="<?php echo base_url();?>cstrength">Cockpit Crew Strength</a>
                                    </h2>
                                    <p class="blog-post-desc"> Cockpit operation and man power planning dashboard </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="blog-post-sm bordered blog-container">
                                <div class="blog-img-thumb">
                                    <a href="<?php echo base_url();?>cis/fhour">
                                        <img src="<?php echo base_url();?>assets/pages/img/page_general_search/03.png" />
                                    </a>
                                </div>
                                <div class="blog-post-content">
                                    <h2 class="blog-title blog-post-title">
                                        <a href="<?php echo base_url();?>cis/fhour">Cabin Crew Flight Hour</a>
                                    </h2>
                                    <p class="blog-post-desc"> Cabin crew summaries flight hour reporting dashboard </p>                                    
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
        
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT
