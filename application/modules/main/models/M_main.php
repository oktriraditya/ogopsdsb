<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_main extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['title'] = 'Flight Operation';
        $data['title_small'] = 'dashboard';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_home';
        return $data;
    }

    function profile() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['title'] = 'My Profile';
        $data['title_small'] = '';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_profile';
        return $data;
    }

}