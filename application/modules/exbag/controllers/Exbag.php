<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exbag extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_exbag');
    }

    function index() {
        $data = $this->M_exbag->home();
        $this->load->view('v_main', $data);
    }

    function countstat() {
        $depStn = $this->input->post('depStn');
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $status = $this->input->post('status');

        $data['depStn'] = $depStn;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['status'] = $status;

        $this->load->view('v_countstat', $data);
    }

    function byStation() {
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $depStn = $this->input->post('depStn');
        $status = $this->input->post('status');

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['depStn'] = $depStn;
        $data['status'] = $status;

        $this->load->view('v_home_chart', $data);
    }
    
    function tableByStation() {
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $depStn = $this->input->post('depStn');
        $status = $this->input->post('status');
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['depStn'] = $depStn;
        $data['status'] = $status;
        /* if ($status == 'WD') $data['status'] = 'Waived';
        else if ($status == 'PD') $data['status'] = 'Paid';
        else if ($status == 'ALL') $data['status'] = 'All Excess'; */
        $data['arrRec'] = getExcBagRec($sDate, $eDate, $depStn, $status);

        $this->load->view('v_home_tabledata', $data);
    }

    function tableAllRec() {
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $depStn = $this->input->post('depStn');
        $arrStn = $this->input->post('arrStn');
        $status = $this->input->post('status');

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['depStn'] = $depStn;
        $data['arrStn'] = $arrStn;
        if ($status == 'WD') $data['status'] = 'Waived';
        else if ($status == 'PD') $data['status'] = 'Paid';
        else if ($status == 'ALL') $data['status'] = 'All Excess';
        $data['arrRec'] = getAllRec($sDate, $eDate, $depStn, $arrStn, $status);

        $this->load->view('v_record_tabledata', $data);
    }

    function perStation() {
        $perstn = $this->input->post('perstn');
        $perstatus = $this->input->post('perstatus');
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');

        $data['perstn'] = $perstn;
        $data['perstatus'] = $perstatus;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;

        $this->load->view('v_countstation', $data);
    }

    function byPercentage() {
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $depStn = $this->input->post('depStn');
        $status = $this->input->post('status');

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['depStn'] = $depStn;
        $data['status'] = $status;

        $this->load->view('v_bypercentage', $data);
    }

    function boxStat() {
        $sDate = $this->input->post('sDate');
        $eDate = $this->input->post('eDate');
        $depStn = $this->input->post('depStn');
        $status = $this->input->post('status');

        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['depStn'] = $depStn;
        $data['status'] = $status;

        $data['wd'] = get_xbaggage($depStn, 'WD', $sDate, $eDate);
        $data['pd'] = get_xbaggage($depStn, 'PD', $sDate, $eDate);

        $this->load->view('v_home_boxstat', $data);
    }

    function error404() {
        $data = $this->M_exbag->error404();
        $this->load->view("v_main", $data);
    }

    function allrec() {
        $data = $this->M_exbag->allrec();
        $this->load->view('v_main', $data);
    }

}
