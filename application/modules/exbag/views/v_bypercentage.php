<script>
jQuery(function(){

    var chart = AmCharts.makeChart( "chartByPercentage", {
        "type": "pie",
        "theme": "light",
        "marginTop": 40,
        "marginBottom": 40,
        "dataProvider": [
            {
            <?php 
            $wd = get_xbaggage($depStn, 'WD', $sDate, $eDate);
            $pd = get_xbaggage($depStn, 'PD', $sDate, $eDate);
            $wdPercent = ($wd['total'] / ($wd['total']+$pd['total']))*100;
            $pdPercent = ($pd['total'] / ($wd['total']+$pd['total']))*100;               
            ?>
            <?php if ($status == 'ALL') { ?>
            "type": "Waived Baggage",            
            "total": <?php echo round($wdPercent,2);?>,
            "color": "#C8D046",
            },
            {
            "type": "Paid Baggage",            
            "total": <?php echo round($pdPercent,2);?>,
            "color": "#4B77BE",
            },
            <?php } else if ($status == 'WD') { ?>
            "type": "Waived Baggage",            
            "total": <?php echo round($wdPercent,2);?>,
            "color": "#C8D046",
            },
            <?php } else if ($status == 'PD') { ?>
            "type": "Paid Baggage",            
            "total": <?php echo round($pdPercent,2);?>,
            "color": "#4B77BE",
            },
            <?php } ?>
        ],
        "valueField": "total",
        "titleField": "type",
        "colorField": "color",
        "startDuration": 1,
        "labelsEnabled": true,
        "labelRadius": 1,
        "labelText": "[[title]]: <br>[[value]] %",
        "fontSize": 13,
        "innerRadius": "20%",        
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]] %</b></span>",
        "angle": 10,
        "outlineAlpha": 0.4,
        "radius": 150,
        "export": {
            "enabled": true
        },                    
        "allLabels": [
        {
          <?php if ($status == 'ALL') { ?>
          "text": "StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], Station[<?php echo $depStn;?>], \nTotal Waived Bag[<?php echo $wd['total'].'kg';?>], Total Paid Bag[<?php echo $pd['total'].'kg';?>]",
          <?php } else if ($status == 'WD') { ?>
            "text": "StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], Station[<?php echo $depStn;?>], \nTotal Waived Bag[<?php echo $wd['total'].'kg';?>]",
          <?php } else if ($status == 'PD') { ?>
            "text": "StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], Station[<?php echo $depStn;?>], \nTotal Paid Bag[<?php echo $pd['total'].'kg';?>]",
          <?php } ?>
          "bold": false,
          "x": 0,
          "y": 0
        }
        ]
    });

}); 
</script>
