<script>
jQuery(function(){

    var chart = AmCharts.makeChart( "chartPerStation", {
        "type": "pie",
        "theme": "light",
        "marginTop": 40,
        "marginBottom": 40,
        "dataProvider": [
            <?php 
            if ($perstn == 'all') {
                $arrStation = get_station();
                foreach ($arrStation as $key => $row) {
            ?>
                {
                "station": "<?php echo $row['STN'];?>",
                "total": <?php echo get_xbaggage($row['STN'], $perstatus, $sDate, $eDate);?>,
                },
            <?php 
                }
            } else {
                if ($perstatus == 'WD') {
            ?>
                    {
                    "type": "Waived",
                    "total": <?php echo get_xbaggage($perstn, 'WD', $sDate, $eDate);?>,
                    },
            <?php
                } else if ($perstatus == 'PD') {
            ?>
                    {
                    "type": "Paid",
                    "total": <?php echo get_xbaggage($perstn, 'PD', $sDate, $eDate);?>,
                    },
            <?php
                } else if ($perstatus == 'ALL') {
            ?>                
                    {
                    "type": "Waived",
                    "total": <?php echo get_xbaggage($perstn, 'WD', $sDate, $eDate);?>,
                    },
                    {
                    "type": "Paid",
                    "total": <?php echo get_xbaggage($perstn, 'PD', $sDate, $eDate);?>,
                    },
            <?php
                }
            }
            ?> 
        ],
        "valueField": "total",
        <?php ($perstn == 'all') ? $titleField = "station" : $titleField = "type"; ?>
        "titleField": "<?php echo $titleField;?>",
        "startDuration": 1,
        "labelsEnabled": true,
        "labelRadius": 1,
        "labelText": "[[title]]: [[value]]",
        "innerRadius": "20%",        
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]] kg</b></span>",
        "angle": 10,
        "outlineAlpha": 0.4,
        "radius": 150,
        "export": {
            "enabled": true
        },                    
        "allLabels": [
        {
          "text": "Filter : Station[<?php echo $perstn;?>], StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], ExcessStatus[<?php echo $perstatus;?>]",
          "bold": false,
          "x": 0,
          "y": 0
        }
        ]
    });

}); 
</script>
