<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> DATA RECORD</span>
            <span class="caption-helper"><?php echo $status;?> Baggage <?php echo $depStn;?> - <?php echo $arrStn;?> Route, between Date <?php echo date('d-m-Y',strtotime($sDate));?> to <?php echo date('d-m-Y',strtotime($eDate));?></span>
        </div>                      
    </div>
    <div class="portlet-body">    
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th> Flight Date </th>
                        <th> Flight Number </th>
                        <th> Departure </th>
                        <th> Arrival </th>                        
                        <th> Passenger </th>
                        <th> Ticket Number </th>
                        <th> Class </th>
                        <th> Subclass </th>
                        <th> Seat No </th>
                        <th> Excess<br/>Weight (kg) </th>
                        <th> FQTV Tier </th>
                        <th> FQTV Number </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>                        
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    foreach ($arrRec as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td align="center"> <?php echo $row['flight_date'];?> </td>
                        <td align="center"> <?php echo $row['flight_number'];?> </td>
                        <td align="center"> <?php echo $row['dep_station'];?> </td>
                        <td align="center"> <?php echo $row['arr_station'];?> </td>
                        <td> <?php echo $row['pass_name'];?> </td>
                        <td align="center"> <?php echo $row['ticket_no'];?> </td>
                        <td align="center"> <?php echo $row['cabin_class'];?> </td>
                        <td align="center"> <?php echo $row['sub_class'];?> </td>
                        <td align="center"> <?php echo $row['seat_no'];?> </td>
                        <td align="right"> <?php echo $row['total_bag'];?> </td>
                        <td> <?php echo $row['fqtv_card'];?> </td>
                        <td align="center"> <?php echo $row['fqtv_number'];?> </td>
                    </tr>
                    <?php $i++; } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>