<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

<?php if ($status == 'ALL') { ?>
<div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-yellow-soft">
                    <span data-counter="counterup" data-value=""></span>
                    <small class="font-yellow-soft">TOTAL BAG WAIVED</small>
                </h3>
                <small></small>
            </div>
            <div class="icon">
                <i class="fa fa-exclamation-circle"></i>
            </div>                        
        </div>
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Passenger : <?php echo $wd['totalpax'];?></div>
                <div class="status-number"></div>                            
            </div>                                      
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>    
        </div>   
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Baggage : <?php echo $wd['total'];?> kg</div>
                <div class="status-number"></div>                            
            </div>    
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>                                
        </div>        
    </div>
</div>
<div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-blue-steel">
                    <span data-counter="counterup" data-value=""></span>
                    <small class="font-blue-steel">TOTAL BAG PAID</small>
                </h3>
                <small></small>
            </div>
            <div class="icon">
                <i class="fa fa-money"></i>
            </div>                        
        </div>
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Passenger : <?php echo $pd['totalpax'];?></div>
                <div class="status-number"></div>                            
            </div>                                      
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>    
        </div>   
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Baggage : <?php echo $pd['total'];?> kg</div>
                <div class="status-number"></div>                            
            </div>    
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>                                
        </div>          
    </div>
</div>
<?php } ?>
<?php if ($status == 'WD') { ?>
<div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-yellow-soft">
                    <span data-counter="counterup" data-value=""></span>
                    <small class="font-yellow-soft">TOTAL BAG WAIVED</small>
                </h3>
                <small></small>
            </div>
            <div class="icon">
                <i class="fa fa-exclamation-circle"></i>
            </div>                        
        </div>
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Passenger : <?php echo $wd['totalpax'];?></div>
                <div class="status-number"></div>                            
            </div>                                      
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>    
        </div>   
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Baggage : <?php echo $wd['total'];?> kg</div>
                <div class="status-number"></div>                            
            </div>    
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>                                
        </div>        
    </div>
</div>
<?php } ?>
<?php if ($status == 'PD') { ?>
<div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-blue-steel">
                    <span data-counter="counterup" data-value=""></span>
                    <small class="font-blue-steel">TOTAL BAG PAID</small>
                </h3>
                <small></small>
            </div>
            <div class="icon">
                <i class="fa fa-money"></i>
            </div>                        
        </div>
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Passenger : <?php echo $pd['totalpax'];?></div>
                <div class="status-number"></div>                            
            </div>                                      
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>    
        </div>   
        <div class="progress-info">
            <div class="status">
                <div class="status-title" style="font-size:15px;">Amount of Baggage : <?php echo $pd['total'];?> kg</div>
                <div class="status-number"></div>                            
            </div>    
            <div class="progress">
                <span style="width: %;" class="progress-bar progress-bar-success blue-steel">
                    <span class="sr-only"> on schedule</span>
                </span>
            </div>                                
        </div>          
    </div>
</div>
<?php } ?>
