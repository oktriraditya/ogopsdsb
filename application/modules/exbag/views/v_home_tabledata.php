<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> DATA RECORD</span>
            <span class="caption-helper">Excess Baggage <?php echo $depStn;?> Station, between Date <?php echo date('d-m-Y',strtotime($sDate));?> to <?php echo date('d-m-Y',strtotime($eDate));?></span>
        </div>                      
    </div>
    <div class="portlet-body">    
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th width="10%"> Station </th>
                        <?php if ($status == 'ALL') { ?>
                        <th width="20%" align="right"> Total Passenger (Bag Waived) </th>
                        <th width="20%" align="right"> Total Baggage Waived (kg) </th>                        
                        <th width="20%" align="right"> Total Passenger (Bag Paid) </th>
                        <th width="20%" align="right"> Total Baggage Paid (kg) </th>                        
                        <?php } else if ($status == 'WD') { ?>
                        <th width="20%" align="right"> Total Passenger (Bag Waived) </th>
                        <th width="20%" align="right"> Total Baggage Waived (kg) </th>                        
                        <?php } else if ($status == 'PD') { ?>
                            <th width="20%" align="right"> Total Passenger (Bag Paid) </th>
                        <th width="20%" align="right"> Total Baggage Paid (kg) </th>                        
                        <?php } ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    <?php if ($status == 'ALL') { ?>
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                        <th> </th>
                        <th> </th>                  
                    <?php } else { ?>      
                        <th> </th>
                        <th> </th>
                        <th> </th>                        
                    <?php } ?>
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    foreach ($arrRec as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td> <?php echo $row['dep_station'];?> </td>
                        <?php if ($status == 'ALL') { ?>
                        <td align="right"> <?php echo $row['total_wd'];?> </td>
                        <td align="right"> <?php echo $row['total_bag_w'];?> </td>
                        <td align="right"> <?php echo $row['total_pd'];?> </td>
                        <td align="right"> <?php echo $row['total_bag_p'];?> </td>
                        <?php } else if ($status == 'WD') { ?>
                        <td align="right"> <?php echo $row['total_wd'];?> </td>
                        <td align="right"> <?php echo $row['total_bag_w'];?> </td>
                        <?php } else if ($status == 'PD') { ?>
                        <td align="right"> <?php echo $row['total_pd'];?> </td>
                        <td align="right"> <?php echo $row['total_bag_p'];?> </td>
                        <?php } ?>
                    </tr>
                    <?php $i++; } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>