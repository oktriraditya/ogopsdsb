<script>
jQuery(function(){
    var chart = AmCharts.makeChart( "chartCountStat", {
        "type": "serial",
        "theme": "none",
        "marginRight": 60,
        "marginLeft": 20,
        "marginTop": 40,
        "autoMarginOffset": 40,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v2",
            "position": "right",
            "ignoreAxisWidth":true,
            "axisAlpha": 0.1,
            "gridAlpha": 0.1
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        dataProvider: [ 
        <?php          
        $arrDay = getDatesFromRange($sDate, $eDate);
        foreach($arrDay as $sDay) {
            echo "{";
            echo "'date': '".$sDay."',";            
            if ($status == 'ALL') {
                $arrWD = listExcess($sDay, $depStn, 'WD');
                $arrPD = listExcess($sDay, $depStn, 'PD');  
                echo "WD: ".$arrWD.",";
                echo "PD: ".$arrPD.",";            
            } else {
                $arrExcess = listExcess($sDay, $depStn, $status);
                echo "Excess: ".$arrExcess.",";
            }            
            echo "},";        
        }
        ?>
        ],      
        "graphs": [
          <?php if ($status == 'ALL') { ?>
          {
            valueAxis: "v2",
            valueField: "WD",
            title: "Waived Baggage",
            type: "column",
            fillColors : "#F3C200",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span>",        
            alphaField: "alpha",
          }, 
          {    
            valueAxis: "v2",
            valueField: "PD",
            title: "Paid Baggage",
            type: "column",
            fillColors : "#4B77BE",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span> [[additional]]</span>",        
            alphaField: "alpha",
          },
        <?php } else { ?>      
          {
            valueAxis: "v2",
            valueField: "Excess",
            title: "<?php echo ($status == 'WD') ? "Waived Baggage" : "Paid Baggage"; ?>",
            type: "column",
            fillColors : "<?php echo ($status == 'WD') ? "#F3C200" : "#4B77BE"; ?>",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span> [[additional]]</span>",        
            alphaField: "alpha",
          },
        <?php } ?>
        ],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left",
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
          "oppositeAxis":false,
          "offset":50,
          "scrollbarHeight":10
        },
        "depth3D": 15,
        "angle": 20,        
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
        {
          "text": "Filter : Station:[<?php echo $depStn;?>], StartDate:[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate:[<?php echo date('d-m-Y',strtotime($eDate));?>], ExcessStatus:[<?php echo $status;?>]",
          "bold": false,
          "x": 0,
          "y": 0
        }
	    ]
    });     
}); 
</script>