<script>
jQuery(function(){
    var chart = AmCharts.makeChart( "chartByStation", {
        "type": "serial",
        "theme": "none",
        "marginRight": 60,
        "marginLeft": 20,
        "marginTop": 40,
        "autoMarginOffset": 40,
        "dataDateFormat": "YYYY-MM-DD",        
        "valueAxes": [{
            "stackType": "regular",
            "position": "right",
            "axisAlpha": 0,
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        dataProvider: [ 
        <?php          
        $arrStation = get_stations();
        foreach ($arrStation as $key => $row) {
            echo "{";
            echo "station: '".$row['STN']."',";                
            $arrWD = get_xbaggage($row['STN'], 'WD', $sDate, $eDate);
            $arrPD = get_xbaggage($row['STN'], 'PD', $sDate, $eDate);
            echo "WD: ".$arrWD.",";
            echo "PD: ".$arrPD.",";           
            echo "},";             
        }
        ?>
        ],      
        "graphs": [
          {
            valueAxis: "v2",
            valueField: "WD",
            title: "Waived Baggage",
            type: "column",
            fillColors : "#D91E18",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span>",        
            alphaField: "alpha",
          }, 
          {    
            valueAxis: "v2",
            valueField: "PD",
            title: "Paid Baggage",
            type: "column",
            fillColors : "#26C281",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span> [[additional]]</span>",        
            alphaField: "alpha",
          },        
        ],
        "categoryField": "station",
        "categoryAxis": {
          "minPeriod": "mm",
          "autoGridCount": false,
          "equalSpacing": true,
          "gridCount": 1000,
          "labelRotation": 90, 
          "axisHeight": 50,
        },       
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
        {
          "text": "Filter : StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>]",
          "bold": false,
          "x": 0,
          "y": 0
        }
	    ]
    });     
}); 
</script>