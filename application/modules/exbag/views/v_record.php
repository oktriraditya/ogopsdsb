<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">
function loadTable() {
    blockUI(boxTable);
    var sDate = $("#sDate").val();
    var eDate = $("#eDate").val();
    var depStn = $("#depstn").val(); 
    var arrStn = $("#arrstn").val(); 
    var status = $("#status").val(); 

    $.ajax({
        type    : "POST",
        url     : "<?php echo base_url(); ?>exbag/tableAllRec",
        data    : {"sDate": sDate, "eDate": eDate, "depStn": depStn, "arrStn": arrStn, "status": status},
        success :
            function(msg) {
                $("#boxTable").html(msg);
                unblockUI(boxTable);            
            },
        error   : function(){
                unblockUI(boxTable);
                $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
        }
    });
}

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}


jQuery(document).ready(function() {     
    loadTable();
    
    $('#submit').click(function(){
        loadTable();
    });
});
</script>
                      

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height blue">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>                                
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->           
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Dashboard</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->        
        <!-- BEGIN PAGE BASE CONTENT -->  
        <div class="row">
            <div class="col-md-12">                
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Start Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" value="<?php echo date("Y-m-d");?>" name="date" id="sDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>                            
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">End Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" value="<?php echo date('Y-m-d');?>" name="date" id="eDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>                            
                        </div>                    
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Departure</span>
                                </button>
                            </span>
                            <select class="form-control input-small selectStn" name="depstn" id="depstn">
                                <option value="all">All</option>
                                <?php foreach ($arrStation as $key => $row) {?>
                                <option value="<?php echo $row['STN'];?>"><?php echo $row['STN'];?></option>
                                <?php } ?>
                            </select>
                        </div> 
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Arrival</span>
                                </button>
                            </span>
                            <select class="form-control input-small selectStn" name="arrstn" id="arrstn">
                                <option value="all">All</option>
                                <?php foreach ($arrStation as $key => $row) {?>
                                <option value="<?php echo $row['STN'];?>" ><?php echo $row['STN'];?></option>
                                <?php } ?>
                            </select>
                        </div>                      
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Excess Status</span>
                                </button>
                            </span>
                            <select class="form-control input-small selectStatus" name="status" id="status">
                                <option value="ALL">All</option>
                                <option value="WD">Waived</option>
                                <option value="PD">Paid</option>                                
                            </select>
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>    
                </div>                
            </div>
        </div>        
        <div id="boxTable"></div>                       
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT