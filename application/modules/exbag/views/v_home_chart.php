<script>
jQuery(function(){
    var chart = AmCharts.makeChart( "chartByStation", {
        "type": "serial",
        "theme": "none",
        "marginRight": 60,
        "marginLeft": 20,
        "marginTop": 40,
        "autoMarginOffset": 40,
        "dataDateFormat": "YYYY-MM-DD",    
        "startDuration": 1,    
        "valueAxes": [{
            "stackType": "3d",
            "position": "right",
            "axisAlpha": 0,
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        dataProvider: [ 
        <?php
        if ($depStn == 'all') {
          $arrStation = get_station();
          foreach ($arrStation as $key => $row) {
              echo "{";
              echo "station: '".$row['STN']."',";   
              $arrPass = getNumPass($row['STN'], $sDate, $eDate);             
              echo "pass: '".$arrPass."',";   
              $arrWD = get_xbaggage($row['STN'], 'WD', $sDate, $eDate);
              $arrPD = get_xbaggage($row['STN'], 'PD', $sDate, $eDate);   
              $arrTotal = $arrWD + $arrPD;           
              if ($status == 'WD') echo "exbag: '".$arrWD."',";
              else if ($status == 'PD') echo "exbag: '".$arrPD."',";           
              else if ($status == 'ALL') echo "exbag: '".$arrTotal."',";           
              echo "},";             
          }
        } else {
              echo "{";
              echo "station: '".$depStn."',";                
              $arrPass = getNumPass($depStn, $sDate, $eDate);             
              echo "pass: '".$arrPass."',";   
              $arrWD = get_xbaggage($depStn, 'WD', $sDate, $eDate);
              $arrPD = get_xbaggage($depStn, 'PD', $sDate, $eDate);
              $arrTotal = $arrWD + $arrPD;           
              if ($status == 'WD') echo "exbag: '".$arrWD."',";
              else if ($status == 'PD') echo "exbag: '".$arrPD."',";           
              else if ($status == 'ALL') echo "exbag: '".$arrTotal."',";           
              echo "},";             
        }
        ?>
        ],      
        "graphs": [
          {
            valueAxis: "v2",
            valueField: "pass",
            title: "Total Passenger",
            type: "column",
            fillColors : "#3598DC",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]]</span>",        
            alphaField: "alpha",
          }, 
          {    
            valueAxis: "v2",
            valueField: "exbag",
            title: "Excess Baggage Amount (kg)",
            type: "column",
            fillColors : "#F4D03F",
            fillAlphas: 0.8,
            balloonText: "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]] kg</span> [[additional]]</span>",        
            alphaField: "alpha",
          },        
        ],
        "categoryField": "station",
        "categoryAxis": {
          "minPeriod": "mm",
          "autoGridCount": false,
          "equalSpacing": true,
          "gridCount": 1000,
          "labelRotation": 90, 
          "axisHeight": 50,
          "gridPosition": "start",
          "title": "Station",
        },        
        "plotAreaFillAlphas": 0.1,
        "depth3D": 60,
        "angle": 30,         
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "align" : "center",
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
        {
          "text": "Filter : StartDate:[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate:[<?php echo date('d-m-Y',strtotime($eDate));?>], Station:[<?php echo $depStn;?>], ExcessBaggageType:[<?php echo $status;?>]",
          "bold": false,
          "x": 0,
          "y": 0
        }
	    ]
    });     
}); 
</script>