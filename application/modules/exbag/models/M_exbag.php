<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_exbag extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function home() {
        $data['arrStation'] = get_station();
        $data['title'] = 'Excess Baggage';
        $data['title_small'] = 'Report';
        $data['view'] = 'v_home';

        return $data;
    }

    function allrec() {
        $data['arrStation'] = get_station();
        $data['title'] = 'Excess Baggage';
        $data['title_small'] = 'all record';
        $data['view'] = 'v_record';

        return $data;
    }

    function error404() {
        $data['title'] = 'Excess Baggage';
        $data['title_small'] = 'Report';
        $data['view'] = 'v_404';

        return $data;
    }

}
