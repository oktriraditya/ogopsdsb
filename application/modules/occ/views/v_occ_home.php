<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/dashboard.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/css/all.css" rel="stylesheet" type="text/css" />

<style type="text/css">
#daily {
    cursor: pointer;
}
#cDelayAll, #cDelayCGK {
    height: 370px;
    overflow: hidden;
}
</style>
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">
//autorefresh
function refresh(){
    refreshcon = setInterval(function(){
        $('#autoref').load('<?php echo base_url();?>occ/home_autoref', function stop(){clearInterval(refreshcon);});
    }, 100000);
}

function loadDelayAll() {
    blockUI(cDelayAll);

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/causedelayall",
        success :
            function(msg) {
                $("#chartCdelayAll").html(msg);
                unblockUI(cDelayAll);            
            },
        error   : 
            function(){
                unblockUI(cDelayAll);
                $("#chartCdelayAll").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function loadDelayCGK() {
    blockUI(cDelayCGK);

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/causedelaycgk",
        success :
            function(msg) {
                $("#chartCdelayCGK").html(msg);
                unblockUI(cDelayCGK);
            },
        error   : 
            function(){
                unblockUI(cDelayCGK);
                $("#chartCdelayCGK").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}

jQuery(document).ready(function() {     
    refresh();    
    loadDelayAll();
    loadDelayCGK();
    
    $('#statistik').change(function () {
            if ($("#statistik").val() == "all") {
                $('#otpStats').show();
                $('#delayStats').show();
            }
            else if ($("#statistik").val() == "otpStats") {
                $('#otpStats').show();
                $('#delayStats').hide();
            }
            else if ($("#statistik").val() == "delayStats") {
                $('#otpStats').hide();
                $('#delayStats').show();
            }
        });
});

//flight number table jquery database
function showFlightNumber() {
    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/home_autoref",
        success : function(msg){
                
                  },
        error   : function(){
                
                  }
            
    });
}

</script>



<!-- BEGIN CONTENT -->
<div id="autoref" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php //date_default_timezone_set('Asia/Jakarta'); 
                echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>                

                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>  
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->  
        <!-- BEGIN PAGE BASE CONTENT -->       
<!--        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Stats</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="statistik" id="statistik">
                                <option value="all">All</option>
                                <option value="otpStats">OTP Stats</option>
                                <option value="delayStats">Delay Stats</option>
                            </select>
                        </div>
                    </div>           
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn dark" id="submit">Show</button>
                            </span>
                        </div>
                    </div>           
                </div>                
            </div>
        </div>-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">OTP Dashboard</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->   
        <div id="otpStats"  class="row">
            <div  class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-steel">
                                <span data-counter="counterup" data-value="<?php echo $nOnschedule;?>"></span>
                                <small class="font-blue-steel">flight</small>
                            </h3>
                            <small>TOTAL SCHEDULE ALL STATION</small>
                        </div>
                        <div class="icon">
                            <i class="fa fa-plane"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDeparted;?>%;" class="progress-bar progress-bar-success blue-steel">
                                <span class="sr-only"><?php echo $pDeparted;?> on schedule</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"></div>
                            <div class="status-number"> <?php echo $nDeparted;?> / <?php echo $pDeparted;?>% </div>                            
                        </div>                        
                    </div>    
                    <br/>
                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/all"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>            
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green-jungle">
                                <span data-counter="counterup" data-value="<?php echo $pOTP;?>"></span>
                                <small class="font-green-jungle">%</small>
                            </h3>
                            <small>OTP Departure all station  until <?php echo $jam;?></small>
                        </div>
                        <div class="icon">
                            <i class="fas fa-plane-departure"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDelay;?>%;" class="progress-bar progress-bar-success green-jungle">
                                <span class="sr-only"><?php echo $pDelay;?>% delay percentage</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="pull-left"><div class="status-number">Ontime: <?php echo $nOntime;?></div></div>
                            <div class="pull-right">
                            <div class="status-title"></div>
                            <div class="status-number">Delay: <?php echo $nDelay;?> / <?php echo $pDelay;?>% </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/all"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>  
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-yellow-soft">
                                <span data-counter="counterup" data-value="<?php echo $pOTPArrival;?>"></span>
                                <small class="font-yellow-soft">%</small>
                            </h3>
                            <small>OTP Arrival All Station until <?php echo $jam;?></small>
                        </div>
                        <div class="icon">
                            <i class="fas fa-plane-arrival"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDelayArrival;?>%;" class="progress-bar progress-bar-success yellow-soft">
                                <span class="sr-only"><?php echo $pDelayArrival;?>% delay percentage</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="pull-left"><div class="status-number">Ontime: <?php echo $nOntimeArrival;?></div></div>
                            <div class="pull-right">
                            <div class="status-title"></div>
                            <div class="status-number">Delay: <?php echo $nDelayArrival;?> / <?php echo $pDelayArrival;?>% </div>
                            </div>
                            
                        </div>
                    </div>
                    <br/>
<!--                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/all"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>-->
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-soft">
                                <span data-counter="counterup" data-value="<?php echo $nOnscheduleCgk;?>"></span>
                                <small class="font-blue-soft">flight</small>
                            </h3>
                            <small>TOTAL SCHEDULE CGK Station</small>
                        </div>
                        <div class="icon">
                            <i class="fa fa-plane"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDepartedCgk;?>%;" class="progress-bar progress-bar-success blue-soft">
                                <span class="sr-only"><?php echo $pDepartedCgk;?> on schedule</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"></div>
                            <div class="status-number"> <?php echo $nDepartedCgk;?> / <?php echo $pDepartedCgk;?>% </div>
                        </div>
                    </div>
                    <br/>
                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/cgk"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green-jungle">
                                <span data-counter="counterup" data-value="<?php echo $pOTPCgk;?>"></span>
                                <small class="font-green-jungle">%</small>
                            </h3>
                            <small>OTP Departure CGK until <?php echo $jam;?></small>
                        </div>
                        <div class="icon">
                            <i class="fas fa-plane-departure"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDelayCgk;?>%;" class="progress-bar progress-bar-success green-jungle">
                                <span class="sr-only"><?php echo $pDelayCgk;?>% delay percentage</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="pull-left"><div class="status-number">Ontime: <?php echo $nOntimeCgk;?></div></div>
                            <div class="pull-right">
                            <div class="status-title"></div>
                            <div class="status-number">Delay: <?php echo $nDelayCgk;?> / <?php echo $pDelayCgk;?>% </div></div>
                        </div>
                    </div>
                    <br/>
                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/cgk"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>    
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-yellow-soft">
                                <span data-counter="counterup" data-value="<?php echo $pOTPCgkArrival;?>"></span>
                                <small class="font-yellow-soft">%</small>
                            </h3>
                            <small>OTP Arrival CGK until <?php echo $jam;?></small>
                        </div>
                        <div class="icon">
                            <i class="fas fa-plane-arrival"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: <?php echo $pDelayCgkArrival;?>%;" class="progress-bar progress-bar-success yellow-soft">
                                <span class="sr-only"><?php echo $pDelayCgkArrival;?>% delay percentage</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="pull-left"><div class="status-number">Ontime: <?php echo $nOntimeCgkArrival;?></div></div>
                            <div class="pull-right">
                            <div class="status-title"></div>
                            <div class="status-number">Delay: <?php echo $nDelayCgkArrival;?> / <?php echo $pDelayCgkArrival;?>% </div></div>
                            
                        </div>
                    </div>
                    <br/>
<!--                    <div class="progress-info pull-right">
                        <a class="btn red btn-sm" href="<?php echo base_url();?>occ/daily/cgk"> More details
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>-->
                </div>
            </div>
        </div>    
<!--        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
                <div class="portlet light bordered">
                    <div class="portlet-title">
                    </div>
                    <div class="portlet-body">
                        <div id="chartOTPstats" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div>-->
<!--        <div id="otpStats" class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">            
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">ALL STATION</span>
                            <span class="caption-helper">flight status</span>
                        </div>
                        <div class="actions">
                            <a href="<?php echo base_url();?>occ/daily/all" class="btn btn-sm btn-circle dark">
                                More Details <i class="icon-arrow-right"></i> </a>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <div class="row">                                
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number dep" data-percent="<?php echo $pDeparted;?>">
                                        <?php echo $pDeparted;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/all/departed" class="title"><small>Departure:<?php echo $nDeparted;?></small></a>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number otp" data-percent="<?php echo $pOTP;?>">
                                        <?php echo $pOTP;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/all/ontime" class="title"><small>Ontime:<?php echo $nOntime;?></small> </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number delay" data-percent="<?php echo $pDelay;?>">
                                        <?php echo $pDelay;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/all/delay" class="title"><small>Delay:<?php echo $nDelay;?></small> </i></a>
                                </div>
                            </div>                               
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number cancel" data-percent="<?php echo $pCancel;?>">
                                        <?php echo $pCancel;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/all/cancel" class="title"><small>Cancel:<?php echo $nCancel;?></small></a>
                                </div>
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">CGK STATION</span>
                            <span class="caption-helper">flight status</span>
                        </div>
                        <div class="actions">
                            <a href="<?php echo base_url();?>occ/daily/cgk" class="btn btn-sm btn-circle dark">
                                 More Details <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <div class="row">                                
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number dep" data-percent="<?php echo $pDepartedCgk;?>">
                                        <?php echo $pDepartedCgk;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/cgk/departed" class="title"><small>Departure:<?php echo $nDepartedCgk;?></small></a>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number otp" data-percent="<?php echo $pOTPCgk;?>">
                                        <?php echo $pOTPCgk;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/cgk/ontime" class="title"><small>Ontime:<?php echo $nOntimeCgk;?></small></a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number delay" data-percent="<?php echo $pDelayCgk;?>">
                                        <?php echo $pDelayCgk;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/cgk/delay" class="title"><small>Delay:<?php echo $nDelayCgk;?></small></a>
                                </div>
                            </div>                               
                            <div class="col-md-3">
                                <div class="easy-pie-chart">
                                    <div class="number cancel" data-percent="<?php echo $pCancelCgk;?>">
                                        <?php echo $pCancelCgk;?>%</div> 
                                    <a href="<?php echo base_url();?>occ/record/cgk/cancel" class="title"><small>Cancel:<?php echo $nCancelCgk;?></small></a>
                                </div>
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        <div id="delayStats" class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >            
                <div class="portlet light bordered" id="cDelayAll">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">CAUSE OF DELAY ALL</span>
                        </div>
                        <div class="actions">
                            <a href="<?php echo base_url();?>occ/daily/all" class="btn btn-sm btn-circle dark">
                                 More Details <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <div id="chartCdelayAll" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  
                <div class="portlet light bordered" id="cDelayCGK">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">CAUSE OF DELAY CGK</span>
                        </div>
                        <div class="actions">
                            <a href="<?php echo base_url();?>occ/daily/cgk" class="btn btn-sm btn-circle dark">
                                 More Details <i class="icon-arrow-right"></i></a>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <div id="chartCdelayCGK" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
  
    </div>   
    <!-- END CONTENT BODY -->
    
    
</div>

<!-- END CONTENT -->