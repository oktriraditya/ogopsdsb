<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">
function loadRec() {
    blockUI(boxTable);
    var sDate = $("#sDate").val();
    var eDate = $("#eDate").val();
    var stn = $("#stn").val();
    var org = $("#org").val();
    var dest = $("#dest").val();
    var statuspp = $("#statuspp").val();
    var delay = 'all';

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/tableotpperday_v2",
        data    : {"datefrom": sDate, "dateto": eDate, "stn": stn, "org": org, "dest": dest, "statuspp": statuspp, "delay": delay},
        success :
            function(msg) {
                $("#boxTable").html(msg);
                unblockUI(boxTable);
            },
        error   :
            function(){
                unblockUI(boxTable);
                $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}

jQuery(document).ready(function() {
    loadRec();

    $('#submit').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();
        var eDate = $("#eDate").val();
        var stn = $("#stn").val();
        var org = $("#org").val();
        var dest = $("#dest").val();
        var statuspp = $("#statuspp").val();
        var delay = 'all';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tableotpperday_v2",
            data    : {"datefrom": sDate, "dateto": eDate, "stn": stn, "org": org, "dest": dest, "statuspp": statuspp, "delay": delay},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);
                },
            error   :
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });
    $('#org,#dest').change(function () {
            if ($("#org").val() == "all" || $("#dest").val() == "all") {
                $(':checkbox').each(function () {
                    //$('#statuspp').val("unchecked");
                    $(this).prop('disabled', true);
                });
            }
            else {
                $(':checkbox').each(function () {
                    $(this).prop('disabled', false);
                    $(this).prop('checked', false);
                    $('#statuspp').val("unchecked");
                });
            }
        });
    $('#statuspp').change(function () {
            if ($("#statuspp").is(":checked")) {
                $('#statuspp').val("checked");
            }
            else {
                $('#statuspp').val("unchecked");
            }
        });
});
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">On Time Performance</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">OTP Daily</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Service</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="stn" id="stn">
                                <option value="all">All</option>
                                <option value="dom">DOM</option>
                                <option value="int">INT</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Delay</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="delay" id="delay">
                                <option value="all">All</option>
                                <option value="origin">Origin</option>
                                <option value="previous">Previous</option>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Org</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="org" id="org">
                                <option value="all">All</option>
                                <?php foreach ($arrStation as $key => $row) {?>
                                <option value="<?php echo $row['STN'];?>" <?php if ($row['STN'] == strtoupper($stnto)) echo "selected=selected";?>><?php echo $row['STN'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Dest</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="dest" id="dest">
                                <option value="all">All</option>
                                <?php foreach ($arrStation as $key => $row) {?>
                                <option value="<?php echo $row['STN'];?>" <?php if ($row['STN'] == strtoupper($stnto)) echo "selected=selected";?>><?php echo $row['STN'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="form-control">
                            <label><input type="checkbox" disabled="true" id="statuspp" value=""> Return</label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd" data-date-end-date="0d">
                                <input type="text" class="form-control" name="datefrom" id="sDate" value="<?php echo get_dateotp('-7 days');?>">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="dateto" id="eDate" value="<?php echo date('Y-m-d');?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-right">
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-sm">
                                <small>2020 &copy; <a href="#">Made by: Taufan & Oktri - JKTOGL3</a></small>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div id="boxTable"></div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
