<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
    .no-wrap{
        white-space: nowrap;
    }
    tfoot input {
        width: 100%;
        margin: 1px;
        box-sizing: border-box;
    }

</style>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> <?php echo $stn; ?> Flight Record, <?php echo date('d-m-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-m-Y', strtotime($dateto)); ?></span>
        </div>  

    </div>

    <div class="portlet-body"> 
        <div class="tools"> </div> 
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="fltData" style="width:100%">
            <thead>
                <tr> 
                    <th class="tengah">No.</th>
                    <th class="tengah">Departure Date</th>
                    <th class="tengah">Flight Number</th>
                    <th class="tengah">Aircraft Reg</th>
                    <th class="tengah">Aircraft Type</th>
                    <th class="tengah">Origin</th>
                    <th class="tengah">Destination</th>
                    <th class="tengah">Actual Destination</th>
                    <th class="tengah">Standard Time Departure</th>
                    <th class="tengah">Standard Time Arrival</th>
                    <th class="tengah">Actual Time Departure</th>
                    <th class="tengah">Actual Time Arrival</th>
                    <th class="tengah">Delay Departure (mins)</th>
                    <th class="tengah">Delay Arrival (mins)</th>
                    <th class="tengah">CD1 Departure</th>
                    <th class="tengah">CD2 Departure</th>
                    <th class="tengah">Duration CD1 Departure (mins)</th>                
                    <th class="tengah">Duration CD2 Departure (mins)</th>
                    <th class="tengah">Divert Status</th>
                    <th class="tengah">Route</th>
                    <th class="tengah">Flight Status</th>
                    <th class="tengah">Service Type</th>
                    <th class="tengah">Suffix</th>
                    <th class="tengah">REMARKS</th>
                </tr>
            </thead>
            <?php
            $data = array();
            foreach ($daily_record_per_flight as $key => $value) {
                $data['TANGGAL'][] = $value['TANGGAL'];
                $data['FLTNUM'][] = $value['FLTNUM'];
                $data['AIRCRAFTREG'][] = $value['AIRCRAFTREG'];
                $data['AIRCRAFTTYPE'][] = $value['AIRCRAFTTYPE'];
                $data['SCHED_DEPARTUREAIRPORT'][] = $value['SCHED_DEPARTUREAIRPORT'];
                $data['SCHED_ARRIVALAIRPORT'][] = $value['SCHED_ARRIVALAIRPORT'];
                $data['LATEST_DEPARTUREAIRPORT'][] = $value['LATEST_DEPARTUREAIRPORT'];
                $data['LATEST_ARRIVALAIRPORT'][] = $value['LATEST_ARRIVALAIRPORT'];
                $data['SCHEDULED_DEPDT_LC'][] = $value['SCHEDULED_DEPDT_LC'];
                $data['SCHEDULED_ARRDT_LC'][] = $value['SCHEDULED_ARRDT_LC'];
                $data['ACTUAL_BLOCKOFF_LC'][] = $value['ACTUAL_BLOCKOFF_LC'];
                $data['ACTUAL_BLOCKON_LC'][] = $value['ACTUAL_BLOCKON_LC'];
                $data['OTPDEP'][] = $value['OTPDEP'];
                $data['OTPARR'][] = $value['OTPARR'];
                $data['OTPZMD'][] = $value['OTPZMD'];
                $data['delaylength'][] = date_diff(new DateTime($value['SCHEDULED_DEPDT_LC']), new DateTime($value['ACTUAL_BLOCKOFF_LC']));
                $data['delaylengtharr'][] = date_diff(new DateTime($value['SCHEDULED_ARRDT_LC']), new DateTime($value['ACTUAL_BLOCKON_LC']));
                $data['estimatedbt'][] = date_diff(new DateTime($value['SCHEDULED_DEPDT_LC']), new DateTime($value['SCHEDULED_ARRDT_LC']));
                $data['actualbt'][] = date_diff(new DateTime($value['ACTUAL_BLOCKOFF_LC']), new DateTime($value['ACTUAL_BLOCKON_LC']));
                $data['ACTUAL_TAXIOUT_LC'][] = date_diff(new DateTime($value['ACTUAL_TAKEOFF_LC']), new DateTime($value['ACTUAL_BLOCKOFF_LC']));
                $data['ACTUAL_TAXIIN_LC'][] = date_diff(new DateTime($value['ACTUAL_BLOCKON_LC']), new DateTime($value['ACTUAL_TOUCHDOWN_LC']));
                $data['ESTIMATED_TAXIOUT_LC'][] = date_diff(new DateTime($value['ESTIMATED_TAKEOFF_LC']), new DateTime($value['SCHEDULED_DEPDT_LC']));
                $data['ESTIMATED_TAXIIN_LC'][] = date_diff(new DateTime($value['SCHEDULED_ARRDT_LC']), new DateTime($value['ESTIMATED_TOUCHDOWN_LC']));
                $data['cd1'][] = $value['CD1'];
                $data['cd2'][] = $value['CD2'];
                $data['delaycd1'][] = $value['DELAYLENGTH1'];
                $data['delaycd2'][] = $value['DELAYLENGTH2'];
                $data['REMARKS'][] = $value['REMARKS'];
                $data['ROUTE'][] = $value['ROUTE'];
                $data['SUFFIX'][] = $value['SUFFIX'];
                $data['SERVICETYPE'][] = $value['SERVICETYPE'];
                $data['STATUS'][] = $value['STATUS'];
                /* $data['technic'][] = $value['TECH'];
                  $data['STNH'][] = $value['STNH'];
                  $data['COMM'][] = $value['COMM'];
                  $data['SYST'][] = $value['SYST'];
                  $data['FLOPS'][] = $value['FLOPS'];
                  $data['APTF'][] = $value['APTF'];
                  $data['WEATHER'][] = $value['WEATHER'];
                  $data['MISC'][] = $value['MISC']; */
            }
            $count = count($daily_record_per_flight);
            ?>
            <tfoot>
                <tr>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>

                </tr>
            </tfoot>
            <tbody>
                <?php
//                $domstn = array('AMQ', 'BDO', 'BDJ', 'BEJ', 'BIK', 'BKS', 'BMU', 'BPN', 'BTH', 'BTJ', 'BUW', 'BWX', 'CGK', 'DJB', 'DJJ', 'DPS', 'DTB', 'ENE'
//                    , 'FLZ', 'GNS', 'GTO', 'JOG', 'JBB', 'KDI', 'KNG', 'KNO', 'KOE', 'KTG', 'LBJ', 'LOP', 'LSW', 'LUV', 'LUW', 'MDC', 'MJU', 'MKW', 'MKQ'
//                    , 'MLG', 'MOF', 'NBX', 'PDG', 'PGK', 'PKN', 'PKU', 'PKY', 'PLM', 'PLW', 'PNK', 'PSU', 'SBG', 'SOC', 'SOQ', 'SQG', 'SRG', 'SUB', 'SWQ'
//                    , 'SXK', 'TIM', 'TJQ', 'TNJ', 'TRK', 'TKG', 'TMC', 'TTE', 'UPG', 'RAQ', 'BMU', 'LLO', 'WNI', 'KSR');
//                $intstn = array('PEK', 'BKK', 'CAN', 'HKG', 'ICN', 'JED', 'KIX', 'KUL', 'MEL', 'NRT', 'HND', 'PER', 'PVG', 'SIN', 'AMS', 'SYD', 'LHR', 'MED', 'BOM', 'CTU', 'XIY', 'CGO');
                $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
                $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
                $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
                $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
                $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
                $arrSYST = array('50', '55', '56', '57', '58');
                $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
                $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');

                $arrAPTFP = array('80');
                $arrCOMCP = array('30');
                $arrFLOPP = array('60');
                $arrOTHRP = array('90');
                $arrSTNHP = array('10', '20');
                $arrSYSTP = array('50');
                $arrTECHP = array('40');
                $arrWTHRP = array('70');

                $arrAPTFO = array('81', '82', '83', '84', '85', '86', '87', '88', '89');
                $arrCOMCO = array('09', '14', '16', '25', '91', '92');
                $arrFLOPO = array('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
                $arrOTHRO = array('51', '52', '93', '97', '98', '99');
                $arrSTNHO = array('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
                $arrSYSTO = array('55', '56', '57', '58');
                $arrTECHO = array('41', '42', '43', '44', '45', '46', '47', '48');
                $arrWTHRO = array('71', '72', '73', '75', '76', '77');

                $remark[1] = "Flight Deck Crew Late On-Board due to Transport";
                $remark[2] = "Cabin Crew Late On-Board due to Transport";
                $remark[9] = "Schedule Ground Time Less than Declared Minimum Ground Time, Minimum Connection Time";
                $remark[10] = "Late Arrival from Previous Station due to Pax and Baggage";
                $remark[11] = "Late Check-In, Acceptance after Deadline";
                $remark[12] = "Late Check-In, Congestion in Check-In Area";
                $remark[13] = "Check-In Error, Passenger and Baggage";
                $remark[14] = "Oversales, Booking Errors for Passenger and Baggage";
                $remark[15] = "Boarding, Disrepancies and Paging, etc";
                $remark[16] = "Commercial Publicity / Passenger Convenience, VIP, Press, Ground Meals and missing Personal Items";
                $remark[17] = "Catering Order, Late or Incorrect Order, etc";
                $remark[18] = "Baggage Processing, Sorting, etc";
                $remark[20] = "Late Arrival from Previous Station due to Cargo, Mail and Ramp Handling";
                $remark[21] = "Cargo Documentation, Error, Etc";
                $remark[22] = "Cargo Late Positioning";
                $remark[23] = "Cargo Late Acceptance";
                $remark[24] = "Cargo Inadequate Packing";
                $remark[25] = "Oversales, Booking Errors for Cargo and Mail";
                $remark[26] = "Cargo Late Preparation in Warehouse, Incorrect Build Ups of ULD's";
                $remark[27] = "Cargo Documentation, Packing, etc";
                $remark[28] = "Mail Late Positioning";
                $remark[29] = "Mail Late Acceptance";
                $remark[30] = "Late Arrival from Previous Station due to Commercial";
                $remark[31] = "Aircraft Documentation Late / Inaccurate, Weight and Balance, General Declaration, Pax Manifest, Etc";
                $remark[32] = "Loading / Unloading, Bulky, Special Load, etc";
                $remark[33] = "Loading Equipment, Lack of Breakdown, etc";
                $remark[34] = "Servicing Equipment, Lack of Breakdown, etc";
                $remark[35] = "Aircraft Cleaning";
                $remark[36] = "Fueling / Defueling, Fuel Supplier";
                $remark[37] = "Catering, Late Delivery or Loading";
                $remark[38] = "ULD, Lack of Serviceability";
                $remark[39] = "Technical Equipment, Lack of Breakdown, Lack of Staff, e.g. Push Back";
                $remark[40] = "Late Arrival from Previous Station due to Technical";
                $remark[41] = "Aircraft Defects";
                $remark[42] = "Scheduled Maintenance, Late Release";
                $remark[43] = "Non Sched Maintenance, Special Checks and / or Additional Works beyond normal maintenance Sched";
                $remark[44] = "Spares and Maintenance Equipment, Lack of or Breakdown";
                $remark[45] = "AOG Spares, to be carried to another Station";
                $remark[46] = "Aircraft change, for Technical Reasons";
                $remark[47] = "No Standby Aircraft, Lack of Planned Standby Aircraft for Tech Reasons";
                $remark[48] = "Scheduled Cabin Configuration / Version Adjustment, Install / Uninstall Stretcher Case";
                $remark[50] = "Late Arrival from Previous Station due to System";
                $remark[51] = "Damage During Flight Operations, Bird or Lightning Strike, Turb, Heavy ir Overweight Loading, Collision";
                $remark[52] = "Damage during Ground Operations Collisions, Loading / Off-Loading Damage, Contamination, Towing, Extreme Weather Conditions";
                $remark[55] = "Departure Control";
                $remark[56] = "Cargo Preparation / Documentation";
                $remark[57] = "Flight Plans Computer";
                $remark[58] = "Other Automation System Forrmat";
                $remark[60] = "Late Arrival from Previous Station due to Flight Operation and Crewing";
                $remark[61] = "Flight Plan late Completion or change of Flight Documentation";
                $remark[62] = "Operational Requirement, Fuel Load Alternation";
                $remark[63] = "Late Crew Boarding or Departure Procedures, other than connection and Standby";
                $remark[64] = "Flight Deck Crew Shortage / Sickness, Awaiting Standby, Flight Time Limitations, Valid Visa, Health";
                $remark[65] = "Flight Deck Crew Special Requirements, but no within Operational Requirement";
                $remark[66] = "Late Cabin Crew Boarding or Departure Procedures, other than connection and  Standby";
                $remark[67] = "Cabin Crew Shortage, Sickness, Awaiting Standby, Flight Time Limitations, Crew Meals, Health Documents";
                $remark[68] = "Cabin Crew Error or Special Request, not within operational requirements";
                $remark[69] = "Captain request for security check, extra ordinary";
                $remark[70] = "Late Arrival from Previous Station due to Weather";
                $remark[71] = "Departure Station";
                $remark[72] = "Destination Station";
                $remark[73] = "En-route or Alternate";
                $remark[75] = "De-Icing of Aircraft, Removal of Ice and / or Snow, Frost Prevention Excluding Unserviceability";
                $remark[76] = "Removal of Snow, Ice, Water and Sand from Airport";
                $remark[77] = "Ground Handling Impaired by Adverse Weather Conditions";
                $remark[80] = "Late Arrival from Previous Station due to Airport and Governmental Authorities";
                $remark[81] = "ATFM due to ATC En-Route Demand / Capacity, Standard Demand / Capacity Problems";
                $remark[82] = "ATFM due to ATC Staff / Equipment En-Route Reduced Capacity caused by Industrial Action or Staff or Equipment Failure, Extraordinary Demand due to Cap";
                $remark[83] = "ATFM due to Restriction at Destination Airport, Airport and / or Runway Closed due to Obstruction, Industrial Action, Staff Shortage, Political Unrest";
                $remark[84] = "ATFM due to Weather at Destination";
                $remark[85] = "Mandatory Security";
                $remark[86] = "Immigration, Customs, Health";
                $remark[87] = "Airport Facilities, Parking Stands, Ramp Congestion, Lighting, Buildings, gate Limititations, etc";
                $remark[88] = "Restriction at Airport of Destination, Airport and / or Runway Closed due to obstruction, Industrial Action, Staff Shortage, Political Unrest, Noise A";
                $remark[89] = "Restriction at Airport of Departure with or without ATFM Restrictions, Including Air Traffic Services, Start-Up and Push Back, Airport and / or Runway";
                $remark[90] = "Late Arrival from Previous Station due to Miscelleaneous";
                $remark[91] = "Load Connection, awaiting Load from Another Flight";
                $remark[92] = "Through Check-In Error, Passenger and Baggage";
                $remark[93] = "Aircraft Rotation, Late of Aircraft from another flight or Previous Sector without Delay Code";
                $remark[94] = "Cabin Crew Rotations, Awaiting Cabin Crew from other flight";
                $remark[95] = "Crew Rotation, Awaiting Crew from Other Flight (Flight Deck or Entire Crew)";
                $remark[96] = "Operations Control, Rerouting, Diversion, Consolidation, Aircraft change for reasons other than technical";
                $remark[97] = "";
                $remark[98] = "";
                $remark[99] = "";
                $remark["00"] = "";
                $remark[0] = "";

                $sDiff = '+000000015 00:00:00.000000000';
                $firstdep = array();
                $delaylengthmin = 0;
                $delaylengthplus = 0;
                for ($i = 0; $i < $count; $i++) {
                    ?>
                    <tr class="odd gradeX tengah">
                        <td> <?php echo $i + 1; ?> </td>
                        <td> <?php echo $data['TANGGAL'][$i]; ?></td>
                        <td> <?php echo $data['FLTNUM'][$i]; ?> </td>
                        <td> <?php echo substr($data['AIRCRAFTREG'][$i], 3); ?></td>
                        <td> <?php echo $data['AIRCRAFTTYPE'][$i]; ?></td>
                        <td> <?php echo $data['SCHED_DEPARTUREAIRPORT'][$i]; ?></td>
                        <td> <?php echo $data['SCHED_ARRIVALAIRPORT'][$i]; ?></td>
                        <td> <?php echo $data['LATEST_ARRIVALAIRPORT'][$i]; ?></td>
                        <td> <?php echo $data['SCHEDULED_DEPDT_LC'][$i]; ?></td>
                        <td> <?php echo $data['SCHEDULED_ARRDT_LC'][$i]; ?></td>
                        <td> <?php echo $data['ACTUAL_BLOCKOFF_LC'][$i]; ?></td>
                        <td> <?php echo $data['ACTUAL_BLOCKON_LC'][$i]; ?></td>

                        <td> <?php
                            if (!empty($data['ACTUAL_BLOCKOFF_LC'][$i])) {
                                if ($data['delaylength'][$i]->invert) {
                                    $delaylengthmin = -1 * $data['delaylength'][$i]->h * 60 + -1 * $data['delaylength'][$i]->i;
                                    echo $delaylengthmin;
                                } else {
                                    $delaylengthplus = $data['delaylength'][$i]->h * 60 + $data['delaylength'][$i]->i;
                                    echo $delaylengthplus;
                                }
                            }
                            ?></td>
                        <td> <?php
                            if (!empty($data['ACTUAL_BLOCKON_LC'][$i])) {
                                if ($data['delaylengtharr'][$i]->invert) {
                                    $delaylengthmin = -1 * $data['delaylengtharr'][$i]->h * 60 + -1 * $data['delaylengtharr'][$i]->i;
                                    echo $delaylengthmin;
                                } else {
                                    $delaylengthplus = $data['delaylengtharr'][$i]->h * 60 + $data['delaylengtharr'][$i]->i;
                                    echo $delaylengthplus;
                                }
                            }
                            ?></td>
                        <td> <?php
                            if ($data['cd1'][$i] != "00")
                                echo $data['cd1'][$i];
                            else
                                echo "";
                            ?></td>
                        <td> <?php
                            if ($data['cd2'][$i] != "00")
                                echo $data['cd2'][$i];
                            else
                                echo "";
                            ?></td>
                        <td> <?php
                            //echo substr($data['delaycd1'][$i],2); 
                            if (strpos(substr($data['delaycd1'][$i], 2), "H") !== false && strpos(substr($data['delaycd1'][$i], 2), "M") !== false) {
                                $hour = explode("H", substr($data['delaycd1'][$i], 2)); //$mins = explode("M", substr($data['delaycd1'][$i],2));
                                $inthour1 = (int) $hour[0];
                                $inthour2 = (int) $hour[1];
                                echo ($inthour1 * 60) + $inthour2;
                            } else if (strpos(substr($data['delaycd1'][$i], 2), "H") !== false) {
                                $hour = explode("H", substr($data['delaycd1'][$i], 2));
                                $inthour1 = (int) $hour[0];
                                echo $inthour1 * 60;
                            } else if (strpos(substr($data['delaycd1'][$i], 2), "M") !== false) {
                                $mins = explode("M", substr($data['delaycd1'][$i], 2));
                                echo $mins[0];
                            }
                            ?> </td>
                        <td> <?php
                            //echo substr($data['delaycd2'][$i],2); 
                            if (strpos(substr($data['delaycd2'][$i], 2), "H") !== false && strpos(substr($data['delaycd2'][$i], 2), "M") !== false) {
                                $hour = explode("H", substr($data['delaycd2'][$i], 2)); //$mins = explode("M", substr($data['delaycd2'][$i],2));
                                $inthour1 = (int) $hour[0];
                                $inthour2 = (int) $hour[1];
                                echo ($inthour1 * 60) + $inthour2;
                            } else if (strpos(substr($data['delaycd2'][$i], 2), "H") !== false) {
                                $hour = explode("H", substr($data['delaycd2'][$i], 2));
                                $inthour1 = (int) $hour[0];
                                echo $inthour1 * 60;
                            } else if (strpos(substr($data['delaycd2'][$i], 2), "M") !== false) {
                                $mins = explode("M", substr($data['delaycd2'][$i], 2));
                                echo $mins[0];
                            }
                            ?></td>

                        <td> <?php
                            if ($data['LATEST_ARRIVALAIRPORT'][$i] == $data['SCHED_DEPARTUREAIRPORT'][$i])
                                echo "RTB";
                            else if ($data['LATEST_ARRIVALAIRPORT'][$i] != $data['SCHED_ARRIVALAIRPORT'][$i])
                                echo "Divert";
                            else
                                echo ""
                                ?></td>
                        <td><?php echo $data['ROUTE'][$i]; ?></td>
                        <td><?php echo $data['STATUS'][$i]; ?></td>
                        <td><?php echo $data['SERVICETYPE'][$i]; ?></td>
                        <td><?php echo $data['SUFFIX'][$i]; ?></td>
                        <td> <?php if ($data['cd1'][$i] != "00" || $data['cd2'][$i] != "00"){if ($data['cd1'][$i] != "" || $data['cd1'][$i] != NULL) echo $remark[$data['cd1'][$i]];if ($data['cd2'][$i] != "" || $data['cd2'][$i] != NULL) echo " & ".$remark[$data['cd2'][$i]]; }?></td>
<?php } ?>

                </tr>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData tfoot th').each(function (i) {
            var title = $('#fltData thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="' + title + '" data-index="' + i + '" />');
        });

        var table = $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[15, 50, 100, -1], [15, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf'],
//            columnDefs: [
//                {
//                    targets: [-46,-45,-44,-43,-42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-28,-27,-26,-25,-24,-23,-22,-21,-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1],
//                    visible: false
//                }
//            ],
//            language: {
//                buttons: {
//                    colvis: 'Columns'
//                }
//            },
            scrollX: true,
            scrollCollapse: true,
            scrollY: '450px'

        });
        $(table.table().container()).on('keyup', 'tfoot input', function () {
            table
                    .column($(this).data('index'))
                    .search(this.value, true)
                    .draw();
        });
    });
</script>
