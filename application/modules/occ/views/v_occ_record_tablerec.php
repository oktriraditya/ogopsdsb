<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery(document).ready(function() { 
    $("#sample_1").on('click', '.view', function(){
        var id = this.id;
        var target_url = "<?php echo base_url().'occ/viewrec/';?>"+id;
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
    });
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->


    
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> <?php echo $stn;?> Flight Record, <?php echo date('d-m-Y', strtotime($sDate));?> <?php if($eDate) echo 'to '.date('d-m-Y', strtotime($eDate));?></span>
        </div>  
        <div class="tools"> </div>                     
    </div>
    <div class="portlet-body">                        
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
            <thead>
                <tr>
                    <th width="1%"> No. </th>
                    <th width="10%"> Flight Number </th>
                    <th width="10%"> Dep. Stn </th>
                    <th width="10%"> Arr. Stn </th>
                    <th width="10%"> Dep.Time </th>
                    <th width="10%"> Arr. Time </th>
                    <th width="10%"> Actual Dep </th>
                    <th width="10%"> AC Reg </th>
                    <th width="10%"> AC Type </th>  
                    <th width="10%"> Status </th>
                    <?php if ($status == 'delay') echo '<th width="10%"> Cause Of Delay </th>';?>
                    <th width="10%"> Remarks </th>
                    <th width="10%"> Actions </th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <?php if ($status == 'delay') echo '<th> </th>';?>
                    <th> </th>
                    <th> </th>
                </tr>
            </tfoot>
            <tbody>
                <?php 
                $i = 1;
                foreach ($arrRecords as $key => $row) {                                
                ?>                               
                <tr class="odd gradeX">     
                    <td> <?php echo $i;?>. </td>
                    <td> GA <?php echo $row['FLTNUM'];?> </td>
                    <td> <?php echo $row['DEPAIRPORT'];?> </td>
                    <td> <?php echo $row['SCHED_ARRIVALAIRPORT'];?> </td>
                    <td> <?php echo $row['DEPTIME'];?> </td>
                    <td> <?php echo $row['ARRTIME'];?> </td>
                    <td> <?php echo $row['OFF_BLOCK'];?> </td>     
                    <td> <?php echo $row['AIRCRAFTREG'];?> </td>
                    <td> <?php echo $row['AIRCRAFTTYPE'];?> </td>
                    <?php 
                    if (!empty($row['OFF_BLOCK'])) { 
                        $diff = date_diff(date_create($row['DEPTIME']), date_create($row['OFF_BLOCK'])); 
                        if (date_create($row['OFF_BLOCK']) > date_create($row['DEPTIME'])) {
                            if (($diff->i > 15) || ($diff->h > 0)) echo '<td>Delay ('.$diff->format("%H:%I").')</td>'; 
                            else echo '<td>On Time</td>'; 
                            //echo '<td>'.$diff->h.'</td>'; 
                        } else echo '<td>On Time</td>';                      
                    } else
                        echo '<td>'.$row['STATUS'].'</td>';
                    ?>
                    <?php 
                    if ($status == 'delay') {
                        $dCode = get_delaycode($row['FLIGHTLEGREF'], $row['FLTNUM']); 
                        $rCode = '';
                        foreach($dCode as $key => $cd) {
                            $dCodeText = get_causeOfDelayText($cd['REASONCODE']);
                            $rCode .= $dCodeText.'<br/>';
                        }
                    ?>
                    <td> <?php echo $rCode."-".$cd['REASONCODE'];?> </td>
                    <?php } ?>
                    <td> <?php echo $row['REMARKS'];?> </td>
                    <td>
                        <a class="btn btn-sm btn-outline grey-salsa view" data-target="#view" data-toggle="modal" id="<?php echo $row['FLIGHTLEGREF'];?>"><i class="fa fa-search"></i> View </a>                
                    </td>                                    
                </tr>
                <?php $i++; } ?>                                
            </tbody>
        </table>
    </div>
</div>

<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Flight Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>