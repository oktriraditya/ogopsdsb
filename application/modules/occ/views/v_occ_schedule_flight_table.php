<!-- BEGIN PAGE LEVEL STYLES -->
<!--<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
    .no-wrap{
        white-space: nowrap;
    }
    tfoot input {
        width: 100%;
        margin: 1px;
        box-sizing: border-box;
    }

</style>
<div class="portlet light bordered">

    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> <?php echo $stn; ?> Flight Schedule, <?php echo date('d-M-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-M-Y', strtotime($dateto)); ?></span>
        </div>
    </div>

    <div class="portlet-body">
        <div class="tools"> </div>
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column no-wrap" id="fltData" style="width:100%">
            <thead>
                <tr>
                    <th class="tengah">No</th>
                    <th class="tengah">FlightLegRef</th>
                    <th class="tengah">DoT Schedule UTC</th>
                    <th class="tengah">DoT Schedule CGK</th>
                    <th class="tengah">Flight Num.</th>
                    <th class="tengah">Aircraft Reg.</th>
                    <th class="tengah">Aircraft Type</th>
                    <th class="tengah">Schedule Origin</th>
                    <th class="tengah">Schedule Destination</th>
                    <th class="tengah">Actual Destination</th>
                    <th class="tengah">Actual Destination</th>
                    <th class="tengah">Suffix</th>
                    <th class="tengah">Service Type</th>
                    <th class="tengah">Status</th>
                    <th class="tengah">Cancel Reason Code</th>
                    <th class="tengah">Status Created Dated</th>
                    <th class="tengah">Status Last Modified Date</th>
                    <th class="tengah">Cancel Duration Before Flight (day)</th>
                    <th class="tengah">Standard Time Departure (UTC)</th>
                    <th class="tengah">Standard Time Arrival (UTC)</th>
                    <th class="tengah">Actual Time Departure (UTC)</th>
                    <th class="tengah">Actual Time Arrival (UTC)</th>
                    <th class="tengah">Actual Take Off (UTC)</th>
                    <th class="tengah">Actual Touchdown (UTC)</th>
                    <th class="tengah">Estimated Take Off (UTC)</th>
                    <th class="tengah">Estimated Touchdown (UTC)</th>
                    <th class="tengah">OTP Departure</th>
                    <th class="tengah">OTP Arrival</th>
                    <th class="tengah">OTP ZMD</th>
                    <th class="tengah">Delay Code 1</th>
                    <th class="tengah">Duration DC1 (mins)</th>
                    <th class="tengah">Delay Code 2</th>
                    <th class="tengah">Duration DC2 (mins)</th>
                    <th class="tengah">Flight Service</th>
                    <th class="tengah">Remarks</th>
                    <th class="tengah">Booked Pax</th>
                    <th class="tengah">Actual Pax</th>
                    <th class="tengah">Seat Capacity</th>
                    <th class="tengah">Cockpit Crew</th>
                    <th class="tengah">Cabin Crew</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                $daily_record_per_flight = $daily_record_per_flight->result();
                foreach ($daily_record_per_flight as $key => $value) {
                ?>
                <tr>
                    <td> <?php echo $no++; ?> </td>
                    <td> <?php echo $value->FLIGHTLEGREF; ?> </td>
                    <td> <?php echo $value->SCHED_DAY_OF_TRAVEL_CGK; ?> </td>
                    <td> <?php echo $value->SCHED_DAY_OF_TRAVEL_UTC; ?> </td>
                    <td> <?php echo $value->FLTNUM; ?> </td>
                    <td> <?php echo $value->AIRCRAFTREG; ?> </td>
                    <td> <?php echo $value->AIRCRAFTTYPE; ?> </td>
                    <td> <?php echo $value->SCHED_DEPARTUREAIRPORT; ?> </td>
                    <td> <?php echo $value->SCHED_ARRIVALAIRPORT; ?> </td>
                    <td> <?php echo $value->LATEST_DEPARTUREAIRPORT; ?> </td>
                    <td> <?php echo $value->LATEST_ARRIVALAIRPORT; ?> </td>
                    <td> <?php echo $value->SUFFIX; ?> </td>
                    <td> <?php echo $value->SERVICETYPE; ?> </td>
                    <td> <?php echo $value->STATUS; ?> </td>
                    <td> <?php echo $value->CANCELREASONCODE; ?> </td>
                    <td> <?php echo $value->CREATED_DATE; ?> </td>
                    <td> <?php echo $value->MODIFIED_DATE; ?> </td>
                    <td> <?php echo $value->CANCEL_DURATION_BEFORE_FLIGHT; ?> </td>
                    <td> <?php echo $value->SCHEDULED_DEPDT_UTC; ?> </td>
                    <td> <?php echo $value->SCHEDULED_ARRDT_UTC; ?> </td>
                    <td> <?php echo $value->ACTUAL_BLOCKOFF_UTC; ?> </td>
                    <td> <?php echo $value->ACTUAL_BLOCKON_UTC; ?> </td>
                    <td> <?php echo $value->ACTUAL_TAKEOFF_UTC; ?> </td>
                    <td> <?php echo $value->ACTUAL_TOUCHDOWN_UTC; ?> </td>
                    <td> <?php echo $value->ESTIMATED_TAKEOFF_UTC; ?> </td>
                    <td> <?php echo $value->ESTIMATED_TOUCHDOWN_UTC; ?> </td>
                    <td> <?php echo $value->OTPDEP; ?> </td>
                    <td> <?php echo $value->OTPARR; ?> </td>
                    <td> <?php echo $value->OTPZMD; ?> </td>
                    <td> <?php echo $value->CD1; ?> </td>
                    <td> <?php echo $value->DELAYLENGTH1; ?> </td>
                    <td> <?php echo $value->CD2; ?> </td>
                    <td> <?php echo $value->DELAYLENGTH2; ?> </td>
                    <td> <?php echo $value->ROUTE; ?> </td>
                    <td> <?php echo $value->REMARKS; ?> </td>
                    <td> <?php echo $value->BOOKEDPAX; ?> </td>
                    <td> <?php echo $value->ACTUALPAX; ?> </td>
                    <td> <?php echo $value->SEATCAPACITY; ?> </td>
                    <td> <?php echo $value->COCKPIT; ?> </td>
                    <td> <?php echo $value->CABIN; ?> </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData tfoot th').each(function (i) {
            var title = $('#fltData thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="' + title + '" data-index="' + i + '" />');
        });

        var table = $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[15, 50, 100, -1], [15, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf', 'colvis'],
            columnDefs: [
                    { targets: [1,2,14,15,16,17], visible: false},
                    { targets: '_all', visible: true }
            ],
            language: {
                buttons: {
                    colvis: 'Columns'
                }
            },
            scrollX: true,
            scrollCollapse: true,
            scrollY: '450px',
            autoWidth: true,
            // responsive: true

        });

        $(table.table().container()).on('keyup', 'tfoot input', function () {
            table
                    .column($(this).data('index'))
                    .search(this.value, true)
                    .draw();
        });
    });
</script>
