<script type="text/javascript">
jQuery(document).ready(function() { 
    $('#boxDataOTP').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'ontime';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataOntime').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'ontime';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataSchedPlan').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'all';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataCancelPlan').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'cancelplan';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataOnSchedule').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'onschedule';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataDeparted').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'departed';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataDelay').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'delay';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxDataCancel').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'cancel';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });
});
</script>

<div class="row widget-row">
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION OTP</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-green-jungle fa fa-line-chart"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $pOTP;?>"><?php echo $pOTP;?> %</span>
                    <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?><br/>&nbsp;</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataOTP"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION SCHEDULE PLAN</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-blue-dark fa fa-calendar-plus-o"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nSchedule;?>"><?php echo $nSchedule;?></span>
                    <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?><br/>&nbsp;</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataSchedPlan"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION CANCELLED PLAN </h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-red-haze fa fa-calendar-times-o"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nCancelPlan;?>"><?php echo $nCancelPlan;?></span>
                    <span class="widget-thumb-subtitle"><?php echo $pCancelPlan;?>% from Schedule Plan</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataCancelPlan"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION ON SCHEDULE</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-grey-salsa fa fa-calendar-check-o"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nOnschedule;?>"><?php echo $nOnschedule;?></span>
                    <span class="widget-thumb-subtitle"><?php echo $pOnschedule;?>% from schedule plan</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataOnSchedule"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION DEPARTED</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-blue-soft fa fa-plane"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nDeparted;?>"><?php echo $nDeparted;?></span>
                    <span class="widget-thumb-subtitle"><?php echo $pDeparted?>% departure progress</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataDeparted"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION ON TIME</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-green-jungle fa fa-check-square-o"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nOntime;?>"><?php echo $nOntime;?></span>
                    <span class="widget-thumb-subtitle"><?php echo $pOTP;?>% OTP<br/>&nbsp;</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataOntime"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION DELAY</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-yellow-soft fa fa-clock-o"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nDelay;?>"><?php echo $nDelay;?></span>
                    <span class="widget-thumb-subtitle"><?php echo $pDelay;?>% from departure<br/>&nbsp;</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataDelay"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"><?php echo $stn;?> STATION CANCEL</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-red-haze fa fa-times-circle"></i>
                <div class="widget-thumb-body">                    
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $nCancel;?>"><?php echo $nCancel;?></span>
                    <span class="widget-thumb-subtitle">CANCEL WITHIN <?php echo date('d-m-Y', strtotime($sDate));?><br/>&nbsp;</span>
                </div>
            </div>
            <br/>
            <div class="widget-thumb-heading pull-right">
                <a class="btn yellow btn-circle btn-sm" href="#boxData" id="boxDataCancel"> Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
</div>             