<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    .DTFC_LeftBodyLiner {
        overflow-x: hidden;
    }
</style>
<!--<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#fltData").on('click', '.view', function () {
            var id = this.id.split(" ");
            //alert(id[1]);
            //var kata2 = id.split(":");
            var target_url = "<?php echo base_url() . 'occ/details_delay/'; ?>" + id[0] + "/" + id[1] + "/" + id[2];
            var iframe = $("#viewFrame");
            iframe.attr("src", target_url);
            //ajaxStart: function(){iframe.attr("src", "loading...");},
            //ajaxStop: function(){iframe.attr("src", target_url);}


        });
    });
</script>-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> OTP per Station, <?php echo date('d-m-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-m-Y', strtotime($dateto)); ?></span>
        </div>  

    </div>

    <div class="portlet-body"> 
        <div class="tools"> </div> 
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column " id="fltData">
            <thead>
                <tr>
                    <th  class="tengah">No.</th>
                    <th  class="tengah">Station</th>
                    <th  class="tengah">#Departure</th>
                    <th  class="tengah">#On-Time</th>
                    <th  class="tengah">#Delay</th>
                    <th  class="tengah">#Arr. On-Time</th>
                    <th  class="tengah">OTP Departure</th>
                    <th  class="tengah">OTP Arrival</th>
                    <th  class="tengah">Ground Time Performance (margin 15 mins)</th>
                    <th  class="tengah">Ground Time Performance </th>
<!--                    <th  class="tengah">GTP due to Station Handling</th>
                    <th  class="tengah">Quick Handling Performance</th>-->
                    <th  class="tengah">#First Departure</th>
                    <th  class="tengah">OTP First Departure</th>
                    <th  class="tengah">ZMD First Departure</th>
<!--                    <th  class="tengah">Average Delay Departure (mins)</th>
                    <th  class="tengah">Average Delay Arrival (mins)</th>-->
                    <th  class="tengah">Average Ground Time (mins)</th>
                    <th  class="tengah">OTP Departure (margin 30 mins)</th>
                    <th  class="tengah">#Delay Technic</th>
                    <th  class="tengah">#Delay Station Handling</th>
                    <th  class="tengah">#Delay Commercial</th>
                    <th  class="tengah">#Delay System</th>
                    <th  class="tengah">#Delay Flight Ops</th>
                    <th  class="tengah">#Delay Airport Facility</th>
                    <th  class="tengah">#Delay Weather</th>
                    <th  class="tengah">#Delay Misc</th>
                    <th  class="tengah">%Delay Technic</th>
                    <th  class="tengah">%Delay Station Handling</th>
                    <th  class="tengah">%Delay Commercial</th>
                    <th  class="tengah">%Delay System</th>
                    <th  class="tengah">%Delay Flight Ops</th>
                    <th  class="tengah">%Delay Airport Facility</th>
                    <th  class="tengah">%Delay Weather</th>
                    <th  class="tengah">%Delay Misc</th>
<!--                    <th  class="tengah">80</th>
                    <th  class="tengah">81</th>
                    <th  class="tengah">82</th>
                    <th  class="tengah">83</th>
                    <th  class="tengah">84</th>
                    <th  class="tengah">89</th>
                    <th  class="tengah">86</th>
                    <th  class="tengah">87</th>
                    <th  class="tengah">88</th>
                    <th  class="tengah">85</th>-->
                    <th  class="tengah">Late Arrival</th>
                    <th  class="tengah">Navigation Service</th>
                    <th  class="tengah">Airport Management</th>
                </tr>
            </thead>
            <?php
            $data = array();
            $dataTotal = array();
            $dataTotal['schedule'] = 0;
            $dataTotal['ontime'] = 0;
            $dataTotal['arrontime'] = 0;
            $dataTotal['delay'] = 0;
            $dataTotal['SCHEDARR'] = 0;
            $dataTotal['technic'] = 0;
            $dataTotal['STNH'] = 0;
            $dataTotal['COMM'] = 0;
            $dataTotal['SYST'] = 0;
            $dataTotal['FLOPS'] = 0;
            $dataTotal['APTF'] = 0;
            $dataTotal['WEATHER'] = 0;
            $dataTotal['MISC'] = 0;
            $dataTotal['TOTALCOD'] = 0;
            $dataTotal['AVGDELAY'] = 0;
            $dataTotal['A80'] = 0;
            $dataTotal['A81'] = 0;
            $dataTotal['A82'] = 0;
            $dataTotal['A83'] = 0;
            $dataTotal['A84'] = 0;
            $dataTotal['A85'] = 0;
            $dataTotal['A86'] = 0;
            $dataTotal['A87'] = 0;
            $dataTotal['A88'] = 0;
            $dataTotal['A89'] = 0;
            $dataTotal['FIRSTFLIGHT'] = 0;
            $dataTotal['OTPFIRSTFLIGHT'] = 0;
            $dataTotal['ZMDFIRSTFLIGHT'] = 0;
            $dataTotal['AVGGROUNDTIME'] = 0;
            $dataTotal['GTP'] = 0;
            $dataTotal['GTPSTRICT'] = 0;
            $dataTotal['GTPSTNH'] = 0;
            $dataTotal['QUICKHANDLING'] = 0;
            $dataTotal['QUICKHANDLINGCHANCE'] = 0;
            $dataTotal['AVGDELAYARR'] = 0;
            $dataTotal['ONTIME30'] = 0;

            foreach ($daily_record_flight as $key => $value) {
                $data['schedule'][] = $value['SCHEDULED'];
                $data['station'][] = $value['DEPAIRPORT'];
                $data['station2'][] = $value['ARRAIRPORT'];
                $data['ontime'][] = $value['ONTIME'];
                $data['ONTIME30'][] = $value['ONTIME30'];
                $data['arrontime'][] = $value['ARRONTIME'];
                $data['SCHEDARR'][] = $value['SCHEDARR'];
                $data['FIRSTFLIGHT'][] = $value['FIRSTFLIGHT'];
                $data['OTPFIRSTFLIGHT'][] = $value['OTPFIRSTFLIGHT'];
                $data['ZMDFIRSTFLIGHT'][] = $value['ZMDFIRSTFLIGHT'];
                $data['AVGGROUNDTIME'][] = $value['AVGGROUNDTIME'];
                $data['GTP'][] = $value['GTP'];
                $data['GTPSTRICT'][] = $value['GTPSTRICT'];
                $data['GTPSTNH'][] = $value['GTPSTNH'];
                $data['QUICKHANDLING'][] = $value['QUICKHANDLING'];
                $data['QUICKHANDLINGCHANCE'][] = $value['QUICKHANDLINGCHANCE'];
                $data['AVGDELAYARR'][] = $value['AVGDELAYARR'];

                $data['AVGDELAY'][] = $value['AVGDELAY'];
                $data['technic'][] = $value['TECH'];
                $data['STNH'][] = $value['STNH'];
                $data['COMM'][] = $value['COMM'];
                $data['SYST'][] = $value['SYST'];
                $data['FLOPS'][] = $value['FLOPS'];
                $data['APTF'][] = $value['APTF'];
                $data['WEATHER'][] = $value['WEATHER'];
                $data['MISC'][] = $value['MISC'];
                $data['TOTALCOD'][] = $value['TECH'] + $value['STNH'] + $value['COMM'] + $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'];
                $data['A80'][] = $value['A80'];
                $data['A81'][] = $value['A81'];
                $data['A82'][] = $value['A82'];
                $data['A83'][] = $value['A83'];
                $data['A84'][] = $value['A84'];
                $data['A85'][] = $value['A85'];
                $data['A86'][] = $value['A86'];
                $data['A87'][] = $value['A87'];
                $data['A88'][] = $value['A88'];
                $data['A89'][] = $value['A89'];
            }
//            foreach ($daily_record_cod as $key => $value) {
//                $data['AVGDELAY'][] = $value['AVGDELAY'];
//                $data['technic'][] = $value['TECH'];
//                $data['STNH'][] = $value['STNH'];
//                $data['COMM'][] = $value['COMM'];
//                $data['SYST'][] = $value['SYST'];
//                $data['FLOPS'][] = $value['FLOPS'];
//                $data['APTF'][] = $value['APTF'];
//                $data['WEATHER'][] = $value['WEATHER'];
//                $data['MISC'][] = $value['MISC'];
//                $data['TOTALCOD'][] = $value['TECH'] + $value['STNH'] + $value['COMM'] + $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'];
//                $data['A80'][] = $value['A80'];
//                $data['A81'][] = $value['A81'];
//                $data['A82'][] = $value['A82'];
//                $data['A83'][] = $value['A83'];
//                $data['A84'][] = $value['A84'];
//                $data['A85'][] = $value['A85'];
//                $data['A86'][] = $value['A86'];
//                $data['A87'][] = $value['A87'];
//                $data['A88'][] = $value['A88'];
//                $data['A89'][] = $value['A89'];
//            }

            $count = count($daily_record_flight);
            $countdelay = 0;
            $countdelayarr = 0;
            ?>
            <tbody>
                <?php
                for ($i = 0; $i < $count; $i++) {
                    ?>
                    <tr class="tengah">
                        <td> <?php echo $i + 1; ?> </td>
                        <td> <?php if (empty($data['station'][$i])) echo $data['station2'][$i];
                    else echo $data['station'][$i]; ?></td>
                        <td> <?php
                            $dataTotal['schedule']+= $data['schedule'][$i];
                            echo $data['schedule'][$i] > 0 ? $data['schedule'][$i] : "-";
                            ?> </td>
                        <td> <?php
                            $dataTotal['ontime']+= $data['ontime'][$i];
                            if ($data['ontime'][$i] >= 0 && $data['schedule'][$i] > 0)
                                echo $data['ontime'][$i];
                            else
                                echo "-";
                            ?></td>
                        <td> <?php
                            if ($data['AVGDELAY'][$i] != 0)
                                $countdelay++;if ($data['AVGDELAYARR'][$i] != 0)
                                $countdelayarr++;
                            $dataTotal['delay']+= $data['schedule'][$i] - $data['ontime'][$i];
                            echo $data['schedule'][$i] > 0 ? $data['schedule'][$i] - $data['ontime'][$i] : "-";
                            ?></td>
                        <td> <?php
                            $dataTotal['arrontime']+= $data['arrontime'][$i];
                            echo $data['arrontime'][$i] >= 0 && $data['SCHEDARR'][$i] > 0 ? $data['arrontime'][$i] : "-";
                            ?></td>
                        <td> <?php
                            if ($data['schedule'][$i] != 0)
                                echo round(($data['ontime'][$i] / $data['schedule'][$i]) * 100, 2) . "%";
                            else
                                echo "-";
                            ?></td>
                        <td> <?php
                            if ($data['SCHEDARR'][$i] != 0)
                                echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100, 2) . "%";
                            else
                                echo "-";
                            ?></td>
                        <td> <?php
                            $dataTotal['GTP']+= $data['GTP'][$i];
                            if ($data['schedule'][$i] != 0)
                                echo round(($data['GTP'][$i] / $data['schedule'][$i]) * 100, 2) . "%";
                            else
                                echo "-";
                            ?></td>
                        <td> <?php
                        $dataTotal['GTPSTRICT']+= $data['GTPSTRICT'][$i];
                        if ($data['schedule'][$i] != 0)
                            echo round(($data['GTPSTRICT'][$i] / $data['schedule'][$i]) * 100, 2) . "%";
                        else
                            echo "-";
                        ?></td>
    <!--                        <td> <?php
                        /* $dataTotal['GTPSTNH']+= $data['GTPSTNH'][$i];
                          if ($data['STNH'][$i] != 0)
                          echo round(($data['GTPSTNH'][$i] / $data['STNH'][$i]) * 100, 2)."%";
                          else
                          echo "-"; */
                        ?></td>
                        <td> <?php
                        //if($data['QUICKHANDLINGCHANCE'][$i]>0){
                        /* $dataTotal['QUICKHANDLING']+= $data['QUICKHANDLING'][$i];
                          $dataTotal['QUICKHANDLINGCHANCE']+= $data['QUICKHANDLINGCHANCE'][$i];
                          //}
                          if ($data['QUICKHANDLINGCHANCE'][$i] != 0)
                          echo round(($data['QUICKHANDLING'][$i] / $data['QUICKHANDLINGCHANCE'][$i]) * 100, 2)."%";
                          else
                          echo "-"; */
                        ?></td>-->

                        <td> <?php
                            echo $data['FIRSTFLIGHT'][$i] > 0 ? $data['FIRSTFLIGHT'][$i] : "-";
                            ?></td>
                        <td> <?php
                        $dataTotal['FIRSTFLIGHT']+= $data['FIRSTFLIGHT'][$i];
                        $dataTotal['OTPFIRSTFLIGHT']+= $data['OTPFIRSTFLIGHT'][$i];
                        if ($data['FIRSTFLIGHT'][$i] != 0)
                            echo round(($data['OTPFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2) . "%";
                        else
                            echo "-";
                            ?></td>
                        <td> <?php
                        $dataTotal['ZMDFIRSTFLIGHT']+= $data['ZMDFIRSTFLIGHT'][$i];
                        if ($data['FIRSTFLIGHT'][$i] != 0)
                            echo round(($data['ZMDFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2) . "%";
                        else
                            echo "-";
                        ?></td>
    <!--                        <td> <?php
                        $dataTotal['AVGDELAY']+= $data['AVGDELAY'][$i];
                        if ($data['AVGDELAY'][$i] != 0)
                            echo round($data['AVGDELAY'][$i], 1);
                        else
                            echo 0;
                        ?></td>
                        <td> <?php
                            $dataTotal['AVGDELAYARR']+= $data['AVGDELAYARR'][$i];
                            if ($data['AVGDELAYARR'][$i] != 0)
                                echo round($data['AVGDELAYARR'][$i], 1);
                            else
                                echo 0;
                            ?></td>-->
                        <td> <?php
                            $dataTotal['AVGGROUNDTIME']+= $data['AVGGROUNDTIME'][$i];
                            if ($data['AVGGROUNDTIME'][$i] != 0)
                                echo round($data['AVGGROUNDTIME'][$i], 1);
                            else
                                echo 0;
                            ?></td>
                        <td> <?php
                        $dataTotal['ONTIME30']+= $data['ONTIME30'][$i];
                        if ($data['schedule'][$i] != 0)
                            echo round(($data['ONTIME30'][$i] / $data['schedule'][$i]) * 100, 2) . "%";
                        else
                            echo "-";
                        ?></td>
                            <?php $dataTotal['SCHEDARR']+= $data['SCHEDARR'][$i]; ?>
                            <?php if ($data['TOTALCOD'][$i] > 0) { ?>
                                <?php $dataTotal['TOTALCOD']+=$data['TOTALCOD'][$i] ?>
                            <td> <?php
                        $dataTotal['technic']+= $data['technic'][$i];
                        echo $data['technic'][$i];
                                ?> </td>
                            <td> <?php
                        $dataTotal['STNH']+= $data['STNH'][$i];
                        echo $data['STNH'][$i];
                                ?>  </td>
                            <td> <?php
                        $dataTotal['COMM']+= $data['COMM'][$i];
                        echo $data['COMM'][$i];
                                ?> </td>
                            <td> <?php
                        $dataTotal['SYST']+= $data['SYST'][$i];
                        echo $data['SYST'][$i];
                                ?> </td>
                            <td> <?php
                        $dataTotal['FLOPS']+= $data['FLOPS'][$i];
                        echo $data['FLOPS'][$i];
                                ?>  </td>
                            <td> <?php
                        $dataTotal['APTF']+= $data['APTF'][$i];
                        echo $data['APTF'][$i];
                                ?> </td>
                            <td> <?php
                        $dataTotal['WEATHER']+= $data['WEATHER'][$i];
                        echo $data['WEATHER'][$i];
                        ?>  </td>
                            <td> <?php
                        $dataTotal['MISC']+= $data['MISC'][$i];
                        echo $data['MISC'][$i];
                        ?> </td>
                            <td> <?php //echo $data['technic'][$i];        ?>  <?php echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php // $data['STNH'][$i];         ?>  <?php echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php //echo $data['COMM'][$i];        ?>  <?php echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php //echo $data['SYST'][$i];         ?>  <?php echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php // $data['FLOPS'][$i];         ?>  <?php echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php //echo $data['APTF'][$i];         ?>  <?php echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php //echo $data['WEATHER'][$i];         ?>  <?php echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                            <td> <?php //echo $data['MISC'][$i];        ?>  <?php echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>

    <?php } else { ?>
                            <td> 0 </td>
                            <td> 0  </td>
                            <td> 0  </td>
                            <td> 0  </td>
                            <td> 0  </td>
                            <td> 0  </td>
                            <td> 0  </td>
                            <td> 0 </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
                        <?php } ?>
                        <?php $dataTotal['A80']+=$data['A80'][$i]; ?>
                        <?php $dataTotal['A81']+=$data['A81'][$i]; ?>
                        <?php $dataTotal['A82']+=$data['A82'][$i]; ?>
    <?php $dataTotal['A83']+=$data['A83'][$i]; ?>
    <?php $dataTotal['A84']+=$data['A84'][$i]; ?>
    <?php $dataTotal['A85']+=$data['A85'][$i]; ?>
    <?php $dataTotal['A86']+=$data['A86'][$i]; ?>
    <?php $dataTotal['A87']+=$data['A87'][$i]; ?>
    <?php $dataTotal['A88']+=$data['A88'][$i]; ?>
    <?php $dataTotal['A89']+=$data['A89'][$i]; ?>
    <!--                        <td> <?php //echo $data['A80'][$i];  ?> </td>
                    <td> <?php //echo $data['A81'][$i];  ?> </td>
                    <td> <?php //echo $data['A82'][$i];  ?> </td>
                    <td> <?php //echo $data['A83'][$i]; ?> </td>
                    <td> <?php //echo $data['A84'][$i];  ?> </td>
                    <td> <?php //echo $data['A89'][$i];  ?> </td>
                    <td> <?php //echo $data['A86'][$i];  ?> </td>
                    <td> <?php //echo $data['A87'][$i]; ?> </td>
                    <td> <?php //echo $data['A88'][$i];  ?> </td>
                    <td> <?php //echo $data['A85'][$i];  ?> </td>-->
                        <?php if ($data['APTF'][$i] > 0) { ?>
                            <td> <?php echo round(($data['A80'][$i] / $data['APTF'][$i]) * round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2), 2); ?>% </td>
                            <td> <?php echo round((($data['A81'][$i] + $data['A82'][$i] + $data['A83'][$i] + $data['A84'][$i] + $data['A89'][$i]) / $data['APTF'][$i]) * round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2), 2); ?>% </td>
                            <td> <?php echo round((($data['A85'][$i] + $data['A86'][$i] + $data['A87'][$i] + $data['A88'][$i]) / $data['APTF'][$i]) * round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2), 2); ?>% </td>
    <?php } else { ?>
                            <td>  0% </td>
                            <td>  0% </td>
                            <td>  0% </td>
    <?php } ?>
                    </tr>
<?php } ?>
                <tr class="tengah">
                    <td></td>
                    <td>Total</td>
                    <td><?php echo $dataTotal['schedule']; ?></td>
                    <td><?php echo $dataTotal['ontime']; ?></td>
                    <td><?php echo $dataTotal['delay']; ?></td>
                    <td><?php echo $dataTotal['arrontime']; ?></td>
                    <td><?php echo round($dataTotal['ontime'] / $dataTotal['schedule'] * 100, 2); ?>%</td>
                    <td><?php echo round($dataTotal['arrontime'] / $dataTotal['SCHEDARR'] * 100, 2); ?>%</td>
                    <td><?php echo round($dataTotal['GTP'] / $dataTotal['schedule'] * 100, 2); ?>%</td>
                    <td><?php echo round($dataTotal['GTPSTRICT'] / $dataTotal['schedule'] * 100, 2); ?>%</td>
<!--                    <td><?php echo $dataTotal['STNH'] > 0 ? round($dataTotal['GTPSTNH'] / $dataTotal['STNH'] * 100, 2) : 0; ?>%</td>
                    <td><?php echo round($dataTotal['QUICKHANDLING'] / $dataTotal['QUICKHANDLINGCHANCE'] * 100, 2); ?>%</td>-->
                    <td><?php echo $dataTotal['FIRSTFLIGHT']; ?></td>
                    <td><?php echo round($dataTotal['OTPFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2); ?>%</td>
                    <td><?php echo round($dataTotal['ZMDFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2); ?>%</td>
<!--                    <td><?php echo round($dataTotal['AVGDELAY'] / $countdelay, 1); ?></td>
                    <td><?php echo round($dataTotal['AVGDELAYARR'] / $countdelayarr, 1); ?></td>-->
                    <td><?php echo round($dataTotal['AVGGROUNDTIME'] / $count, 1); ?></td>
                    <td><?php echo round($dataTotal['ONTIME30'] / $dataTotal['schedule'] * 100, 2); ?>%</td>
                    <td> <?php echo $dataTotal['technic']; ?> </td>
                    <td> <?php echo $dataTotal['STNH']; ?>  </td>
                    <td> <?php echo $dataTotal['COMM']; ?> </td>
                    <td> <?php echo $dataTotal['SYST']; ?> </td>
                    <td> <?php echo $dataTotal['FLOPS']; ?>  </td>
                    <td> <?php echo $dataTotal['APTF']; ?> </td>
                    <td> <?php echo $dataTotal['WEATHER'] ?>  </td>
                    <td> <?php echo $dataTotal['MISC']; ?> </td>
                    <td> <?php //echo $data['technic'][$i];         ?>  <?php echo round(($dataTotal['technic'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php // $dataTotal['STNH'];         ?>  <?php echo round(($dataTotal['STNH'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php //echo $dataTotal['COMM'];         ?>  <?php echo round(($dataTotal['COMM'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php //echo $dataTotal['SYST'];         ?>  <?php echo round(($dataTotal['SYST'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php // $dataTotal['FLOPS'];         ?>  <?php echo round(($dataTotal['FLOPS'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php //echo $dataTotal['APTF'];         ?>  <?php echo round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php //echo $dataTotal['WEATHER'];         ?>  <?php echo round(($dataTotal['WEATHER'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
                    <td> <?php //echo $dataTotal['MISC'];         ?>  <?php echo round(($dataTotal['MISC'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2); ?>%</td>
<!--                    <td> <?php //echo $dataTotal['A80'];  ?> </td>
                    <td> <?php //echo $dataTotal['A81'];  ?> </td>
                    <td> <?php //echo $dataTotal['A82'];  ?> </td>
                    <td> <?php //echo $dataTotal['A83'];  ?> </td>
                    <td> <?php //echo $dataTotal['A84'];  ?> </td>
                    <td> <?php //echo $dataTotal['A89'];  ?> </td>
                    <td> <?php //echo $dataTotal['A86'];  ?> </td>
                    <td> <?php //echo $dataTotal['A87'];  ?> </td>
                    <td> <?php // echo $dataTotal['A88'];  ?> </td>
                    <td> <?php //echo $dataTotal['A85'];  ?> </td>-->
                    <td> <?php echo round(($dataTotal['A80'] / $dataTotal['APTF']) * round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2), 2); ?>% </td>
                    <td> <?php echo round((($dataTotal['A81'] + $dataTotal['A82'] + $dataTotal['A83'] + $dataTotal['A84'] + $dataTotal['A89']) / $dataTotal['APTF']) * round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2), 2); ?>% </td>
                    <td> <?php echo round((($dataTotal['A85'] + $dataTotal['A86'] + $dataTotal['A87'] + $dataTotal['A88']) / $dataTotal['APTF']) * round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * (($dataTotal['schedule'] - $dataTotal['ontime']) / $dataTotal['schedule']) * 100, 2), 2); ?>% </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!--<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>-->

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            ordering: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf'],
            scrollX: true,
            scrollY: "500px",
            scrollCollapse: true
        });
    });
</script>
