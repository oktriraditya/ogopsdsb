<script>
jQuery(function(){
    //am4core.useTheme(am4themes_animated);
    var maxes = <?php print_r(get_max_cs($sDate, $eDate, $sFleet, $sRank)); ?>;
    var chart = AmCharts.makeChart( "chartFstats", {
        "type": "serial",
        "hideCredits":true,
        "addClassNames": true,
        "theme": "light",
        "autoMargins": true,
        "marginRight": 40,
        "marginLeft": 40,
        "marginTop": 40,
        "autoMarginOffset": 20,

        "balloon": {
          "adjustBorderColor": false,
          "horizontalPadding": 10,
          "verticalPadding": 8,
          "color": "#ffffff"
        },
        "defs": {
            "filter": {
              "id": "dropshadow",
              "x": "-10%",
              "y": "-10%",
              "width": "120%",
              "height": "120%",
              "feOffset": {
                "result": "offOut",
                "in": "SourceAlpha",
                "dx": "3",
                "dy": "3"
              },
              "feGaussianBlur": {
                "result": "blurOut",
                "in": "offOut",
                "stdDeviation": "5"
              },
              "feBlend": {
                "in": "SourceGraphic",
                "in2": "blurOut",
                "mode": "normal"
              }
            }
          },
        "mouseWheelZoomEnabled":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id" : "v1",
            "stackType": "regular",
            "axisAlpha": 0.1,
            "position": "left",
            "gridAlpha": 0.1,
            "maximum": maxes+5,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        },{
            "id" : "v2",
            "axisAlpha": 0.1,
            "position": "right",
            "gridAlpha": 0.1,
            "maximum":maxes+5,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        }],
        "dataProvider": [
        <?php
        $arrDay = getDatesFromRange($sDate, $eDate);
        foreach($arrDay as $sDay) {
            $set_act  = get_act_breakdown($sDay, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther);
            //print_r($set_act);
            $arrAvail = $set_act[0]['online_cap'];
            $arrOlReq = list_olReg($sDay, $sRank, $sFleet, $ssim);
            //$arrAvail = list_avail($sDay, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther);
            //$arrOnLeave = list_onLeave($sDay, $sRank, $sFleet);
            $arrPilotR = list_pilot($sDay, $sRank, $sFleet, 'count_route');
            //$arrPilotG = list_pilot($sDay, $sRank, $sFleet, 'count_ground');
            //$arrTrainAlloc = list_trainAlloc($sDay, $sRank, $sFleet);

            echo "{";
            echo "'date': '".$sDay."',";
            echo "olreq: ".$arrOlReq.",";
            echo "avail: ".round($arrAvail, 0).",";
            //echo "onleave: ".$arrOnLeave.",";
            echo "arrPilotR: ".$arrPilotR.",";
            //echo "arrPilotG: ".$arrPilotG.",";
            echo "online_cap: ".round($set_act[0]['online_cap'],0).",";
            echo "standby: ".round($set_act[0]['standby'],0).",";
            //echo "qual: ".round($set_act[0]['qual'],0).",";
            echo "office: ".round($set_act[0]['office'],0).",";
            echo "tr_typequal: ".round($set_act[0]['tr_typequal'],0).",";
            echo "tr_recurrent: ".round($set_act[0]['tr_recurrent'],0).",";
            echo "annual_leave: ".round($set_act[0]['annual_leave'],0).",";
            echo "day_off: ".round($set_act[0]['day_off'],0).",";
            echo "medex_lic: ".round($set_act[0]['medex_lic'],0).",";
            echo "margin_sick: ".round($set_act[0]['margin_sick'],0).",";
            echo "other: ".round($set_act[0]['other'],0).",";
            echo "},";

        }
        ?>
        ],
        "graphs": [{
            "fillAlphas": 0.7,
            "fillColors": "rgb(209,212,217)",
            "lineAlpha": 0,
            "type": "column",
            "valueField": "online_cap",
            "balloonText": "",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(175,180,189)",
            "title": "standby alloc.",
            "type": "column",
            "valueField": "standby",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(247,163,165)",
            "title": "office alloc.",
            "type": "column",
            "valueField": "office",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "title": "other duty alloc.",
            "fillColors": "rgb(247,163,165)",
            "type": "column",
            "valueField": "other",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(250,192,106)",
            "title": "training alloc.(TQ)",
            "type": "column",
            "valueField": "tr_typequal",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(250,192,106)",
            "title": "training alloc.(rec)",
            "type": "column",
            "valueField": "tr_recurrent",
        }, {
              "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
              "fillAlphas": 0.8,
              "lineAlpha": 0,
              "fillColors": "rgb(250,192,106)",
              "title": "medex & license alloc",
              "type": "column",
              "valueField": "medex_lic",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(146,218,216)",
            "title": "annual leave",
            "type": "column",
            "valueField": "annual_leave",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(144,192,244)",
            "title": "day off alloc.",
            "type": "column",
            "valueField": "day_off",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(84,157,238)",
            "title": "margin sick",
            "type": "column",
            "valueField": "margin_sick",
        }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(70,139,252)",
            "title": "route training",
            "type": "column",
            "valueField": "arrPilotR"
        }, {
          /*  "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(70,139,252)",
            "title": "ground training",
            "type": "column",
            "valueField": "arrPilotG"
        }, {*/
            "id": "g1",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletSize": 4,
            "bulletBorderThickness": 2,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "lineThickness": 3.2,
            "lineColor": "rgb(254,66,63)",
            "title": "Online Requirement",
            "useLineColorForBulletBorder": true,
            "valueField": "olreq",
            "balloonText": "<b>[[title]]</b><span style='font-size:22px'> : <b>[[value]]</b></span>",
        }, {
          "id": "g2",
          "valueAxis": "v2",
          "bullet": "round",
          "bulletSize": 4,
          "bulletBorderThickness": 2,
          "bulletBorderAlpha": 1,
          "bulletColor": "#FFFFFF",
          "hideBulletsCount": 50,
          "lineThickness": 3.2,
          "lineColor": "rgb(11,180,170)",
          "title": "Online Capacity",
          "useLineColorForBulletBorder": true,
          "valueField": "avail",
          "balloonText": "<b>[[title]]</b><span style='font-size:22px'> : <b>[[value]]</b></span>",
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left"
        },
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis":false,
            "offset":30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount":true,
            "color":"#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
        "oppositeAxis":false,
        "offset":50,
        "scrollbarHeight":10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "Parameter : Fleet[<?php echo $sFleet;?>], StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], SSIM[<?php echo ($ssim == null) ? "all" : $ssim;?>], Mode[<?php echo ($mode == 1) ? "Operational" : "Man Power Planning";?>], DO[<?php echo $DO;?>], DataMode[<?php echo ($dataMode == 1 ? "Schedule ODS Only" : ($dataMode == 2 ? "Formula" : ($dataMode == 3 ? "Formula (blend TR & AL)" : ($dataMode == 4 ? "Formula (blend AL Only)" : ""))));?>], IncludeTraining[<?php echo ($incTraining == 1) ? "Yes" : "No";?>], IncludeOffice[<?php echo ($incOffice == 1) ? "Yes" : "No";?>], IncludeOther[<?php echo ($incOther == 1) ? "Yes" : "No";?>]",
			"bold": false,
			"x": 0,
			"y": 0
		}
	    ]
    });
});
</script>
