<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />

<!-- BEGIN CONTENT -->
<div id="autoref" class="page-content-wrapper" >
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php
                    //date_default_timezone_set('Asia/Jakarta'); 
                    echo $title;
                    ?>
                    <small><?php echo $title_small; ?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>              
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->  
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">On Time Performance</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">About</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB --> 
        <!-- BEGIN PAGE BASE CONTENT -->                                   


<!--style="background-image: url('<?php echo base_url(); ?>assets/images/ga_1.jpg'); background-position-y: -400px;"-->
        <div class="portlet light bordered" >
            <div class="portlet-body">  
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" >            
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject uppercase font-blue-soft" style="font-size: 18pt;">FAQ'S</span>
                                </div>
                                <div class="actions">
                                    <i class="fa fa-info fa-2x" ></i>
                                </div>
                            </div>                        
                            <div class="portlet-body">
                                <span class="caption-subject uppercase font-blue-soft"><h3 class="bold">What is OTP ?</h3></span>
                                <p>On Time Performance (OTP) is a measure of the ability of transport services to be on time. Almost all transportation systems have timetables, which describe when vehicles are to arrive at scheduled stops. Transport services have a higher utility where services run on time, as anyone planning on making use of the service can align their activities with that of the transport system. On time performance is particularly important where services are infrequent, and people need to plan to meet services.
                                    According to best practice in aviation industries, OTP calculation divided to 2 categories: Departures OTP and Arrivals OTP.
                                </p>
                                <span class="caption-subject uppercase font-blue-soft"><h3 class="bold">How do we calculate OTP ?</h3></span>
                                <p>Departures OTP: (Number of Departure Ontime) / (No. of Departure Flight) * 100%<br>
                                    Departure is On-Time, if actual departure no more than 15 minutes from departure schedule time.

                                </p>
                                <p>
                                    Arrivals OTP: (Number of Arrival Ontime) / (No. of Arrival Flight) * 100%<br>
                                    Arrival is On-Time, if actual arrival no more than 15 minutes from arrival schedule time.

                                </p>

                                <span class="caption-subject uppercase font-blue-soft"><h3 class="bold">What is GTP ?</h3></span>
                                <p>
                                    Ground Time Performance (GTP) is a measure of the ability of aircraft ground time to be on time. Ground Time Performance is one of important component 
                                    that will affect On-Time Performance. 
                                    The definition of ground time itself means the time during aircraft on the ground in order to prepare aircraft before depart. 
                                    The standard of ground time of aircraft is different depends on type of aircraft, such as:
                                <ol>
                                    <li>
                                        B777 and A330: 60 minutes
                                    </li>
                                    <li>
                                        B738 and B7M8 (737 Max 8): 45 minutes
                                    </li>
                                    <li>
                                        CRJ: 40 minutes
                                    </li>
                                    <li>
                                        ATR: 30 minutes
                                    </li>
                                </ol>

                                </p>

                                <span class="caption-subject uppercase font-blue-soft"><h3 class="bold">How do we Calculate GTP ?</h3></span>

                                <p>3 criterias to calculate Ground Time Performance, such as:</p>
                                <ol>
                                    <li>
                                        Actual Ground Time (Time interval between aircraft arrived until the next departure actual) <= Expected Ground Time (Time interval between aircraft arrived until the next schedule)
                                    </li>
                                    <li>
                                        Actual Ground Time <= Standard Ground Time
                                    </li>
                                    <li>
                                        For first departure of aircraft, the departure actual shall no more than departure schedule time.
                                    </li>
                                </ol>
                                <p class="bold" style="font-size: 10px;">*source of data: ODS (ACARS and MVT)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" >            
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject uppercase font-blue-soft" style="font-size: 18pt;">Contact</span>
                                </div>
                                <div class="actions">
                                    <i class="fa fa-phone fa-2x" ></i>
                                </div>
                            </div>                        
                            <div  class="portlet-body">
                                <p style="text-align: justify;">If you have any inquiries or issues regarding the web, kindly please contact by email: 
                                <ul> 
                                    <li>mohammad.oktri@garuda-indonesia.com (JKTOGL-3) - JIDOM: 1696 </li>
                                    </ul></p>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>


    </div>
    <!-- END PAGE BASE CONTENT -->

</div>   
<!-- END CONTENT BODY -->


</div>

<!-- END CONTENT -->