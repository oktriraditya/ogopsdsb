<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/ga-icon.png" /> 
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<style>
    @import url("https://fonts.googleapis.com/css?family=Montserrat");
    .huruf{
        font-family: 'Montserrat',sans-serif;
    }
    #watermark {
        position: fixed;

        /** 
            Set a position in the page for your image
            This should center it vertically
        **/
        bottom:   12cm;
        left:     -1cm;

        /** Change image dimensions**/
        width:    8cm;
        height:   8cm;

        /** Your watermark should be behind every content**/
        z-index:  -1000;
        opacity: .5;
    }

</style>
<!-- BEGIN CONTENT -->
<div id="watermark">
    <img src="gia.png" height="476" width="800" style="opacity: .3" />
</div>
<!--<form method="GET" action="<?php echo base_url(); ?>occ/download_pdf">

                            <input type="hidden" name="station" value="<?php echo $station ?>"><br>
                            <input type="submit" value="PDF" class="btn blue"><i class="fa fa-download"></i>
                        </form> -->
<!--style="background-image: url('<?php echo base_url(); ?>assets/images/ga_1.jpg'); background-position-y: -400px;"-->


    <div class="portlet light" >
        <div class="portlet-body">  
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >            
                    <div class="portlet light huruf">
                        <div class="portlet-title bold" style="font-size: 18pt; font-family: sans-serif; color: #0088cc" align="center">

                            CREW INFORMATION SHEET<br>
                            STATION <?php if (isset($org)) echo $org ?>
                        </div>                        
                        <div class="portlet-body">
                            <h3 style="text-align: center">Schedule of Departure</h3>
                            <table align="center" style="border: 1px solid black; background-color: #f5f5f5; border-radius: 5px; padding-left: 10px; ">
                                <thead>
                                    <tr>
                                        <th>
                                            GA
                                        </th>
                                        <th>
                                            :&nbsp;
                                        </th>
                                        <th>
                                            <?php if (isset($fnum)) echo $fnum; ?> / <?php if (isset($org)) echo $org . "-" . $dest; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            DATE&nbsp; 
                                        </th>
                                        <th>
                                            :&nbsp;
                                        </th>
                                        <th>
                                            <?php if(isset($sdate))echo date("l, d F Y",  strtotime($sdate)); ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            STD
                                        </th>
                                        <th>
                                            :&nbsp;
                                        </th>
                                        <th>
                                            <?php if (isset($std)) echo $std; ?> LT
                                        </th>
                                    </tr>
                                </thead>
                            </table><br>
                            <table align="center">
                                <thead>
                                    <tr>
                                        <th>
                                            Hotel Calling Time
                                        </th>
                                        <th>
                                            :&nbsp;
                                        </th>
                                        <th>
                                            <?php
                                            
                                            if (isset($std) && isset($hotel_calling)){
                                                $sTime = new DateTime($std);
                                                echo $sTime->modify("-".$hotel_calling." minutes")->format("H:i");
                                            }
                                            ?> Local Time
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            Hotel Pick Up&nbsp; 
                                        </th>
                                        <th>
                                            :&nbsp;
                                        </th>
                                        <th>
                                            <?php
                                            if (isset($std)&& isset($hotel_pickup)){
                                                $sTime = new DateTime($std);
                                                echo $sTime->modify("-".$hotel_pickup." minutes")->format("H:i");
                                            }
                                            ?> Local Time
                                        </th>
                                    </tr>
                                </thead>
                            </table><br>

                            <div>
                                <div style="vertical-align: middle; font-size: 14pt"> Contact Person</div>
                                <table style="width: 100%; border: 1px solid black; background-color: #f5f5f5; opacity: .1; border-radius: 5px; padding-left: 10px; " >

                                    <tr style="font-size: 12pt; font-family: sans-serif; color: #0480be; text-decoration: none;">
                                        <th >
                                            <img src="cisicon/clerk-with-tie.png" width='20' height="20" style="margin-top: 4px;"/> General Manager
                                        </th>
                                        <th>
                                            <img src="cisicon/cpicon.png" width='20' height="20" style="margin-top: 4px;"/> Station Manager
                                        </th>
                                    </tr>


                                    <tr>
                                        <td>
                                            <?php if (isset($gm_name))echo $gm_name;?>
                                        </td>
                                        <td>
                                            <?php if (isset($kk_name))echo $kk_name;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($gm_phone1))echo $gm_phone1;?>
                                        </td>
                                        <td>
                                            <?php if (isset($kk_phone1))echo $gm_phone1;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($gm_phone2))echo $gm_phone2;?>

                                        </td>
                                        <td>
                                            <?php if (isset($kk_phone2))echo $kk_phone2;?>
                                        </td>
                                    </tr>
                                    <tr style="font-size: 12pt; font-family: sans-serif; color: #0480be; text-decoration: none;">
                                        <th >
                                            <img src="cisicon/cpicon.png" width='20' height="20" style="margin-top: 4px;"/> <?php if (isset($country) && $country == "ID"){?>Duty Manager Operation<?php } else {?>Indonesian Embassy<?php }?>
                                        </th>
                                        <th>
                                            <img src="cisicon/help-operator.png" width='20' height="20" style="margin-top: 4px;"/> Customer Service
                                        </th>
                                    </tr>


                                    <tr>
                                        <td>
                                            <?php if (isset($ko_name))echo $ko_name;?>
                                        </td>
                                        <td>
                                            <?php if (isset($ks_name))echo $ks_name;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($ko_phone1))echo $ko_phone1;?>
                                        </td>
                                        <td>
                                            <?php if (isset($ks_phone1))echo $ks_phone1;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <?php if (isset($ko_phone2))echo $ko_phone2;?>

                                        </td>
                                        <td>
                                            <?php if (isset($ks_phone2))echo $ks_phone2;?>
                                        </td>
                                    </tr>
                                    <tr style="font-size: 12pt; font-family: sans-serif; color: #0480be; text-decoration: none;">
                                        <th >
                                            <img src="cisicon/maintenance.png" width='20' height="20" style="margin-top: 4px;"/> Maintenance / GMFAA
                                        </th>
                                        <th>
                                            <img src="cisicon/hotel-building.png" width='20' height="20" style="margin-top: 4px;"/> Hotel
                                        </th>
                                    </tr>


                                    <tr>
                                        <td>
                                            <?php if (isset($gmf_name))echo $gmf_name;?>
                                        </td>
                                        <td>
                                            <?php if (isset($hotel_name))echo $hotel_name;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($gmf_phone1))echo $gmf_phone1;?>
                                        </td>
                                        <td>
                                           <?php if (isset($hotel_address))echo $hotel_address;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($gmf_phone2))echo $gmf_phone2;?>

                                        </td>
                                        <td>
                                            <?php if (isset($hotel_phone1))echo $hotel_phone1;?><?php if (isset($hotel_phone2))echo " / ".$hotel_phone2;?>
                                        </td>
                                    </tr>
                                    <tr style="font-size: 12pt; font-family: sans-serif; color: #0480be; text-decoration: none;">
                                        <th >
                                            <img src="cisicon/frontal-taxi-cab.png" width='20' height="20" style="margin-top: 4px;"/> Crew Transportation
                                        </th>
                                        <th>
                                            <img src="cisicon/emergency-call.png" width='20' height="20" style="margin-top: 4px;"/> Hospital
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($transport_name))echo $transport_name;?>
                                        </td>
                                        <td>
                                            <?php if (isset($hospital_name))echo $hospital_name;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($transport_address))echo $transport_address;?>
                                        </td>
                                        <td>
                                           <?php if (isset($hospital_address))echo $hospital_address;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if (isset($transport_phone1))echo $transport_phone1;?><?php if (isset($transport_phone2))echo " / ".$transport_phone2;?>
                                        </td>
                                        <td>
                                           <?php if (isset($hospital_phone1))echo $hospital_phone1;?><?php if (isset($hospital_phone2))echo " / ".$hospital_phone2;?>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <?php //echo $jadwalsholat["status"];   ?>



                        </div>



                    </div>
                </div>
            </div>
            <div class="row" style="bottom: 225;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >            
                    <div class="portlet light huruf">
                        <div class="portlet-title bold" style="margin-top: 20px;">
                            <b>Note for Crew:</b>
                        </div>
                        <div class="portlet-body">
                           <?php if (isset($noted))echo $noted;?>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >            
                    <div class="portlet light huruf">
                        <div class="portlet-title bold" style="font-size: 18pt; font-family: sans-serif; color: #0088cc; margin-bottom: 10px;" align="center">
                            <b>Prayer Times Schedule</b>
                        </div>
                        <div class="portlet-body">
                            <table style=" margin-top: 0px; width: 100%; border-collapse: collapse; background-color: #f5f5f5; opacity: .1; border-radius: 5px;" >

                                <tr style="font-size: 10pt; font-family: sans-serif; color: #0480be; text-align: center; vertical-align: middle; background-color: #f5f5f5; opacity: .3;">
                                    <th style="border: 1px solid black;border-collapse: collapse;" rowspan="2">
                                        Date
                                    </th>
                                    <th style="border: 1px solid black;border-collapse: collapse;" colspan="6">
                                        Prayer Times (Local Time)*
                                    </th>
                                </tr>
                                <tr style="font-size: 10pt; font-family: sans-serif; color: #0480be; text-align: center; vertical-align: middle; background-color: #f5f5f5; opacity: .3;">
                                    <th style="border: 1px solid black; border-collapse: collapse;">
                                        Imsak
                                    </th>
                                    <th style="border: 1px solid black; border-collapse: collapse;">
                                        Subuh / Fajr
                                    </th>
                                    <th style="border: 1px solid black;border-collapse: collapse;" >
                                        Dzuhur
                                    </th>
                                    <th style="border: 1px solid black;border-collapse: collapse;" >
                                        Ashar
                                    </th>
                                    <th style="border: 1px solid black;border-collapse: collapse;" >
                                        Maghrib
                                    </th>
                                    <th style="border: 1px solid black;border-collapse: collapse;" >
                                        Isya
                                    </th>
                                </tr>

                                <?php if ($jadwalsholat["status"] == "OK" && $jadwalsholat["code"] == 200) { ?>
                                    <?php foreach ($jadwalsholat["data"] as $key => $value) { 
                                        if (strtotime($value["date"]["gregorian"]["date"]) >= strtotime(date("d-m-Y"))){?>
                                
                                        <tr style="font-size: 10pt; font-family: sans-serif; color: #000000; text-align: center; vertical-align: middle;">
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php echo date("D, d M Y", strtotime($value["date"]["gregorian"]["date"])); ?>
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $imsak = explode(' ', $value["timings"]["Imsak"]); echo $imsak[0]; ?> 
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $fajr = explode(' ', $value["timings"]["Fajr"]);echo $fajr[0]; ?> 
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $dhuhr = explode(' ', $value["timings"]["Dhuhr"]);echo $dhuhr[0]; ?> 
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $asr = explode(' ', $value["timings"]["Asr"]);echo $asr[0]; ?> 
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $maghrib = explode(' ', $value["timings"]["Maghrib"]);echo $maghrib[0]; ?> 
                                            </td>
                                            <td style="border: 1px solid black;border-collapse: collapse;" >
                                                <?php $isha = explode(' ', $value["timings"]["Isha"]);echo $isha[0]; ?> 
                                            </td>
                                        </tr>

                                        <?php }} ?>
                                <?php } ?>
                            </table>
                            <small style="font-size: 8pt;"><b>*Calculation Method: Muslim World League. Juristics Method: Shafii. source:https://aladhan.com/</b></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- END PAGE BASE CONTENT -->

<!-- END CONTENT BODY -->
<!-- END CONTENT -->