<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
<style type="text/css">
	.kotak {
		margin: auto;
		background-color: white;
		width: 100%;
		padding: 10px 10px 10px 10px;
	}

	.tengah {
		text-align: center;
	}
	
	#judul {
		text-align: center;
		font-family: Corbel;
		font-size: 24;		
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('.formTraining').hide();
		$('.formBatch').hide();
		$('.formGround').hide();
		$('.formRoute').hide();
		$('.formQualified').hide();
		var btn = "<?php echo $button; ?>";		
		switch (btn) {
		   case 'tr':
				$('.formTraining').show();
				$('.formBatch').show();
		   break;
		
		   case 'rt': $('.formRoute').show();		   
		   break;
		
		   case 'ql': $('.formQualified').show();
		   break;		
		  //default:  document.write("unknown")
		}
	});
</script>

<!--css end-->     
    <div class="kotak">
		<form id="rendered-form"><div class="rendered-form">
			<!--<div class=""><h1 id="control-7960701">Edit Trainees<br></h1></div>-->
			<div class="fb-text form-group field-text-1548150064580">
				<label for="text-1548150064580" class="fb-text-label">ID & Ground Start</label>
				<input type="text" class="form-control" name="text-1548150064580" id="text-1548150064580" value="<?php
				foreach ($trainees as $key => $value){
				echo "{".$key."/".$trainees[$key]."}  ";
				}				
				?>" disabled>
			</div>
			<div class="formTraining">
				<label for="select-1548149985511" class="fb-select-label">Training Program<br></label>
				<select class="form-control" name="select-1548149985511" id="select-1548149985511">								
					<option value="option-1" id="select-1548149985511-0">Option 1</option>
					<option value="option-2" id="select-1548149985511-1">Option 2</option>
					<option value="option-3" id="select-1548149985511-2">Option 3</option>			
				</select>
				<a id="editTraining">Add & Edit Training Program</a>
			</div>
			<br> 
			<div class="formBatch">
				<label for="select-1548149985511" class="fb-select-label">Batch Name<br></label>
				<select class="form-control" name="select-1548149985511" id="select-1548149985511">
					<option value="option-1" id="select-1548149985511-0">Option 1</option>
					<option value="option-2" id="select-1548149985511-1">Option 2</option>
					<option value="option-3" id="select-1548149985511-2">Option 3</option>
				</select>
				<a id="editBatch">Add & Edit Batch name</a>
			</div> 			
			<div class="formGround">
				<label for="date-1548149782354" class="fb-date-label">Start Ground<br></label>
				<input type="date" class="form-control" name="date-1548149782354" id="date-1548149782354">
			</div>
			<div class="formRoute">
				<label for="date-1548149782354" class="fb-date-label">Start Route<br></label>
				<input type="text" class="form-control" name="dateto" id="eDate" value="<?php echo date('Y-m-d');?>"> 
			</div>
			<div class="formQualified">
				<label for="date-1548149782354" class="fb-date-label">Qualified<br></label>
				<input type="text" class="form-control" name="dateto" id="eDate" value="<?php echo date('Y-m-d');?>"> 
			</div>
			<br>	
			<div class="fb-button form-group field-button-1548149560109">
				<button type="button" name="button-1548149560109" id="button-1548149560109">Submit</button>
			</div>		
		</div></form>
	</div>	

	
	<div id="view" class="modal container fade" tabindex="-1" style="margin:auto;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Add Training Program</h4>
		</div>
		<div class="modal-body" style="height:450px !important;">
			<iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">		
			</iframe>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		</div>
	</div>



