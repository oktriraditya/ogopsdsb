<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<div id="autoref" class="page-content-wrapper" >
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php
                    //date_default_timezone_set('Asia/Jakarta'); 
                    echo $title;
                    ?>
                    <small><?php echo $title_small; ?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height blue">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>      

                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>  
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->  
        <!-- BEGIN PAGE BASE CONTENT -->                                   
		<ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>            
            <li>
                <span class="active">Dashboad Crew Strength</span>
            </li>
        </ul>
		

<!--style="background-image: url('<?php echo base_url(); ?>assets/images/ga_1.jpg'); background-position-y: -400px;"-->
        <div class="portlet light bordered" >
            <div class="portlet-body">  
            <!--    <div class="row" style='text-align: center;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" > 
                        <div class="portlet light">
                            <div class="caption ">
                                <span class="caption-subject font-dark ">A system that can show data of operational performance REAL TIME from all airports and routes of Garuda Indonesia.
                                    The system will be automating the whole step of operational data collection and processing.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" > 
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-blue-soft" style="font-size: 18pt;">Real Time</span>
                                </div>
                                <div class="actions">
                                    <i class="fa fa-flash fa-2x" ></i>
                                </div>
                            </div>                        
                            <div class="portlet-body">
                               Data will be available faster, more accurate, and precision.
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" > 
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject  font-blue-soft" style="font-size: 18pt;">Computerize</span>
                                </div>
                                <div class="actions">
                                    <i class="fa fa-desktop fa-2x" ></i>
                                </div>
                            </div>                        
                            <div class="portlet-body">No need to collect and process the data manually. </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" > 
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject  font-blue-soft" style="font-size: 18pt;">Easy to Use</span>
                                </div>
                                <div class="actions">
                                    <i class="fa fa-gg fa-2x" ></i>
                                </div>
                            </div>                        
                            <div class="portlet-body">The objective is we hope this system will help everyone in their day-to-day job. So, we made this system easy to use and easy to understand.</div>
                        </div>
                    </div>
                </div>-->
                
                <div class="row">                       
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject uppercase font-blue-soft" style="font-size: 18pt;"> Crew Strength </span>
							</div>
						</div>  							
						<div class="portlet-body"> 
							<div id="chartdiv"></div>
						</div>
					</div>               
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>  

<div id="chartdiv"></div>





<style>
body {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}

#chartdiv {
  width: 100%;
  height: 500px;
}
</style>


<script>
/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 * 
 * For more information visit:
 * https://www.amcharts.com/
 * 
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.paddingRight = 20;

var data = [];
var visits = 10;
var previousValue;

for (var i = 0; i < 100; i++) {
    visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

    if(i > 0){
        // add color to previous data item depending on whether current value is less or more than previous value
        if(previousValue <= visits){
            data[i - 1].color = chart.colors.getIndex(0);
        }
        else{
            data[i - 1].color = chart.colors.getIndex(5);
        }
    }    
    
    data.push({ date: new Date(2018, 0, i + 1), value: visits });
    previousValue = visits;
}

chart.data = data;

var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.axisFills.template.disabled = true;
dateAxis.renderer.ticks.template.disabled = true;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.tooltip.disabled = true;
valueAxis.renderer.minWidth = 40;
valueAxis.renderer.axisFills.template.disabled = true;
valueAxis.renderer.ticks.template.disabled = true;

var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.dateX = "date";
series.dataFields.valueY = "value";
series.strokeWidth = 3;
series.tooltipText = "value: {valueY}, day change: {valueY.previousChange}";

// set stroke property field
series.propertyFields.stroke = "color";

chart.cursor = new am4charts.XYCursor();

var scrollbarX = new am4core.Scrollbar();
chart.scrollbarX = scrollbarX;

chart.events.on("ready", function(ev) {
  dateAxis.zoomToDates(
    chart.data[50].date,
    chart.data[80].date
  );
});
</script>


