<style>
    /* Full-width input fields */
    .uname, .psw{
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    /* Set a style for all buttons */
    .tombolsignin {
        background-color: #0099cc;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    button:hover {
        opacity: 0.8;
    }

    span.psw {
        float: right;
        padding-top: 16px;
    }
    .formlogin{
        margin: 10px 10px 10px 10px;
    }
    /* The Modal (background) */
    .animated {
            background-image: url(/css/images/logo.png);
            background-repeat: no-repeat;
            background-position: center top;
            text-align: center; 
            width: 100%;
            -webkit-animation-duration: 8s;animation-duration: 8s;
            -webkit-animation-fill-mode: both;animation-fill-mode: both;
         }
         
         @-webkit-keyframes fadeOut {
            0% {opacity: 1;}
            100% {opacity: 0;}
         }
         
         @keyframes fadeOut {
            0% {opacity: 1;}
            100% {opacity: 0;}
         }
         
         .errorFadeout {
            -webkit-animation-name: fadeOut;
            animation-name: fadeOut;
         }
</style>
<!-- BEGIN HEADER -->
       
<div class="page-header navbar navbar-fixed-top">
    
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <!--        <div class="page-logo">           
                    <div class="menu-toggler sidebar-toggler">
                         DOC: Remove the above "hide" to enable the sidebar toggler button on header 
                    </div>
                </div>-->
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <div class="page-actions">
            <a href="<?php echo base_url(); ?>main"><img src="<?php echo base_url(); ?>assets/images/ga.png" alt="logo" class="logo-default" width="180" height="35" /> </a>
            </div>
        
        
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">        
            <!-- BEGIN TOP NAVIGATION MENU -->
            
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">                                                           
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <?php if ($this->session->userdata('sNopeg')) { ?>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php echo base_url();?>assets/layouts/layout4/img/avatar.png" />
                            <span class="username username-hide-on-mobile"> 
                            <?php echo isset($this->session->userdata('aProfile')['name']) ? $this->session->userdata('aProfile')['name'] : $this->session->userdata('sNopeg');?>  
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo base_url();?>main/profile">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>                            
                            <li class="divider"> </li>
                            <li>
                                <a href="<?php echo base_url();?>auth/logout">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                    <!-- END USER LOGIN DROPDOWN -->                   
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>

<!-- END HEADER