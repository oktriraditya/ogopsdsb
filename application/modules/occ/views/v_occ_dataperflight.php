<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
<style type="text/css">
    .kotak {
        margin: auto;
        background-color: white;
        width: 90%;
        padding: 10px 10px 10px 10px;
    }

    .tengah {
        text-align: center;
    }

    #judul {
        text-align: center;
        font-family: corbel;
        font-size: 24;
    
    }
    .kanan{
        text-align: right;
    }
</style>
<!--css end-->

<!--FLIGHT NUMBER-->
<!--table flight number begin-->      


    <h3 id="judul"> Daily per Flight </h3>
    <div class="page-footer-inner kanan"> 2018 &copy; <a href="#">created by: Oktri - JKTRGL3</a>
    <form action="<?php echo base_url() ?>occ/dataperflight">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" name="datefrom" id="sDate" value="<?php if (isset($datefrom)) echo date($datefrom);
else echo date('Y-m-d'); ?>">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="dateto" id="eDate" value="<?php if (isset($dateto)) echo date($dateto);
else echo date('Y-m-d'); ?>"> 
                            </div>                            
                        </div>
                    </div>
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>           
                </div>       
            </div>

        </div>
    </form> 
    <div class="tools"> </div> 
    <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="fltData">
        <thead>
            <tr> 
                <th width="5%" class="tengah">No.</th>
                <th width="5%" class="tengah">Departure Date</th>
                <th width="5%" class="tengah">Flight Number</th>
                <th width="5%" class="tengah">Aircraft Reg</th>
                <th width="5%" class="tengah">Aircraft Type</th>
                <th width="5%" class="tengah">Origin</th>
                <th width="5%" class="tengah">Dest</th>
                <th width="5%" class="tengah">Latest Dest</th>
                <th width="5%" class="tengah">STD</th>
                <th width="5%" class="tengah">STA</th>
                <th width="5%" class="tengah">ATD</th>
                <th width="5%" class="tengah">ATA</th>
                <th width="5%" class="tengah">Delay Length</th>
                <th width="5%" class="tengah">Delay Length Arrival</th>
                <th width="5%" class="tengah">CD1 Departure</th>
                <th width="5%" class="tengah">CD2 Departure</th>
                <th width="5%" class="tengah">Duration CD1 Departure</th>                
                <th width="5%" class="tengah">Duration CD2 Departure</th>
                <th width="5%" class="tengah">CD1 Arrival</th>
                <th width="5%" class="tengah">CD2 Arrival</th>
                <th width="5%" class="tengah">Duration CD1 Arrival</th>                
                <th width="5%" class="tengah">Duration CD2 Arrival</th>
                <th width="5%" class="tengah">Tipe Penerbangan</th>
                <th width="5%" class="tengah">Estimated Block Time</th>                
                <th width="5%" class="tengah">Actual Block Time</th>
                <th width="5%" class="tengah">Estimated TAXI OUT</th>                
                <th width="5%" class="tengah">Actual TAXI OUT</th>
                <th width="5%" class="tengah">Estimated TAXI IN</th>                
                <th width="5%" class="tengah">Actual TAXI IN</th>
                <th width="5%" class="tengah">First Departure</th>
                <th width="5%" class="tengah">ZMD First Departure</th>
                <th width="5%" class="tengah">OTP First Departure</th>
                <th width="5%" class="tengah">OTP Dep</th>
                <th width="5%" class="tengah">Delay Status</th>
                <th width="5%" class="tengah">OTP Arr</th>
                <th width="5%" class="tengah">Divert Status</th>
                <th width="5%" class="tengah">TECHNIC PREVIOUS</th>
                <th width="5%" class="tengah">STNH PREVIOUS</th>
                <th width="5%" class="tengah">COMM PREVIOUS</th>
                <th width="5%" class="tengah">SYST PREVIOUS</th>
                <th width="5%" class="tengah">FLOPS PREVIOUS</th>
                <th width="5%" class="tengah">APTF PREVIOUS</th>
                <th width="5%" class="tengah">WEATHER PREVIOUS</th>
                <th width="5%" class="tengah">MISC PREVIOUS</th>
                <th width="5%" class="tengah">TECHNIC ORIGIN</th>
                <th width="5%" class="tengah">STNH ORIGIN</th>
                <th width="5%" class="tengah">COMM ORIGIN</th>
                <th width="5%" class="tengah">SYST ORIGIN</th>
                <th width="5%" class="tengah">FLOPS ORIGIN</th>
                <th width="5%" class="tengah">APTF ORIGIN</th>
                <th width="5%" class="tengah">WEATHER ORIGIN</th>
                <th width="5%" class="tengah">MISC ORIGIN</th>
                <th width="5%" class="tengah">TECHNIC</th>
                <th width="5%" class="tengah">STNH</th>
                <th width="5%" class="tengah">COMM</th>
                <th width="5%" class="tengah">SYST</th>
                <th width="5%" class="tengah">FLOPS</th>
                <th width="5%" class="tengah">APTF</th>
                <th width="5%" class="tengah">WEATHER</th>
                <th width="5%" class="tengah">MISC</th>
                <th width="5%" class="tengah">REMARKS</th>
            </tr>
        </thead>
        <?php
        $data = array();
        foreach ($daily_record_per_flight as $key => $value) {
            $data['TANGGAL'][] = $value['TANGGAL'];
            $data['FLTNUM'][] = $value['FLTNUM'];
            $data['AIRCRAFTREG'][] = $value['AIRCRAFTREG'];
            $data['AIRCRAFTTYPE'][] = $value['AIRCRAFTTYPE'];
            $data['SCHED_DEPARTUREAIRPORT'][] = $value['SCHED_DEPARTUREAIRPORT'];
            $data['SCHED_ARRIVALAIRPORT'][] = $value['SCHED_ARRIVALAIRPORT'];
            $data['LATEST_DEPARTUREAIRPORT'][] = $value['LATEST_DEPARTUREAIRPORT'];
            $data['LATEST_ARRIVALAIRPORT'][] = $value['LATEST_ARRIVALAIRPORT'];
            $data['SCHEDULED_DEPDT_LC'][] = $value['SCHEDULED_DEPDT_LC'];
            $data['SCHEDULED_ARRDT_LC'][] = $value['SCHEDULED_ARRDT_LC'];
            $data['ACTUAL_BLOCKOFF_LC'][] = $value['ACTUAL_BLOCKOFF_LC'];
            $data['ACTUAL_BLOCKON_LC'][] = $value['ACTUAL_BLOCKON_LC'];
            $data['OTPDEP'][] = $value['OTPDEP'];
            $data['OTPARR'][] = $value['OTPARR'];
            $data['OTPZMD'][] = $value['OTPZMD'];
            $data['delaylength'][] = date_diff(new DateTime($value['SCHEDULED_DEPDT_LC']),new DateTime($value['ACTUAL_BLOCKOFF_LC']));
            $data['delaylengtharr'][] = date_diff(new DateTime($value['SCHEDULED_ARRDT_LC']),new DateTime($value['ACTUAL_BLOCKON_LC']));
            $data['estimatedbt'][] = date_diff(new DateTime($value['SCHEDULED_DEPDT_LC']),new DateTime($value['SCHEDULED_ARRDT_LC']));
            $data['actualbt'][] = date_diff(new DateTime($value['ACTUAL_BLOCKOFF_LC']),new DateTime($value['ACTUAL_BLOCKON_LC']));
            $data['ACTUAL_TAXIOUT_LC'][] = date_diff(new DateTime($value['ACTUAL_TAKEOFF_LC']),new DateTime($value['ACTUAL_BLOCKOFF_LC']));
            $data['ACTUAL_TAXIIN_LC'][] = date_diff(new DateTime($value['ACTUAL_BLOCKON_LC']),new DateTime($value['ACTUAL_TOUCHDOWN_LC']));
            $data['ESTIMATED_TAXIOUT_LC'][] = date_diff(new DateTime($value['ESTIMATED_TAKEOFF_LC']),new DateTime($value['SCHEDULED_DEPDT_LC']));
            $data['ESTIMATED_TAXIIN_LC'][] = date_diff(new DateTime($value['SCHEDULED_ARRDT_LC']),new DateTime($value['ESTIMATED_TOUCHDOWN_LC']));
            $data['cd1'][] = $value['CD1'];
            $data['cd2'][] = $value['CD2'];
            $data['delaycd1'][] = $value['DELAYLENGTH1'];
            $data['delaycd2'][] = $value['DELAYLENGTH2'];
            $data['REMARKS'][] = $value['REMARKS'];
            /*$data['technic'][] = $value['TECH'];
                    $data['STNH'][] = $value['STNH'];
                    $data['COMM'][] = $value['COMM'];
                    $data['SYST'][] = $value['SYST'];
                    $data['FLOPS'][] = $value['FLOPS'];
                    $data['APTF'][] = $value['APTF'];
                    $data['WEATHER'][] = $value['WEATHER'];
                    $data['MISC'][] = $value['MISC'];*/
        }
        $count = count($daily_record_per_flight);
        ?>
        <tbody>
            <?php
            $domstn=array('AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
                ,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
                ,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
                ,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR');
            $intstn=array('PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO');
            $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
            $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
            $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
            $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
            $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
            $arrSYST = array('50', '55', '56', '57', '58');
            $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
            $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');
            
            $arrAPTFP = array('80');
            $arrCOMCP = array('30');
            $arrFLOPP = array('60');
            $arrOTHRP = array('90');
            $arrSTNHP = array('10','20');
            $arrSYSTP = array('50');
            $arrTECHP = array('40');
            $arrWTHRP = array('70');
            
            $arrAPTFO = array('81', '82', '83', '84', '85', '86', '87', '88', '89');
            $arrCOMCO = array('09', '14', '16', '25', '91', '92');
            $arrFLOPO = array('01', '02', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
            $arrOTHRO = array('51', '52', '93', '97', '98', '99');
            $arrSTNHO = array('11', '12', '13', '15', '17', '18', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
            $arrSYSTO = array('55', '56', '57', '58');
            $arrTECHO = array('41', '42', '43', '44', '45', '46', '47', '48');
            $arrWTHRO = array('71', '72', '73', '75', '76', '77');
            
            $sDiff = '+000000015 00:00:00.000000000';
            $firstdep = array();
            $delaylengthmin=0;
            $delaylengthplus=0;
            for ($i = 0; $i < $count; $i++) {
                ?>
                <tr class="tengah">
                    <td> <?php echo $i + 1; ?> </td>
                    <td> <?php echo $data['TANGGAL'][$i]; ?></td>
                    <td> <?php echo $data['FLTNUM'][$i]; ?> </td>
                    <td> <?php echo substr($data['AIRCRAFTREG'][$i],3); ?></td>
                    <td> <?php echo $data['AIRCRAFTTYPE'][$i]; ?></td>
                    <td> <?php echo $data['SCHED_DEPARTUREAIRPORT'][$i]; ?></td>
                    <td> <?php echo $data['SCHED_ARRIVALAIRPORT'][$i]; ?></td>
                    <td> <?php echo $data['LATEST_ARRIVALAIRPORT'][$i]; ?></td>
                    <td> <?php echo $data['SCHEDULED_DEPDT_LC'][$i]; ?></td>
                    <td> <?php echo $data['SCHEDULED_ARRDT_LC'][$i]; ?></td>
                    <td> <?php echo $data['ACTUAL_BLOCKOFF_LC'][$i]; ?></td>
                    <td> <?php echo $data['ACTUAL_BLOCKON_LC'][$i]; ?></td>
                    
                    <td> <?php if ($data['delaylength'][$i]->invert){$delaylengthmin = -1*$data['delaylength'][$i]->h*60 + -1*$data['delaylength'][$i]->i;echo $delaylengthmin;}
                    else{$delaylengthplus = $data['delaylength'][$i]->h*60 + $data['delaylength'][$i]->i; echo $delaylengthplus;} ?></td>
                    <td> <?php if ($data['delaylengtharr'][$i]->invert){$delaylengthmin = -1*$data['delaylengtharr'][$i]->h*60 + -1*$data['delaylengtharr'][$i]->i;echo $delaylengthmin;}
                    else{$delaylengthplus = $data['delaylengtharr'][$i]->h*60 + $data['delaylengtharr'][$i]->i; echo $delaylengthplus;} ?></td>
                    <td> <?php if($data['cd1'][$i] != "00")echo $data['cd1'][$i]; else echo "";?></td>
                    <td> <?php if($data['cd2'][$i] != "00")echo $data['cd2'][$i]; else echo "";?></td>
                    <td> <?php //echo substr($data['delaycd1'][$i],2); 
                    if (strpos(substr($data['delaycd1'][$i],2),"H")!== false && strpos(substr($data['delaycd1'][$i],2),"M")!== false ){
                        $hour = explode("H", substr($data['delaycd1'][$i],2));//$mins = explode("M", substr($data['delaycd1'][$i],2));
                        $inthour1 = (int)$hour[0];
                        $inthour2 = (int)$hour[1];
                        echo ($inthour1*60)+$inthour2;
                    }else if(strpos(substr($data['delaycd1'][$i],2),"H")!== false){
                        $hour = explode("H", substr($data['delaycd1'][$i],2));
                        $inthour1 = (int)$hour[0];
                        echo $inthour1*60;
                    }
                    else if(strpos(substr($data['delaycd1'][$i],2),"M")!== false){
                        $mins = explode("M", substr($data['delaycd1'][$i],2));
                        echo $mins[0];
                    }
                    ?> </td>
                    <td> <?php //echo substr($data['delaycd2'][$i],2); 
                    if (strpos(substr($data['delaycd2'][$i],2),"H")!== false && strpos(substr($data['delaycd2'][$i],2),"M")!== false ){
                        $hour = explode("H", substr($data['delaycd2'][$i],2));//$mins = explode("M", substr($data['delaycd2'][$i],2));
                        $inthour1 = (int)$hour[0];
                        $inthour2 = (int)$hour[1];
                        echo ($inthour1*60)+$inthour2;
                    }else if(strpos(substr($data['delaycd2'][$i],2),"H")!== false){
                        $hour = explode("H", substr($data['delaycd2'][$i],2));
                        $inthour1 = (int)$hour[0];
                        echo $inthour1*60;
                    }
                    else if(strpos(substr($data['delaycd2'][$i],2),"M")!== false){
                        $mins = explode("M", substr($data['delaycd2'][$i],2));
                        echo $mins[0];
                    }?></td>
                    <?php if(strpos($data['REMARKS'][$i],"ARR_DL")!== false){
                        //$cdarr = explode("-", $data['REMARKS'][$i]);
                        //$lengthcd1 = explode("/", $cdarr[0]);
                        //$lengthcd2 = explode("/", $cdarr[1]);
                        //$cd1arr = explode(":",$lengthcd1[0]);
                        //$cd2arr = explode(":",$lengthcd2[0]);
                        //$durationcd1 = explode("MIN",$lengthcd1[1]);
                        //$durationcd2 = explode("MIN",$lengthcd2[1]);
                    ?>
                    <td> <?php
                        //echo $cd1arr[1];
                    ?></td>
                    <td> <?php //echo $cd2arr[1];?></td> 
                    <td> <?php //echo $durationcd1[0]; ?></td>
                    <td> <?php //echo $durationcd2[0]; ?></td>
                    <?php } else {?>
                    <td><?php echo "";?></td>
                    <td><?php echo "";?></td>
                    <td><?php echo "";?></td>
                    <td><?php echo "";?></td>
                    <?php }?>
                    
                    <td> <?php 
                    if (in_array($data['SCHED_DEPARTUREAIRPORT'][$i], $domstn) && in_array($data['SCHED_ARRIVALAIRPORT'][$i], $domstn))echo 'DOM';
                    else if (in_array($data['SCHED_DEPARTUREAIRPORT'][$i], $domstn) && in_array($data['SCHED_ARRIVALAIRPORT'][$i], $intstn)||
                            in_array($data['SCHED_DEPARTUREAIRPORT'][$i], $intstn) && in_array($data['SCHED_ARRIVALAIRPORT'][$i], $domstn))echo 'INT';
                    else echo '';?></td>
                    <td> <?php $estimatedbt = $data['estimatedbt'][$i]->h*60 + $data['estimatedbt'][$i]->i; echo $estimatedbt;?></td>
                    <td> <?php $actualbt = $data['actualbt'][$i]->h*60 + $data['actualbt'][$i]->i; echo $actualbt;?></td>
                    <td> <?php $estimatedtaxiout = $data['ESTIMATED_TAXIOUT_LC'][$i]->h*60 + $data['ESTIMATED_TAXIOUT_LC'][$i]->i; echo $estimatedtaxiout; ?></td>
                    <td> <?php $actualtaxiout = $data['ACTUAL_TAXIOUT_LC'][$i]->h*60 + $data['ACTUAL_TAXIOUT_LC'][$i]->i; echo $actualtaxiout; ?></td>
                    <td> <?php $estimatedtaxiin = $data['ESTIMATED_TAXIIN_LC'][$i]->h*60 + $data['ESTIMATED_TAXIIN_LC'][$i]->i; echo $estimatedtaxiin; ?></td>
                    <td> <?php $actualtaxiin = $data['ACTUAL_TAXIIN_LC'][$i]->h*60 + $data['ACTUAL_TAXIIN_LC'][$i]->i; echo $actualtaxiin; ?></td>
                    <td> <?php if ($i==0){ $firstdep[$i]=1; echo $firstdep[$i];}else if ($data['AIRCRAFTREG'][$i] == $data['AIRCRAFTREG'][$i-1]){ $firstdep[$i]=0; echo $firstdep[$i];}else if ($data['FLTNUM'][$i] == $data['FLTNUM'][$i-1]){ $firstdep[$i]=0; echo $firstdep[$i];} else { $firstdep[$i]=1; echo $firstdep[$i];}?></td>
                    <td> <?php if ($data['OTPZMD'][$i] == 1 && $firstdep[$i] == 1)echo 1;else echo 0;?></td>
                    <td> <?php if ($data['OTPDEP'][$i] == 1 && $firstdep[$i] == 1)echo 1;else echo 0;?></td>
                    <td> <?php if ($data['OTPDEP'][$i] == 1)echo 1; ELSE echo 0;?></td>
                    <td> <?php if ($data['OTPDEP'][$i] == 1)echo 0; ELSE echo 1;?></td>
                    <td> <?php if ($data['OTPARR'][$i] == 1)echo 1; ELSE echo 0;?></td>
                    <td> <?php if ($data['LATEST_ARRIVALAIRPORT'][$i] == $data['SCHED_DEPARTUREAIRPORT'][$i])echo "RTB"; 
                    else if ($data['LATEST_ARRIVALAIRPORT'][$i] != $data['SCHED_ARRIVALAIRPORT'][$i]) echo "Divert"; else echo ""?></td>
                    
                    <td> <?php if ((in_array($data['cd1'][$i], $arrTECHP) && in_array($data['cd2'][$i], $arrTECHP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrTECHP) || in_array($data['cd2'][$i], $arrTECHP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['technic'][$i];?> </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSTNHP) && in_array($data['cd2'][$i], $arrSTNHP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSTNHP) || in_array($data['cd2'][$i], $arrSTNHP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrCOMCP) && in_array($data['cd2'][$i], $arrCOMCP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrCOMCP) || in_array($data['cd2'][$i], $arrCOMCP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSYSTP) && in_array($data['cd2'][$i], $arrSYSTP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSYSTP) || in_array($data['cd2'][$i], $arrSYSTP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrFLOPP) && in_array($data['cd2'][$i], $arrFLOPP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrFLOPP) || in_array($data['cd2'][$i], $arrFLOPP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrAPTFP) && in_array($data['cd2'][$i], $arrAPTFP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrAPTFP) || in_array($data['cd2'][$i], $arrAPTFP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['APTF'][$i];?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrWTHRP) && in_array($data['cd2'][$i], $arrWTHRP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrWTHRP) || in_array($data['cd2'][$i], $arrWTHRP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrOTHRP) && in_array($data['cd2'][$i], $arrOTHRP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrOTHRP) || in_array($data['cd2'][$i], $arrOTHRP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    
                    <td> <?php if ((in_array($data['cd1'][$i], $arrTECHO) && in_array($data['cd2'][$i], $arrTECHO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrTECHO) || in_array($data['cd2'][$i], $arrTECHO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['technic'][$i];?> </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSTNHO) && in_array($data['cd2'][$i], $arrSTNHO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSTNHO) || in_array($data['cd2'][$i], $arrSTNHO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrCOMCO) && in_array($data['cd2'][$i], $arrCOMCO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrCOMCO) || in_array($data['cd2'][$i], $arrCOMCO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSYSTO) && in_array($data['cd2'][$i], $arrSYSTO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSYSTO) || in_array($data['cd2'][$i], $arrSYSTO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrFLOPO) && in_array($data['cd2'][$i], $arrFLOPO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrFLOPO) || in_array($data['cd2'][$i], $arrFLOPO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrAPTFO) && in_array($data['cd2'][$i], $arrAPTFO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrAPTFO) || in_array($data['cd2'][$i], $arrAPTFO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['APTF'][$i];?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrWTHRO) && in_array($data['cd2'][$i], $arrWTHRO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrWTHRO) || in_array($data['cd2'][$i], $arrWTHRO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrOTHRO) && in_array($data['cd2'][$i], $arrOTHRO))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrOTHRO) || in_array($data['cd2'][$i], $arrOTHRO))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    
                    <td> <?php if ((in_array($data['cd1'][$i], $arrTECH) && in_array($data['cd2'][$i], $arrTECH))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrTECH) || in_array($data['cd2'][$i], $arrTECH))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['technic'][$i];?> </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSTNH) && in_array($data['cd2'][$i], $arrSTNH))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSTNH) || in_array($data['cd2'][$i], $arrSTNH))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrCOMC) && in_array($data['cd2'][$i], $arrCOMC))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrCOMC) || in_array($data['cd2'][$i], $arrCOMC))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrSYST) && in_array($data['cd2'][$i], $arrSYST))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrSYST) || in_array($data['cd2'][$i], $arrSYST))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrFLOP) && in_array($data['cd2'][$i], $arrFLOP))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrFLOP) || in_array($data['cd2'][$i], $arrFLOP))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrAPTF) && in_array($data['cd2'][$i], $arrAPTF))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrAPTF) || in_array($data['cd2'][$i], $arrAPTF))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;//echo $data['APTF'][$i];?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrWTHR) && in_array($data['cd2'][$i], $arrWTHR))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrWTHR) || in_array($data['cd2'][$i], $arrWTHR))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    <td> <?php if ((in_array($data['cd1'][$i], $arrOTHR) && in_array($data['cd2'][$i], $arrOTHR))&& $data['OTPDEP'][$i] == 0) echo 2; else if ((in_array($data['cd1'][$i], $arrOTHR) || in_array($data['cd2'][$i], $arrOTHR))&& $data['OTPDEP'][$i] == 0)echo 1; else echo 0;?>  </td>
                    
                    <td> <?php echo $data['REMARKS'][$i];?></td>
                <?php } ?>
                   
                </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true
        });
    });
</script>
