<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/css/fixedColumns.bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">-->
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">	
    .tengah {
        text-align: center;
        vertical-align: central;			
    }    
</style>

<div id="output"> </div>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> List of Type Qualification Trainees </span>
        </div>
    </div>
	
    <div class="portlet-body"> 
        <div class="tools"></div>   		
		<table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="datatable">		
            <thead>
			    <tr style="font-weight:bold; background-color: rgb(233,236,243); color: rgb(60, 60, 60);">	
					<th class="tengah"> </th>						
					<th class="tengah"> Emp. ID </th>
					<th class="tengah"> Name </th>
					<th class="tengah"> Rank </th>	
					<th class="tengah"> Training Program </th>	
					<th class="tengah"> Batch Name </th>
					<th class="tengah"> Start Ground </th>						
					<th class="tengah"> Start Route </th>
					<th class="tengah"> Qualified </th>	
					<th class="tengah"> Training Duration </th>	
					<th class="tengah"> Training Status </th>	
					<th class="tengah"> Action </th>
				</tr>
            </thead>				
            <tbody> 
				<?php 
				$i=1;
				//echo $traineelist;
				//echo '<script type="text/javascript">alert("'. $traineelist .'")</script>';	
				//$traineelist=array();
				//echo $ok;
				$progress=0;
				$today = date('Y-m-d');
				$route = 0;
				$qualified = 0;

				foreach ($traineelist as $key => $value){					
				
				$startdate = new DateTime ($value['STARTTRAINING']);
				if ($value['ENDTRAINING'] >= $today) {					
					$enddate = new DateTime ($today);
					if (isset( $register[$value['STAFFID']][$value['STARTTRAINING']]['QUAL_EST'] )) {
						$qualified = $register[$value['STAFFID']][$value['STARTTRAINING']]['QUAL_EST']."<a>&#9998;</a>";
					} else {
						$qualified = "&#9998; <a>please fill</a>";
					}					
					if (isset($value['STARTROUTETRAINING'])) { 
						$progress = "in route training";
						$route = $value['STARTROUTETRAINING'];
					} else { 
						$progress = "in ground training";
						if (isset( $register[$value['STAFFID']][$value['STARTTRAINING']]['ROUTE_EST'] )) {
							$route = $register[$value['STAFFID']][$value['STARTTRAINING']]['ROUTE_EST']."<a>&#9998;</a>";
						} else {
							$route = "&#9998; <a>please fill</a>";
						}					
					}
				} else {
					$progress = "qualified / ended";
					$enddate = new DateTime ($value['ENDTRAINING']);	
					$qualified = $value['ENDTRAINING'];
				}			
				
				//$enddate = new DateTime ($value['ENDTRAINING']);
				//$enddate = new DateTime ($today);				
				$interval = $enddate->diff($startdate);				
			
				?>
					
				<tr>
					<td class="tengah"> <?php //echo $i;$i++; ?> </td>
					<td class="tengah"> <?php echo $value['STAFFID']; ?> </td>
					<td> <?php echo $value['FULLNAME']; ?> </td>
					<td class="tengah"> <?php echo $value['RANKS']; ?> </td>
					<td class="tengah"> <?php 	if (isset( $register[$value['STAFFID']][$value['STARTTRAINING']]['PROGRAM'] )) {
													echo $register[$value['STAFFID']][$value['STARTTRAINING']]['PROGRAM'];
												} else {
													echo ""; ?> &#9998; <a class="BtnEdit" data-target="#view" data-toggle="modal" id="tr">please fill</a> <?php
												}	?> </td>				
					<td class="tengah"> <?php 	if (isset( $register[$value['STAFFID']][$value['STARTTRAINING']]['BATCH'] )) {
													echo $register[$value['STAFFID']][$value['STARTTRAINING']]['BATCH'];
												} else {
													echo ""; ?> &#9998; <a class="BtnEdit" data-target="#view" data-toggle="modal" id="tr">please fill</a> 
													<?php
												}	?> </td>					
					<td class="tengah"> <?php echo $value['STARTTRAINING']; ?> </td>
					<td class="tengah"><?php echo $route; ?> </td> 
					<td class="tengah"> <?php echo $qualified;?> </td>
					<td class="tengah"> <?php echo ($interval->m)+ (($interval->y)*12)." month(s), ".$interval->d." day(s)"; ?> </td>
					<td class="tengah"> <?php echo $progress; ?> </td>
					<td class="tengah"> <?php echo "<a>clear</a>"; ?> </td>
				</tr>
				<?php
				//break;
				} ?>
			</tbody>			
		</table>	
	</div>
</div>

<div id="view" class="modal container fade" tabindex="-1" style="margin:auto;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Trainees</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">		
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {	
		table = $('#datatable').DataTable({
			dom: 'Bfrtip',
			pagingType: 'full_numbers',
			ordering: false,
			lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
			buttons: ['selectAll','selectNone','copy'],
			scrollX: true,
			scrollCollapse: true,
			columnDefs: [{	orderable: false,className: 'select-checkbox',	targets:0 }],	
			select: {style: 'os', selector: 'td:first-child'	},
			order: [[ 1, 'asc' ]]		
		});	
		
		$('#btnSelectedRows').on('click', function() {		  
		  var tblData = table.rows('.selected').data();
		  var tmpData;
		  $.each(tblData, function(i, val) {
			tmpData = tblData[i];
			alert(tmpData);
		  }); 
		})
		
		$("#datatable").on('click', '.BtnEdit', function(){   
			//var tblData = table.rows('.selected').count();
			var tblData = table.rows('.selected').data();
			var buttontype = $(this).attr("id");	
			var flt = "<?php echo $fleet; ?>";
			
			var get_url = "";
			$.each(tblData, function(i, val) {				
				get_url += "/"+tblData[i][1]+"/"+tblData[i][6] //ID & Start Ground;
			}); 	 			

			var target_url = "<?php echo base_url().'occ/edit_trainees/';?>"+buttontype+"/"+flt+get_url;
			alert (target_url);
			//var target_url = str.replace(" ", "_");

			var iframe = $("#viewFrame");
			iframe.attr("src", target_url);
			//return false;
		});	
    });
</script>