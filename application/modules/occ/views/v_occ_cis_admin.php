<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<script>
    function blockUI(el) {
        lastBlockedUI = el;
        App.blockUI({
            target: el,
            animate: true
        });
    }

    function unblockUI(el) {
        App.unblockUI(el);
    }

    jQuery(document).ready(function () {
        //loadrec();

        $('#submit').click(function () {
            blockUI(boxTable);
            var gm_name = $('#gm_name').val();
            var gm_phone1 = $('#gm_phone1').val();
            var gm_phone2 = $('#gm_phone2').val();
            var kk_name = $('#kk_name').val();
            var kk_phone1 = $('#kk_phone1').val();
            var kk_phone2 = $('#kk_phone2').val();
            var ko_name = $('#ko_name').val();
            var ko_phone1 = $('#ko_phone1').val();
            var ko_phone2 = $('#ko_phone2').val();
            var gmf_name = $('#gmf_name').val();
            var gmf_phone1 = $('#gmf_phone1').val();
            var gmf_phone2 = $('#gmf_phone2').val();
            var ks_name = $('#ks_name').val();
            var ks_phone1 = $('#ks_phone1').val();
            var ks_phone2 = $('#ks_phone2').val();
            var hotel_name = $('#hotel_name').val();
            var hotel_phone1 = $('#hotel_phone1').val();
            var hotel_phone2 = $('#hotel_phone2').val();
            var hotel_address = $('#hotel_address').val();
            var transport_name = $('#transport_name').val();
            var transport_phone1 = $('#transport_phone1').val();
            var transport_phone2 = $('#transport_phone2').val();
            var transport_address = $('#transport_address').val();
            var hospital_name = $('#hospital_name').val();
            var hospital_phone1 = $('#hospital_phone1').val();
            var hospital_phone2 = $('#hospital_phone2').val();
            var hospital_address = $('#hospital_address').val();
            var noted = $('#editor').val();
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>occ/insert_cis_admin",
                data: {"gm_name": gm_name,
                    "gm_phone1": gm_phone1,
                    "gm_phone2": gm_phone2,
                    "kk_name": kk_name,
                    "kk_phone1": kk_phone1,
                    "kk_phone2": kk_phone2,
                    "ko_name": ko_name,
                    "ko_phone1": ko_phone1,
                    "ko_phone2": ko_phone2,
                    "gmf_name": gmf_name,
                    "gmf_phone1": gmf_phone1,
                    "gmf_phone2": gmf_phone2,
                    "ks_name": ks_name,
                    "ks_phone1": ks_phone1,
                    "ks_phone2": ks_phone2,
                    "hotel_name": hotel_name,
                    "hotel_phone1": hotel_phone1,
                    "hotel_phone2": hotel_phone2,
                    "hotel_address": hotel_address,
                    "transport_name": transport_name,
                    "transport_phone1": transport_phone1,
                    "transport_phone2": transport_phone2,
                    "transport_address": transport_address,
                    "hospital_name": hospital_name,
                    "hospital_phone1": hospital_phone1,
                    "hospital_phone2": hospital_phone2,
                    "hospital_address": hospital_address,
                    "noted": noted
                },
                success:
                        function (msg) {
                            $("#boxTable").html("Data updated successfully");
                            unblockUI(boxTable);
                        },
                error:
                        function () {
                            unblockUI(boxTable);
                            $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                        }
            });
        });
    });</script>

<style type="text/css">
    * {box-sizing: border-box}


    /* Full-width input fields */
    input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Overwrite default styles of hr */
    hr {
        border: 1px solid #f1f1f1;
    }

    /* Set a style for the submit/register button */
    .registerbtn {
        /*background-color: #4CAF50;*/
        color: white;
        /*padding: 16px 20px;*/
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .registerbtn:hover {
        opacity:1;
    }

    /* Add a blue text color to links */
    a {
        color: dodgerblue;
    }

    /* Set a grey background color and center the text of the "sign in" section */
    .signin {
        background-color: #f1f1f1;
        text-align: center;
    }
</style>

<!-- BEGIN CONTENT -->
<div id="autoref" class="page-content-wrapper" >
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php
                    //date_default_timezone_set('Asia/Jakarta'); 
                    echo $title;
                    ?>
                    <small><?php echo $title_small; ?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height blue">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>                

                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>  
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->  
        <!-- BEGIN PAGE BASE CONTENT -->                                   

        <div class="portlet light bordered" style="padding: 30px 50px 50px 50px;">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject uppercase font-blue-soft" style="font-size: 18pt;">Data Crew Information Sheet Station <?php echo $this->session->userdata("station"); ?></span>
                </div>
                <div class="actions">
                    <i class="fa fa-info fa-2x" ></i>
                </div>
            </div>    
            <div class="portlet-body">  
                <form action="<?php echo base_url(); ?>occ/insert_cis_admin" method="GET" >
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >            
<!--                            <small>*required</small>-->
                            <div class="portlet-body">
                                <label for="email"><b>General Manager*</b></label>
                                <input type="text" placeholder="Enter Name of General Manager" name="gm_name" value="<?php if (isset($gm_name)) echo $gm_name; ?>" required>
                                <input type="text" placeholder="Enter Mobile-Phone of General Manager" name="gm_phone1" value="<?php if (isset($gm_phone1)) echo $gm_phone1; ?>" required>
                                <input type="text" placeholder="Enter Office-Phone of General Manager" name="gm_phone2" value="<?php if (isset($gm_phone2)) echo $gm_phone2; ?>" required>

                                <label for="email"><b>Station Manager*</b></label>
                                <input type="text" placeholder="Enter Name of KK" name="kk_name" value="<?php if (isset($kk_name)) echo $kk_name; ?>" required>
                                <input type="text" placeholder="Enter Mobile-Phone of Station Manager" name="kk_phone1" value="<?php if (isset($kk_phone1)) echo $kk_phone1; ?>" required>
                                <input type="text" placeholder="Enter Office-Phone of Station Manager" name="kk_phone2" value="<?php if (isset($kk_phone2)) echo $kk_phone2; ?>" required>
                                 <?php if (isset($country) && $country == "ID"){?>
                                <label for="email"><b>Duty Manager Operation</b></label>
                                <input type="text" placeholder="Enter Name of KO" name="ko_name" value="<?php if (isset($ko_name)) echo $ko_name; ?>" >
                                <input type="text" placeholder="Enter Mobile-Phone of KO" name="ko_phone1" value="<?php if (isset($ko_phone1)) echo $ko_phone1; ?>" >
                                <input type="text" placeholder="Enter Office-Phone of KO" name="ko_phone2" value="<?php if (isset($ko_phone2)) echo $ko_phone2; ?>" >
                                <?php } else {?>
                                    <label for="email"><b>Indonesian Embassy</b></label>
                                <input type="text" placeholder="Enter Address of Embassy" name="ko_name" value="<?php if (isset($ko_name)) echo $ko_name; ?>" required>
                                <input type="text" placeholder="Enter Mobile-Phone of Embassy" name="ko_phone1" value="<?php if (isset($ko_phone1)) echo $ko_phone1; ?>" required>
                                <input type="text" placeholder="Enter Office-Phone of Embassy" name="ko_phone2" value="<?php if (isset($ko_phone2)) echo $ko_phone2; ?>" required>
                                    <?php }?>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >      
                            <div  class="portlet-body">
                                <label for="email"><b>Customer Service*</b></label>
                                <input type="text" placeholder="Enter Name of KS" name="ks_name" value="<?php if (isset($ks_name)) echo $ks_name; ?>"  required>
                                <input type="text" placeholder="Enter Mobile-Phone of Customer Service" name="ks_phone1" value="<?php if (isset($ks_phone1)) echo $ks_phone1; ?>"  required>
                                <input type="text" placeholder="Enter Office-Phone of Customer Service" name="ks_phone2" value="<?php if (isset($ks_phone2)) echo $ks_phone2; ?>"  required>

                                <label for="email"><b>Maintenance / GMFAA*</b></label>
                                <input type="text" placeholder="Enter Name of Technician" name="gmf_name" value="<?php if (isset($gmf_name)) echo $gmf_name; ?>"  required>
                                <input type="text" placeholder="Enter Mobile-Phone of Technician" name="gmf_phone1" value="<?php if (isset($gmf_phone1)) echo $gmf_phone1; ?>"  required>
                                <input type="text" placeholder="Enter Office-Phone of Technician" name="gmf_phone2" value="<?php if (isset($gmf_phone2)) echo $gmf_phone2; ?>"  >

                                <label for="email"><b>Hotel*</b></label>
                                <input type="text" placeholder="Enter Name of Hotel" name="hotel_name" value="<?php if (isset($hotel_name)) echo $hotel_name; ?>"  required>
                                <input type="text" placeholder="Enter Address of Hotel" name="hotel_address" value="<?php if (isset($hotel_address)) echo $hotel_address; ?>"  required>
                                <input type="text" placeholder="Enter Phone 1 of Hotel" name="hotel_phone1" value="<?php if (isset($hotel_phone1)) echo $hotel_phone1; ?>"  required>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >       
                            <div  class="portlet-body">
                                <label for="email"><b>Crew Transportation*</b></label>
                                <input type="text" placeholder="Enter Name of Crew Transport" name="transport_name" value="<?php if (isset($transport_name)) echo $transport_name; ?>"  required>
                                <input type="text" placeholder="Enter Address of transport" name="transport_address" value="<?php if (isset($transport_address)) echo $transport_address; ?>"  required>
                                <input type="text" placeholder="Enter Phone 1 of transport" name="transport_phone1" value="<?php if (isset($transport_phone1)) echo $transport_phone1; ?>"  required>
                                <input type="text" placeholder="Enter Phone 2 of transport" name="transport_phone2" value="<?php if (isset($transport_phone2)) echo $transport_phone2; ?>"  >

                                <label for="email"><b>Hospital / Medical Information*</b></label>
                                <input type="text" placeholder="Enter Name of hospital" name="hospital_name" value="<?php if (isset($hospital_name)) echo $hospital_name; ?>"  required>
                                <input type="text" placeholder="Enter Address of hospital" name="hospital_address" value="<?php if (isset($hospital_address)) echo $hospital_address; ?>"  required>
                                <input type="text" placeholder="Enter Phone 1 of hospital" name="hospital_phone1" value="<?php if (isset($hospital_phone1)) echo $hospital_phone1; ?>"  required>
                                <input type="text" placeholder="Enter Phone 2 of hospital" name="hospital_phone2"  value="<?php if (isset($hospital_phone2)) echo $hospital_phone2; ?>"  >
                            </div>

                        </div>

                    </div>
                    <div class="row" >
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >           
                            <div class="portlet-body">
                                <label for="email"><b>Noted for Crew*</b></label>
                                <textarea name="noted" id="editor"  >
                                    <?php if (isset($noted)) echo $noted; ?>
                                </textarea>
                                <script>
                                    CKEDITOR.replace('noted');
                                </script>
                                
<!--                                <script type="text/javascript">
                                            ClassicEditor
                                            .create( document.querySelector( '#editor' ) )
                                            .then( editor => {
                                                console.log( editor );
                                            } )
                                            .catch( error => {
                                                console.error( error );
                                            } );
                                            </script>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" > 
                            <div class="portlet-body">
                                <button type="submit" id="submit" class="registerbtn btn blue-sharp">Update</button>
                            </div>    
                        </div>
                        </form> 
<!--                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" > 
                            <div class="portlet-body">
                                <a href='<?php echo base_url(); ?>occ/download_pdf?station=<?php echo $this->session->userdata('station');?>' id="submit" target='_blank' class="registerbtn btn blue">Preview CIS</a>
                            </div>    
                        </div>-->
                    </div>
                
            </div>
        </div>


    </div>
    <!-- END PAGE BASE CONTENT -->

</div>   
<!-- END CONTENT BODY -->


