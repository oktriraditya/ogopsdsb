<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
<style type="text/css">
    .kotak {
        margin: auto;
        background-color: white;
        width: 90%;
        padding: 10px 10px 10px 10px;
    }

    .tengah {
        text-align: center;
    }

    #judul {
        text-align: center;
        font-family: lucida;
        font-size: 24;

    }
    .kanan{
        text-align: right;
    }
</style>
<!--css end-->

<!--FLIGHT NUMBER-->
<!--table flight number begin-->        
<div class="kotak">
    <h3 id="judul"> OTP per Station </h3>
    <div class="page-footer-inner kanan"> 2018 &copy; <a href="#">created by: Oktri - JKTRGL3</a>
    <form action="<?php echo base_url() ?>occ/dataflightbystation">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" name="datefrom" id="sDate" value="<?php if (isset($datefrom)) echo date($datefrom);
else echo date('Y-m-d'); ?>">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="dateto" id="eDate" value="<?php if (isset($dateto)) echo date($dateto);
else echo date('Y-m-d'); ?>"> 
                            </div>                            
                        </div>
                    </div>
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>           
                </div>       
            </div>

        </div>
    </form> 
    <div class="tools"> </div> 
    <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="fltData">
        <thead>
            <tr>
                <th width="5%" class="tengah">No.</th>
                <th width="5%" class="tengah">Station</th>
                <th width="5%" class="tengah">Schedule</th>
                <th width="5%" class="tengah">On-Time</th>
                <th width="5%" class="tengah">Delay</th>
                <th width="5%" class="tengah">Arr. On-Time</th>
                <th width="5%" class="tengah">OTP Departure</th>
                <th width="5%" class="tengah">OTP Arrival</th>
                <th width="5%" class="tengah">TECHNIC</th>
                <th width="5%" class="tengah">STNH</th>
                <th width="5%" class="tengah">COMM</th>
                <th width="5%" class="tengah">SYST</th>
                <th width="5%" class="tengah">FLOPS</th>
                <th width="5%" class="tengah">APTF</th>
                <th width="5%" class="tengah">WEATHER</th>
                <th width="5%" class="tengah">MISC</th>
                <th width="5%" class="tengah">TECHNIC</th>
                <th width="5%" class="tengah">STNH</th>
                <th width="5%" class="tengah">COMM</th>
                <th width="5%" class="tengah">SYST</th>
                <th width="5%" class="tengah">FLOPS</th>
                <th width="5%" class="tengah">APTF</th>
                <th width="5%" class="tengah">WEATHER</th>
                <th width="5%" class="tengah">MISC</th>
                <th width="5%" class="tengah">80</th>
                <th width="5%" class="tengah">81</th>
                <th width="5%" class="tengah">82</th>
                <th width="5%" class="tengah">83</th>
                <th width="5%" class="tengah">84</th>
                <th width="5%" class="tengah">85</th>
                <th width="5%" class="tengah">86</th>
                <th width="5%" class="tengah">87</th>
                <th width="5%" class="tengah">88</th>
                <th width="5%" class="tengah">89</th>
            </tr>
        </thead>
        <?php
        $data = array();
        foreach ($daily_record_flight as $key => $value) {
            $data['schedule'][] = $value['SCHEDULED'];
            $data['station'][] = $value['DEPAIRPORT'];
            $data['ontime'][] = $value['ONTIME'];
            $data['arrontime'][] = $value['ARRONTIME'];
            $data['SCHEDARR'][] = $value['SCHEDARR'];
        }
        foreach ($daily_record_cod as $key => $value) {
            $data['technic'][] = $value['TECH'];
            $data['STNH'][] = $value['STNH'];
            $data['COMM'][] = $value['COMM'];
            $data['SYST'][] = $value['SYST'];
            $data['FLOPS'][] = $value['FLOPS'];
            $data['APTF'][] = $value['APTF'];
            $data['WEATHER'][] = $value['WEATHER'];
            $data['MISC'][] = $value['MISC'];
            $data['TOTALCOD'][] = $value['TECH'] + $value['STNH'] + $value['COMM'] + $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'];
            $data['A80'][] = $value['A80'];
            $data['A81'][] = $value['A81'];
            $data['A82'][] = $value['A82'];
            $data['A83'][] = $value['A83'];
            $data['A84'][] = $value['A84'];
            $data['A85'][] = $value['A85'];
            $data['A86'][] = $value['A86'];
            $data['A87'][] = $value['A87'];
            $data['A88'][] = $value['A88'];
            $data['A89'][] = $value['A89'];
            
            
        }
        /*
          foreach($total_ontime as $key => $value)$data['ontime'][] = $value['ONTIME'];
          foreach ($total_arr_ontime as $key => $value)$data['arrontime'][] = $value['ARRONTIME'];
          foreach ($total_technic as $key => $value)$data['technic'][] = $value['TECHNIC'];
          foreach ($total_stnh as $key => $value)$data['STNH'][] = $value['STNH'];
          foreach ($total_comm as $key => $value)$data['COMM'][] = $value['COMM'];
          foreach ($total_syst as $key => $value)$data['SYST'][] = $value['SYST'];
          foreach ($total_flops as $key => $value)$data['FLOPS'][] = $value['FLOPS'];
          foreach ($total_aptf as $key => $value)$data['APTF'][] = $value['APTF'];
          foreach ($total_weather as $key => $value)$data['WEATHER'][] = $value['WEATHER'];
          foreach ($total_misc as $key => $value)$data['MISC'][] = $value['MISC'];
          foreach ($total_cod as $key => $value)$data['TOTALCOD'][] = $value['TOTALCOD']; */


        $count = count($data['schedule']);
        ?>
        <tbody>
            <?php
            for ($i = 0; $i < $count; $i++) {
                ?>
                <tr class="tengah">
                    <td> <?php echo $i + 1; ?> </td>
                    <td> <?php echo $data['station'][$i]; ?></td>
                    <td> <?php echo $data['schedule'][$i]; ?> </td>
                    <td> <?php echo $data['ontime'][$i]; ?></td>
                    <td> <?php echo $data['schedule'][$i] - $data['ontime'][$i]; ?></td>
                    <td> <?php echo $data['arrontime'][$i]; ?></td>
                    <td> <?php if($data['schedule'][$i] != 0) echo round(($data['ontime'][$i] / $data['schedule'][$i]) * 100, 2); else echo 0; ?>%</td>
                    <td> <?php if($data['SCHEDARR'][$i] != 0) echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100, 2); else echo 0; ?>%</td>
    <?php if ($data['TOTALCOD'][$i] > 0) { ?>
                        <td> <?php echo $data['technic'][$i]; ?>  <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['STNH'][$i]; ?>  <?php //echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['COMM'][$i]; ?>  <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['SYST'][$i]; ?>  <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['FLOPS'][$i]; ?>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['APTF'][$i]; ?>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['WEATHER'][$i]; ?>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php echo $data['MISC'][$i]; ?>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?></td>
                        <td> <?php //echo $data['technic'][$i]; ?>  <?php echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php // $data['STNH'][$i]; ?>  <?php echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php //echo $data['COMM'][$i]; ?>  <?php echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php //echo $data['SYST'][$i]; ?>  <?php echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php // $data['FLOPS'][$i]; ?>  <?php echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php //echo $data['APTF'][$i]; ?>  <?php echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php //echo $data['WEATHER'][$i]; ?>  <?php echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php //echo $data['MISC'][$i]; ?>  <?php echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2); ?>%</td>

    <?php } else { ?>
                        <td> 0 </td>
                        <td> 0  </td>
                        <td> 0  </td>
                        <td> 0  </td>
                        <td> 0  </td>
                        <td> 0  </td>
                        <td> 0  </td>
                        <td> 0 </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                        <td>  0% </td>
                <?php } ?>
                        <td> <?php echo $data['A80'][$i];?> </td>
                        <td> <?php echo $data['A81'][$i];?> </td>
                        <td> <?php echo $data['A82'][$i];?> </td>
                        <td> <?php echo $data['A83'][$i];?> </td>
                        <td> <?php echo $data['A84'][$i];?> </td>
                        <td> <?php echo $data['A85'][$i];?> </td>
                        <td> <?php echo $data['A86'][$i];?> </td>
                        <td> <?php echo $data['A87'][$i];?> </td>
                        <td> <?php echo $data['A88'][$i];?> </td>
                        <td> <?php echo $data['A89'][$i];?> </td>

                </tr>
<?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true
        });
    });
</script>
