<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>


<!--css begin-->
    <style type="text/css">
        .kotak {
            margin: auto;
            background-color: white;
            width: 90%;
            padding: 10px 10px 10px 10px;
        }
    
        .tengah {
            text-align: center;
        }
        
        #judul {
            text-align: center;
            font-family: corbel;
            font-size: 24;
            
        }
        .kanan{
        text-align: right;
    }
    </style>
<!--css end-->
<script type="text/javascript">
jQuery(document).ready(function() { 
    $("#fltData").on('click', '.view', function(){
        var id = this.id.split(" "); 
        //alert(id[1]);
        //var kata2 = id.split(":");
        var target_url = "<?php echo base_url().'occ/details_delay/';?>"+id[0]+"/"+id[1];
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
        //ajaxStart: function(){iframe.attr("src", "loading...");},
        //ajaxStop: function(){iframe.attr("src", target_url);}
        
        
    });
});
</script>
<!--FLIGHT NUMBER-->
<!--table flight number begin-->        
    <div class="kotak">
        <h3 id="judul"> OTP Daily Recap </h3>
        <div class="page-footer-inner kanan"> 2018 &copy; <a href="#">created by: Oktri - JKTRGL3</a>
        <form action="<?php echo base_url() ?>occ/dataflight">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" name="datefrom" id="sDate" value="<?php if (isset($datefrom)) echo date($datefrom);
else echo date('Y-m-d'); ?>">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="dateto" id="eDate" value="<?php if (isset($dateto)) echo date($dateto);
else echo date('Y-m-d'); ?>"> 
                            </div>                            
                        </div>
                    </div>
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>           
                </div>       
            </div>

        </div>
    </form> 
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="fltData">
            
            <thead>
                <tr>
                    <th width="5%" class="tengah">No.</th>
                    <!--<th width="5%" class="tengah">Remarks</th>-->
                    <th width="5%" class="tengah">Dep. Date</th>
                    <th width="5%" class="tengah">Schedule</th>
                    <th width="5%" class="tengah">On-Time</th>
                    <th width="5%" class="tengah">Delay</th>
                    <th width="5%" class="tengah">Arr. On-Time</th>
                    <th width="5%" class="tengah">OTP Departure</th>
                    <th width="5%" class="tengah">OTP Arrival</th>
                    <th width="5%" class="tengah">TECH</th>
                    <th width="5%" class="tengah">STNH</th>
                    <th width="5%" class="tengah">COMM</th>
                    <th width="5%" class="tengah">SYST</th>
                    <th width="5%" class="tengah">FLOPS</th>
                    <th width="5%" class="tengah">APTF</th>
                    <th width="5%" class="tengah">WTHR</th>
                    <th width="5%" class="tengah">MISC</th>
                    <th width="5%" class="tengah">TECHNIC</th>
                    <th width="5%" class="tengah">STNH</th>
                    <th width="5%" class="tengah">COMM</th>
                    <th width="5%" class="tengah">SYST</th>
                    <th width="5%" class="tengah">FLOPS</th>
                    <th width="5%" class="tengah">APTF</th>
                    <th width="5%" class="tengah">WTHR</th>
                    <th width="5%" class="tengah">MISC</th>
                </tr>
            </thead>
        <?php
                $data = array();
                foreach ($daily_record_flight as $key => $value){
                    $data['schedule'][] =  $value['SCHEDULED'];
                    $data['tanggal'][] = $value['TANGGAL'];
                    $data['ontime'][] = $value['ONTIME'];
                    $data['arrontime'][] = $value['ARRONTIME'];
                    $data['SCHEDARR'][] = $value['SCHEDARR'];
                }
                foreach ($daily_record_cod as $key => $value){
                    $data['technic'][] = $value['TECH'];
                    $data['STNH'][] = $value['STNH'];
                    $data['COMM'][] = $value['COMM'];
                    $data['SYST'][] = $value['SYST'];
                    $data['FLOPS'][] = $value['FLOPS'];
                    $data['APTF'][] = $value['APTF'];
                    $data['WEATHER'][] = $value['WEATHER'];
                    $data['MISC'][] = $value['MISC'];
                    $data['TOTALCOD'][] = $value['TECH']+$value['STNH']+$value['COMM']+$value['SYST']+$value['FLOPS']+$value['APTF']+$value['WEATHER']+$value['MISC'];
                }
                /*
                foreach($total_ontime as $key => $value)$data['ontime'][] = $value['ONTIME'];
                foreach ($total_arr_ontime as $key => $value)$data['arrontime'][] = $value['ARRONTIME'];
                foreach ($total_technic as $key => $value)$data['technic'][] = $value['TECHNIC'];
                foreach ($total_stnh as $key => $value)$data['STNH'][] = $value['STNH'];
                foreach ($total_comm as $key => $value)$data['COMM'][] = $value['COMM'];
                foreach ($total_syst as $key => $value)$data['SYST'][] = $value['SYST'];
                foreach ($total_flops as $key => $value)$data['FLOPS'][] = $value['FLOPS'];
                foreach ($total_aptf as $key => $value)$data['APTF'][] = $value['APTF'];
                foreach ($total_weather as $key => $value)$data['WEATHER'][] = $value['WEATHER'];
                foreach ($total_misc as $key => $value)$data['MISC'][] = $value['MISC'];
                foreach ($total_cod as $key => $value)$data['TOTALCOD'][] = $value['TOTALCOD'];*/
                
                
                $count = count($data['schedule']);
                ?>
            <tbody>
                <?php
                for ($i=0;$i<$count;$i++){ 
                ?>
                <tr class="tengah">
                    <td> <?php echo $i+1; ?> </td>
                    <!--<td>
                        <?php 
                        $date_delay = array(
                            'cod_date' => $data['tanggal'][$i]
                        );
                        $type_delay = array(
                            1=>'Technic',
                            2=>'Station Handling',
                            3=>'Commercial',
                            4=>'System' ,
                            5=>'Flops' ,
                            6=>'Airport Fac' ,
                            7=>'Weather',
                            8=>'Misc'
                        );
                        echo form_open('occ/details_delay');
                        echo form_dropdown('type_delay',$type_delay,1);
                        echo form_hidden($date_delay);
                        echo form_submit('submit','view details');
                        echo form_close();
                        ?>
                    </td>-->
                    <td> <?php echo $data['tanggal'][$i];?></td>
                    <td> <?php echo $data['schedule'][$i];?> </td>
                    <td> <?php echo $data['ontime'][$i];?></td>
                    <td> <?php echo $data['schedule'][$i] - $data['ontime'][$i];?></td>
                    <td> <?php echo $data['arrontime'][$i];?></td>
                    <td> <?php echo round(($data['ontime'][$i] / $data['schedule'][$i]) * 100,2);?>%</td>
                    <td> <?php echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100,2);?>%</td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="1 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['technic'][$i];?> - Detail </a>                
                      <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['STNH'][$i];?> - Detail </a>  <?php// echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['COMM'][$i];?> - Detail </a>  <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="4 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['SYST'][$i];?> - Detail  </a> <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="5 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['FLOPS'][$i];?> - Detail </a>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="6 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['APTF'][$i];?> - Detail </a>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="7 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['WEATHER'][$i];?> - Detail </a>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i];?>"><i class="fa fa-search"></i> <?php echo $data['MISC'][$i];?> - Detail </a>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                    <td> <?php //echo $data['technic'][$i];?>  <?php echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['STNH'][$i];?>  <?php echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['COMM'][$i];?>  <?php echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['SYST'][$i];?>  <?php echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['FLOPS'][$i];?>  <?php echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['APTF'][$i];?>  <?php echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['WEATHER'][$i];?>  <?php echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                    <td> <?php //echo $data['MISC'][$i];?>  <?php echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?>%</td>
                                        
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true
        });
    });
</script>
<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>
