<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/css/fixedColumns.bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
    
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#fltData").on('click', '.view', function () {
            var id = this.id.split(" ");
            //alert(id[1]);
            //var kata2 = id.split(":");
            var target_url = "<?php echo base_url() . 'occ/details_delay/'; ?>" + id[0] + "/" + id[1] + "/" + id[2];
            var iframe = $("#viewFrame");
            iframe.attr("src", target_url);
            //ajaxStart: function(){iframe.attr("src", "loading...");},
            //ajaxStop: function(){iframe.attr("src", target_url);}


        });
    });
</script>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> OTP per Month<?php //echo $stn; ?>, <?php echo date('M-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('M-Y', strtotime($dateto)); ?></span>
        </div>  

    </div>

    <div class="portlet-body"> 
        <div class="tools"> </div> 
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column " id="fltData">
            <thead>
                <tr>
                    <th  class="tengah">No.</th>
                    <th  class="tengah">Month</th>
                    <th  class="tengah">Schedule</th>
                    <th  class="tengah">On-Time</th>
                    <th  class="tengah">Delay</th>
                    <th  class="tengah">Arr. On-Time</th>
                    <th  class="tengah">OTP Departure</th>
                    <th  class="tengah">OTP Arrival</th>
                    <th  class="tengah">First Departure</th>
                    <th  class="tengah">OTP First Departure</th>
                    <th  class="tengah">ZMD First Departure</th>
                    <th  class="tengah">Schedule YTD</th>
                    <th  class="tengah">On-Time YTD</th>
                    <th  class="tengah">Arr. On-Time YTD</th>
                    <th  class="tengah">OTP Departure YTD</th>
                    <th  class="tengah">OTP Arrival YTD</th>
                    <th  class="tengah">TECH</th>
                    <th  class="tengah">STNH</th>
                    <th  class="tengah">COMM</th>
                    <th  class="tengah">SYST</th>
                    <th  class="tengah">FLOPS</th>
                    <th  class="tengah">APTF</th>
                    <th  class="tengah">WTHR</th>
                    <th  class="tengah">MISC</th>
                    <th  class="tengah">TECHNIC</th>
                    <th  class="tengah">STNH</th>
                    <th  class="tengah">COMM</th>
                    <th  class="tengah">SYST</th>
                    <th  class="tengah">FLOPS</th>
                    <th  class="tengah">APTF</th>
                    <th  class="tengah">WTHR</th>
                    <th  class="tengah">MISC</th>

                </tr>
            </thead>
            <?php
            $data = array();
            $datamtd = array();
            $dataTotal['technic'] = 0;
            $dataTotal['STNH'] = 0;
            $dataTotal['COMM'] = 0;
            $dataTotal['SYST'] = 0;
            $dataTotal['FLOPS'] = 0;
            $dataTotal['APTF'] = 0;
            $dataTotal['WEATHER'] = 0;
            $dataTotal['MISC'] = 0;
            $dataTotal['TOTALCOD'] = 0;
            $datamtd['FIRSTFLIGHT'][] = 0;
            $datamtd['OTPFIRSTFLIGHT'][] = 0;
            $datamtd['ZMDFIRSTFLIGHT'][] = 0;

            $dataTotal['FIRSTFLIGHT'] = 0;
            $dataTotal['OTPFIRSTFLIGHT'] = 0;
            $dataTotal['ZMDFIRSTFLIGHT'] = 0;
            $datamtd['schedule'][] = 0;
            $datamtd['ontime'][] = 0;
            $datamtd['arrontime'][] = 0;
            $datamtd['SCHEDARR'][] = 0;

            $dataTotal['schedule'] = 0;
            $dataTotal['ontime'] = 0;
            $dataTotal['arrontime'] = 0;
            $dataTotal['delay'] = 0;
            $dataTotal['SCHEDARR'] = 0;
            foreach ($monthly_record_flight as $key => $value) {
                $data['schedule'][] = $value['SCHEDULED'];
                $data['BULAN'][] = $value['BULAN'];
                $data['ontime'][] = $value['ONTIME'];
                $data['arrontime'][] = $value['ARRONTIME'];
                $data['SCHEDARR'][] = $value['SCHEDARR'];
                $data['FIRSTFLIGHT'][] = $value['FIRSTFLIGHT'];
                $data['OTPFIRSTFLIGHT'][] = $value['OTPFIRSTFLIGHT'];
                $data['ZMDFIRSTFLIGHT'][] = $value['ZMDFIRSTFLIGHT'];
            }
//                foreach ($monthly_first_dep as $key => $value){
//                    $data['FIRSTFLIGHT'][] =  $value['FIRSTFLIGHT'];
//                    $data['OTPFIRSTFLIGHT'][] = $value['OTPFIRSTFLIGHT'];
//                    $data['ZMDFIRSTFLIGHT'][] = $value['ZMDFIRSTFLIGHT'];
//                    $datamtd['FIRSTFLIGHT'][] = 0;
//                    $datamtd['OTPFIRSTFLIGHT'][] = 0;
//                    $datamtd['ZMDFIRSTFLIGHT'][] = 0;
//                    
//                    $dataTotal['FIRSTFLIGHT'] =  0;
//                    $dataTotal['OTPFIRSTFLIGHT'] = 0;
//                    $dataTotal['ZMDFIRSTFLIGHT'] = 0;
//                }
            foreach ($monthly_record_cod as $key => $value) {
                $data['technic'][] = $value['TECH'];
                $data['STNH'][] = $value['STNH'];
                $data['COMM'][] = $value['COMM'];
                $data['SYST'][] = $value['SYST'];
                $data['FLOPS'][] = $value['FLOPS'];
                $data['APTF'][] = $value['APTF'];
                $data['WEATHER'][] = $value['WEATHER'];
                $data['MISC'][] = $value['MISC'];
                $data['TOTALCOD'][] = $value['TECH'] + $value['STNH'] + $value['COMM'] + $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'];
            }
            /*
              foreach($total_ontime as $key => $value)$data['ontime'][] = $value['ONTIME'];
              foreach ($total_arr_ontime as $key => $value)$data['arrontime'][] = $value['ARRONTIME'];
              foreach ($total_technic as $key => $value)$data['technic'][] = $value['TECHNIC'];
              foreach ($total_stnh as $key => $value)$data['STNH'][] = $value['STNH'];
              foreach ($total_comm as $key => $value)$data['COMM'][] = $value['COMM'];
              foreach ($total_syst as $key => $value)$data['SYST'][] = $value['SYST'];
              foreach ($total_flops as $key => $value)$data['FLOPS'][] = $value['FLOPS'];
              foreach ($total_aptf as $key => $value)$data['APTF'][] = $value['APTF'];
              foreach ($total_weather as $key => $value)$data['WEATHER'][] = $value['WEATHER'];
              foreach ($total_misc as $key => $value)$data['MISC'][] = $value['MISC'];
              foreach ($total_cod as $key => $value)$data['TOTALCOD'][] = $value['TOTALCOD']; */


            $count = count($monthly_record_flight);
            ?>
            <tbody>
            <?php
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $datamtd['schedule'][$i] = $data['schedule'][$i];
                    $datamtd['ontime'][$i] = $data['ontime'][$i];
                    $datamtd['arrontime'][$i] = $data['arrontime'][$i];
                    $datamtd['SCHEDARR'][$i] = $data['SCHEDARR'][$i];
                } else {
                    $datamtd['schedule'][$i] = $data['schedule'][$i] + $datamtd['schedule'][$i - 1];
                    $datamtd['ontime'][$i] = $data['ontime'][$i] + $datamtd['ontime'][$i - 1];
                    $datamtd['arrontime'][$i] = $data['arrontime'][$i] + $datamtd['arrontime'][$i - 1];
                    $datamtd['SCHEDARR'][$i] = $data['SCHEDARR'][$i] + $datamtd['SCHEDARR'][$i - 1];
                }
                ?>
                    <tr class="tengah">
                        <td> <?php echo $i + 1; ?> </td>
                        <!--<td>
                    <?php
                    $date_delay = array(
                        'cod_date' => $data['tanggal'][$i]
                    );
                    $type_delay = array(
                        1 => 2018,
                        2 => 2017
                    );
                    echo form_open('occ/details_delay');
                    echo form_dropdown('type_delay', $type_delay, 1);
                    echo form_hidden($date_delay);
                    echo form_submit('submit', 'view details');
                    echo form_close();
                    ?>
                        </td>-->
                        <td> <?php echo $data['BULAN'][$i]; ?></td>
                        <td> <?php echo $data['schedule'][$i]; ?> </td>
                        <td> <?php echo $data['ontime'][$i]; ?></td>
                        <td> <?php echo $data['schedule'][$i] - $data['ontime'][$i]; ?></td>
                        <td> <?php echo $data['arrontime'][$i]; ?></td>
                        <td> <?php echo round(($data['ontime'][$i] / $data['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100, 2); ?>%</td>
                        <td> <?php echo $data['FIRSTFLIGHT'][$i]; ?></td>
                        <td> <?php echo round(($data['OTPFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2); ?>% </td>
                        <td> <?php echo round(($data['ZMDFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2); ?>% </td>
                        <td> <?php echo $datamtd['schedule'][$i]; ?></td>
                        <td> <?php echo $datamtd['ontime'][$i]; ?></td>
                        <td> <?php echo $datamtd['arrontime'][$i]; ?></td>
                        <td> 
    <?php echo round(($datamtd['ontime'][$i] / $datamtd['schedule'][$i]) * 100, 2); ?>%</td>
                        <td> <?php echo round(($datamtd['arrontime'][$i] / $datamtd['SCHEDARR'][$i]) * 100, 2); ?>% </td>
                        <td> <?php $dataTotal['technic']+= $data['technic'][$i]; ?> <?php echo $data['technic'][$i]; ?>           
    <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['STNH']+= $data['STNH'][$i]; ?><?php echo $data['STNH'][$i]; ?>   <?php // echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['COMM']+= $data['COMM'][$i]; ?><?php echo $data['COMM'][$i]; ?>   <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                        <td> <?php $dataTotal['SYST']+= $data['SYST'][$i]; ?><?php echo $data['SYST'][$i]; ?>  <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['FLOPS']+= $data['FLOPS'][$i]; ?><?php echo $data['FLOPS'][$i]; ?>   <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['APTF']+= $data['APTF'][$i]; ?><?php echo $data['APTF'][$i]; ?>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                        <td> <?php $dataTotal['WEATHER']+= $data['WEATHER'][$i]; ?><?php echo $data['WEATHER'][$i]; ?>   <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['MISC']+= $data['MISC'][$i]; ?><?php echo $data['MISC'][$i]; ?>   <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?> %</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>

                    </tr>
<?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            ordering: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf'],
            scrollX: true,
            scrollCollapse: true
        });
    });
</script>
