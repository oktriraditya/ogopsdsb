<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }

</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#fltData").on('click', '.view', function () {
            var id = this.id.split(" ");
            //alert(id[1]);
            //var kata2 = id.split(":");
            var target_url = "<?php echo base_url() . 'occ/details_delay/'; ?>" + id[0] + "/" + id[1] + "/" + id[2];
            var iframe = $("#viewFrame");
            iframe.attr("src", target_url);
            //ajaxStart: function(){iframe.attr("src", "loading...");},
            //ajaxStop: function(){iframe.attr("src", target_url);}


        }); 
    });
</script>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> Delay Contributor per Day, <?php echo date('d-m-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-m-Y', strtotime($dateto)); ?></span>
        </div>  

    </div>

    <div class="portlet-body"> 
        <div class="tools"> </div> 
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column " id="fltData">
            <thead>
                <tr>
                    <th width="5%" class="tengah">No.</th>
                    <th width="5%" class="tengah">Date</th>
                    <?php if (isset($delayContributor) && $delayContributor == 1) { ?>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to technical">40</th>
                    <th width="5%" class="tengah" title="Aircraft defects">41</th>
                    <th width="5%" class="tengah" title="Scheduled maintenance, late release">42</th>
                    <th width="5%" class="tengah" title="Non-scheduled maintenance, special checks and / or additional works beyond normal maintenance">43</th>
                    <th width="5%" class="tengah" title="Spares and maintenance equipment, lack of or breakdown">44</th>
                    <th width="5%" class="tengah" title="AOG (Aircraft on ground for technical reasons) Spares, to be carried to another station">45</th>
                    <th width="5%" class="tengah" title="Aircraft change for technical reasons">46</th>
                    <th width="5%" class="tengah" title="Standby aircraft, lack of planned standby aircraft for technical reasons">47</th>
                    <th width="5%" class="tengah" title="Scheduled cabin configuration and version adjustment">48</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 2) { ?>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to passenger and baggage">10</th>
                    <th width="5%" class="tengah" title="Late check-in, acceptance of passengers after deadline">11</th>
                    <th width="5%" class="tengah" title="Late Check-in, congestion in check-in area">12</th>
                    <th width="5%" class="tengah" title="Check-in error">13</th>
                    <th width="5%" class="tengah" title="Boarding, discrepancies and paging, missing checked-in passenger at gate">15</th>
                    <th width="5%" class="tengah" title="Catering order, late or incorrect order given to supplier">17</th>
                    <th width="5%" class="tengah" title="Baggage processing, sorting, etc.">18</th>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to cargo, mail, and ramp handling">20</th>
                    <th width="5%" class="tengah" title="Documentation, errors, etc.">21</th>
                    <th width="5%" class="tengah" title="Late positioning">22</th>
                    <th width="5%" class="tengah" title="Late acceptance">23</th>
                    <th width="5%" class="tengah" title="Inadequate packing">24</th>
                    <th width="5%" class="tengah" title="Late preparation in warehouse">26</th>
                    <th width="5%" class="tengah" title="Mail Oversales, packing, etc.">27</th>
                    <th width="5%" class="tengah" title="Mail Late positioning">28</th>
                    <th width="5%" class="tengah" title="Mail Late acceptance">29</th>
                    <th width="5%" class="tengah" title="Aircraft documentation late or inaccurate, weight and balance (Loadsheet), general declaration, passenger manifest, etc.">31</th>
                    <th width="5%" class="tengah" title="Loading, Unloading, bulky/special load, cabin load, lack of loading staff">32</th>
                    <th width="5%" class="tengah" title="Loading Equipment, lack of or breakdown, e.g. container pallet loader, lack of staff">33</th>
                    <th width="5%" class="tengah" title="Servicing Equipment, lack of or breakdown, lack of staff, e.g. steps">34</th>
                    <th width="5%" class="tengah" title="Aircraft Cleaning">35</th>
                    <th width="5%" class="tengah" title="Fuelling, Defuelling, fuel supplier">36</th>
                    <th width="5%" class="tengah" title="Catering, late delivery or loading">37</th>
                    <th width="5%" class="tengah" title="ULD, Containers, pallets, lack of or breakdown">38</th>
                    <th width="5%" class="tengah" title="Technical equipment, lack of or breakdown, lack of staff, e.g. pushback">39</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 3) { ?>
                    <th width="5%" class="tengah" title="Scheduled ground time less than declared minimum ground time">09</th>
                    <th width="5%" class="tengah" title="Oversales, booking errors">14</th>
                    <th width="5%" class="tengah" title="Commercial Publicity, Passenger Convenience, VIP, Press, Ground meals and missing personal items">16</th>
                    <th width="5%" class="tengah" title="Oversales, booking errors">25</th>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to commercial">30</th>
                    <th width="5%" class="tengah" title="Passenger or Load Connection, awaiting load or passengers from another flight. Protection of stranded passengers onto a new flight.">91</th>
                    <th width="5%" class="tengah" title="Through Check-in error, passenger and baggage">92</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 4) { ?>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to system">50</th>
                    <th width="5%" class="tengah" title="Departure Control System, Check-in, weight and balance (loadcontrol), computer system error, baggage sorting, gate-reader error or problems">55</th>
                    <th width="5%" class="tengah" title="Cargo preparation/documentation system">56</th>
                    <th width="5%" class="tengah" title="Flight plans">57</th>
                    <th width="5%" class="tengah" title="Other computer systems">58</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 5) { ?>
                    <th width="5%" class="tengah"title="Delay due to Transport, Crew Management, and Aircraft Rotation">OG</th>
                    <th width="5%" class="tengah"title="Delay due to Cockpit Crew">OF</th>
                    <th width="5%" class="tengah" title="Delay due to Cabin Crew">CC</th>
                    <th width="5%" class="tengah"title="Flight deck crew late onboard due to transport">01</th>
                    <th width="5%" class="tengah"title="Cabin crew late onboard due to transport">02</th>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to flight operation and crewing">60</th>
                    <th width="5%" class="tengah" title="Flight plan, late completion or change of flight documentation">61</th>
                    <th width="5%" class="tengah" title="Operational requirements, fuel, load alteration">62</th>
                    <th width="5%" class="tengah" title="Late crew boarding or departure procedures">63</th>
                    <th width="5%" class="tengah" title="Flight deck crew shortage, Crew rest">64</th>
                    <th width="5%" class="tengah" title="Flight deck crew special request or error">65</th>
                    <th width="5%" class="tengah" title="Late cabin crew boarding or departure procedures">66</th>
                    <th width="5%" class="tengah" title="Cabin crew shortage">67</th>
                    <th width="5%" class="tengah" title="Cabin crew error or special request">68</th>
                    <th width="5%" class="tengah" title="Captain request for security check, extraordinary">69</th>
                    <th width="5%" class="tengah" title="Cabin crew rotation">94</th>
                    <th width="5%" class="tengah" title="Crew rotation, awaiting crew from another flight (flight deck or entire crew)">95</th>
                    <th width="5%" class="tengah" title="Operations control, rerouting, diversion, consolidation, aircraft change for reasons other than technical">96</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 6) { ?>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to airport facility">80</th>
                    <th width="5%" class="tengah" title="ATC restriction en-route or capacity">81</th>
                    <th width="5%" class="tengah" title="ATC restriction due to staff shortage or equipment failure en-route">82</th>
                    <th width="5%" class="tengah" title="ATC restriction at destination">83</th>
                    <th width="5%" class="tengah" title="ATC restriction due to weather at destination">84</th>
                    <th width="5%" class="tengah" title="Mandatory security">85</th>
                    <th width="5%" class="tengah" title="Immigration, Customs, Health">86</th>
                    <th width="5%" class="tengah" title="Airport Facilities, parking stands, ramp congestion, buildings, gate limitations">87</th>
                    <th width="5%" class="tengah" title="Restrictions at airport of destination, airport/runway closed due obstruction, industrial action, staff shortage, political unrest, noise abatement, night curfew, special flights">88</th>
                    <th width="5%" class="tengah" title="Restrictions at airport of departure with or without ATFM Restriction, including air traffic services, start-up and pushback, airport and/or runway closed due to obstruction or weather (restriction due to weather incase of ATFM regulation only)">89</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 7) { ?>
                    <th width="5%" class="tengah"  title="Late arrival from previous station due to weather">70</th>
                    <th width="5%" class="tengah" title="Departure station">71</th>
                    <th width="5%" class="tengah" title="Destination station">72</th>
                    <th width="5%" class="tengah" title="En route or Alternate">73</th>
                    <th width="5%" class="tengah" title="De-Icing of aircraft, removal of ice/snow, frost prevention">75</th>
                    <th width="5%" class="tengah" title="Removal of snow/ice/water/sand from airport/runway">76</th>
                    <th width="5%" class="tengah" title="Ground handling impaired by adverse weather conditions">77</th>
                    <?php }else if (isset($delayContributor) && $delayContributor == 8) { ?>
                    <th width="5%" class="tengah" title="Damage during flight operations, bird or lightning strike, turbulence, heavy or overweight landing">51</th>
                    <th width="5%" class="tengah" title="Damage during ground operations, collisions (other than during taxiing), loading/offloading damage, contamination, towing, extreme weather conditions.">52</th>
                    <th width="5%" class="tengah" title="Late arrival from previous station due to miscellanous">90</th>
                    <th width="5%" class="tengah" title="Aircraft rotation, late arrival of aircraft from another flight or previous sector">93</th>
                    <th width="5%" class="tengah" title="Industrial action within own airline">97</th>
                    <th width="5%" class="tengah" title="Industrial action outside own airline, excluding ATS">98</th>
                    <th width="5%" class="tengah" title="Miscellaneous, not elsewhere specified">99</th>
                    <?php }else { ?>
                    <th width="5%" class="tengah">40</th>
                    <th width="5%" class="tengah">41</th>
                    <th width="5%" class="tengah">42</th>
                    <th width="5%" class="tengah">43</th>
                    <th width="5%" class="tengah">44</th>
                    <th width="5%" class="tengah">45</th>
                    <th width="5%" class="tengah">46</th>
                    <th width="5%" class="tengah">47</th>
                    <th width="5%" class="tengah">48</th>
                    <th width="5%" class="tengah">10</th>
                    <th width="5%" class="tengah">11</th>
                    <th width="5%" class="tengah">12</th>
                    <th width="5%" class="tengah">13</th>
                    <th width="5%" class="tengah">15</th>
                    <th width="5%" class="tengah">17</th>
                    <th width="5%" class="tengah">18</th>
                    <th width="5%" class="tengah">20</th>
                    <th width="5%" class="tengah">21</th>
                    <th width="5%" class="tengah">22</th>
                    <th width="5%" class="tengah">23</th>
                    <th width="5%" class="tengah">24</th>
                    <th width="5%" class="tengah">26</th>
                    <th width="5%" class="tengah">27</th>
                    <th width="5%" class="tengah">28</th>
                    <th width="5%" class="tengah">29</th>
                    <th width="5%" class="tengah">31</th>
                    <th width="5%" class="tengah">32</th>
                    <th width="5%" class="tengah">33</th>
                    <th width="5%" class="tengah">34</th>
                    <th width="5%" class="tengah">35</th>
                    <th width="5%" class="tengah">36</th>
                    <th width="5%" class="tengah">37</th>
                    <th width="5%" class="tengah">38</th>
                    <th width="5%" class="tengah">39</th>
                    <th width="5%" class="tengah">09</th>
                    <th width="5%" class="tengah">14</th>
                    <th width="5%" class="tengah">16</th>
                    <th width="5%" class="tengah">25</th>
                    <th width="5%" class="tengah">30</th>
                    <th width="5%" class="tengah">91</th>
                    <th width="5%" class="tengah">92</th>
                    <th width="5%" class="tengah">50</th>
                    <th width="5%" class="tengah">55</th>
                    <th width="5%" class="tengah">56</th>
                    <th width="5%" class="tengah">57</th>
                    <th width="5%" class="tengah">58</th>
                    <th width="5%" class="tengah">01</th>
                    <th width="5%" class="tengah">02</th>
                    <th width="5%" class="tengah">60</th>
                    <th width="5%" class="tengah">61</th>
                    <th width="5%" class="tengah">62</th>
                    <th width="5%" class="tengah">63</th>
                    <th width="5%" class="tengah">64</th>
                    <th width="5%" class="tengah">65</th>
                    <th width="5%" class="tengah">66</th>
                    <th width="5%" class="tengah">67</th>
                    <th width="5%" class="tengah">68</th>
                    <th width="5%" class="tengah">69</th>
                    <th width="5%" class="tengah">94</th>
                    <th width="5%" class="tengah">95</th>
                    <th width="5%" class="tengah">96</th>
                    <th width="5%" class="tengah">80</th>
                    <th width="5%" class="tengah">81</th>
                    <th width="5%" class="tengah">82</th>
                    <th width="5%" class="tengah">83</th>
                    <th width="5%" class="tengah">84</th>
                    <th width="5%" class="tengah">85</th>
                    <th width="5%" class="tengah">86</th>
                    <th width="5%" class="tengah">87</th>
                    <th width="5%" class="tengah">88</th>
                    <th width="5%" class="tengah">89</th>
                    <th width="5%" class="tengah">70</th>
                    <th width="5%" class="tengah">71</th>
                    <th width="5%" class="tengah">72</th>
                    <th width="5%" class="tengah">73</th>
                    <th width="5%" class="tengah">75</th>
                    <th width="5%" class="tengah">76</th>
                    <th width="5%" class="tengah">77</th>
                    <th width="5%" class="tengah">51</th>
                    <th width="5%" class="tengah">52</th>
                    <th width="5%" class="tengah">90</th>
                    <th width="5%" class="tengah">93</th>
                    <th width="5%" class="tengah">97</th>
                    <th width="5%" class="tengah">98</th>
                    <th width="5%" class="tengah">99</th>
                    <?php }?>
                </tr>
            </thead>
            <?php
            $data = array();
            $dataTotal = array();
            $dataTotal['A40']=0;
                    $dataTotal['A41']=0;
                    $dataTotal['A42']=0;
                    $dataTotal['A43']=0;
                    $dataTotal['A44']=0;
                    $dataTotal['A45']=0;
                    $dataTotal['A46']=0;
                    $dataTotal['A47']=0;
                    $dataTotal['A48']=0;

                    //Stnh
                    $dataTotal['A10']=0;
                    $dataTotal['A11']=0;
                    $dataTotal['A12']=0;
                    $dataTotal['A13']=0;
                    $dataTotal['A15']=0;
                    $dataTotal['A17']=0;
                    $dataTotal['A18']=0;
                    $dataTotal['A20']=0;
                    $dataTotal['A21']=0;
                    $dataTotal['A22']=0;
                    $dataTotal['A23']=0;
                    $dataTotal['A24']=0;
                    $dataTotal['A26']=0;
                    $dataTotal['A27']=0;
                    $dataTotal['A28']=0;
                    $dataTotal['A29']=0;
                    $dataTotal['A31']=0;
                    $dataTotal['A32']=0;
                    $dataTotal['A33']=0;
                    $dataTotal['A34']=0;
                    $dataTotal['A35']=0;
                    $dataTotal['A36']=0;
                    $dataTotal['A37']=0;
                    $dataTotal['A38']=0;
                    $dataTotal['A39']=0;

                    //Comm
                    $dataTotal['A09']=0;
                    $dataTotal['A14']=0;
                    $dataTotal['A16']=0;
                    $dataTotal['A25']=0;
                    $dataTotal['A30']=0;
                    $dataTotal['A91']=0;
                    $dataTotal['A92']=0;

                    //System
                    $dataTotal['A50']=0;
                    $dataTotal['A55']=0;
                    $dataTotal['A56']=0;
                    $dataTotal['A57']=0;
                    $dataTotal['A58']=0;

                    //Flops
                    $dataTotal['A01']=0;
                    $dataTotal['A02']=0;
                    $dataTotal['A60']=0;
                    $dataTotal['A61']=0;
                    $dataTotal['A62']=0;
                    $dataTotal['A63']=0;
                    $dataTotal['A64']=0;
                    $dataTotal['A65']=0;
                    $dataTotal['A66']=0;
                    $dataTotal['A67']=0;
                    $dataTotal['A68']=0;
                    $dataTotal['A69']=0;
                    $dataTotal['A94']=0;
                    $dataTotal['A95']=0;
                    $dataTotal['A96']=0;

                    //Aptf
                    $dataTotal['A80']=0;
                    $dataTotal['A81']=0;
                    $dataTotal['A82']=0;
                    $dataTotal['A83']=0;
                    $dataTotal['A84']=0;
                    $dataTotal['A85']=0;
                    $dataTotal['A86']=0;
                    $dataTotal['A87']=0;
                    $dataTotal['A88']=0;
                    $dataTotal['A89']=0;

                    //Wthr
                    $dataTotal['A70']=0;
                    $dataTotal['A71']=0;
                    $dataTotal['A72']=0;
                    $dataTotal['A73']=0;
                    $dataTotal['A75']=0;
                    $dataTotal['A76']=0;
                    $dataTotal['A77']=0;

                    //Misc
                    $dataTotal['A51']=0;
                    $dataTotal['A52']=0;
                    $dataTotal['A90']=0;
                    $dataTotal['A93']=0;
                    $dataTotal['A97']=0;
                    $dataTotal['A98']=0;
                    $dataTotal['A99']=0;
            foreach ($delay_code_contributor as $key => $value) {
                $data['tanggal'][] = $value['TANGGAL'];
                if (isset($delayContributor) && $delayContributor == 1) {
                    //Technic
                    $data['A40'][] = $value['A40'];
                    $data['A41'][] = $value['A41'];
                    $data['A42'][] = $value['A42'];
                    $data['A43'][] = $value['A43'];
                    $data['A44'][] = $value['A44'];
                    $data['A45'][] = $value['A45'];
                    $data['A46'][] = $value['A46'];
                    $data['A47'][] = $value['A47'];
                    $data['A48'][] = $value['A48'];
                    $data['TECHTOTAL'] = $value['A40']+$value['A41']+$value['A42']+$value['A43']+$value['A44']+$value['A45']+$value['A46']+$value['A47']+$value['A48'];
                } else
                if (isset($delayContributor) && $delayContributor == 2) {
                    //Stnh
                    $data['A10'][] = $value['A10'];
                    $data['A11'][] = $value['A11'];
                    $data['A12'][] = $value['A12'];
                    $data['A13'][] = $value['A13'];
                    $data['A15'][] = $value['A15'];
                    $data['A17'][] = $value['A17'];
                    $data['A18'][] = $value['A18'];
                    $data['A20'][] = $value['A20'];
                    $data['A21'][] = $value['A21'];
                    $data['A22'][] = $value['A22'];
                    $data['A23'][] = $value['A23'];
                    $data['A24'][] = $value['A24'];
                    $data['A26'][] = $value['A26'];
                    $data['A27'][] = $value['A27'];
                    $data['A28'][] = $value['A28'];
                    $data['A29'][] = $value['A29'];
                    $data['A31'][] = $value['A31'];
                    $data['A32'][] = $value['A32'];
                    $data['A33'][] = $value['A33'];
                    $data['A34'][] = $value['A34'];
                    $data['A35'][] = $value['A35'];
                    $data['A36'][] = $value['A36'];
                    $data['A37'][] = $value['A37'];
                    $data['A38'][] = $value['A38'];
                    $data['A39'][] = $value['A39'];
                    $data['STNHTOTAL'] = $value['A10']+$value['A11']+$value['A12']+$value['A13']+$value['A15']+$value['A17']+$value['A18']+$value['A20']+$value['A21']+$value['A22']+
                            $value['A23']+$value['A24']+$value['A26']+$value['A27']+$value['A28']+$value['A29']+$value['A31']+$value['A32']+$value['A33']+$value['A34']+
                            $value['A35']+$value['A36']+$value['A37']+$value['A38']+$value['A39'];
                } else
                if (isset($delayContributor) && $delayContributor == 3) {
                    //Comm
                    $data['A09'][] = $value['A09'];
                    $data['A14'][] = $value['A14'];
                    $data['A16'][] = $value['A16'];
                    $data['A25'][] = $value['A25'];
                    $data['A30'][] = $value['A30'];
                    $data['A91'][] = $value['A91'];
                    $data['A92'][] = $value['A92'];
                    $data['COMMTOTAL'] = $value['A09']+$value['A14']+$value['A16']+$value['A25']+$value['A30']+$value['A91']+$value['A92'];
                } else
                if (isset($delayContributor) && $delayContributor == 4) {
                    //System
                    $data['A50'][] = $value['A50'];
                    $data['A55'][] = $value['A55'];
                    $data['A56'][] = $value['A56'];
                    $data['A57'][] = $value['A57'];
                    $data['A58'][] = $value['A58'];
                    $data['SYSTTOTAL'] = $value['A50']+$value['A55']+$value['A56']+$value['A57']+$value['A58'];
                } else
                if (isset($delayContributor) && $delayContributor == 5) {
                    //Flops
                    $data['A01'][] = $value['A01'];
                    $data['A02'][] = $value['A02'];
                    $data['A60'][] = $value['A60'];
                    $data['A61'][] = $value['A61'];
                    $data['A62'][] = $value['A62'];
                    $data['A63'][] = $value['A63'];
                    $data['A64'][] = $value['A64'];
                    $data['A65'][] = $value['A65'];
                    $data['A66'][] = $value['A66'];
                    $data['A67'][] = $value['A67'];
                    $data['A68'][] = $value['A68'];
                    $data['A69'][] = $value['A69'];
                    $data['A94'][] = $value['A94'];
                    $data['A95'][] = $value['A95'];
                    $data['A96'][] = $value['A96'];
                    $data['FLOPSTOTAL'] = $value['A01']+$value['A02']+$value['A60']+$value['A61']+$value['A62']+$value['A63']+$value['A64']+$value['A65']+$value['A66']
                            +$value['A67']+$value['A68']+$value['A69']+$value['A94']+$value['A95']+$value['A96'];
                } else
                if (isset($delayContributor) && $delayContributor == 6) {
                    //Aptf
                    $data['A80'][] = $value['A80'];
                    $data['A81'][] = $value['A81'];
                    $data['A82'][] = $value['A82'];
                    $data['A83'][] = $value['A83'];
                    $data['A84'][] = $value['A84'];
                    $data['A85'][] = $value['A85'];
                    $data['A86'][] = $value['A86'];
                    $data['A87'][] = $value['A87'];
                    $data['A88'][] = $value['A88'];
                    $data['A89'][] = $value['A89'];
                    $data['APTFTOTAL'] = $value['A80']+$value['A81']+$value['A82']+$value['A83']+$value['A84']+$value['A85']+$value['A86']+$value['A87']+$value['A88']
                            +$value['A89'];
                } else
                if (isset($delayContributor) && $delayContributor == 7) {
                    //Wthr
                    $data['A70'][] = $value['A70'];
                    $data['A71'][] = $value['A71'];
                    $data['A72'][] = $value['A72'];
                    $data['A73'][] = $value['A73'];
                    $data['A75'][] = $value['A75'];
                    $data['A76'][] = $value['A76'];
                    $data['A77'][] = $value['A77'];
                    $data['WTHRTOTAL'] = $value['A70']+$value['A71']+$value['A72']+$value['A73']+$value['A75']+$value['A76']+$value['A77'];
                } else
                if (isset($delayContributor) && $delayContributor == 8) {
                    //Misc
                    $data['A51'][] = $value['A51'];
                    $data['A52'][] = $value['A52'];
                    $data['A90'][] = $value['A90'];
                    $data['A93'][] = $value['A93'];
                    $data['A97'][] = $value['A97'];
                    $data['A98'][] = $value['A98'];
                    $data['A99'][] = $value['A99'];
                    $data['MISCTOTAL'] = $value['A51']+$value['A52']+$value['A90']+$value['A93']+$value['A97']+$value['A98']+$value['A99'];
                } else {
                    //Technic
                    $data['A40'][] = $value['A40'];
                    $data['A41'][] = $value['A41'];
                    $data['A42'][] = $value['A42'];
                    $data['A43'][] = $value['A43'];
                    $data['A44'][] = $value['A44'];
                    $data['A45'][] = $value['A45'];
                    $data['A46'][] = $value['A46'];
                    $data['A47'][] = $value['A47'];
                    $data['A48'][] = $value['A48'];

                    //Stnh
                    $data['A10'][] = $value['A10'];
                    $data['A11'][] = $value['A11'];
                    $data['A12'][] = $value['A12'];
                    $data['A13'][] = $value['A13'];
                    $data['A15'][] = $value['A15'];
                    $data['A17'][] = $value['A17'];
                    $data['A18'][] = $value['A18'];
                    $data['A20'][] = $value['A20'];
                    $data['A21'][] = $value['A21'];
                    $data['A22'][] = $value['A22'];
                    $data['A23'][] = $value['A23'];
                    $data['A24'][] = $value['A24'];
                    $data['A26'][] = $value['A26'];
                    $data['A27'][] = $value['A27'];
                    $data['A28'][] = $value['A28'];
                    $data['A29'][] = $value['A29'];
                    $data['A31'][] = $value['A31'];
                    $data['A32'][] = $value['A32'];
                    $data['A33'][] = $value['A33'];
                    $data['A34'][] = $value['A34'];
                    $data['A35'][] = $value['A35'];
                    $data['A36'][] = $value['A36'];
                    $data['A37'][] = $value['A37'];
                    $data['A38'][] = $value['A38'];
                    $data['A39'][] = $value['A39'];

                    //Comm
                    $data['A09'][] = $value['A09'];
                    $data['A14'][] = $value['A14'];
                    $data['A16'][] = $value['A16'];
                    $data['A25'][] = $value['A25'];
                    $data['A30'][] = $value['A30'];
                    $data['A91'][] = $value['A91'];
                    $data['A92'][] = $value['A92'];

                    //System
                    $data['A50'][] = $value['A50'];
                    $data['A55'][] = $value['A55'];
                    $data['A56'][] = $value['A56'];
                    $data['A57'][] = $value['A57'];
                    $data['A58'][] = $value['A58'];

                    //Flops
                    $data['A01'][] = $value['A01'];
                    $data['A02'][] = $value['A02'];
                    $data['A60'][] = $value['A60'];
                    $data['A61'][] = $value['A61'];
                    $data['A62'][] = $value['A62'];
                    $data['A63'][] = $value['A63'];
                    $data['A64'][] = $value['A64'];
                    $data['A65'][] = $value['A65'];
                    $data['A66'][] = $value['A66'];
                    $data['A67'][] = $value['A67'];
                    $data['A68'][] = $value['A68'];
                    $data['A69'][] = $value['A69'];
                    $data['A94'][] = $value['A94'];
                    $data['A95'][] = $value['A95'];
                    $data['A96'][] = $value['A96'];

                    //Aptf
                    $data['A80'][] = $value['A80'];
                    $data['A81'][] = $value['A81'];
                    $data['A82'][] = $value['A82'];
                    $data['A83'][] = $value['A83'];
                    $data['A84'][] = $value['A84'];
                    $data['A85'][] = $value['A85'];
                    $data['A86'][] = $value['A86'];
                    $data['A87'][] = $value['A87'];
                    $data['A88'][] = $value['A88'];
                    $data['A89'][] = $value['A89'];

                    //Wthr
                    $data['A70'][] = $value['A70'];
                    $data['A71'][] = $value['A71'];
                    $data['A72'][] = $value['A72'];
                    $data['A73'][] = $value['A73'];
                    $data['A75'][] = $value['A75'];
                    $data['A76'][] = $value['A76'];
                    $data['A77'][] = $value['A77'];

                    //Misc
                    $data['A51'][] = $value['A51'];
                    $data['A52'][] = $value['A52'];
                    $data['A90'][] = $value['A90'];
                    $data['A93'][] = $value['A93'];
                    $data['A97'][] = $value['A97'];
                    $data['A98'][] = $value['A98'];
                    $data['A99'][] = $value['A99'];
                }
            }

            $count = count($delay_code_contributor);
            ?>
            <tbody>
                <?php
                for ($i = 0; $i < $count; $i++) {
                    ?>
                    <tr class="tengah">
                        <td> <?php echo $i + 1; ?> </td>
                        <td> <?php echo $data['tanggal'][$i]; ?></td>
<?php if (isset($delayContributor) && $delayContributor == 1) { ?>
                        <td> <?php $dataTotal['A40']+= $data['A40'][$i];echo $data['A40'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A40'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A41']+= $data['A41'][$i];echo $data['A41'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A41'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td>
                        <td> <?php $dataTotal['A42']+= $data['A42'][$i];echo $data['A42'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A42'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td> 
                        <td> <?php $dataTotal['A43']+= $data['A43'][$i];echo $data['A43'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A43'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td>
                        <td> <?php $dataTotal['A44']+= $data['A44'][$i];echo $data['A44'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A44'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td>
                        <td> <?php $dataTotal['A45']+= $data['A45'][$i];echo $data['A45'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A45'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td> 
                        <td> <?php $dataTotal['A46']+= $data['A46'][$i];echo $data['A46'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A46'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td> 
                        <td> <?php $dataTotal['A47']+= $data['A47'][$i];echo $data['A47'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A47'][$i]/$ndelaycd * $pdelaycd, 2):0;  ?>%</td>
                        <td> <?php $dataTotal['A48']+= $data['A48'][$i];echo $data['A48'][$i]. " - "; echo $data['TECHTOTAL'] > 0 ? round($data['A48'][$i]/$ndelaycd * $pdelaycd, 2):0;?>%</td>
<?php }else  if (isset($delayContributor) && $delayContributor == 2) { ?>
                        <td> <?php $dataTotal['A10']+= $data['A10'][$i];echo $data['A10'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A10'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A11']+= $data['A11'][$i];echo $data['A11'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A11'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A12']+= $data['A12'][$i];echo $data['A12'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A12'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A13']+= $data['A13'][$i];echo $data['A13'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A13'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A15']+= $data['A15'][$i];echo $data['A15'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A15'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A17']+= $data['A17'][$i];echo $data['A17'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A17'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A18']+= $data['A18'][$i];echo $data['A18'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A18'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A20']+= $data['A20'][$i];echo $data['A20'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A20'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A21']+= $data['A21'][$i];echo $data['A21'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A21'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A22']+= $data['A22'][$i];echo $data['A22'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A22'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A23']+= $data['A23'][$i];echo $data['A23'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A23'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A24']+= $data['A24'][$i];echo $data['A24'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A24'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td> 
                        <td> <?php $dataTotal['A26']+= $data['A26'][$i];echo $data['A26'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A26'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A27']+= $data['A27'][$i];echo $data['A27'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A27'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A28']+= $data['A28'][$i];echo $data['A28'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A28'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A29']+= $data['A29'][$i];echo $data['A29'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A29'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A31']+= $data['A31'][$i];echo $data['A31'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A31'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A32']+= $data['A32'][$i];echo $data['A32'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A32'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A33']+= $data['A33'][$i];echo $data['A33'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A33'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A34']+= $data['A34'][$i];echo $data['A34'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A34'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A35']+= $data['A35'][$i];echo $data['A35'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A35'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A36']+= $data['A36'][$i];echo $data['A36'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A36'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A37']+= $data['A37'][$i];echo $data['A37'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A37'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A38']+= $data['A38'][$i];echo $data['A38'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A38'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
                        <td> <?php $dataTotal['A39']+= $data['A39'][$i];echo $data['A39'][$i]. " - "; echo $data['STNHTOTAL'] > 0 ? round($data['A39'][$i]/$ndelaycd * $pdelaycd, 2):0; ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 3) { ?>
                        <td> <?php $dataTotal['A09']+= $data['A09'][$i];echo $data['A09'][$i]." - ";echo round($data['A09'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A14']+= $data['A14'][$i];echo $data['A14'][$i]." - ";echo round($data['A14'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A16']+= $data['A16'][$i];echo $data['A16'][$i]." - ";echo round($data['A16'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A25']+= $data['A25'][$i];echo $data['A25'][$i]." - ";echo round($data['A25'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A30']+= $data['A30'][$i];echo $data['A30'][$i]." - ";echo round($data['A30'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A91']+= $data['A91'][$i];echo $data['A91'][$i]." - ";echo round($data['A91'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A92']+= $data['A92'][$i];echo $data['A92'][$i]." - ";echo round($data['A92'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else  if (isset($delayContributor) && $delayContributor == 4) { ?>
                        <td> <?php $dataTotal['A50']+= $data['A50'][$i];echo $data['A50'][$i]." - ";echo round($data['A50'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td> 
                        <td> <?php $dataTotal['A55']+= $data['A55'][$i];echo $data['A55'][$i]." - ";echo round($data['A55'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A56']+= $data['A56'][$i];echo $data['A56'][$i]." - ";echo round($data['A56'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A57']+= $data['A57'][$i];echo $data['A57'][$i]." - ";echo round($data['A57'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A58']+= $data['A58'][$i];echo $data['A58'][$i]." - ";echo round($data['A58'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 5) { ?>
                        <td> <?php echo $data['A01'][$i]+$data['A02'][$i]+$data['A60'][$i]+$data['A61'][$i]+$data['A62'][$i]+$data['A94'][$i]+$data['A95'][$i]+$data['A96'][$i]." - ";echo round((($data['A01'][$i]+$data['A02'][$i]+$data['A60'][$i]+$data['A61'][$i]+$data['A62'][$i]+$data['A94'][$i]+$data['A95'][$i]+$data['A96'][$i])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $data['A63'][$i]+$data['A64'][$i]+$data['A65'][$i]+$data['A69'][$i]." - ";echo round((($data['A63'][$i]+$data['A64'][$i]+$data['A65'][$i]+$data['A69'][$i])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $data['A66'][$i]+$data['A67'][$i]+$data['A68'][$i]." - ";echo round((($data['A66'][$i]+$data['A67'][$i]+$data['A68'][$i])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        
                        <td> <?php $dataTotal['A01']+= $data['A01'][$i];echo $data['A01'][$i]." - ";echo round($data['A01'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A02']+= $data['A02'][$i];echo $data['A02'][$i]." - ";echo round($data['A02'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A60']+= $data['A60'][$i];echo $data['A60'][$i]." - ";echo round($data['A60'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A61']+= $data['A61'][$i];echo $data['A61'][$i]." - ";echo round($data['A61'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A62']+= $data['A62'][$i];echo $data['A62'][$i]." - ";echo round($data['A62'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A63']+= $data['A63'][$i];echo $data['A63'][$i]." - ";echo round($data['A63'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A64']+= $data['A64'][$i];echo $data['A64'][$i]." - ";echo round($data['A64'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A65']+= $data['A65'][$i];echo $data['A65'][$i]." - ";echo round($data['A65'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A66']+= $data['A66'][$i];echo $data['A66'][$i]." - ";echo round($data['A66'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A67']+= $data['A67'][$i];echo $data['A67'][$i]." - ";echo round($data['A67'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A68']+= $data['A68'][$i];echo $data['A68'][$i]." - ";echo round($data['A68'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A69']+= $data['A69'][$i];echo $data['A69'][$i]." - ";echo round($data['A69'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A94']+= $data['A94'][$i];echo $data['A94'][$i]." - ";echo round($data['A94'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A95']+= $data['A95'][$i];echo $data['A95'][$i]." - ";echo round($data['A95'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A96']+= $data['A96'][$i];echo $data['A96'][$i]." - ";echo round($data['A96'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 6) { ?>
                        <td> <?php $dataTotal['A80']+= $data['A80'][$i];echo $data['A80'][$i]." - ";echo round($data['A80'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A81']+= $data['A81'][$i];echo $data['A81'][$i]." - ";echo round($data['A81'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A82']+= $data['A82'][$i];echo $data['A82'][$i]." - ";echo round($data['A82'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A83']+= $data['A83'][$i];echo $data['A83'][$i]." - ";echo round($data['A83'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A84']+= $data['A84'][$i];echo $data['A84'][$i]." - ";echo round($data['A84'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A85']+= $data['A85'][$i];echo $data['A85'][$i]." - ";echo round($data['A85'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A86']+= $data['A86'][$i];echo $data['A86'][$i]." - ";echo round($data['A86'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A87']+= $data['A87'][$i];echo $data['A87'][$i]." - ";echo round($data['A87'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A88']+= $data['A88'][$i];echo $data['A88'][$i]." - ";echo round($data['A88'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A89']+= $data['A89'][$i];echo $data['A89'][$i]." - ";echo round($data['A89'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 7) { ?>
                        <td> <?php $dataTotal['A70']+= $data['A70'][$i];echo $data['A70'][$i]." - ";echo round($data['A70'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A71']+= $data['A71'][$i];echo $data['A71'][$i]." - ";echo round($data['A71'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A72']+= $data['A72'][$i];echo $data['A72'][$i]." - ";echo round($data['A72'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A73']+= $data['A73'][$i];echo $data['A73'][$i]." - ";echo round($data['A73'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A75']+= $data['A75'][$i];echo $data['A75'][$i]." - ";echo round($data['A75'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A76']+= $data['A76'][$i];echo $data['A76'][$i]." - ";echo round($data['A76'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A77']+= $data['A77'][$i];echo $data['A77'][$i]." - ";echo round($data['A77'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 8) { ?>
                        <td> <?php $dataTotal['A51']+= $data['A51'][$i];echo $data['A51'][$i]." - ";echo round($data['A51'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A52']+= $data['A52'][$i];echo $data['A52'][$i]." - ";echo round($data['A52'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A90']+= $data['A90'][$i];echo $data['A90'][$i]." - ";echo round($data['A90'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A93']+= $data['A93'][$i];echo $data['A93'][$i]." - ";echo round($data['A93'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A97']+= $data['A97'][$i];echo $data['A97'][$i]." - ";echo round($data['A97'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A98']+= $data['A98'][$i];echo $data['A98'][$i]." - ";echo round($data['A98'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
                        <td> <?php $dataTotal['A99']+= $data['A99'][$i];echo $data['A99'][$i]." - ";echo round($data['A99'][$i]/$ndelaycd * $pdelaycd, 2); ?>%</td>
<?php }else{ ?>
                        <td> <?php $dataTotal['A40']+= $data['A40'][$i];echo $data['A40'][$i]; ?></td>
                        <td> <?php $dataTotal['A41']+= $data['A41'][$i];echo $data['A41'][$i]; ?></td>
                        <td> <?php $dataTotal['A42']+= $data['A42'][$i];echo $data['A42'][$i]; ?></td> 
                        <td> <?php $dataTotal['A43']+= $data['A43'][$i];echo $data['A43'][$i]; ?></td>
                        <td> <?php $dataTotal['A44']+= $data['A44'][$i];echo $data['A44'][$i]; ?></td>
                        <td> <?php $dataTotal['A45']+= $data['A45'][$i];echo $data['A45'][$i]; ?></td> 
                        <td> <?php $dataTotal['A46']+= $data['A46'][$i];echo $data['A46'][$i]; ?></td> 
                        <td> <?php $dataTotal['A47']+= $data['A47'][$i];echo $data['A47'][$i]; ?></td>
                        <td> <?php $dataTotal['A48']+= $data['A48'][$i];echo $data['A48'][$i]; ?></td>
                        <td> <?php $dataTotal['A10']+= $data['A10'][$i];echo $data['A10'][$i]; ?></td>
                        <td> <?php $dataTotal['A11']+= $data['A11'][$i];echo $data['A11'][$i]; ?></td>
                        <td> <?php $dataTotal['A12']+= $data['A12'][$i];echo $data['A12'][$i]; ?></td>
                        <td> <?php $dataTotal['A13']+= $data['A13'][$i];echo $data['A13'][$i]; ?></td>
                        <td> <?php $dataTotal['A15']+= $data['A15'][$i];echo $data['A15'][$i]; ?></td>
                        <td> <?php $dataTotal['A17']+= $data['A17'][$i];echo $data['A17'][$i]; ?></td>
                        <td> <?php $dataTotal['A18']+= $data['A18'][$i];echo $data['A18'][$i]; ?></td>
                        <td> <?php $dataTotal['A20']+= $data['A20'][$i];echo $data['A20'][$i]; ?></td>
                        <td> <?php $dataTotal['A21']+= $data['A21'][$i];echo $data['A21'][$i]; ?></td>
                        <td> <?php $dataTotal['A22']+= $data['A22'][$i]; echo $data['A22'][$i]; ?></td>
                        <td> <?php $dataTotal['A23']+= $data['A23'][$i];echo $data['A23'][$i]; ?></td>
                        <td> <?php $dataTotal['A24']+= $data['A24'][$i];echo $data['A24'][$i]; ?></td> 
                        <td> <?php $dataTotal['A26']+= $data['A26'][$i];echo $data['A26'][$i]; ?></td>
                        <td> <?php $dataTotal['A27']+= $data['A27'][$i];echo $data['A27'][$i]; ?></td>
                        <td> <?php $dataTotal['A28']+= $data['A28'][$i];echo $data['A28'][$i]; ?></td>
                        <td> <?php $dataTotal['A29']+= $data['A29'][$i];echo $data['A29'][$i]; ?></td>
                        <td> <?php $dataTotal['A31']+= $data['A31'][$i];echo $data['A31'][$i]; ?></td>
                        <td> <?php $dataTotal['A32']+= $data['A32'][$i];echo $data['A32'][$i]; ?></td>
                        <td> <?php $dataTotal['A33']+= $data['A33'][$i];echo $data['A33'][$i]; ?></td>
                        <td> <?php $dataTotal['A34']+= $data['A34'][$i];echo $data['A34'][$i]; ?></td>
                        <td> <?php $dataTotal['A35']+= $data['A35'][$i];echo $data['A35'][$i]; ?></td>
                        <td> <?php $dataTotal['A36']+= $data['A36'][$i];echo $data['A36'][$i]; ?></td>
                        <td> <?php $dataTotal['A37']+= $data['A37'][$i];echo $data['A37'][$i]; ?></td>
                        <td> <?php $dataTotal['A38']+= $data['A38'][$i];echo $data['A38'][$i]; ?></td>
                        <td> <?php $dataTotal['A39']+= $data['A39'][$i];echo $data['A39'][$i]; ?></td>
                        <td> <?php $dataTotal['A09']+= $data['A09'][$i];echo $data['A09'][$i]; ?></td>
                        <td> <?php $dataTotal['A14']+= $data['A14'][$i];echo $data['A14'][$i]; ?></td>
                        <td> <?php $dataTotal['A16']+= $data['A16'][$i];echo $data['A16'][$i]; ?></td>
                        <td> <?php $dataTotal['A25']+= $data['A25'][$i];echo $data['A25'][$i]; ?></td>
                        <td> <?php $dataTotal['A30']+= $data['A30'][$i];echo $data['A30'][$i]; ?></td>
                        <td> <?php $dataTotal['A91']+= $data['A91'][$i];echo $data['A91'][$i]; ?></td>
                        <td> <?php $dataTotal['A92']+= $data['A92'][$i];echo $data['A92'][$i]; ?></td>
                        <td> <?php $dataTotal['A50']+= $data['A50'][$i];echo $data['A50'][$i]; ?></td> 
                        <td> <?php $dataTotal['A55']+= $data['A55'][$i];echo $data['A55'][$i]; ?></td>
                        <td> <?php $dataTotal['A56']+= $data['A56'][$i];echo $data['A56'][$i]; ?></td>
                        <td> <?php $dataTotal['A57']+= $data['A57'][$i];echo $data['A57'][$i]; ?></td>
                        <td> <?php $dataTotal['A58']+= $data['A58'][$i];echo $data['A58'][$i]; ?></td>
                        <td> <?php $dataTotal['A01']+= $data['A01'][$i];echo $data['A01'][$i]; ?></td>
                        <td> <?php $dataTotal['A02']+= $data['A02'][$i];echo $data['A02'][$i]; ?></td>
                        <td> <?php $dataTotal['A60']+= $data['A60'][$i];echo $data['A60'][$i]; ?></td>
                        <td> <?php $dataTotal['A61']+= $data['A61'][$i];echo $data['A61'][$i]; ?></td>
                        <td> <?php $dataTotal['A62']+= $data['A62'][$i];echo $data['A62'][$i]; ?></td>
                        <td> <?php $dataTotal['A63']+= $data['A63'][$i];echo $data['A63'][$i]; ?></td>
                        <td> <?php $dataTotal['A64']+= $data['A64'][$i];echo $data['A64'][$i]; ?></td>
                        <td> <?php $dataTotal['A65']+= $data['A65'][$i];echo $data['A65'][$i]; ?></td>
                        <td> <?php $dataTotal['A66']+= $data['A66'][$i];echo $data['A66'][$i]; ?></td>
                        <td> <?php $dataTotal['A67']+= $data['A67'][$i];echo $data['A67'][$i]; ?></td>
                        <td> <?php $dataTotal['A68']+= $data['A68'][$i];echo $data['A68'][$i]; ?></td>
                        <td> <?php $dataTotal['A69']+= $data['A69'][$i];echo $data['A69'][$i]; ?></td>
                        <td> <?php $dataTotal['A94']+= $data['A94'][$i];echo $data['A94'][$i]; ?></td>
                        <td> <?php $dataTotal['A95']+= $data['A95'][$i];echo $data['A95'][$i]; ?></td>
                        <td> <?php $dataTotal['A96']+= $data['A96'][$i];echo $data['A96'][$i]; ?></td>
                        <td> <?php $dataTotal['A80']+= $data['A80'][$i];echo $data['A80'][$i]; ?></td>
                        <td> <?php $dataTotal['A81']+= $data['A81'][$i];echo $data['A81'][$i]; ?></td>
                        <td> <?php $dataTotal['A82']+= $data['A82'][$i];echo $data['A82'][$i]; ?></td>
                        <td> <?php $dataTotal['A83']+= $data['A83'][$i];echo $data['A83'][$i]; ?></td>
                        <td> <?php $dataTotal['A84']+= $data['A84'][$i];echo $data['A84'][$i]; ?></td>
                        <td> <?php $dataTotal['A85']+= $data['A85'][$i];echo $data['A85'][$i]; ?></td>
                        <td> <?php $dataTotal['A86']+= $data['A86'][$i];echo $data['A86'][$i]; ?></td>
                        <td> <?php $dataTotal['A87']+= $data['A87'][$i];echo $data['A87'][$i]; ?></td>
                        <td> <?php $dataTotal['A88']+= $data['A88'][$i];echo $data['A88'][$i]; ?></td>
                        <td> <?php $dataTotal['A89']+= $data['A89'][$i];echo $data['A89'][$i]; ?></td>
                        <td> <?php $dataTotal['A70']+= $data['A70'][$i];echo $data['A70'][$i]; ?></td>
                        <td> <?php $dataTotal['A71']+= $data['A71'][$i];echo $data['A71'][$i]; ?></td>
                        <td> <?php $dataTotal['A72']+= $data['A72'][$i];echo $data['A72'][$i]; ?></td>
                        <td> <?php $dataTotal['A73']+= $data['A73'][$i];echo $data['A73'][$i]; ?></td>
                        <td> <?php $dataTotal['A75']+= $data['A75'][$i];echo $data['A75'][$i]; ?></td>
                        <td> <?php $dataTotal['A76']+= $data['A76'][$i];echo $data['A76'][$i]; ?></td>
                        <td> <?php $dataTotal['A77']+= $data['A77'][$i];echo $data['A77'][$i]; ?></td>
                        <td> <?php $dataTotal['A51']+= $data['A51'][$i];echo $data['A51'][$i]; ?></td>
                        <td> <?php $dataTotal['A52']+= $data['A52'][$i];echo $data['A52'][$i]; ?></td>
                        <td> <?php $dataTotal['A90']+= $data['A90'][$i];echo $data['A90'][$i]; ?></td>
                        <td> <?php $dataTotal['A93']+= $data['A93'][$i];echo $data['A93'][$i]; ?></td>
                        <td> <?php $dataTotal['A97']+= $data['A97'][$i];echo $data['A97'][$i]; ?></td>
                        <td> <?php $dataTotal['A98']+= $data['A98'][$i];echo $data['A98'][$i]; ?></td>
                        <td> <?php $dataTotal['A99']+= $data['A99'][$i];echo $data['A99'][$i]; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                    <tr class="tengah">
                        <td> - </td>
                        <td> Total</td>
                        <?php if (isset($delayContributor) && $delayContributor == 1) { ?>
                        <td> <?php echo $dataTotal['A40']." - "; echo round($dataTotal['A40']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A41']." - "; echo round($dataTotal['A41']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A42']." - "; echo round($dataTotal['A42']/ $ndelaycd * $pdelaycd,2); ?>%</td> 
                        <td> <?php echo $dataTotal['A43']." - "; echo round($dataTotal['A43']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A44']." - "; echo round($dataTotal['A44']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A45']." - "; echo round($dataTotal['A45']/ $ndelaycd * $pdelaycd,2); ?>%</td> 
                        <td> <?php echo $dataTotal['A46']." - "; echo round($dataTotal['A46']/ $ndelaycd * $pdelaycd,2); ?>%</td> 
                        <td> <?php echo $dataTotal['A47']." - "; echo round($dataTotal['A47']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A48']." - "; echo round($dataTotal['A48']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else  if (isset($delayContributor) && $delayContributor == 2) { ?>
                        <td> <?php echo $dataTotal['A10']." - ";echo round($dataTotal['A10']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A11']." - ";echo round($dataTotal['A11']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A12']." - ";echo round($dataTotal['A12']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A13']." - ";echo round($dataTotal['A13']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A15']." - ";echo round($dataTotal['A15']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A17']." - ";echo round($dataTotal['A17']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A18']." - ";echo round($dataTotal['A18']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A20']." - ";echo round($dataTotal['A20']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A21']." - ";echo round($dataTotal['A21']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A22']." - ";echo round($dataTotal['A22']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A23']." - ";echo round($dataTotal['A23']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A24']." - ";echo round($dataTotal['A24']/ $ndelaycd * $pdelaycd,2); ?>%</td> 
                        <td> <?php echo $dataTotal['A26']." - ";echo round($dataTotal['A26']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A27']." - ";echo round($dataTotal['A27']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A28']." - ";echo round($dataTotal['A28']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A29']." - ";echo round($dataTotal['A29']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A31']." - ";echo round($dataTotal['A31']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A32']." - ";echo round($dataTotal['A32']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A33']." - ";echo round($dataTotal['A33']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A34']." - ";echo round($dataTotal['A34']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A35']." - ";echo round($dataTotal['A35']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A36']." - ";echo round($dataTotal['A36']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A37']." - ";echo round($dataTotal['A37']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A38']." - ";echo round($dataTotal['A38']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A39']." - ";echo round($dataTotal['A39']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 3) { ?>
                        <td> <?php echo $dataTotal['A09']." - ";echo round($dataTotal['A09']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A14']." - ";echo round($dataTotal['A14']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A16']." - ";echo round($dataTotal['A16']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A25']." - ";echo round($dataTotal['A25']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A30']." - ";echo round($dataTotal['A30']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A91']." - ";echo round($dataTotal['A91']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A92']." - ";echo round($dataTotal['A92']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else  if (isset($delayContributor) && $delayContributor == 4) { ?>
                        <td> <?php echo $dataTotal['A50']." - ";echo round($dataTotal['A50']/ $ndelaycd * $pdelaycd,2); ?>%</td> 
                        <td> <?php echo $dataTotal['A55']." - ";echo round($dataTotal['A55']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A56']." - ";echo round($dataTotal['A56']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A57']." - ";echo round($dataTotal['A57']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A58']." - ";echo round($dataTotal['A58']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 5) { ?>
                        <td> <?php echo $dataTotal['A01']+$dataTotal['A02']+$dataTotal['A60']+$dataTotal['A61']+$dataTotal['A62']+$dataTotal['A94']+$dataTotal['A95']+$dataTotal['A96']." - ";echo round((($dataTotal['A01']+$dataTotal['A02']+$dataTotal['A60']+$dataTotal['A61']+$dataTotal['A62']+$dataTotal['A94']+$dataTotal['A95']+$dataTotal['A96'])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A63']+$dataTotal['A64']+$dataTotal['A65']+$dataTotal['A69']." - ";echo round((($dataTotal['A63']+$dataTotal['A64']+$dataTotal['A65']+$dataTotal['A69'])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A66']+$dataTotal['A67']+$dataTotal['A68']." - ";echo round((($dataTotal['A66']+$dataTotal['A67']+$dataTotal['A68'])/ $ndelaycd) * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A01']." - ";echo round($dataTotal['A01']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A02']." - ";echo round($dataTotal['A02']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A60']." - ";echo round($dataTotal['A60']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A61']." - ";echo round($dataTotal['A61']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A62']." - ";echo round($dataTotal['A62']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A63']." - ";echo round($dataTotal['A63']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A64']." - ";echo round($dataTotal['A64']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A65']." - ";echo round($dataTotal['A65']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A66']." - ";echo round($dataTotal['A66']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A67']." - ";echo round($dataTotal['A67']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A68']." - ";echo round($dataTotal['A68']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A69']." - ";echo round($dataTotal['A69']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A94']." - ";echo round($dataTotal['A94']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A95']." - ";echo round($dataTotal['A95']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A96']." - ";echo round($dataTotal['A96']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 6) { ?>
                        <td> <?php echo $dataTotal['A80']." - ".round($dataTotal['A80']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A81']." - ".round($dataTotal['A81']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A82']." - ".round($dataTotal['A82']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A83']." - ".round($dataTotal['A83']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A84']." - ".round($dataTotal['A84']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A85']." - ".round($dataTotal['A85']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A86']." - ".round($dataTotal['A86']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A87']." - ".round($dataTotal['A87']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A88']." - ".round($dataTotal['A88']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A89']." - ".round($dataTotal['A89']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 7) { ?>
                        <td> <?php echo $dataTotal['A70']." - ".round($dataTotal['A70']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A71']." - ".round($dataTotal['A71']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A72']." - ".round($dataTotal['A72']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A73']." - ".round($dataTotal['A73']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A75']." - ".round($dataTotal['A75']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A76']." - ".round($dataTotal['A76']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A77']." - ".round($dataTotal['A77']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else if (isset($delayContributor) && $delayContributor == 8) { ?>
                        <td> <?php echo $dataTotal['A51']." - ".round($dataTotal['A51']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A52']." - ".round($dataTotal['A52']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A90']." - ".round($dataTotal['A90']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A93']." - ".round($dataTotal['A93']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A97']." - ".round($dataTotal['A97']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A98']." - ".round($dataTotal['A98']/ $ndelaycd * $pdelaycd,2); ?>%</td>
                        <td> <?php echo $dataTotal['A99']." - ".round($dataTotal['A99']/ $ndelaycd * $pdelaycd,2); ?>%</td>
<?php }else{ ?>
                        <td> <?php echo $dataTotal['A40']; ?></td>
                        <td> <?php echo $dataTotal['A41']; ?></td>
                        <td> <?php echo $dataTotal['A42']; ?></td> 
                        <td> <?php echo $dataTotal['A43']; ?></td>
                        <td> <?php echo $dataTotal['A44']; ?></td>
                        <td> <?php echo $dataTotal['A45']; ?></td> 
                        <td> <?php echo $dataTotal['A46']; ?></td> 
                        <td> <?php echo $dataTotal['A47']; ?></td>
                        <td> <?php echo $dataTotal['A48']; ?></td>


                        <td> <?php echo $dataTotal['A10']; ?></td>
                        <td> <?php echo $dataTotal['A11']; ?></td>
                        <td> <?php echo $dataTotal['A12']; ?></td>
                        <td> <?php echo $dataTotal['A13']; ?></td>
                        <td> <?php echo $dataTotal['A15']; ?></td>
                        <td> <?php echo $dataTotal['A17']; ?></td>
                        <td> <?php echo $dataTotal['A18']; ?></td>
                        <td> <?php echo $dataTotal['A20']; ?></td>
                        <td> <?php echo $dataTotal['A21']; ?></td>
                        <td> <?php echo $dataTotal['A22']; ?></td>
                        <td> <?php echo $dataTotal['A23']; ?></td>
                        <td> <?php echo $dataTotal['A24']; ?></td> 
                        <td> <?php echo $dataTotal['A26']; ?></td>
                        <td> <?php echo $dataTotal['A27']; ?></td>
                        <td> <?php echo $dataTotal['A28']; ?></td>
                        <td> <?php echo $dataTotal['A29']; ?></td>
                        <td> <?php echo $dataTotal['A31']; ?></td>
                        <td> <?php echo $dataTotal['A32']; ?></td>
                        <td> <?php echo $dataTotal['A33']; ?></td>
                        <td> <?php echo $dataTotal['A34']; ?></td>
                        <td> <?php echo $dataTotal['A35']; ?></td>
                        <td> <?php echo $dataTotal['A36']; ?></td>
                        <td> <?php echo $dataTotal['A37']; ?></td>
                        <td> <?php echo $dataTotal['A38']; ?></td>
                        <td> <?php echo $dataTotal['A39']; ?></td>


                        <td> <?php echo $dataTotal['A09']; ?></td>
                        <td> <?php echo $dataTotal['A14']; ?></td>
                        <td> <?php echo $dataTotal['A16']; ?></td>
                        <td> <?php echo $dataTotal['A25']; ?></td>
                        <td> <?php echo $dataTotal['A30']; ?></td>
                        <td> <?php echo $dataTotal['A91']; ?></td>
                        <td> <?php echo $dataTotal['A92']; ?></td>


                        <td> <?php echo $dataTotal['A50']; ?></td> 
                        <td> <?php echo $dataTotal['A55']; ?></td>
                        <td> <?php echo $dataTotal['A56']; ?></td>
                        <td> <?php echo $dataTotal['A57']; ?></td>
                        <td> <?php echo $dataTotal['A58']; ?></td>


                        <td> <?php echo $dataTotal['A01']; ?></td>
                        <td> <?php echo $dataTotal['A02']; ?></td>
                        <td> <?php echo $dataTotal['A60']; ?></td>
                        <td> <?php echo $dataTotal['A61']; ?></td>
                        <td> <?php echo $dataTotal['A62']; ?></td>
                        <td> <?php echo $dataTotal['A63']; ?></td>
                        <td> <?php echo $dataTotal['A64']; ?></td>
                        <td> <?php echo $dataTotal['A65']; ?></td>
                        <td> <?php echo $dataTotal['A66']; ?></td>
                        <td> <?php echo $dataTotal['A67']; ?></td>
                        <td> <?php echo $dataTotal['A68']; ?></td>
                        <td> <?php echo $dataTotal['A69']; ?></td>
                        <td> <?php echo $dataTotal['A94']; ?></td>
                        <td> <?php echo $dataTotal['A95']; ?></td>
                        <td> <?php echo $dataTotal['A96']; ?></td>


                        <td> <?php echo $dataTotal['A80']; ?></td>
                        <td> <?php echo $dataTotal['A81']; ?></td>
                        <td> <?php echo $dataTotal['A82']; ?></td>
                        <td> <?php echo $dataTotal['A83']; ?></td>
                        <td> <?php echo $dataTotal['A84']; ?></td>
                        <td> <?php echo $dataTotal['A85']; ?></td>
                        <td> <?php echo $dataTotal['A86']; ?></td>
                        <td> <?php echo $dataTotal['A87']; ?></td>
                        <td> <?php echo $dataTotal['A88']; ?></td>
                        <td> <?php echo $dataTotal['A89']; ?></td>


                        <td> <?php echo $dataTotal['A70']; ?></td>
                        <td> <?php echo $dataTotal['A71']; ?></td>
                        <td> <?php echo $dataTotal['A72']; ?></td>
                        <td> <?php echo $dataTotal['A73']; ?></td>
                        <td> <?php echo $dataTotal['A75']; ?></td>
                        <td> <?php echo $dataTotal['A76']; ?></td>
                        <td> <?php echo $dataTotal['A77']; ?></td>


                        <td> <?php echo $dataTotal['A51']; ?></td>
                        <td> <?php echo $dataTotal['A52']; ?></td>
                        <td> <?php echo $dataTotal['A90']; ?></td>
                        <td> <?php echo $dataTotal['A93']; ?></td>
                        <td> <?php echo $dataTotal['A97']; ?></td>
                        <td> <?php echo $dataTotal['A98']; ?></td>
                        <td> <?php echo $dataTotal['A99']; ?></td>
                        <?php } ?>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf'],
            scrollX: true,
            scrollY: "500px",
            scrollCollapse: true,
            autoWidth: true
        });
    });
</script>
