<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<style type="text/css">
#tabledata {
    cursor: pointer;
}
#station {
    margin-bottom: 10px;
}
</style>
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">
function loadBoxTop() {
    blockUI(boxtop);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();      

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/sdata",
        data    : {"sDate": sDate, "stn": stn},
        success :
            function(msg) {
                $("#boxtop").html(msg);
                unblockUI(boxtop);            
            },
        error   : function(){
                unblockUI(boxtop);
                $("#boxtop").html("<div class='alert alert-error'>No Data</div>");
        }
    });
}

function loadOTPStat() {
    blockUI(OTPStats);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/otpstat",
        data    : {"sDate": sDate, "stn": stn},
        success :
            function(msg) {
                $("#chartOTPstats").html(msg);
                unblockUI(OTPStats);            
            },
        error   : 
            function(){
                unblockUI(OTPStats);
                $("#chartOTPstats").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function loadDelay() {
    blockUI(cDelay);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/causedelay",
        data    : {"sDate": sDate, "stn": stn},
        success :
            function(msg) {
                $("#chartCdelay").html(msg);
                unblockUI(cDelay);            
            },
        error   : 
            function(){
                unblockUI(cDelay);
                $("#chartCdelay").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function loadFdata() {
    blockUI(boxFdata);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();      

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/fdata",
        data    : {"sDate": sDate, "stn": stn},
        success :
            function(msg) {
                $("#boxFdata").html(msg);
                unblockUI(boxFdata);            
            },
        error   : function(){
                unblockUI(boxFdata);
                $("#boxFdata").html("<div class='alert alert-error'>No Data</div>");
        }
    });
}

function loadRec() {
    blockUI(boxTable);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();
    var status = $("#status").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/tabledata",
        data    : {"sDate": sDate, "stn": stn, "status": status},
        success :
            function(msg) {
                $("#boxTable").html(msg);
                unblockUI(boxTable);            
            },
        error   : 
            function(){
                unblockUI(boxTable);
                $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function loadOTPlast() {
    blockUI(OTPlast);
    var sDate = $("#sDate").val();      
    var stn = $("#stn").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>occ/otplast",
        data    : {"sDate": sDate, "stn": stn},
        success :
            function(msg) {
                $("#chartOTPlast").html(msg);
                unblockUI(OTPlast);            
            },
        error   : 
            function(){
                unblockUI(OTPlast);
                $("#chartOTPlast").html("<div class='alert alert-error'>No Data</div>");
            }
    });
}

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}

jQuery(document).ready(function() {  
    loadBoxTop();    
    loadOTPStat();
    loadDelay();
    loadFdata();
    loadRec();
    //loadOTPlast();

    $('#submit').click(function(){
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();    

        blockUI(boxtop);
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/sdata",
            data    : {"sDate": sDate, "stn": stn},
            success :
                function(msg) {
                    $("#boxtop").html(msg);
                    unblockUI(boxtop);            
                },
            error   : function(){
                    unblockUI(boxtop);
                    $("#boxtop").html("<div class='alert alert-error'>No Data</div>");
            }
        });      

        blockUI(OTPStats);
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/otpstat",
            data    : {"sDate": sDate, "stn": stn},
            success :
                function(msg) {
                    $("#chartOTPstats").html(msg);
                    unblockUI(OTPStats);            
                },
            error   : 
                function(){
                    unblockUI(OTPStats);
                    $("#chartOTPstats").html("<div class='alert alert-error'>No Data</div>");
                }
        });

        blockUI(cDelay);
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/causedelay",
            data    : {"sDate": sDate, "stn": stn},
            success :
                function(msg) {
                    $("#chartCdelay").html(msg);
                    unblockUI(cDelay);            
                },
            error   : 
                function(){
                    unblockUI(cDelay);
                    $("#chartCdelay").html("<div class='alert alert-error'>No Data</div>");
                }
        });

        blockUI(boxFdata);
        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/fdata",
            data    : {"sDate": sDate, "stn": stn},
            success :
                function(msg) {
                    $("#boxFdata").html(msg);
                    unblockUI(boxFdata);            
                },
            error   : function(){
                    unblockUI(boxFdata);
                    $("#boxFdata").html("<div class='alert alert-error'>No Data</div>");
            }
        });

        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = $("#status").val();

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });

        /*blockUI(OTPlast);
        $.ajax({
            type    : "GET",
            url     : "<?php //echo base_url(); ?>occ/otplast",
            data    : {"sDate": sDate, "stn": stn},
            success :
                function(msg) {
                    $("#chartOTPlast").html(msg);
                    unblockUI(OTPlast);            
                },
            error   : 
                function(){
                    unblockUI(OTPlast);
                    $("#chartOTPlast").html("<div class='alert alert-error'>No Data</div>");
                }
        });*/
    });
});
</script>

                               

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height blue">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">                    
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time_utc"></span>
                    <script type="text/javascript">window.onload = date_time_utc('date_time_utc');</script>
                </div>              
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->  
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo base_url();?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">On Time Performance</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Daily</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->      
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Station</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="stn" id="stn">
                                <option value="all">All</option>
                                <?php foreach ($arrStation as $key => $row) {?>
                                <option value="<?php echo $row['STN'];?>" <?php if ($row['STN'] == strtoupper($stn)) echo "selected=selected";?>><?php echo $row['STN'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>           
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+0d">
                                <input type="text" class="form-control" value="<?php echo date('Y-m-d');?>" name="date" id="sDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>                            
                        </div>
                    </div>
                </div> 
                <div class="portlet-body pull-left">            
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>           
                </div>                
            </div>
        </div>
        <div id="boxtop"></div>
        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered" id="OTPStats">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">OTP</span>
                            <span class="caption-helper">(per 2 hours)</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartOTPstats" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered" id="cDelay">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">Cause of Delay</span>
                            <span class="caption-helper"></span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartCdelay" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div>      
        <div id="boxFdata"></div>  
        <div class="row">
            <div id="boxData"><p>&nbsp;</p></div>
        </div>
        <div id="boxTable"></div>
        <!-- <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light bordered" id="OTPlast">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">OTP</span>
                            <span class="caption-helper">last 30 days</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartOTPlast" class="CSSAnimationChart"></div>
                    </div>
                </div>
            </div>
        </div> -->         
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT