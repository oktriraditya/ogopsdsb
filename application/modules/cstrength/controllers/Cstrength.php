<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cstrength extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_cstrength');

        $i_isLogon = is_login();
        if (!$i_isLogon) redirect('auth', 'refresh');
    }

    function index() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/home');
        if ($isAuth) {
            redirect('cstrength/home', 'refresh');
        } else {
            redirect('main', 'refresh');
        }
    }

    function login_page() {
        $this->load->view("v_login");
    }

    function home() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/home');
        if ($isAuth) {
            $data = $this->M_cstrength->home();
            $this->load->view('v_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function dashboard() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/dashboard');
        if ($isAuth) {
            $data = $this->M_cstrength->dashboard();
            $this->load->view('v_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function error404() {
        $this->load->view("v_404");
    }

    function login() {
        $username = $this->input->post('uname');
        $password = $this->input->post('psw');

        if ($username == "admin" && $password == "adminotp") {
            $data = array(
                'username' => $username,
                'password' => $password,
                'logged_in' => true
            );
            $this->session->set_userdata($data);
            redirect("cstrength/home", 'refresh');
        } else {
            $this->session->set_flashdata("wrongpass", 1);
            redirect('cstrength/login_page', 'refresh');
        }
    }

    function logout() {
        $this->session->unset_userdata(array("username" => "", "logged_in" => "", "password" => ""));
        $this->session->sess_destroy();
        redirect('cstrength/login_page', 'refresh');
    }

    function ssim() {
		$sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $sFleet = $this->input->get('sFleet');
        $ssim_data = get_ssim($sDate, $eDate, $sFleet);
        echo json_encode($ssim_data);
    }

    function cstats() {
        $sFleet = $this->input->get('sFleet');
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $ssim = $this->input->get('ssim');
        //$mode = $this->input->get('mode');
        $DO = $this->input->get('DO');
        $dataMode = $this->input->get('dataMode');
        $stackBar = $this->input->get('stackBar');
        $incTraining = $this->input->get('incTraining');
        $incOffice = $this->input->get('incOffice');
        $incOther = $this->input->get('incOther');

        $data['sFleet'] = $sFleet;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['ssim'] = $ssim;
        $data['sRank'] = 'CP';
        //$data['mode'] = $mode;
        $data['DO'] = $DO;
        $data['dataMode'] = $dataMode;
        $data['stackBar'] = $stackBar;
        $data['incTraining'] = $incTraining;
        $data['incOffice'] = $incOffice;
        $data['incOther'] = $incOther;

        if ($stackBar == 3) {$this->load->view('v_cstats_mpp', $data);}
        else if ($stackBar == 2) {$this->load->view('v_cstats', $data);}
        else if ($stackBar == 1) {$this->load->view('v_cstats_fl', $data);}
    }

    function fstats() {
        $sFleet = $this->input->get('sFleet');
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $ssim = $this->input->get('ssim');
        //$mode = $this->input->get('mode');
        $DO = $this->input->get('DO');
        $dataMode = $this->input->get('dataMode');
        $stackBar = $this->input->get('stackBar');
        $incTraining = $this->input->get('incTraining');
        $incOffice = $this->input->get('incOffice');
        $incOther = $this->input->get('incOther');
        //$showAct = $this->input->get('showAct');

        $data['sFleet'] = $sFleet;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['ssim'] = $ssim;
        $data['sRank'] = 'FO';
        //$data['mode'] = $mode;
        $data['DO'] = $DO;
        $data['dataMode'] = $dataMode;
        $data['stackBar'] = $stackBar;
        $data['incTraining'] = $incTraining;
        $data['incOffice'] = $incOffice;
        $data['incOther'] = $incOther;

        if ($stackBar == 3) {$this->load->view('v_fstats_mpp', $data);}
        else if ($stackBar == 2) {$this->load->view('v_fstats', $data);}
        else if ($stackBar == 1) {$this->load->view('v_fstats_fl', $data);}
    }


    function trainingmonitoring() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/trainingmonitoring');
        if ($isAuth) {
            $data['title'] = "Training Monitoring";
            $data['title_small'] = "for Type Qualification Trainees";
            $data['view'] = 'v_cre_trainingmonitoring';
            $this->load->view('v_main', $data);
        } else {
            redirect('main','refresh');
        }
    }

    function tabletrainingmonitoring() {
        $dt = $this->input->get('datefrom');
        $flt = $this->input->get('fleet');
        $rk = $this->input->get('rank');
        $data = $this->M_cstrength->trainingmonitoring($flt, $rk, $dt);
        $this->load->view('v_cre_tabletrainingmonitor', $data);
    }

    function pilotassignment() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/pilotassignment');
        if ($isAuth) {
            ini_set('max_execution_time', 0);
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $qual = $this->input->get('qual');
            $fleet = $this->input->get('fleet');
            $rank = $this->input->get('rank');
            $data = $this->M_cstrength->pilotassignment($datefrom, $dateto, $fleet, $rank, $qual);
            $this->load->view('v_main', $data);
        } else {
            redirect('main','refresh');
        }
    }

    function assignmentperday() {
        $fleet = $this->input->get('fleet');
        $rank = $this->input->get('rank');
        $qual = $this->input->get('qual');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $data = $this->M_cstrength->pilotassignment($datefrom, $dateto, $fleet, $rank, $qual);
        $this->load->view('v_cre_tablepilotassignment', $data);
    }

    function detail_who_are_assigned() {
        //$type_cod = $this->input->post('type_delay');
        //$cod_date = $this->input->post('cod_date');
        $key = $this->uri->segment(3);
        $date = $this->uri->segment(4);
        $rank = $this->uri->segment(5);
        $fleet = $this->uri->segment(6);
        $status = $this->uri->segment(7);
        $data = $this->M_cstrength->detail_who_are_assigned($key, $date, $rank, $fleet, $status);
        $this->load->view('v_cre_main_modal', $data);
    }

    function edit_trainees() {
        //$type_cod = $this->input->post('type_delay');
        //$cod_date = $this->input->post('cod_date');
        $btn = $this->uri->segment(3);
        $flt = $this->uri->segment(4);
        $count = $this->uri->total_segments();
        //echo '<script type="text/javascript">alert("'.$btn.'")</script>';
        $trainees_start = array();
        for ($i = 5; $i <= $count; $i = $i + 2) {
            $trainees_start[$this->uri->segment($i)] = $this->uri->segment($i + 1);
        }
        $data = $this->M_cstrength->edit_trainees($trainees_start, $flt, $btn);
        $this->load->view('v_main_modal', $data);
    }

    function update_trainee() {
        $btn = $this->input->post('btn');
        //$btn = "ql";
        //echo '<script type="text/javascript">alert("'.$btn.'")</script>';
        $id_start = $this->input->post('id_start');
        //$id_start = "535251,2018-09-03,";
        $explotion = explode(",", $id_start);
        $trainees_start = array();

        for ($i = 0; $i < count($explotion) - 1; $i = $i + 2) {
            $trainees_start[$explotion[$i]] = $explotion[$i + 1];
        }
        if($btn == "ql"){
        $fup = $this->input->post('qualdate');
        //$fup = "2019-04-03";
        $data = $this->M_cstrength->update_trainee($btn,$trainees_start, $fup);
        }
        elseif($btn == "rt"){
        $fup = $this->input->post('routedate');
        $data = $this->M_cstrength->update_trainee($btn,$trainees_start, $fup);
        }
        else if($btn == "tr"){
        $training_batch = $this->input->post('batch_program');
        $data = $this->M_cstrength->update_trainee($btn,$trainees_start, NULL,$training_batch);
        }
    }

    function service_crewstrength() {
        $rk = $this->input->get('rank');
        $flt = $this->input->get('fleet');
        $sdt = $this->input->get('start');
        $edt = $this->input->get('end');
        $data = $this->M_cstrength->service_crewstrength($flt, $rk, $sdt, $edt);
        $sent = json_encode($data);
        echo $sent;
        return $sent;
    }

    function synccrewlist() {
      ini_set('max_execution_time', 0);
      $today = date("Y-m-d");
      $stat = $this->M_cstrength->synccrewlist($today);
      print_r("msg: ");
      print_r($stat==1?"synccrewlist succeed":"synccrewlist failed");
      return $stat;
    }

    function synccrewstrength() {
      ini_set('max_execution_time', 0);
      $nday = $this->input->get('nday');
      $today = date("Y-m-d");
      $data = $this->M_cstrength->synccrewstrength($today,$nday);
      print_r("msg: ");
      print_r($data==1?"synccrewstrength succeed":"synccrewstrength failed");
      return $data;
    }

    function movement() {
        $fleet = $this->uri->segment(3);
        $rk = $this->uri->segment(4);
        $end = $this->uri->segment(5);
        $today = date("Y-m-d");
        $data = $this->M_cstrength->movement($fleet,$rk,$today,$end);
        $this->load->view('v_cre_main_modal', $data);
    }

    function movement_merge() {
        //$fleet = $this->uri->segment(3);
        //$rk = $this->uri->segment(4);
        //$end = $this->uri->segment(5);
        //$today = date("Y-m-d");
        $today  = date("Y-m-d");
        $end  = $this->input->get('sDate');
        $fleet = $this->input->get('sFleet');
        $rk  = $this->input->get('sRank');
        $data = $this->M_cstrength->movement($fleet,$rk,$today,$end);
        $this->load->view('v_cre_main_modal', $data);
    }

    function crew_list() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/crew_list/ALL/ALL');
        if ($isAuth) {
            $fleet = $this->uri->segment(3);
            $rk = $this->uri->segment(4);
            $data = $this->M_cstrength->crew_list($fleet,$rk);
            $this->load->view('v_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function crewlist() {
        // $isAuth = is_allow($this->session->userdata('iGrp'), 'cstrength/crew_list');
        // if ($isAuth) {
            $fleet = $this->uri->segment(3);
            $rk = $this->uri->segment(4);
            $data = $this->M_cstrength->crewlist($fleet,$rk);
            $this->load->view('v_cre_main_modal', $data);
        // } else {
        //     redirect('main', 'refresh');
        // }
    }

    function current_cs() {
        $fleet = $this->input->get('fleet');
        $rk = $this->input->get('rank');
        $data = $this->M_cstrength->current_cs($fleet,$rk);
        $this->load->view('v_current_cs', $data);
    }

    function defaultssim(){
        $sFleet = $this->input->get('fleet');
        $default = get_default_ssim($sFleet);
        return $default;
    }

    function sync_ods_roster() {
        ini_set('max_execution_time', 0);
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $data = $this->M_cstrength->sync_ods_roster($datefrom, $dateto);
        print_r("msg: ");
        print_r($data==1?"sync_ods_roster succeed":"sync_ods_roster failed");
        return TRUE;
    }

    function sync_ods_flight_count(){
        ini_set('max_execution_time', 0);
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $data = $this->M_cstrength->sync_ods_flight_count($datefrom, $dateto);
        print_r("msg: ");
        print_r($data==1?"sync_ods_flight_count succeed":"sync_ods_flight_count failed");
        return TRUE;
    }

  /*  function crewAmount() {
        $data['sDate'] = $this->input->get('sDate');
        $data['Fleet'] = list_fleet();
        //$this->load->view('v_cre_boxcrewamountall', $data);
    }*/

    function tableCrewAmount() {
        $data['sDate']  = $this->input->get('sDate');
        $data['sFleet'] = $this->input->get('sFleet');
        $data['sRank']  = $this->input->get('sRank');
        $CS = $this->M_cstrength->crewAmountPerDateFleetRank($data['sDate'],$data['sFleet'],$data['sRank']);
        $data['CS'] = $CS;
        $this->load->view('v_cre_boxcrewamountfleet', $data);
    }

    function syncback(){
        ini_set('max_execution_time', 0);
        $dt = $this->input->get('backdate');
        $backdt = new datetime($dt);
        $dt2 = $this->input->get('enddate');
        $enddt  = new datetime($dt2);
        $today  = new datetime(date("Y-m-d"));
        if ($dt<$today && $dt2<$today && $dt<$dt2) $data = $this->M_cstrength->syncback($backdt, $enddt);
        else $data = FALSE;
        print_r("msg: ");
        print_r($data==1?"syncback succeed":"syncback failed");
        return $data;
    }

    function boxMovement() {
        $sFleet = $this->uri->segment(3);
        $sRank  = $this->uri->segment(4);
        $sDate  = $this->uri->segment(5);
        $today  = new datetime(date("Y-m-d"));
        $data = $this->M_cstrength->crewMovementSum($sFleet, $sRank, $today, $sDate);
        $this->load->view('v_cre_main_modal', $data);
    }

    function tableCrewMovement() {
        $data['sSTart']  = $this->input->get('sStart');
        $data['sDate']  = $this->input->get('sDate');
        $data['sFleet'] = $this->input->get('sFleet');
        $data['sRank']  = $this->input->get('sRank');
        $data['sStat']  = $this->input->get('sStat');
        $mov = $this->M_cstrength->crewMovementFleetRank($data['sSTart'],$data['sDate'],$data['sStat'],$data['sFleet'],$data['sRank']);
        $data['mov'] = $mov;
        //$this->load->view('v_cre_boxcrewmovementdata', $data);

        if ($data['sStat'] == "Q") {
          $this->load->view('v_cre_boxcrewmovdataQ', $data);
        } else if ($data['sStat'] == "RT") {
          $this->load->view('v_cre_boxcrewmovdataRT', $data);
        } else if ($data['sStat'] == "GT") {
          $this->load->view('v_cre_boxcrewmovdataGT', $data);
        }
    }
}
