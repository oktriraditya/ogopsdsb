<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_cstrength extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function home() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['fleet'] = list_fleet();
        $data['title'] = 'Pilot Man Power Dashboard';
        $data['title_small'] = '';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_home';
        return $data;
    }

    function dashboard() {
        $date = date('H:i');
        $time = strtotime($date);
        $time = $time - (2 * 60);
        $data['fleet'] = list_fleet();
        $data['title'] = 'Pilot Man Power Dashboard';
        $data['title_small'] = '';
        $data['jam'] = date('H:i', $time);
        $data['view'] = 'v_dashboard';
        return $data;
    }

    function trainingmonitoring($flt, $rk, $dt){
        $data['fleet'] = $flt;
        $data['rank'] = $rk;
        $data['date'] = $dt;
        $data['traineelist'] = get_trainee_n_register_inprogress($flt, $rk, $dt);
        $data['view'] = 'v_cre_trainingmonitoring';
        return $data;
    }

    function pilotassignment($datefrom, $dateto, $fleet, $rank, $status) {
    //echo '<script type="text/javascript">alert("' . $datefrom .' ' . $dateto .' '.$fleet.' '.$rank.' '.$status.'")</script>';
    //ini_set('max_execution_time',0);
    $data['title'] = 'Crew Strength Dashboard';
    $data['title_small'] = 'Pilot Assignment';
    $data['date_from'] = $datefrom;
    $data['fleet'] = $fleet;
    $data['rank'] = $rank;
    $data['status'] = $status;
    $date1 = new DateTime ($datefrom);
    $date2 = new DateTime ($dateto);
    $diff = $date2->diff($date1);
    $day = $diff->d;
    $alldate = 0;
    $alldate = array();
    $total = 0;
    $total = array();
    $Query = 0;
    for( $i=0; $i<=$day; $i++ ) {
        $ttl = 0;
        $date1_str = $date1->format('Y-m-d');

        $perdate = query_number_pilot_assignment($date1_str, $fleet, $rank, $status);
        //echo '<script type="text/javascript">alert("'.count($perdate).'")</script>';
        foreach ($perdate as $key => $value) {

            if (!isset( $alldate[$value['ACTIVITIES_GROUP']][$date1_str] )){
                $alldate[$value['ACTIVITIES_GROUP']][$date1_str]= $value['TOTAL_CREW'];
            } else { $alldate[$value['ACTIVITIES_GROUP']][$date1_str]= $alldate[$value['ACTIVITIES_GROUP']][$date1_str] + $value['TOTAL_CREW'];}

            $ttl = $ttl + $value['TOTAL_CREW'];


            $Query = insert_crew_assignment($value['ACTIVITIES_GROUP'],$value['TOTAL_CREW'],$fleet,$rank,$date1_str,$status);
            //echo '<script type="text/javascript">alert("'.$Query.'")</script>';

        }
        $total[$date1_str]= $ttl;
        $date1 = $date1->modify('+1 day');
        //break;
    }
    ksort($alldate);
    $data['dailyassignment'] = $alldate;
    $data['total']=$total;
    $data['date_to']=$dateto;
    $data['day'] = $day;
    $data['view'] = 'v_cre_pilotassignment';
    return $data;
    }

    function detail_who_are_assigned($key, $date, $rank, $fleet, $status) {
    //echo '<script type="text/javascript">alert("'.$key.' '.$date.' '.$rank.' '.$fleet.' '.$status.'")</script>';
    $data['whoareassigned'] = get_detail_who_are_assigned($key, $date, $rank, $fleet, $status);
    //$query = get_detail_who_are_assigned($key, $date, $rank, $fleet, $status);
    //echo '<script type="text/javascript">alert("'.$query.'")</script>';
    //$data['query'] = $query;
    $data['fleet'] = $fleet;
    $data['rank'] = $rank;
    $data['key'] = $key;
    //$data['typeCOD'] = $type_cod;
    //echo '<script type="text/javascript">alert("M_OCC")</script>';
    $data['view'] = 'v_cre_detailwhoareassigned';
    return $data;
    }

    function edit_trainees($trainees, $flt, $btn){
    $data['trainees'] = $trainees;
    $data['button'] = $btn;
    $data['fleet'] = $flt;
    $data['program_batch'] = get_tr_program_batch($flt);
    $data['view'] = 'v_cre_edittrainees';
    return $data;
    }

    function update_trainee($btn, $trainees, $fup, $trbatch = NULL) {
    foreach ($trainees as $key => $value)  {
        $cek = check_register($key, $value);
        if ($cek == 0) {
            insert_register($btn, $key, $trainees[$key], $fup, $trbatch);
        } else {
            update_register($btn, $key, $trainees[$key], $fup, $trbatch);
        }
    }
    }

    function service_crewstrength($flt,$rk,$sdt,$edt) {
    $data = query_table_crewstrength($flt,$rk,$sdt,$edt);
    return $data;
    }

    function synccrewlist($dt){
    //Query List of Crew & Progress Training
    $data_crew = get_pilot_and_progress($dt);

    if (isset($data_crew)) {

        //Clean Temp_Crew_List
            $clean = clean_temp_crew_list();

        //Preparing & Writing Temp_Crew_List

            //Merge Data Pilot End Date dari Database Apps
            $out_by_input = get_pilot_out_plan($dt);
            $out = array();

            //echo '<script type="text/javascript">alert('.$data_crew[0]['IDPEG'].')</script>';
            foreach ($out_by_input as $key => $value){
                $out[$value['emp_id']]['out_date']=$value['out_date'];
                $out[$value['emp_id']]['out_reason']=$value['out_reason'];
            }

            foreach ($data_crew as $key => $value){

                //Update Emp_End_Date dengan inputan user (dengan syarat tglnya lebih dulu dari emp_end_date dari ODS)
                if (isset($out[$value['EMP_ID']]['out_date'])){
                    if ( $out[$value['EMP_ID']]['out_date'] < $value['END_EMPLOYEE_DATE'] ){
                        $data_crew[$key]['END_EMPLOYEE_DATE'] = $out[$value['EMP_ID']]['out_date'];
                    }
                }
                $tr_est = get_single_registered_training($value['EMP_ID'],$value['STARTTRAINING']);

                // Update Est_Route_Tr dengan inputan user (jika ID masih Ground Training)
                if ($value['QUALSTAT']=="GT" and isset($tr_est[0]['route_est'])){
                    $data_crew[$key]['STARTROUTETRAINING'] = $tr_est[0]['route_est'];
                }

                // Update Est_Qualified dengan inputan user (jika ID masih Ground Training/Route Training)
                if (($value['QUALSTAT']=="GT" or $value['QUALSTAT']=="RT") and isset($tr_est[0]['qual_est']) and $tr_est[0]['qual_est'] > $dt){
                    if (($tr_est[0]['qual_est'] < $value['ENDTRAINING']) or ($value['ENDTRAINING']=='0000-00-00') or !isset($value['ENDTRAINING']) ) {
                        $data_crew[$key]['ENDTRAINING'] = $tr_est[0]['qual_est'];
                    }
                }

        (isset($tr_est[0]['tr_name']) ? $data_crew[$key]['TR_NAME'] = $tr_est[0]['tr_name'] : $data_crew[$key]['TR_NAME'] = "");
        (isset($tr_est[0]['batch_name']) ? $data_crew[$key]['BATCH_NAME'] = $tr_est[0]['batch_name'] : $data_crew[$key]['BATCH_NAME'] ="");


        //Check Tr_737 Max atau bukan
        if (($value['QUALSTAT']=="GT" || $value['QUALSTAT']=="RT") && isset($tr_est[0]['tr_name'])) {
        if (check_trmax($tr_est[0]['tr_name']) == 1 ) {
            $data_crew[$key]['QUALSTAT'] = 'Q';
        }

        }

        // Insert to Table & Cek Apakah crew masuk ke Crew_list atau tidak (filter dari table pilot estimation, karena ada pilot QG yang datanya masuk di Sabre seolah-olah sebagai pilot GA)
        $exclude = check_pilot_must_not_include($value['EMP_ID'], $dt);
        if ($exclude == 0){
          insert_temp_crew_list($data_crew[$key]);
        }
        //break;

        }	//END OF LOOP
    $stat= TRUE;
    } else {
    $stat = FALSE;
    }
    return $stat;
    }

    function synccrewstrength($dt,$nday) {
    $daily_strength= array();
    $count_crew = count_temp_crew_list();
    $dt_dt = new DateTime ($dt);
    $current_dt = $dt;
    $max_dt = clone $dt_dt;
    $max_dt = $max_dt->modify('+'.$nday.' day');
    $fleets = fleet_list();
    $clean = clean_crew_strength($dt);

    foreach ($count_crew as $key => $value){
    $daily_strength[$value['fleet']][$value['rank']][$dt][$value['qualstat']] = $value['count'];
    //print_r("fleet:".$value['fleet'].", rank:".$value['rank'].", date:".$dt.", stat: ".$value['qualstat'].", TOTAL: ".$value['count']);
    }

    foreach ($fleets as $key => $value){
    if (!isset($daily_strength[$value['fleet']]['CP'][$dt]['Q'])){
    $daily_strength[$value['fleet']]['CP'][$dt]['Q']=0;
    }
    if (!isset($daily_strength[$value['fleet']]['CP'][$dt]['RT'])){
    $daily_strength[$value['fleet']]['CP'][$dt]['RT']=0;
    }
    if (!isset($daily_strength[$value['fleet']]['CP'][$dt]['GT'])){
    $daily_strength[$value['fleet']]['CP'][$dt]['GT']=0;
    }
    if (!isset($daily_strength[$value['fleet']]['FO'][$dt]['Q'])){
    $daily_strength[$value['fleet']]['FO'][$dt]['Q']=0;
    }
    if (!isset($daily_strength[$value['fleet']]['FO'][$dt]['RT'])){
    $daily_strength[$value['fleet']]['FO'][$dt]['RT']=0;
    }
    if (!isset($daily_strength[$value['fleet']]['FO'][$dt]['GT'])){
    $daily_strength[$value['fleet']]['FO'][$dt]['GT']=0;
    }
    insert_crew_strength($dt_dt->format('Y-m-d'),$value['fleet'],'CP',$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['Q'],$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['RT'],$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['GT']);
    insert_crew_strength($dt_dt->format('Y-m-d'),$value['fleet'],'FO',$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['Q'],$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['RT'],$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['GT']);
    }

    $dt_prev = clone $dt_dt;
    $dt_dt = $dt_dt->modify('+1 day');

    while ($dt_dt <= $max_dt) {
    foreach ($fleets as $key => $value) {
    //Hitung Parameter Penambahan untuk CP
    $add_q1 = get_qual_prev_route_temp_crew($value['fleet'],'CP',$dt_dt->format('Y-m-d'))+get_qual_prev_route_course($value['fleet'],'CP',$dt_dt->format('Y-m-d'),$current_dt);
    $add_q2 = get_qual_prev_ground_temp_crew($value['fleet'],'CP',$dt_dt->format('Y-m-d'))+get_qual_prev_ground_course($value['fleet'],'CP',$dt_dt->format('Y-m-d'),$current_dt);
    $add_rt = get_route_prev_ground_temp_crew($value['fleet'],'CP',$dt_dt->format('Y-m-d')) + get_route_prev_ground_course($value['fleet'],'CP',$dt_dt->format('Y-m-d'),$current_dt);
    $add_gt = get_ground_course($value['fleet'],'CP',$dt_dt->format('Y-m-d'));
    $reduce_course = get_ground_course_prev($value['fleet'],'CP',$dt_dt->format('Y-m-d'));
    $reduce_q = get_end_date_q($value['fleet'],'CP',$dt_dt->format('Y-m-d'));
    $reduce_rt = get_end_date_rt($value['fleet'],'CP',$dt_dt->format('Y-m-d'));
    $reduce_gt = get_end_date_gt($value['fleet'],'CP',$dt_dt->format('Y-m-d'));

    //Hitung Jumlah Captain
    $daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['Q'] = $daily_strength[$value['fleet']]['CP'][$dt_prev->format('Y-m-d')]['Q'] + $add_q1 + $add_q2 - $reduce_course - $reduce_q;
    $daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['RT']= $daily_strength[$value['fleet']]['CP'][$dt_prev->format('Y-m-d')]['RT'] + $add_rt - $add_q1 - $reduce_rt;
    $daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['GT']= $daily_strength[$value['fleet']]['CP'][$dt_prev->format('Y-m-d')]['GT'] - $add_q2 + $add_gt - $reduce_gt - $add_rt;

    $stat = insert_crew_strength($dt_dt->format('Y-m-d'),$value['fleet'],'CP',$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['Q'],$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['RT'],$daily_strength[$value['fleet']]['CP'][$dt_dt->format('Y-m-d')]['GT']);

    //Hitung Parameter Penambahan untuk FO
    $add_q1 = get_qual_prev_route_temp_crew($value['fleet'],'FO',$dt_dt->format('Y-m-d'))+get_qual_prev_route_course($value['fleet'],'FO',$dt_dt->format('Y-m-d'),$current_dt);
    $add_q2 = get_qual_prev_ground_temp_crew($value['fleet'],'FO',$dt_dt->format('Y-m-d'))+get_qual_prev_ground_course($value['fleet'],'FO',$dt_dt->format('Y-m-d'),$current_dt);
    $add_rt = get_route_prev_ground_temp_crew($value['fleet'],'FO',$dt_dt->format('Y-m-d')) + get_route_prev_ground_course($value['fleet'],'FO',$dt_dt->format('Y-m-d'),$current_dt);
    $add_gt = get_ground_course($value['fleet'],'FO',$dt_dt->format('Y-m-d'));
    $reduce_course = get_ground_course_prev($value['fleet'],'FO',$dt_dt->format('Y-m-d'));
    $reduce_q = get_end_date_q($value['fleet'],'FO',$dt_dt->format('Y-m-d'));
    $reduce_rt = get_end_date_rt($value['fleet'],'FO',$dt_dt->format('Y-m-d'));
    $reduce_gt = get_end_date_gt($value['fleet'],'FO',$dt_dt->format('Y-m-d'));

    //Hitung Jumlah First Officer
    $daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['Q'] = $daily_strength[$value['fleet']]['FO'][$dt_prev->format('Y-m-d')]['Q'] + $add_q1 + $add_q2 - $reduce_course - $reduce_q;
    $daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['RT']= $daily_strength[$value['fleet']]['FO'][$dt_prev->format('Y-m-d')]['RT'] + $add_rt - $add_q1 - $reduce_rt;
    $daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['GT']= $daily_strength[$value['fleet']]['FO'][$dt_prev->format('Y-m-d')]['GT'] - $add_q2 + $add_gt - $reduce_gt - $add_rt;

    $stat = insert_crew_strength($dt_dt->format('Y-m-d'),$value['fleet'],'FO',$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['Q'],$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['RT'],$daily_strength[$value['fleet']]['FO'][$dt_dt->format('Y-m-d')]['GT']);
    //break;
    }
    $dt_prev = clone $dt_dt;
    $dt_dt = $dt_dt->modify('+1 day');
    //break;
    } //end of while
    return TRUE;
    }

    function movement ($flt, $rk, $start, $end) {
    $mov = array();

    // Get Pilot Qualified
    $rt_q = get_person_qual_prev_route_temp_crew($flt,$rk,$start,$end);
    $gt_q = get_person_qual_prev_ground_temp_crew($flt,$rk,$start,$end);
    $gt_rt = get_person_route_prev_ground_temp_crew($flt,$rk,$start,$end);
    $rt_q_c = get_person_qual_prev_route_course($flt,$rk,$start,$end);
    $gt_q_c = get_person_qual_prev_ground_course($flt,$rk,$start,$end);
    $gt_rt_c = get_person_route_prev_ground_course($flt,$rk,$start,$end);
    $c_gt = get_course_ground_course($flt,$rk,$start,$end);
    $c_out = get_course_ground_course_prev($flt,$rk,$start,$end);
    $end_q = get_person_end_date_q($flt,$rk,$start,$end);
    $end_rt = get_person_end_date_rt($flt,$rk,$start,$end);
    $end_gt = get_person_end_date_gt($flt,$rk,$start,$end);
    $mov = array_merge($rt_q,$gt_q,$gt_rt,$rt_q_c,$gt_q_c,$gt_rt_c, $c_gt, $c_out, $end_q, $end_rt, $end_gt);
    foreach ($mov as $key => $row) {
    $date[$key]  = $row['date'];
    }
    if ($mov != NULL)array_multisort($date,SORT_ASC,$mov);
    $data['mov'] = $mov;
    $data['rank'] = $rk;
    $data['fleet'] = $flt;
    $data['view'] = 'v_cre_movement';
    return $data;
    }

    function crewlist ($flt, $rk) {
    $data['rank'] = $rk;
    $data['fleet'] = $flt;
    $data['crew'] = retrieve_crewlist($flt, $rk);
    $data['view'] = 'v_crewlist';
    return $data;
    }

    function crew_list ($flt, $rk) {
    $data['rank'] = $rk;
    $data['fleet'] = $flt;
    $data['crew'] = retrieve_crewlist($flt, $rk);
    $data['view'] = 'v_crew_listx';
    return $data;
    }

    function current_cs() {
        $dt = date('Y-m-d');
        $fleet = $this->input->get('fleet');
        $rk = $this->input->get('rank');
        $data['rk'] = $rk;
        $data['cs'] = query_table_crewstrength($fleet,$rk,$dt,$dt);
        $data['view'] = 'v_current_cs';
        return $data;
    }

    function crewAmountPerDateFleetRank($sDate,$sFleet,$sRank) {
        $data = query_table_crewstrength($sFleet,$sRank,$sDate,$sDate);
        return $data;
    }

    function syncback($dt_i,$dt_end) {
      $cs = array();
      $temp = array();
      $cpt_fot = array();
      clean_crew_strength($dt_i->format('Y-m-d'),$dt_end->format('Y-m-d'));

      while ($dt_i->format('Y-m-d') < $dt_end->format('Y-m-d')) {

          $temp = get_ods_crewstrength($dt_i->format('Y-m-d'));

          foreach ($temp as $key => $value) {
            $cs[$value['FLEET']] [$value['QUALSTAT']] [$value['RANK_']] = $value['COUNT_ID'];
          }

          //CEK ADA TRAINING MAX atau TIDAK
          $cpt_fot = get_cpt_fot($dt_i->format('Y-m-d'));
          foreach ($cpt_fot as $key => $value) {
              if (check_cpt_fot_max($value['EMPID'],$value['STARTQUAL']) == TRUE )
                ($value['QUALNAME'] == "GACPT"? $cs['737']['Q']['CP']++ : $cs['737']['Q']['FO']++ );
          }

          foreach ($cs as $key => $value) {
            insert_crew_strength($dt_i->format('Y-m-d'),$key,"CP",(isset($cs[$key]['Q']['CP'])?$cs[$key]['Q']['CP']:0),(isset($cs[$key]['RT']['CP'])?$cs[$key]['RT']['CP']:0),(isset($cs[$key]['GT']['CP'])?$cs[$key]['GT']['CP']:0));
            insert_crew_strength($dt_i->format('Y-m-d'),$key,"FO",(isset($cs[$key]['Q']['FO'])?$cs[$key]['Q']['FO']:0),(isset($cs[$key]['RT']['FO'])?$cs[$key]['RT']['FO']:0),(isset($cs[$key]['GT']['FO'])?$cs[$key]['GT']['FO']:0));
          }

          $cs = 0; $temp = 0;
          $cs = array(); $temp = array();
          $dt_i = $dt_i->modify('+ 1 day');
      }
      return TRUE;
    }

    function sync_ods_roster($datefrom, $dateto){
      $arrDay = getDatesFromRange($datefrom, $dateto);
      $fleets = fleet_list();
      clean_crew_assign($datefrom,$dateto,"Qualified");
      foreach($arrDay as $sDay) {
          foreach ($fleets as $key => $flt) {
            //INSERT CAPTAIN
            $perdate = query_number_pilot_assignment($sDay, $flt['fleet'], "CP", "Qualified");
            foreach ($perdate as $key => $value) {
                $Query = insert_crew_assignment($value['ACTIVITIES_GROUP'],$value['TOTAL_CREW'],$flt['fleet'],"CP",$sDay,"Qualified");
            }
            //INSERT FO
            $perdate = query_number_pilot_assignment($sDay, $flt['fleet'], "FO", "Qualified");
            foreach ($perdate as $key => $value) {
                $Query = insert_crew_assignment($value['ACTIVITIES_GROUP'],$value['TOTAL_CREW'],$flt['fleet'],"FO",$sDay,"Qualified");
            }
          }
      }
      return TRUE;
    }

    function sync_ods_flight_count($datefrom, $dateto){
      $arrDay = getDatesFromRange($datefrom, $dateto);
      $fleets = fleet_list();
      clean_flight_count($datefrom,$dateto,"ODS");
      foreach($arrDay as $sDay) {
          foreach ($fleets as $key => $flt) {
            //INSERT COUNT OF FLIGHT PER DAY
            $perdate = get_ods_flight_count($sDay, $sDay, $flt['fleet']);
            foreach ($perdate as $key => $value) {
                insert_fligth_count($value['COUNT'],$flt['fleet'], $sDay, $value['SERVICETYPE'],"ODS");
            }
          }
          $perdate = 0;
      }
      return TRUE;
    }

    function crewMovementSum($sFleet, $sRank, $today, $sDate) {
      $data['rank'] = $sRank;
      $data['fleet'] = $sFleet;
      $data['today'] = $today->format('Y-m-d');
      $data['sDate'] = $sDate;
      $data['title'] = $sFleet." 's ".$sRank." Movement";
      $data['title_small'] = $data['today']." until ".$sDate;
      $data['view'] = 'v_cre_boxmovementpage';
      return $data;
    }

    function crewMovementFleetRank($start,$end,$sStat,$flt,$rk) {
        //$mov = array();
        // Get Pilot Qualified
        if ($sStat=='Q') {
          //NEW QUALIFIED
          $rt_q = get_person_qual_prev_route_temp_crew($flt,$rk,$start,$end);
          $gt_q = get_person_qual_prev_ground_temp_crew($flt,$rk,$start,$end);
          $rt_q_c = get_person_qual_prev_route_course_count($flt,$rk,$start,$end);
          $gt_q_c = get_person_qual_prev_ground_course_count($flt,$rk,$start,$end);
          $var1 = count($rt_q) + count($gt_q) + $rt_q_c + $gt_q_c ;
          //MUTATION/PROMOTION
          $c_out = get_course_ground_course_prev_count($flt,$rk,$start,$end);
          if (!isset($c_out)) $c_out = 0;
          $var2 = $c_out;
          //RETIRED65/RESIGN/OUT
          $end_q = get_person_end_date_q($flt,$rk,$start,$end);
          $var3 = count($end_q);

        } else if ($sStat=='RT') {
          //NEW ROUTE (ADD)
          $gt_rt = get_person_route_prev_ground_temp_crew($flt,$rk,$start,$end);
          $gt_rt_c = get_person_route_prev_ground_course_count($flt,$rk,$start,$end);
          $var1 = count($gt_rt) + $gt_rt_c;
          //FINISH ROUTE (MINUS)
          $rt_q = get_person_qual_prev_route_temp_crew($flt,$rk,$start,$end);
          $rt_q_c = get_person_qual_prev_route_course_count($flt,$rk,$start,$end);
          $var2 = count($rt_q ) + $gt_rt_c;

          //RETIRED 65 Y/RESIGN/OUT/RETIRED
          $end_rt = get_person_end_date_rt($flt,$rk,$start,$end);
          //RETIRED 65 Y
          $var3 = count($end_rt);
        } else {
          //NEW GROUND TRAINING (ADD)
          $c_gt = get_course_ground_course_count($flt,$rk,$start,$end);
          $var1 = $c_gt;
          //FINISH GROUND TRAINING (MINUS)
          $gt_rt = get_person_route_prev_ground_temp_crew($flt,$rk,$start,$end);
          $gt_q = get_person_qual_prev_ground_temp_crew($flt,$rk,$start,$end);
          $gt_q_c = get_person_qual_prev_ground_course_count($flt,$rk,$start,$end);
          $gt_rt_c = get_person_route_prev_ground_course_count($flt,$rk,$start,$end);
          $var2 = count($gt_rt) + count($gt_q) + $gt_q_c + $gt_rt_c;
          //MUTATION

          //RETIRED 65 YRESIGN/OUT
          $end_gt = get_person_end_date_gt($flt,$rk,$start,$end);
          $var3 = count($end_gt);
        }
        $data['add'] = $var1;
        $data['mov'] = $var2;
        $data['out'] = $var3;
        return $data;
    }




}
