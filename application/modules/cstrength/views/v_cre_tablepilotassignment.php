<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/css/fixedColumns.bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> Count of Duties <?php echo $fleet."'s "; if ($rank=="CP"){echo "Captain";} else {echo "First Officer";}
			$dt_pr=new DateTime($date_from); echo ": ".$dt_pr->format('d-M-y'); ?> <?php if ($date_from<>$date_to) {$dt_prt=new DateTime($date_to); echo 'to ' .$dt_prt->format('d-M-y');} ?> </span>
        </div>
    </div>

    <div class="portlet-body">
        <div class="tools"> </div>
		<table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="datatable">
            <thead >
			    <tr style="font-weight:bold; background-color: rgb(233,236,243); color: rgb(60, 60, 60);">
					<th class="tengah" style="background-color:#DCDCDC;"> </th>
					<th class="tengah" style="background-color:#DCDCDC;"> Duty Codes </th>
					<?php
					$date_pr = new DateTime ($date_from);
					for ($i=0; $i<=$day; $i++){
					?>
					<th class="tengah"> <?php echo $date_pr->format('D').", ".$date_pr->format('d-M-y'); $date_pr = $date_pr->modify('+1 day'); ?> </th>
					<?php
					} ?>
				</tr>
            </thead>

            <tbody>
				<?php

				foreach ($dailyassignment as $key => $value){
				?>
				<tr class="tengah">
					<td> &#9725; </td>
					<td> <?php echo $key; ?> </td>
					<?php
					//$date_matrix = $date_from;
					$date_matrix = new DateTime ($date_from);
					for ($i=0; $i<=$day; $i++){
					if (!isset( $dailyassignment[$key][$date_matrix->format('Y-m-d')] )){
						$dailyassignment[$key][$date_matrix->format('Y-m-d')] = null;
					?> <td> - </td>
					<?php
					} else {
					?>

					<?//php echo $key; ?> <?php //echo $date_matrix->format('Y-m-d');?> <?php //echo $rank;?> <?php //echo $fleet;?> <?php //echo $status;?>
					<td><a class="view" data-target="#view" data-toggle="modal" id="<?php echo $key." ".$date_matrix->format('Y-m-d')." ".$rank." ".$fleet." ".$status;?>"><?php echo $dailyassignment[$key][$date_matrix->format('Y-m-d')];?></a></td>
					<?php
					}
					$date_matrix = $date_matrix->modify('+1 day');
					} ?>
				</tr>

				<?php
				}?>
				<tr class="tengah" style="font-weight:bold; background-color:rgb(233,236,243); color: rgb(60, 60, 60); border-bottom-color: white;">
					<td></td>
					<td style="letter-spacing: 5px;"> Total </td>
					<?php
					foreach ($total as $key => $value) {
					?>
					<td> <?php echo $value; ?> </td>
					<?php
					}
					?>
				</tr>
			</tbody>
		</table>
	</div>
</div>


<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Who were Assigned</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">

        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
		var nDay = <?php echo $day; ?>;
		if (nDay > 7) {
			$('#datatable').DataTable({
				dom: 'Bfrtip',
				pagingType: 'full_numbers',
				ordering: false,
				lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
				buttons: ['copy', 'excel', 'pdf'],
				scrollX: true,
				scrollCollapse: true
			});
		} else {
			$('#datatable').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            ordering: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: ['copy', 'excel', 'pdf'],
			scrollX: false,
            scrollCollapse: true
			});
		}
    });
</script>
<script type="text/javascript">
jQuery(document).ready(function() {
    $("#datatable").on('click', '.view', function(){
		var id = this.id.split(" ");
        //var kata2 = id.split(":");
        var target_url = "<?php echo base_url().'cstrength/detail_who_are_assigned/';?>"+id[0]+"/"+id[1]+"/"+id[2]+"/"+id[3]+"/"+id[4];
    	var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
        //ajaxStart: function(){iframe.attr("src", "loading...");},
        //ajaxStop: function(){iframe.attr("src", target_url);}


    });
});
</script>
