<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
    <style type="text/css">
        .kotak {
            margin: auto;
            background-color: white;
            width: 100%;
            padding: 10px 10px 10px 10px;
        }
        #judul {
            text-align: center;
            font-family: Corbel;
            font-size: 24;
        }
        thead {
          white-space: nowrap;
          background-color: #DCDCDC;
          text-align: center;
          vertical-align:middle;
        }
        td {
          white-space: nowrap;
          border-color: white;
          border: 1px solid;
        }
        th {
          text-align: center;
          vertical-align:middle;
        }
        #tabledata2 {
          margin: 0 auto;
        }
        th input { width: 90%;
        }
        tr:nth-child(odd) td {background-color: #F8F8FF;border-color: white;
        }
        tr:nth-child(even) td {background-color:  #E6E6FA;border-color: white;
        }
        td.tengah, th.tengah {
            text-align: center;
            vertical-align:middle;
        }
    </style>
<!--css end-->

<!--FLIGHT NUMBER-->
<!--table flight number begin-->
    <div class="kotak">
            <div id="judul" style="font-size:24pt;"> <b><?php echo $rank." ".$fleet."'s movement"; ?></b> </div>
        <!--<h3 id="judul">  </h3>-->

        <table id="tabledata2">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Movement</th>
                    <th>Number of Crew</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Qualification Status</th>
                    <th>Training Name</th>
                    <th>Batch</th>
                    <th>Start Training</th>
                    <th>Start Route</th>
                    <th>End Training</th>
                    <th>Employee End Date</th>
      	        </tr>
            </thead>
            <tbody>
				        <?php
                $i=1;
                foreach ($mov as $key => $value){
				        ?>

                <tr>
                    <td> <?php echo $i; $i++;?> </td>
                    <td class="tengah"> <?php echo $value['date'];?> </td>
                    <td> <?php echo $value['movement'];?></td>
                    <td class="tengah"> <?php echo $value['count_of_movement'];?></td>
                    <td class="tengah"> <?php (isset($value['id']) ? print_r($value['id']) : print_r('') ); ?> </td>
                    <td> <?php (isset($value['name']) ? print_r($value['name']) : print_r('') ); ?></td>
                    <td class="tengah"> <?php (isset($value['qualstat']) ? print_r($value['qualstat']) : print_r('') ); ?></td>
                    <td> <?php echo $value['tr_name'];?></td>
                    <td> <?php echo $value['batch_name'];?></td>
                    <td> <?php (isset($value['start_training']) && $value['start_training'] <> '0000-00-00' ? print_r($value['start_training']) : print_r('') );?></td>
                    <td> <?php (isset($value['start_route']) && $value['start_route'] <> '0000-00-00' ? print_r($value['start_route']) : print_r('') ); ?></td>
                    <td> <?php (isset($value['end_training']) && $value['end_training'] <> '0000-00-00' ? print_r($value['end_training']) : print_r('') ); ?></td>
                    <td> <?php (isset($value['emp_end_date']) && $value['emp_end_date'] <> '0000-00-00' ? print_r($value['emp_end_date']) : print_r('') ); ?></td>
                </tr>
                <?php }
				        ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Movement</th>
                    <th>Number of Crew</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Qualification Status</th>
                    <th>Training Name</th>
                    <th>Batch</th>
                    <th>Start Training</th>
                    <th>Start Route</th>
                    <th>End Training</th>
                    <th>Employee End Date</th>
      	        </tr>
            </tfoot>
        </table>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tabledata2 tfoot th').each( function (i) {
          var title = $('#tabledata2 tfoot th').eq( $(this).index() ).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
        } );

        var table = $('#tabledata2').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true,
            scrollY: true,
        });

        $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
        } );
    });
</script>
