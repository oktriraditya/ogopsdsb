<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/css/fixedColumns.bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
th, td {
  white-space: nowrap;
}

/div.dataTables_wrapper {
  margin: 0 auto;
}

th input {
  width: 90%;
}

.tengah {
  text-align: center;
  vertical-align:middle;
}
#view {
  width:35%;
}
.kotak {
    margin: auto;
    background-color: white;
    width: 100%;
    padding: 10px 10px 10px 10px;
}
#judul {
    text-align: center;
    font-family: Corbel;
    font-size: 24;
}
thead {
  white-space: nowrap;
  background-color: #DCDCDC;
  text-align: center;
  vertical-align:middle;
}
td {
  white-space: nowrap;
  border-color: white;
  border: 1px solid;
}
th {
  text-align: center;
  vertical-align:middle;
}
#tabledata2 {
  margin: 0 auto;
}
th input { width: 90%;
}
tr:nth-child(odd) td {background-color: #F8F8FF;border-color: white;
}
tr:nth-child(even) td {background-color:  #E6E6FA;border-color: white;
}
td.tengah, th.tengah {
    text-align: center;
    vertical-align: middle;
}

</style>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-dark">
      <i class="fa fa-th-list font-dark"></i>
      <span class="caption-subject bold uppercase"> List of Type Qualification Trainees </span>
    </div>
  </div>
  <div class="portlet-body">
    <div class="tools"></div>
    <button id="tr" class="btn blue BtnEdit" > &#9998; Tr. Program</button>
    <button id="rt" class="btn blue BtnEdit" > &#9998; Route Training Date </button>
    <button id="ql" class="btn blue BtnEdit" > &#9998; Est. Qualified Date</button>
    <table class="table table-striped table-hover table-condensed table-checkable" valign="middle" id="datatable">
      <thead >
        <tr style="font-weight:bold; background-color: rgb(233,236,243); color: rgb(60, 60, 60);">
          <th> </th>
          <th> Emp.ID </th>
          <th> Name </th>
          <th> Rank </th>
          <th> Training Program </th>
          <th> Batch Name </th>
          <th> Start Ground </th>
          <th> Start Route </th>
          <th> Qualified </th>
          <th> Training Duration </th>
          <th> Training Status </th>
        </tr>
      </thead>
      <tbody>
        <?php
        $progress=0;
        $today = date('Y-m-d');
        $batch = 0; $training = 0; $route = 0; $qual = 0;
        foreach ($traineelist as $key => $value){
          $startdate = new DateTime ($value['STARTTRAINING']);
          $enddate = new DateTime ($today);
          //KOLOM TRAINING PROGRAM & TRAINING BATCH

          ( isset($value['batch_name']) ?  $batch = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr"> &#9998; </a> '.$value['batch_name'] : $batch = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr">&#9998; </a><a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr"> please fill </a> ' );
          ( isset($value['tr_name']) ?  $training = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr"> &#9998; </a> '.$value['tr_name'] : $training = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr">&#9998; </a><a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr"> please fill </a> ' );

          //KOLOM ROUTE TR, QUALIFIED, PROGRESS, & STATUS
          // if ($value['QUALSTAT'] == "GT") {
          //   //Jika sedang ground training
          //   $progress = "in ground training";
          //   $enddate = new DateTime ($today);
          //   (isset($value['route_est']) ? $route = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt"> &#9998; </a> '.$value['route_est'] : $route = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt">&#9998; </a><a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt"> please fill </a> ' );
          //   (isset($value['qual_est']) ? $qual = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> &#9998; </a> '.$value['qual_est'] : $qual = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql">&#9998; </a><a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> please fill </a> '   );
          //
          // } elseif ($value['QUALSTAT'] == "RT") {
          //   //Jika sedang route training
          //   $progress = "in route training";
          //   $enddate = new DateTime ($today);
          //   $route = $value['STARTROUTETRAINING'];
          //   (isset($value['qual_est']) ? $qual = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> &#9998; </a> '.$value['qual_est'] : $qual = '<a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql">&#9998; </a><a class="DEdit" data-target="#view" data-toggle="modal" id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> please fill </a> '   );
          //
          // } else {
          //   //Jika qualified
          //   $progress = "qualified / ended";
          //   $enddate = new DateTime ($value['ENDTRAINING']);
          //   $route = $value['STARTROUTETRAINING'];
          //   $qual = $value['ENDTRAINING'];
          // }

          if ($value['QUALSTAT'] == "GT") {
            //Jika sedang ground training
            $progress = "in ground training";
            $enddate = new DateTime ($today);
            (isset($value['route_est']) ? $route = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt"> &#9998; </a> '.$value['route_est'] : $route = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt">&#9998; </a><a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/rt"> please fill </a> ' );
            (isset($value['qual_est']) ? $qual = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> &#9998; </a> '.$value['qual_est'] : $qual = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql">&#9998; </a><a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> please fill </a> '   );

          } elseif ($value['QUALSTAT'] == "RT") {
            //Jika sedang route training
            $progress = "in route training";
            $enddate = new DateTime ($today);
            $route = $value['STARTROUTETRAINING'];
            (isset($value['qual_est']) ? $qual = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> &#9998; </a> '.$value['qual_est'] : $qual = '<a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql">&#9998; </a><a  id="'.$value['EMP_ID'].'/'.$value['STARTTRAINING'].'/ql"> please fill </a> '   );

          } else {
            //Jika qualified
            $progress = "qualified / ended";
            $enddate = new DateTime ($value['ENDTRAINING']);
            $route = $value['STARTROUTETRAINING'];
            $qual = $value['ENDTRAINING'];
          }

          $interval = $enddate->diff($startdate);
          $program_batch = get_tr_program_batch($fleet);
          ?>
          <tr>
            <td class="tengah"> </td>
            <td class="tengah"> <?php echo $value['EMP_ID']; ?> </td>
            <td> <?php echo $value['FULLNAME']; ?> </td>
            <td class="tengah"> <?php echo $value['RANK_']; ?> </td>
            <td class="tengah" id="<?php echo $value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tr/td'; ?>"> <?php echo $training; ?>  </td>
            <td class="tengah" id="<?php echo $value['EMP_ID'].'/'.$value['STARTTRAINING'].'/tb/td'; ?>"> <?php echo $batch; ?>  </td>
            <td class="tengah"> <?php echo $value['STARTTRAINING']; ?> </td>
            <td class="tengah" id="<?php echo $value['EMP_ID'].'/'.$value['STARTTRAINING'];?>/rt/td"> <?php echo $route;?> </td>
            <td class="tengah" id="<?php echo $value['EMP_ID']."/".$value['STARTTRAINING'];?>/ql/td"> <?php echo $qual; ?> </td>
            <td class="tengah"> <?php echo ($interval->m)+ (($interval->y)*12)." month(s), ".$interval->d." day(s)"; ?> </td>
            <td class="tengah"> <?php echo $progress; ?> </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
      <tfoot>
        <tr>
          <th> </th>
          <th> Emp.ID </th>
          <th> Name </th>
          <th> Rank </th>
          <th> Training Program </th>
          <th> Batch Name </th>
          <th> Start Ground </th>
          <th> Start Route </th>
          <th> Qualified </th>
          <th> Training Duration </th>
          <th> Training Status </th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


<div id="view" class="modal modal-dialog-centered" tabindex="-1" style="margin:auto;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title" id="modal-title">Update...</h4>
  </div>
  <div class="modal-body" style="height:190px !important;">
    <form id="rendered-form" action="#">
        <div class="rendered-form">
          <input type="hidden" class="form-control" name="btn" value="about:blank" id="formbtn">
          <div class="formList">
            <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-default red" type="button" style="width:100px;">
                        <span class="">ID & Start Tr.</span>
                    </button>
                </span>
                <div class="input-group input-large">
                  <input type="text" class="form-control" name="id_view"  value="about:blank" id="listid" disabled>
                </div>
                <div class="input-group input-large">
                    <input type="hidden" class="form-control" name="id_start"  value="about:blank" id="id_start">
                </div>
          </div>
        </div>
        <br>
        <div class="formBatch">
          <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-default red" type="button" style="width:100px;">
                    <span class="">Batch & Prg.</span>
                </button>
            </span>
            <div class="input-group input-large">
              <select class="form-control" name="batch_program" id="idbatch">
                  <?php foreach ($program_batch as $key => $row) { ?>
                      <option value="<?php echo $row['batch_id'] . "/" . $row['batch_name'] . "/" . $row['tr_name']; ?>">
                          <?php
                          echo "<b>" . $row['batch_name'] . "</b> , Program: <b>" . $row['tr_name'] . "</b>";
                          ?>
                      </option>
                  <?php }
                  ?>
              </select>
            </div>
          </div>
        </div>
        <div class="formRoute">
            <div class="input-group">
                <span class="input-group-btn">
                    <button class="btn btn-default red" type="button" style="width:100px;">
                        <span class="">Route Tr.</span>
                    </button>
                </span>
                <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
                    <input type="text" style="text-align: left;" class="form-control" name="routedate" id="idroute" value="please fill...">
                </div>
            </div>
        </div>
        <div class="formQualified">
          <div class="input-group">
              <span class="input-group-btn">
                  <button class="btn btn-default red" type="button" style="width:100px;">
                      <span class="">Qualified</span>
                  </button>
              </span>
              <div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
                  <input type="text" style="text-align: left;" class="form-control" name="qualdate" id="idqual" value="please fill...">
              </div>
          </div>
        </div>
        <br>
          <!--<div class="fb-button form-group field-button-1548149560109">
            <button class="btn blue" id="submit01" type="button">Submit</button>
          </div>-->
        <div class="modal-footer">
          <button type="button" class="btn btn-outline dark" id="trsubmit">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
  $('#datatable tfoot th').each( function (i) {
    var title = $('#datatable tfoot th').eq( $(this).index() ).text();
    $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
  } );

  $("#msgstatus").hide();
  $('.formList').hide();
  $('.formBatch').hide();
  $('.formRoute').hide();
  $('.formQualified').hide();

  var table = $('#datatable').DataTable({
    dom: 'Bfrtip',
    pagingType: 'full_numbers',
    //ordering: true,
    bsort: true,
    lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
    pageLength: 10,
    buttons: ['selectAll', 'selectNone', 'copy'],
    columnDefs: [{orderable: false, className: 'select-checkbox', targets: 0}],
    select: {style: 'multi', selector: 'td:first-child'},
    order: [[6, 'asc']],
    //orderCellsTop: true,
    //fixedColumns: 2,
    scrollY: true,
    scrollX: true,
    //scrollCollapse: true,
    //fixedHeader: {
      //header: true,
    //}
    //paging: false,
  });

  $( table.table().container() ).on( 'keyup', 'tfoot input', function () {
    table
    .column( $(this).data('index') )
    .search( this.value )
    .draw();
  });

  $('.close').on('click',function(){
    $("#msgstatus").hide();
    $('.formBatch').hide();
    $('.formRoute').hide();
    $('.formQualified').hide();
  });

  $('.BtnEdit').on('click', function () {
      var tblData = table.rows('.selected').data();
      //alert (tblData.length);
      if (tblData.length <= 0) {
          alert("Please, select minimum 1 record");
      } else {
          var buttontype = $(this).attr("id");
          var flt = "<?php echo $fleet; ?>";

          var get_list1 = "";
          var get_list2 = "";
          $.each(tblData, function (i, val) {
              get_list1 += "{" + tblData[i][1] + "/" + tblData[i][6] + "} "; //ID & Start Ground;
              get_list2 += tblData[i][1] + "," + tblData[i][6] + ",";
          });

          //var target_url = "<?php //echo base_url() . 'cre/edit_trainees/'; ?>" + buttontype + "/" + flt + get_url;
          //alert (target_url);
          //var target_url = str.replace(" ", "_");
          $('#formbtn').attr("value", buttontype);
          $('#listid').attr("value", get_list1);
          $('#id_start').attr("value", get_list2);
          $('.formList').show();
          switch (buttontype) {
              case 'tr':
                  $('.formBatch').show();
                  document.getElementById("modal-title").innerHTML = "Update Tr. Program & Batch";
                  break;

              case 'rt':
                  $('.formRoute').show();
                  document.getElementById("modal-title").innerHTML = "Update Route Tr. Date Estimation ";
                  break;

              case 'ql':
                  $('.formQualified').show();
                  document.getElementById("modal-title").innerHTML = "Update Qualified Date Estimation ";
                  break;
          }
          //  iframe.attr("src", target_url);
          $("#view").modal("show");
      }
  });

  $('#trsubmit').click(function () {
      //blockUI(boxTable);
      $.ajax({
          url: "<?php echo base_url().'cstrength/update_trainee/'; ?>",
          type: 'POST',
          cache: false,
          data: $('.form-control').serialize(),
          success: function (response) {
              var listid = $("#id_start").val();
              var btn =  $('#formbtn').val();
              var chg = 0; var chg2 = 0;
              if (btn == "ql") {
                chg = $("#idqual").val();
              } else if (btn == "rt") {
                chg = $("#idroute").val();
              } else if (btn == "tr") {
                chg2 = $("#idbatch").val();
                chg = chg2.split("/");
              }
              list_id = listid.split(",");
              var ln = list_id.length;
              var idLoc = "";
              var lnk = "";
              $.each(list_id, function (i, val) {
                  if ((i%2 == 0) && (ln-3 >= i) )  {
                    if ((btn == "ql") || (btn == "rt")) {
                      idLoc = list_id[i] + "/" + list_id[i+1] + "/" + btn + "/td";
                      lnk = '<a id="' + list_id[i] + '/' + list_id[i+1] + '/' + btn + '" > &#9998; </a> ' + chg ;
                      //lnk = '<a> &#9998; </a> ' + chg ;
                      document.getElementById(idLoc).innerHTML = lnk;
                      document.getElementById(idLoc).style.backgroundColor = "#2ED1A2";
                    } else if (btn == "tr") {
                      idLoc = list_id[i] + "/" + list_id[i+1] + "/tr/td";
                      lnk = '<a id="' + list_id[i] + '/' + list_id[i+1] + '/' + btn + '" > &#9998; </a> ' + chg[2] ;
                      //lnk = '<a> &#9998; </a> ' + chg[2] ;
                      document.getElementById(idLoc).innerHTML = lnk;
                      document.getElementById(idLoc).style.backgroundColor = "#2ED1A2";
                      idLoc = list_id[i] + "/" + list_id[i+1] + "/tb/td";
                      lnk = '<a id="' + list_id[i] + '/' + list_id[i+1] + '/' + btn + '" > &#9998; </a> ' + chg[1] ;
                      //lnk = '<a> &#9998; </a> ' + chg[1] ;
                      document.getElementById(idLoc).innerHTML = lnk;
                      document.getElementById(idLoc).style.backgroundColor = "#2ED1A2";
                    }
                  }
              });
              alert("trainee(s)'s attribute updated");
              $("#view").modal("hide");
              $('.formBatch').hide();
              $('.formRoute').hide();
              $('.formQualified').hide();
          },
          error: function () {
              alert(error.message());
              $("#view").modal("hide");
              $('.formBatch').hide();
              $('.formRoute').hide();
              $('.formQualified').hide();
          }
      });
    });

  $('.DEdit').on('click', function () {
          var idents = $(this).attr("id");
          var flt = "<?php echo $fleet; ?>";
          var ident = idents.split("/");
          var buttontype = ident[2];
          var get_list1 = "{" + ident[0] + "/" + ident[1] + "} "; //ID & Start Ground;
          var get_list2 = ident[0] + "," + ident[1] + ",";
          $('#formbtn').attr("value", buttontype);
          $('#listid').attr("value", get_list1);
          $('#id_start').attr("value", get_list2);
          $('.formList').show();
          switch (buttontype) {
              case 'tr':
                  $('.formBatch').show();
                  document.getElementById("modal-title").innerHTML = "Update Tr. Program & Batch";
                  break;

              case 'rt':
                  $('.formRoute').show();
                  document.getElementById("modal-title").innerHTML = "Update Route Tr. Date Estimation ";
                  break;

              case 'ql':
                  $('.formQualified').show();
                  document.getElementById("modal-title").innerHTML = "Update Qualified Date Estimation ";
                  break;
          }
          //$("#view").modal("show");
  });


});

</script>
