<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">

function loadCrewAmount() {
    alert("test");
    blockUI(boxFdata);
    var sDate = $("#sDate").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cstrength/crewAmount",
        data    : {"sDate": sDate}
        success :
            function(msg) {
                $("#boxFdata").html(msg);
                unblockUI(boxFdata);
            },
            error   : function() {
                $("#boxFdata").html("<div class='alert alert-error'>No Data</div>");
                unblockUI(boxFdata);
            }
    });
}


jQuery(document).ready(function() {

    loadCrewAmount();
    $('#submit').click(function(){
    });

});
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-male"></i> &nbsp; <i class="fa fa-plane"></i>
                    <span>  </span>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <!--
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="icon-calendar"></i>&nbsp;
                    <span> Last Sync: XXXX</span>
                </div>-->
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+1y">
                                <input type="text" class="form-control" value="<?php echo date('Y-m-d');?>" name="date" id="sDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light bordered" id="FStats">
                  <div id="boxFdata"></div>
                </div>
            </div>
        </div>


        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="view" class="modal modal-dialog-centered container" tabindex="-1" style="margin:auto;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title" id="modal-title"></h4>
  </div>
  <div class="modal-body" style="height:450px !important;">
    <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">

    </iframe>
  </div>
</div>
<!-- END CONTENT
