<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}
/*
function loadcrewlist(sFleet,rk){
  //var sFleet = $("#sFleet").val();
  //var rk = "CP";
  var target_url = "<?php //echo base_url().'cstrength/crewlist/';?>"+sFleet+"/"+rk;
  //alert(target_url);
  var iframe = $("#viewFrame");
  iframe.attr("src", target_url);
  //ajaxStart: function(){iframe.attr("src", "loading...");},
  //ajaxStop: function(){iframe.attr("src", target_url);}
}*/

function loadFdata(sFleet,sRank,sDiv,sDate) {
    blockUI(sDiv);
    //var sDate = $("#sDate").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cstrength/tableCrewAmount",
        data    : {"sDate": sDate, "sFleet": sFleet, "sRank": sRank},
        success :
            function(msg) {
                $("#"+sDiv).html(msg);
                unblockUI(sDiv);
            },
        error   : function(){
                unblockUI(sDiv);
                $("#"+sDiv).html("<div class='alert alert-error'>No Data</div>");
        }
    });
}
/*
function loadFMovement(sFleet,sRank,sDiv,sStart,sEnd) {
    blockUI(sDiv);
    //var sDate = $("#sDate").val();

    $.ajax({
        type    : "GET",
        url     : "<?php //echo base_url(); ?>cstrength/tableMovement",
        data    : {"sDate": sDate, "sFleet": sFleet, "sRank": sRank, "sStart": sStart, "sEnd": sEnd},
        success :
            function(msg) {
                $("#"+sDiv).html(msg);
                unblockUI(sDiv);
            },
        error   : function(){
                unblockUI(sDiv);
                $("#"+sDiv).html("<div class='alert alert-error'>No Data</div>");
        }
    });
}*/

jQuery(document).ready(function() {
      sFleet = $("#sFleet").val();
      sDate = $("#sDate").val();
      sTitle = "";
      today = new Date();
      today_date = today.getFullYear()+'-'+String(today.getMonth() + 1).padStart(2, '0')+'-'+String(today.getDate()).padStart(2, '0');
      if (sFleet == "All Fleets") sTitle = sFleet ; else sTitle = "Fleet "+sFleet;
      $(".divFlt").html(sTitle);
      $(".dt_day").html(today_date);
      $(".dt_end").html(sDate);
      loadFdata('All Fleets','All Ranks','boxFdataall',sDate);
      loadFdata('All Fleets','CP','boxFdatacp',sDate);
      loadFdata('All Fleets','FO','boxFdatafo',sDate);

    $('#submit').click(function(){
          sFleet = $("#sFleet").val();
          sDate = $("#sDate").val();
          if (sFleet == "All Fleets") sTitle = sFleet ; else sTitle = "Fleet "+sFleet;
          $(".divFlt").html(sTitle);
          loadFdata(sFleet,'All Ranks','boxFdataall',sDate);
          loadFdata(sFleet,'CP','boxFdatacp',sDate);
          loadFdata(sFleet,'FO','boxFdatafo',sDate);
    });
});
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><?php echo $title;?>
                    <small><?php echo $title_small;?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <div class="page-toolbar">
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="fa fa-plane"></i>
                </div>
                <div class="pull-right tooltips btn btn-fit-height green">
                    <i class="icon-calendar"></i>&nbsp;
                    <span id="date_time"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </div>
                <!--
                <div class="pull-right tooltips btn btn-fit-height red">
                    <i class="icon-calendar"></i>&nbsp;
                    <span> Last Sync: XXXX</span>
                </div>-->
            </div>
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">&nbsp;</div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Fleet</span>
                                </button>
                            </span>
                            <select class="form-control selectStn" name="fleet" id="sFleet">
                                <option value="All Fleets"><?php echo 'ALL';?></option>
                                <?php foreach ($fleet as $key => $row) {?>
                                <option value="<?php echo $row['fleet'];?>"><?php echo $row['fleet'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10 margin-right-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default red" type="button">
                                    <span class="">Date</span>
                                </button>
                            </span>
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0" data-date-end-date="+5y">
                                <input type="text" class="form-control" value="<?php echo date('Y-m-d');?>" name="date" id="sDate">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body pull-left">
                    <div class="form-inline margin-bottom-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
              <div style="font-size:18px;color:#666666;">
                  <div class="caption">
                    <span class="caption-subject bold uppercase font-light" > <b class="divFlt"></b> - Total All Captain & First Officer: </span>
                  </div>
              </div>
            </div>
          </div>
          <!-- END PAGE BASE CONTENT -->
      </div>

      <div class="row">
          <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet bordered" id="FStats">
              <div id="boxFdataall"></div>
            </div>
          </div>
          <!-- END PAGE BASE CONTENT -->
      </div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
          <div class="portlet light bordered" id="FStats">
            <div style="font-size:18px;color:#666666;">
                <div class="caption">
                  <span class="caption-subject bold uppercase font-light" > <b class="divFlt"></b> - Total Per Rank: </span>
                </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
          <div class="portlet bordered" id="FStats">
            <div id="boxFdatacp"></div>
            <div id="boxFdatafo"></div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>

<!-- END CONTENT
