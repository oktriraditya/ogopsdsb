<?php

$rk = 0;
$rk_id = $sRank;
if($sRank == 'CP') {
  $rk = "Captain";
} else if ($sRank == 'FO') {
  $rk = "First Officer";
} else {
  $rk = "Captain & FO";
  $sRank = "CP & FO";
  $rk_id = "ALL";
}
?>

<script type="text/javascript">
function get_movement(sEnd,sFleet,sRk){
  var target_url = "<?php echo base_url().'cstrength/boxMovement/'; ?>"+sFleet+"/"+sRk+"/"+sEnd;
  var iframe = $("#viewFrame");
  iframe.attr("src", target_url);
}

function get_crewlist(sFleet,sRk){
  var target_url = "<?php echo base_url().'cstrength/crewlist/'; ?>"+sFleet+"/"+sRk;
  var iframe = $("#viewFrame");
  iframe.attr("src", target_url);
}

jQuery(document).ready(function() {
  sFlt = '<?php echo $sFleet; ?>';
  if (sFlt == 'All Fleets') sFlt = 'ALL';
  var sRk = [];
  sRk["<?php echo $rk_id; ?>"] = '<?php echo $rk_id; ?>';
  //alert(sRk["ALL"]+sRk["CP"]);
  sEnd = '<?php echo $sDate; ?>';

  $('#getMovement-q-'+sRk["ALL"]).click(function() {
    get_movement(sEnd,sFlt,sRk["ALL"]);
  });
  $('#getMovement-rt-'+sRk["ALL"]).click(function() {
    get_movement(sEnd,sFlt,sRk["ALL"]);
  });
  $('#getMovement-gt-'+sRk["ALL"]).click(function() {
    get_movement(sEnd,sFlt,sRk["ALL"]);
  });
  $('#getMovement-all-'+sRk["ALL"]).click(function() {
    get_movement(sEnd,sFlt,sRk["ALL"]);
  });
  $('#getList-q-'+sRk["ALL"]).click(function() {
    get_crewlist(sFlt,sRk["ALL"]);
  });
  $('#getList-rt-'+sRk["ALL"]).click(function() {
    get_crewlist(sFlt,sRk["ALL"]);
  });
  $('#getList-gt-'+sRk["ALL"]).click(function() {
    get_crewlist(sFlt,sRk["ALL"]);
  });
  $('#getList-all-'+sRk["ALL"]).click(function() {
    get_crewlist(sFlt,sRk["ALL"]);
  });


  $('#getMovement-q-'+sRk["CP"]).click(function() {
    get_movement(sEnd,sFlt,sRk["CP"]);
  });
  $('#getMovement-rt-'+sRk["CP"]).click(function() {
    get_movement(sEnd,sFlt,sRk["CP"]);
  });
  $('#getMovement-gt-'+sRk["CP"]).click(function() {
    get_movement(sEnd,sFlt,sRk["CP"]);
  });
  $('#getMovement-all-'+sRk["CP"]).click(function() {
    get_movement(sEnd,sFlt,sRk["CP"]);
  });
  $('#getList-q-'+sRk["CP"]).click(function() {
    get_crewlist(sFlt,sRk["CP"]);
  });
  $('#getList-rt-'+sRk["CP"]).click(function() {
    get_crewlist(sFlt,sRk["CP"]);
  });
  $('#getList-gt-'+sRk["CP"]).click(function() {
    get_crewlist(sFlt,sRk["CP"]);
  });
  $('#getList-all-'+sRk["CP"]).click(function() {
    get_crewlist(sFlt,sRk["CP"]);
  });

  $('#getMovement-q-'+sRk["FO"]).click(function() {
    get_movement(sEnd,sFlt,sRk["FO"]);
  });
  $('#getMovement-rt-'+sRk["FO"]).click(function() {
    get_movement(sEnd,sFlt,sRk["FO"]);
  });
  $('#getMovement-gt-'+sRk["FO"]).click(function() {
    get_movement(sEnd,sFlt,sRk["FO"]);
  });
  $('#getMovement-all-'+sRk["FO"]).click(function() {
    get_movement(sEnd,sFlt,sRk["FO"]);
  });
  $('#getList-q-'+sRk["FO"]).click(function() {
    get_crewlist(sFlt,sRk["FO"]);
  });
  $('#getList-rt-'+sRk["FO"]).click(function() {
    get_crewlist(sFlt,sRk["FO"]);
  });
  $('#getList-gt-'+sRk["FO"]).click(function() {
    get_crewlist(sFlt,sRk["FO"]);
  });
  $('#getList-all-'+sRk["FO"]).click(function() {
    get_crewlist(sFlt,sRk["FO"]);
  });
});

</script>


    <div class="portlet-body">
      <div class="row widget-row">
        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"> Amount of Qualified <?php echo $sFleet."'s ".$rk; ?></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-blue-soft fa fa-plane";></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $CS[0]['Q']; ?>"> <?php echo $CS[0]['Q']; ?> <span style="font-size:20px;font-weight: bold;"> <?php echo $sRank; ?> </span> </span>
                <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
            <div class='widget-thumb-heading pull-right'>
               <?php if (date("Y-m-d") != $sDate && $rk_id != "ALL" && $sFleet != "All Fleets") {?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getMovement-q-<?php echo $rk_id;?>'> Movement
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
              <?php }?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getList-q-<?php echo $rk_id;?>'>Crew List
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
            </div>
          </div>
          <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Amount of Route Trainee <?php echo $sFleet."'s ".$rk; ?></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-green-jungle fa fa-plane"></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $CS[0]['RT']; ?>"> <?php echo $CS[0]['RT']; ?> <span style="font-size:20px;font-weight: bold;"> <?php echo $sRank; ?> </span> </span>
                <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
            <div class='widget-thumb-heading pull-right'>
               <?php if (date("Y-m-d") != $sDate && $rk_id != "ALL" && $sFleet != "All Fleets") {?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getMovement-rt-<?php echo $rk_id;?>'>Movement
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
              <?php }?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getList-rt-<?php echo $rk_id;?>'>Crew List
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
            </div>
          </div>
          <!-- END WIDGET THUMB -->
        </div>

        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Amount of Ground Trainee <?php echo $sFleet."'s ".$rk; ?></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-yellow-soft fa fa-leanpub"></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $CS[0]['GT']; ?>"> <?php echo $CS[0]['GT']; ?> <span style="font-size:20px;font-weight: bold;"> <?php echo $sRank; ?> </span> </span>
                <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
            <div class='widget-thumb-heading pull-right'>
               <?php if (date("Y-m-d") != $sDate && $rk_id != "ALL" && $sFleet != "All Fleets") {?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getMovement-gt-<?php echo $rk_id;?>'>Movement
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
              <?php }?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getList-gt-<?php echo $rk_id;?>'>Crew List
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
            </div>
          </div>
          <!-- END WIDGET THUMB -->
        </div>

        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Total Amount of <?php echo $sFleet."'s ".$rk; ?></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-red-haze fa fa-users"></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $CS[0]['Q']+$CS[0]['RT']+$CS[0]['GT']; ?>"> <?php echo $CS[0]['Q']+$CS[0]['RT']+$CS[0]['GT']; ?> <span style="font-size:20px;font-weight: bold;"> <?php echo $sRank; ?> </span> </span>
                <span class="widget-thumb-subtitle">on <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
            <div class='widget-thumb-heading pull-right'>
              <?php if (date("Y-m-d") != $sDate && $rk_id != "ALL" && $sFleet != "All Fleets") {?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getMovement-all-<?php echo $rk_id;?>'>Movement
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
              <?php }?>
                <a class='btn yellow btn-circle btn-sm' data-target='#view' data-toggle='modal'  id='getList-all-<?php echo $rk_id;?>'>Crew List
                  <i class='m-icon-swapright m-icon-white'></i>
                </a>
            </div>
          </div>
          <!-- END WIDGET THUMB -->
        </div>
        <div id="view" class="modal modal-dialog-centered container"  tabindex="-1" style="margin:auto;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" id="modal-title"></h4>
          </div>
          <div class="modal-body" style="height:450px !important;">
            <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
            </iframe>
          </div>
        </div>
      </div>
    </div>
