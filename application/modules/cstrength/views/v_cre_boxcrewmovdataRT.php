<script type="text/javascript">

jQuery(document).ready(function() {
      var stat = "<?php echo $sStat; ?>";
      $("#val01rt").html("<?php echo "-".$mov['mov']; ?>");
      $("#val02rt").html("<?php echo "+".$mov['add']; ?>");
      $("#val03rt").html("<?php echo "-".$mov['out']; ?>");
      $("#head01rt").html("Finish Route Training");
      $("#head02rt").html("New Route Training");
      $("#head03rt").html("Retired 65Y/Resign/Out");
});


</script>

    <div class="portlet-body">
      <div class="row widget-row">
        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"> <div id="head01rt"></div></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-blue-soft fa fa-plane";></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value=""> <div id="val01rt"></div> </span>
                <span class="widget-thumb-subtitle">until <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
          </div>
          <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"> <div id="head02rt"></div></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-green-jungle fa fa-plane";></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value=""> <div id="val02rt"></div> </span>
                <span class="widget-thumb-subtitle">until <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
          </div>
          <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
          <!-- BEGIN WIDGET THUMB -->
          <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading"> <div id="head03rt"></div></h4>
            <div class="widget-thumb-wrap">
              <i class="widget-thumb-icon bg-red-haze fa fa-times-circle";></i>
              <div class="widget-thumb-body">
                <span class="widget-thumb-body-stat" data-counter="counterup" data-value=""> <div id="val03rt"></div> </span>
                <span class="widget-thumb-subtitle">until <?php echo date('d F Y', strtotime($sDate));?></span>
              </div>
            </div>
            <br/>
          </div>
          <!-- END WIDGET THUMB -->
        </div>

        <div id="view" class="modal modal-dialog-centered container" tabindex="-1" style="margin:auto;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" id="modal-title"></h4>
          </div>
          <div class="modal-body" style="height:450px !important;">
            <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
            </iframe>
          </div>
        </div>
      </div>
    </div>
