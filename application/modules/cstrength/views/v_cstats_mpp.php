<script>
jQuery(function(){
    //am4core.useTheme(am4themes_animated);
    var maxes = <?php print_r(get_max_cs($sDate, $eDate, $sFleet, $sRank,"cs.count_qual+cs.count_route+cs.count_ground")); ?>;
    var chart = AmCharts.makeChart( "chartCstats", {
        "type": "serial",
        "hideCredits":true,
        "addClassNames": true,
        "theme": "light",
        "autoMargins": true,
        "marginRight": 40,
        "marginLeft": 40,
        "marginTop": 40,
        "autoMarginOffset": 20,

        "balloon": {
          "adjustBorderColor": false,
          "horizontalPadding": 10,
          "verticalPadding": 8,
          "color": "#ffffff"
        },
        "defs": {
            "filter": {
              "id": "dropshadow",
              "x": "-10%",
              "y": "-10%",
              "width": "120%",
              "height": "120%",
              "feOffset": {
                "result": "offOut",
                "in": "SourceAlpha",
                "dx": "3",
                "dy": "3"
              },
              "feGaussianBlur": {
                "result": "blurOut",
                "in": "offOut",
                "stdDeviation": "5"
              },
              "feBlend": {
                "in": "SourceGraphic",
                "in2": "blurOut",
                "mode": "normal"
              }
            }
          },
        "mouseWheelZoomEnabled":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id" : "v1",
            "stackType": "regular",
            "axisAlpha": 0.1,
            "position": "left",
            "gridAlpha": 0.1,
            "maximum": maxes+15,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        },{
            "id" : "v2",
            "axisAlpha": 0.1,
            "position": "right",
            "gridAlpha": 0.1,
            "maximum": maxes+15,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        }],
        "dataProvider": [
        <?php
        $arrDay = getDatesFromRange($sDate, $eDate);
        foreach($arrDay as $sDay) {
            $set_act  = get_act_breakdown($sDay, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther);
            $arrOlReq_tot = list_pilotReq($sDay, $sRank, $sFleet, $ssim);
            $arrOlReq = list_olReg($sDay, $sRank, $sFleet, $ssim);
            $arrPilotQ = list_pilot($sDay, $sRank, $sFleet, 'count_qual');
            $arrPilotR = list_pilot($sDay, $sRank, $sFleet, 'count_route');
            $arrPilotG = list_pilot($sDay, $sRank, $sFleet, 'count_ground');
            $gaps = round($set_act[0]['online_cap'], 0) - $arrOlReq;
            $gaps_tot = $arrPilotQ - round($arrOlReq_tot,0);

            echo "{";
            if ( round($set_act[0]['online_cap'], 0) < $arrOlReq ) {
              echo "signal_low: 'lastBullet',";
            }
            if ( $sDay == date("Y-m-d") ) {
              echo "current: 'lastBullet',";
            }
            echo "'date': '".$sDay."',";
            echo "online_requirement: ".$arrOlReq.",";
            echo "online_capacity: ".round($set_act[0]['online_cap'], 0).",";
            echo "pilot_requirement: ".round($arrOlReq_tot,0).",";
            echo "qualified_pilot: ".$arrPilotQ.",";
            echo "route_training: ".$arrPilotR.",";
            echo "ground_training: ".$arrPilotG.",";
            echo "online_gaps: ".$gaps.",";
            echo "headcount_gaps: ".$gaps_tot.",";
            echo "},";
        }
        ?>
        ],
        "graphs": [{
            "balloonText": "<b style='color:black;'>[[title]]</b><span style='font-size:22px; color:black;'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.95,
            "lineAlpha": 0,
            "fillColors": "rgb(209,212,217)",
            "title": "Qualified Pilot",
            "classNameField": "bulletClass3",
            "type": "column",
            "valueField": "qualified_pilot"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(110,110,110)",
            "title": "route training",
            "type": "column",
            "valueField": "route_training"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "fillColors": "rgb(80,80,80)",
            "title": "ground training",
            "type": "column",
            "valueField": "ground_training"
          }, {
            //"id": "g3",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletSize": 4,
            "bulletBorderThickness": 2,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "lineThickness": 3.2,
            "lineColor": "rgb(13,35,67)",
            "title": "Pilot Requirement",
            "useLineColorForBulletBorder": true,
            "valueField": "pilot_requirement",
            "balloonText": "<b>[[title]]</b><span style='font-size:22px'> : <b>[[value]]</b></span>",
          }, {
            "id": "g1",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletSize": 4,
            "bulletBorderThickness": 2,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "lineThickness": 3.2,
            "lineColor": "rgb(254,66,63)",
            "title": "Online Requirement",
            "classNameField": "signal_low",
            "useLineColorForBulletBorder": true,
            "valueField": "online_requirement",
            "balloonText": "<b>[[title]]</b><span style='font-size:16px'> : <b>[[value]]</b></span>",
        }, {
          "id": "g2",
          "valueAxis": "v2",
          "bullet": "round",
          "bulletSize": 4,
          "bulletBorderThickness": 2,
          "bulletBorderAlpha": 1,
          "bulletColor": "#FFFFFF",
          "hideBulletsCount": 50,
          "lineThickness": 3.2,
          "lineColor": "rgb(11,180,170)",
          "title": "Online Capacity",
          "classNameField": "current",
          "useLineColorForBulletBorder": true,
          "valueField": "online_capacity",
          "balloonText": "<b>[[title]]</b><span style='font-size:16px'> : <b>[[value]]</b></span>",
        }, {
          "valueAxis": "v2",
          "lineAlpha": 0,
          "fillColors": "rgb(300,0,0)",
          "title": "Online Gaps",
          "valueField": "online_gaps",
          "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
        }, {
          "valueAxis": "v2",
          "lineAlpha": 0,
          "fillColors": "rgb(208,0,0)",
          "title": "Headcount Gaps",
          "valueField": "headcount_gaps",
          "balloonText": "<b>[[title]]</b><span style='font-size:12px'> : <b>[[value]]</b></span>",
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left"
        },
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis":false,
            "offset":30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount":true,
            "color":"#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
        "oppositeAxis":false,
        "offset":50,
        "scrollbarHeight":10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "Parameter : Fleet[<?php echo $sFleet;?>], StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], SSIM[<?php echo ($ssim == null) ? "default" : $ssim;?>], DO[<?php echo $DO;?>], DataMode[<?php echo ($dataMode == 1 ? "Schedule ODS Only" : ($dataMode == 2 ? "Full Formula" : ($dataMode == 3 ? "Formula (+TR & AL)" : ($dataMode == 4 ? "Formula (+AL)" : ""))));?>], IncludeTraining[<?php echo ($incTraining == 1) ? "Yes" : "No";?>], IncludeOffice[<?php echo ($incOffice == 1) ? "Yes" : "No";?>], IncludeOther[<?php echo ($incOther == 1) ? "Yes" : "No";?>]",
			"bold": false,
			"x": 0,
			"y": 0
		}
	    ]
    });
});
</script>
