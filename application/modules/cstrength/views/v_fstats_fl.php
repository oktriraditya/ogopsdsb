<script>
jQuery(function(){
    //am4core.useTheme(am4themes_animated);
    var maxes = <?php print_r(round(get_max_cs($sDate, $eDate, $sFleet, $sRank,"cs.count_qual")*0.7,0)); ?>;
    var chart = AmCharts.makeChart( "chartFstats", {
        "type": "serial",
        "hideCredits":true,
        "addClassNames": true,
        "theme": "light",
        "autoMargins": true,
        "marginRight": 40,
        "marginLeft": 40,
        "marginTop": 40,
        "autoMarginOffset": 20,

        "balloon": {
          "adjustBorderColor": false,
          "horizontalPadding": 10,
          "verticalPadding": 8,
          "color": "#ffffff"
        },
        "defs": {
            "filter": {
              "id": "dropshadow",
              "x": "-10%",
              "y": "-10%",
              "width": "120%",
              "height": "120%",
              "feOffset": {
                "result": "offOut",
                "in": "SourceAlpha",
                "dx": "3",
                "dy": "3"
              },
              "feGaussianBlur": {
                "result": "blurOut",
                "in": "offOut",
                "stdDeviation": "5"
              },
              "feBlend": {
                "in": "SourceGraphic",
                "in2": "blurOut",
                "mode": "normal"
              }
            }
          },
        "mouseWheelZoomEnabled":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id" : "v1",
            "stackType": "regular",
            "axisAlpha": 0.1,
            "position": "left",
            "gridAlpha": 0.1,
            //"maximum": maxes,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        },{
            "id" : "v2",
            "axisAlpha": 0.1,
            "position": "right",
            "gridAlpha": 0.1,
            "maximum": maxes,
            "minimum": 0,
            "minVerticalGap": 50,
            "minHorizontalGap": 50,
            "step":50
        }],
        "dataProvider": [
        <?php
        $arrDay = getDatesFromRange($sDate, $eDate);
        foreach($arrDay as $sDay) {
            $set_act  = get_act_breakdown($sDay, $sRank, $sFleet, $dataMode, $DO, $incTraining, $incOffice, $incOther);
            $arrOlReq = list_olReg($sDay, $sRank, $sFleet, $ssim);
            $arrPilotQ = list_pilot($sDay, $sRank, $sFleet, 'count_qual');
            $arrFlight_J = get_flight_count($sDay, $sFleet, 'J');
            $arrFlight_G = get_flight_count($sDay, $sFleet, 'G');
            $arrFlight_C = get_flight_count($sDay, $sFleet, 'C');
            $arrFlight_O = get_flight_count($sDay, $sFleet, 'O');
            $arrFlight_P = get_flight_count($sDay, $sFleet, 'P');
            $total = $arrFlight_J + $arrFlight_G + $arrFlight_C + $arrFlight_O + $arrFlight_P;
            $gaps = round($set_act[0]['online_cap'], 0) - $arrOlReq;

            echo "{";
            echo "'date': '".$sDay."',";
            echo "online_requirement: ".$arrOlReq.",";
            echo "online_capacity: ".round($set_act[0]['online_cap'], 0).",";
            echo "qualified_pilot: ".$arrPilotQ.",";
            if ($arrFlight_J > 0) echo "svc_type_J: ".$arrFlight_J.",";
            if ($arrFlight_G > 0) echo "svc_type_G: ".$arrFlight_G.",";
            if ($arrFlight_C > 0) echo "svc_type_C: ".$arrFlight_C.",";
            if ($arrFlight_O > 0) echo "svc_type_O: ".$arrFlight_O.",";
            if ($arrFlight_P > 0) echo "svc_type_P: ".$arrFlight_P.",";
            echo "total_flight: ".$total.",";
            echo "gaps: ".$gaps.",";
            echo "},";
        }
        ?>
        ],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.7,
            "lineAlpha": 0,
            "fillColors": "rgb(125,125,125)",
            "title": "regular flight",
            "type": "column",
            "valueField": "svc_type_J"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.9,
            "lineAlpha": 0,
            "fillColors": "rgb(145,145,145)",
            "title": "extra flight",
            "type": "column",
            "valueField": "svc_type_G"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.9,
            "lineAlpha": 0,
            "fillColors": "rgb(171,171,171)",
            "title": "charter flight",
            "type": "column",
            "valueField": "svc_type_C"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.9,
            "lineAlpha": 0,
            "fillColors": "rgb(179,179,179)",
            "title": "hajj flight",
            "type": "column",
            "valueField": "svc_type_O"
          }, {
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.9,
            "lineAlpha": 0,
            "fillColors": "rgb(186,186,186)",
            "title": "positioning flight",
            "type": "column",
            "valueField": "svc_type_P"
        }, {
            "type": "column",
            "fillAlphas": 0,
            "lineAlpha": 0,
            "fillColors": "rgb(250,192,106)",
            "title": "total flight",
            "valueField": "total_flight",
            "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
        }, {
            "id": "g1",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletSize": 4,
            "bulletBorderThickness": 2,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "lineThickness": 3.2,
            "lineColor": "rgb(254,66,63)",
            "title": "Online Requirement",
            "useLineColorForBulletBorder": true,
            "valueField": "online_requirement",
            "balloonText": "<b>[[title]]</b><span style='font-size:22px'> : <b>[[value]]</b></span>",
        }, {
          "id": "g2",
          "valueAxis": "v2",
          "bullet": "round",
          "bulletSize": 4,
          "bulletBorderThickness": 2,
          "bulletBorderAlpha": 1,
          "bulletColor": "#FFFFFF",
          "hideBulletsCount": 50,
          "lineThickness": 3.2,
          "lineColor": "rgb(11,180,170)",
          "title": "Online Capacity",
          "useLineColorForBulletBorder": true,
          "valueField": "online_capacity",
          "balloonText": "<b>[[title]]</b><span style='font-size:22px'> : <b>[[value]]</b></span>",
        }, {
          //"id": "g3",
          "valueAxis": "v2",
          "lineAlpha": 0,
          "fillColors": "rgb(250,192,106)",
          "title": "qualified pilot",
          "valueField": "qualified_pilot",
          "balloonText": "<b>[[title]]</b><span style='font-size:10px'> : <b>[[value]]</b></span>",
        }, {
          "valueAxis": "v2",
          "lineAlpha": 0,
          "fillColors": "rgb(208,0,0)",
          "title": "Gaps Online",
          "valueField": "gaps",
          "balloonText": "<b>[[title]]</b><span style='font-size:12px'> : <b>[[value]]</b></span>",
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left"
        },
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis":false,
            "offset":30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount":true,
            "color":"#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
        "oppositeAxis":false,
        "offset":50,
        "scrollbarHeight":10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "Parameter : Fleet[<?php echo $sFleet;?>], StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], SSIM[<?php echo ($ssim == null) ? "default" : $ssim;?>], DataMode[<?php echo ($dataMode == 1 ? "Schedule ODS Only" : ($dataMode == 2 ? "Full Formula" : ($dataMode == 3 ? "Formula (+TR & AL)" : ($dataMode == 4 ? "Formula (+AL)" : ""))));?>], IncludeTraining[<?php echo ($incTraining == 1) ? "Yes" : "No";?>], IncludeOffice[<?php echo ($incOffice == 1) ? "Yes" : "No";?>], IncludeOther[<?php echo ($incOther == 1) ? "Yes" : "No";?>]",
			"bold": false,
			"x": 0,
			"y": 0
		}
	    ]
    });
});
</script>
