<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<style>
#chartFstats, #chartCstats{
  height: 700px;
}
</style>
<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">

function loadCstat() {
  blockUI(CStats);
  var sDate = $("#sDate").val();
  var eDate = $("#eDate").val();
  var ssim = $("#ssim").val();
  var sFleet = $("#sFleet").val();
  //var mode = $("input[name='mode']:checked").val();
  var dataMode = $("input[name='dataMode']:checked").val();
  var stackBar = $("input[name='stackbar']:checked").val();
  var DO = $("input[name='DO']:checked").val();
  var incTraining = $("input[name='incTraining']:checked").val();
  var incOffice = $("input[name='incOffice']:checked").val();
  var incOther = $("input[name='incOther']:checked").val();

  $.ajax({
    type    : "GET",
    url     : "<?php echo base_url(); ?>cstrength/cstats",
    //data    : {"sDate": sDate, "eDate": eDate, "ssim": ssim, "sFleet": sFleet, "mode": mode, "dataMode": dataMode, "stackBar": stackBar, "DO": DO, "incTraining": incTraining, "incOffice": incOffice, "incOther": incOther, },
    data    : {"sDate": sDate, "eDate": eDate, "ssim": ssim, "sFleet": sFleet, "dataMode": dataMode, "stackBar": stackBar, "DO": DO, "incTraining": incTraining, "incOffice": incOffice, "incOther": incOther, },
    success :
    function(msg) {
      $("#chartCstats").html(msg);
      unblockUI(CStats);
    },
    error   :
    function(){
      unblockUI(CStats);
      $("#chartCstats").html("<div class='alert alert-error'>No Data</div>");
    }
  });
}

function loadFstat() {
  blockUI(FStats);
  var sDate = $("#sDate").val();
  var eDate = $("#eDate").val();
  var ssim = $("#ssim").val();
  var sFleet = $("#sFleet").val();
  //var mode = $("input[name='mode']:checked").val();
  var dataMode = $("input[name='dataMode']:checked").val();
  var stackBar = $("input[name='stackbar']:checked").val();
  var DO = $("input[name='DO']:checked").val();
  var incTraining = $("input[name='incTraining']:checked").val();
  var incOffice = $("input[name='incOffice']:checked").val();
  var incOther = $("input[name='incOther']:checked").val();

  $.ajax({
    type    : "GET",
    url     : "<?php echo base_url(); ?>cstrength/fstats",
    //data    : {"sDate": sDate, "eDate": eDate, "ssim": ssim, "sFleet": sFleet, "mode": mode, "dataMode": dataMode, "stackBar": stackBar, "DO": DO, "incTraining": incTraining, "incOffice": incOffice, "incOther": incOther, },
    data    : {"sDate": sDate, "eDate": eDate, "ssim": ssim, "sFleet": sFleet, "dataMode": dataMode, "stackBar": stackBar, "DO": DO, "incTraining": incTraining, "incOffice": incOffice, "incOther": incOther, },
    success :
    function(msg) {
      $("#chartFstats").html(msg);
      unblockUI(FStats);
    },
    error   :
    function(){
      unblockUI(FStats);
      $("#chartFstats").html("<div class='alert alert-error'>No Data</div>");
    }
  });
}

function blockUI(el){
  lastBlockedUI = el;
  App.blockUI({
    target: el,
    animate: true
  });
}

function unblockUI(el){
  App.unblockUI(el);
}

function loadSSIM() {
  var sDate = $("#sDate").val();
  var eDate = $("#eDate").val();
  var sFleet = $("#sFleet").val();
  $.ajax({
    url : "<?php echo base_url();?>cstrength/ssim",
    method : "GET",
    data : {sDate: sDate, eDate: eDate, sFleet: sFleet},
    async : false,
    dataType : 'json',
    success: function(data){
      var html = '';
      var i;
      html += '<option value="'+data[0].file_scenario+'" selected>'+data[0].file_scenario+'</option>';
      for(i=1; i<data.length; i++){
        html += '<option value="'+data[i].file_scenario+'">'+data[i].file_scenario+'</option>';
      }
      $('#ssim').html(html);
    }
  });
}

function loadcurrent_cp() {
  var sFleet = $("#sFleet").val();
  var rk = "CP";
  $.ajax({
    url : "<?php echo base_url();?>cstrength/current_cs/",
    type : "GET",
    data : {"fleet": sFleet, "rank": rk},
    success: function(msg){
      $('#currentcp').html(msg);
      //unblockUI(current_cs);
    }
  });
}

function loadcurrent_fo() {
  var sFleet = $("#sFleet").val();
  var rk = "FO";
  $.ajax({
    url : "<?php echo base_url();?>cstrength/current_cs/",
    type : "GET",
    data : {"fleet": sFleet, "rank": rk},
    success: function(msg){
      $('#currentfo').html(msg);
      //unblockUI(current_cs);
    }
  });
}

function loadcrewlist(sFleet,rk){
  //var sFleet = $("#sFleet").val();
  //var rk = "CP";
  var target_url = "<?php echo base_url().'cstrength/crewlist/';?>"+sFleet+"/"+rk;
  //alert(target_url);
  var iframe = $("#viewFrame");
  iframe.attr("src", target_url);
  //ajaxStart: function(){iframe.attr("src", "loading...");},
  //ajaxStop: function(){iframe.attr("src", target_url);}
}

function loadmovement(sFleet,rk){
  //var sFleet = $("#sFleet").val();
  //var rk = "CP";
  var eDate = $("#eDate").val();
  var target_url = "<?php echo base_url().'cstrength/movement/';?>"+sFleet+"/"+rk+"/"+eDate;
  var iframe = $("#viewFrame");
  iframe.attr("src", target_url);
  //ajaxStart: function(){iframe.attr("src", "loading...");},
  //ajaxStop: function(){iframe.attr("src", target_url);}
}

var sfleet = "";

jQuery(document).ready(function() {
  sFleet = $("#sFleet").val();
  loadSSIM();
  loadcurrent_cp();
  loadcurrent_fo();
  loadCstat();
  loadFstat();


  $('#submit').click(function(){
    sFleet = $("#sFleet").val();
    loadcurrent_cp();
    loadcurrent_fo();
    loadCstat();
    loadFstat();
  });

  $('#apply').click(function() {
    sFleet = $("#sFleet").val();
    loadcurrent_cp();
    loadcurrent_fo();
    loadCstat();
    loadFstat();
    //$('.expand').hide();
  });

  $('#sDate').change(function() {
    loadSSIM();
  });

  $('#eDate').change(function() {
    loadSSIM();
  });

  $('#sFleet').change(function() {
    loadSSIM();
  });

  $("#getlistcp").click(function() {
    loadcrewlist(sFleet,"CP");
  });

  $("#getlistfo").click(function() {
    loadcrewlist(sFleet,"FO");
  });

  $("#movementcp").click(function() {
    loadmovement(sFleet,"CP");
  });

  $("#movementfo").click(function() {
    loadmovement(sFleet,"FO");
  });

});
</script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1><?php echo $title;?>
          <small><?php echo $title_small;?></small>
        </h1>
      </div>
      <!-- END PAGE TITLE -->
      <!-- BEGIN PAGE TOOLBAR -->
      <div class="page-toolbar">
        <div class="pull-right tooltips btn btn-fit-height red">
          <i class="fa fa-plane"></i>
        </div>
        <div class="pull-right tooltips btn btn-fit-height green">
          <i class="icon-calendar"></i>&nbsp;
          <span id="date_time"></span>
          <script type="text/javascript">window.onload = date_time('date_time');</script>
        </div>
        <!--
        <div class="pull-right tooltips btn btn-fit-height red">
        <i class="icon-calendar"></i>&nbsp;
        <span> Last Sync: XXXX</span>
      </div>-->
    </div>
    <!-- END PAGE TOOLBAR -->
  </div>
  <!-- END PAGE HEAD-->
  <!-- BEGIN PAGE BASE CONTENT -->
  <div class="row">&nbsp;</div>

  <div class="row">
    <div class="col-md-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="portlet-body pull-left">
            <div class="form-inline margin-bottom-10 margin-right-10">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default red" type="button">
                    <span class="">Fleet</span>
                  </button>
                </span>
                <select class="form-control selectStn" name="fleet" id="sFleet">
                  <?php foreach ($fleet as $key => $row) {?>
                    <option value="<?php echo $row['fleet'];?>"><?php echo $row['fleet'];?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="portlet-body pull-left">
            <div class="form-inline margin-bottom-10 margin-right-10">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default red" type="button">
                    <span class="">Start Date</span>
                  </button>
                </span>
                <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+5y">
                  <input type="text" class="form-control" value="<?php echo date('Y-m-d');?>" name="date" id="sDate">
                  <span class="input-group-btn">
                    <button class="btn default" type="button">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="portlet-body pull-left">
            <div class="form-inline margin-bottom-10 margin-right-10">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default red" type="button">
                    <span class="">End Date</span>
                  </button>
                </span>
                <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="+5y">
                  <input type="text" class="form-control" value="<?php echo date("Y-m-d", strtotime("+30 day"));?>" name="date" id="eDate">
                  <span class="input-group-btn">
                    <button class="btn default" type="button">
                      <i class="fa fa-calendar"></i>
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="portlet-body pull-left">
            <div class="form-inline margin-bottom-10 margin-right-10">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-default red" type="button">
                    <span class="">SSIM Data</span>
                  </button>
                </span>
                <select class="form-control" name="ssim" id="ssim"></select>
              </div>
            </div>
          </div>
          <div class="portlet-body pull-left">
            <div class="form-inline margin-bottom-10">
              <div class="input-group">
                <span class="input-group-btn">
                  <button type="submit" class="btn dark" id="submit">Load <i class="fa fa-sign-out"></i></button>
                </span>
              </div>
            </div>
          </div>
          <div class="tools pull-right">
            <a href="javascript:;" class="expand"> </a>
          </div>
          <div class="caption pull-right">
            <span class="caption-helper"><i class="fa fa-sliders"></i>&nbsp;Parameter Option</span>
          </div>
        </div>
        <div class="portlet-body portlet-collapsed form">
          <div class="form-horizontal" role="form">
        <!--
            <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default active">
            <input type="radio" class="toggle" name="mode" value="2" checked> <i class="fa fa-male"></i> &nbsp;Man Power Planning Mode </label>
            <label class="btn btn-default">
            <input type="radio" class="toggle" name="mode" value="1"> <i class="fa fa-plane"></i> &nbsp;Operational Mode </a> </label>
          </div>
        -->
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-2 control-label bold">Day Off</label>
            <div class="col-md-10">
              <div class="mt-radio-inline">
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="DO" id="DO" value="8" checked> 8
                  <span></span>
                </label>
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="DO" id="DO" value="7"> 7
                  <span></span>
                </label>
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="DO" id="DO" value="6"> 6
                  <span></span>
                </label>
              </div>
            </div>
            <label class="col-md-2 control-label bold">Online Capacity</label>
            <div class="col-md-10">
              <div class="mt-radio-inline">
                <!--<label class="mt-radio mt-radio-outline">
                <input type="radio" name="dataMode" value="1" id="dataMode"> Actual Assignment (source: ODS)
                <span></span>
              </label>-->
              <small>
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="dataMode" value="2" id="dataMode" > Full Formula
                <span></span>
              </label>
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="dataMode" value="3" id="dataMode"> Formula (Mixed with Training & Leave Sched.)
                <span></span>
              </label>
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="dataMode" value="4" id="dataMode" checked> Formula (Mixed with Leave Sched.)
                <span></span>
              </label>
            </small>
            </div>
          </div>
          <label class="col-md-2 control-label bold">Stacked Bar Info</label>
          <div class="col-md-10">
            <div class="mt-radio-inline">
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="stackbar" value="3" id="stackbar" checked> Qualification
                <span></span>
              </label>
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="stackbar" value="1" id="stackbar"> #Flight
                <span></span>
              </label>
              <label class="mt-radio mt-radio-outline">
                <input type="radio" name="stackbar" value="2" id="stackbar"> Duties Allocation
                <span></span>
              </label>
              <!--<label class="mt-radio mt-radio-outline">
              <input type="radio" name="stackbar" value="3" id="stackbar"> Actual Assignment (source: ODS)
              <span></span>
            </label>-->
          </div>
        </div>
        <label class="col-md-2 control-label bold">Include Training</label>
        <div class="col-md-10">
          <div class="mt-checkbox-inline">
            <label class="mt-checkbox mt-checkbox-outline">
              <input type="checkbox" value="1" name="incTraining" checked />
              <span></span>
            </label>
          </div>
        </div>
        <label class="col-md-2 control-label bold">Include Office</label>
        <div class="col-md-10">
          <div class="mt-checkbox-inline">
            <label class="mt-checkbox mt-checkbox-outline">
              <input type="checkbox" value="1" name="incOffice" checked />
              <span></span>
            </label>
          </div>
        </div>
        <label class="col-md-2 control-label bold">Include Other</label>
        <div class="col-md-10">
          <div class="mt-checkbox-inline">
            <label class="mt-checkbox mt-checkbox-outline">
              <input type="checkbox" value="1" name="incOther" checked />
              <span></span>
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-offset-2 col-md-12">
          <button type="" class="btn red" id="apply">Apply Parameter</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<div class="row">
  <div class="col-lg-12 col-xs-12 col-sm-12">
    <div class="portlet light bordered" id="CStats">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase font-dark" >Captain</span>
          <span class="caption-helper" style="display:inline;">
            <button type="button" class="btn btn-default btn-circle" data-target="#view" data-toggle="modal" id="getlistcp"><i class="fas fa-download"></i>
              <span class="caption-helper" id="currentcp" style="display:inline;"></span>
            </button>
          </span>
        </div>
        <div class="actions">
          <a class="btn btn-circle btn-icon-only btn-default" data-target="#view" data-toggle="modal" id="movementcp"> <i class="fas fa-chart-line" data-toggle="tooltip" title="CP Movement"></i> </a>
        </div>
        <div class="actions">
          <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
          &nbsp;
        </div>
      </div>
      <div class="portlet-body">
        <div id="chartCstats" class="CSSAnimationChart"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-xs-12 col-sm-12">
    <div class="portlet light bordered" id="FStats">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase font-dark" >First Officer</span>
          <span class="caption-helper" style="display:inline;">
            <button type="button" class="btn btn-default btn-circle" data-target="#view" data-toggle="modal" id="getlistfo"><i class="fas fa-download"></i>
              <span class="caption-helper" id="currentfo" style="display:inline;"></span>
            </button>
          </span>
        </div>
        <div class="actions">
          <a class="btn btn-circle btn-icon-only btn-default" data-target="#view" data-toggle="modal" id="movementfo"> <i class="fas fa-chart-line" data-toggle="tooltip" title="FO Movement"></i> </a>
        </div>
        <div class="actions">
          <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
          &nbsp;
        </div>
      </div>
      <div class="portlet-body">
        <div id="chartFstats" class="CSSAnimationChart"></div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

<div id="view" class="modal modal-dialog-centered container" tabindex="-1" style="margin:auto;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title" id="modal-title"></h4>
  </div>
  <div class="modal-body" style="height:450px !important;">
    <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
    </iframe>
  </div>
</div>
<!-- END CONTENT
