<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
    <style type="text/css">
        .kotak {
            margin: auto;
            background-color: white;
            width: 100%;
            padding: 10px 10px 10px 10px;
        }
    
        .tengah {
            text-align: center;
        }
        
        #judul {
            text-align: center;
            font-family: Corbel;
            font-size: 24;
            
        }
    </style>
<!--css end-->

<!--FLIGHT NUMBER-->
<!--table flight number begin-->        
    <div class="kotak">
        <h3 id="judul"> LIST <?php echo $fleet."'s ".$rank." : ".$key; ?> </h3>

        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="tabledata2">
            <thead>
                <tr>
                    <th width="1%" class="tengah">No.</th>
                    <th width="10%" class="tengah">ID</th>
                    <th width="10%" class="tengah">Name</th>
                    <th width="10%" class="tengah">Duty</th>         
				</tr>
            </thead>               
				<?php
				$i=0;
                foreach ($whoareassigned as $key => $value){
				?>
                <tr class="tengah">
                    <td> <?php echo $i+1; $i++;?> </td>
                    <td> <?php echo $value['IDPEG'];?></td>
					<td style="clear;"> <?php echo $value['FULLNAME'];?> </td>
					<td> <?php echo $value['ACTIVITIES_GROUP'];?></td>                   
                </tr>
                <?php }
				?>			
            <tbody>
                
            </tbody>
        </table>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tabledata2').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true
        });
    });
</script>
