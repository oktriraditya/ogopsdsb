<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVE STYLES -->

<script type="text/javascript">

function blockUI(el){
    lastBlockedUI = el;
    App.blockUI({
        target: el,
        animate: true
    });
}

function unblockUI(el){
    App.unblockUI(el);
}

function loadFdataMov(sFleet,sRank,sDiv,sStart,sDate,sStat) {
    blockUI(sDiv);
    //var sDate = $("#sDate").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cstrength/tableCrewMovement",
        data    : {"sStart": sStart, "sDate" : sDate, "sFleet": sFleet, "sRank": sRank, "sStat" : sStat},
        success :
            function(msg) {
                $("#"+sDiv).html(msg);
                unblockUI(sDiv);
            },
        error   : function(){
                unblockUI(sDiv);
                $("#"+sDiv).html("<div class='alert alert-error'>No Data</div>");
        }
    });
}

function loadCreMovement(sFleet,sRank,sDiv,sStart,sDate) {
    blockUI(sDiv);
    //var sDate = $("#sDate").val();

    $.ajax({
        type    : "GET",
        url     : "<?php echo base_url(); ?>cstrength/movement_merge",
        data    : {"sFleet": sFleet, "sRank": sRank, "sDate": sDate},
        success :
            function(msg) {
                $("#"+sDiv).html(msg);
                unblockUI(sDiv);
            },
        error   : function(){
                unblockUI(sDiv);
                $("#"+sDiv).html("<div class='alert alert-error'>No Data</div>");
        }
    });
}

jQuery(document).ready(function() {
      var sFleet = "<?php echo $fleet;  ?>";
      var sRank = "<?php echo $rank;  ?>";
      var sToday = "<?php echo $today; ?>";
      var sDate = "<?php echo $sDate; ?>";

      loadFdataMov(sFleet,sRank,'boxFdataQ',sToday,sDate,"Q");
      loadFdataMov(sFleet,sRank,'boxFdataRT',sToday,sDate,"RT");
      loadFdataMov(sFleet,sRank,'boxFdataGT',sToday,sDate,"GT");
      loadCreMovement(sFleet,sRank,'creMovement',sToday,sDate);
      $('#getCrewList').click(function() {
        get_crew_movement();
      });
});
</script>

<!--css begin-->
    <style type="text/css">
        .kotak {
            margin: auto;
            background-color: white;
            width: 100%;
            padding: 10px 10px 10px 10px;
        }
        #judul {
            text-align: center;
            font-family: Corbel;
            font-size: 24;
        }
    </style>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
        <!-- BEGIN PAGE HEAD-->
            <!-- BEGIN PAGE TITLE -->
        <!--<h1 id="judul" style="font-size:18pt;"> <b><?php //echo $title;?></b>
            <small><?php //echo $title_small;?></small>
        </h1>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row">
          <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
              <div style="font-size:18px;color:#666666;">
                  <div class="caption">
                    <span class="caption-subject uppercase bold font-light" > <?php echo $fleet." ".$rank." Qualified's Movement (".$today." - ".$sDate.")"; ?>  </span>
                  </div>
              </div>
            </div>
          </div>
          <!-- END PAGE BASE CONTENT -->
      </div>
      <div class="row">
          <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet bordered" id="FStats">
              <div id="boxFdataQ"></div>
            </div>
          </div>
          <!-- END PAGE BASE CONTENT -->
      </div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
          <div class="portlet light bordered" id="FStats">
            <div style="font-size:18px;color:#666666;">
                <div class="caption">
                  <span class="caption-subject bold uppercase font-light" > <?php echo $fleet." ".$rank." Route Trainee's Movement (".$today." - ".$sDate.")"; ?>   </span>
                </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
          <div class="portlet bordered" id="FStats">
            <div id="boxFdataRT"></div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>

    <div class="row">
      <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet light bordered" id="FStats">
          <div style="font-size:18px;color:#666666;">
              <div class="caption">
                <span class="caption-subject bold uppercase font-light" > <?php echo $fleet." ".$rank." Ground Trainee's Movement (".$today." - ".$sDate.")"; ?>   </span>
              </div>
          </div>
        </div>
      </div>
      <!-- END PAGE BASE CONTENT -->
  </div>

  <div class="row">
      <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet bordered" id="FStats">
          <div id="boxFdataGT"></div>
        </div>
      </div>
      <!-- END PAGE BASE CONTENT -->
  </div>
</div>

<!-- Detail Crew Movement-->
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
      <div class="portlet light bordered" id="FStats">
        <div id="creMovement"></div>
      </div>
    </div>
  </div>
  <!-- END PAGE BASE CONTENT -->


<div id="view" class="modal modal-dialog-centered container" tabindex="-1" style="margin:auto;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title" id="modal-title"></h4>
  </div>
  <div class="modal-body" style="height:450px !important;">
    <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">

    </iframe>
  </div>
</div>
<!-- END CONTENT
