
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $this->config->item('site_title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="<?php echo $this->config->item('site_desc'); ?>" name="description" />
        <meta content="pungky.p@garuda-indonesia.com; mohammad.oktri@garuda-indonesia.com" name="author" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/ga-icon.png" /> 
<!--        <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-css"/>-->
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/all.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!--Bootsrap 4 CDN-->
        <!--        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->

        <!--Fontawesome CDN-->
        <!--        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">-->

        <!--Custom styles-->
        <style type="text/css">
            @import url("https://fonts.googleapis.com/css?family=Corbel");

            html,body{
                background-image: url('<?php echo base_url()?>/assets/images/27756_1266876963.jpg');
                background-size: cover;
                background-repeat: no-repeat;
                height: 100%;
                font-family: 'Corbel', sans-serif;
            }

            .container{
                height: 100%;
                align-content: center;
                vertical-align: central;
            }

            .card{
                height: 370px;
                margin-top: auto;
                margin-bottom: auto;
                margin-left: auto;
                margin-right: auto;
                width: 400px;
                background-color: rgba(255,255,255,0.5) !important;
            }

            .social_icon span{
                font-size: 60px;
                margin-left: 10px;
                color: #FFC312;
            }

            .social_icon span:hover{
                color: white;
                cursor: pointer;
            }

            .card-header h3{
                color: white;
            }
            
            .card-header{
                margin-top: auto;
                margin-bottom: auto;
                margin-left: auto;
                margin-right: auto;
                background-color:  rgba(255,255,255,0) ;
            }
            
            .card-footer{
                margin-top: auto;
                margin-bottom: auto;
                margin-left: auto;
                margin-right: auto;
                background-color:  rgba(255,255,255,0) ;
            }

            .social_icon{
                position: absolute;
                right: 20px;
                top: -45px;
            }

            .input-group-prepend span{
                width: 50px;
                background-color: #0080FF;
                color: white;
                border:0 !important;
            }

            input:focus{
                outline: 0 0 0 0  !important;
                box-shadow: 0 0 0 0 !important;

            }

            .remember{
                color: white;
            }

            .remember input
            {
                width: 20px;
                height: 20px;
                margin-left: 15px;
                margin-right: 5px;
            }

            .login_btn{
                color: white;
                background-color: #0080FF;
                width: 100px;
            }

            .login_btn:hover{
                color: black;
                background-color: white;
            }

            .links{
                color: white;
            }

            .links a{
                margin-left: 4px;
            }
        </style>
    </head>
    <body>
        
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <img src="<?php echo base_url() ?>/assets/images/ga.png" width="300px" height="59px"/>
                        
                    </div>
                    <div class="card-body">
                        <form action="<?php echo base_url() ?>cstrength/login" method="POST">
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control" name="uname" placeholder="username">

                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" class="form-control" name="psw" placeholder="password">
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" value="Login" class="btn float-right login_btn">
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        
                        <div class=" justify-content-center">
                            2019 &copy; <a target="_blank" href="https://www.garuda-indonesia.com">PT Garuda Indonesia (Persero),Tbk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>