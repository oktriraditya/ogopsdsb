<script>
jQuery(function(){
    var chart = AmCharts.makeChart( "chartFstats", {
        "type": "serial",
        "theme": "none",
        "marginRight": 40,
        "marginLeft": 40,
        "marginTop": 40,
        "autoMarginOffset": 20,
        "mouseWheelZoomEnabled":true,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id": "v1",
            "axisAlpha": 0,
            "position": "left",
            "ignoreAxisWidth":true
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "dataProvider": [
        <?php       
        $arrDay = getDatesFromRange($sDate, $eDate);            
        foreach($arrDay as $sDay) {
            $arrOlReq = list_olReg($sDay, $sRank, $sFleet, $ssim);
            $arrDutyCode = list_DutyCode();
            
            echo "{";
            echo "'date': '".$sDay."',";
            echo "olreq: ".$arrOlReq.",";
            foreach ($arrDutyCode as $key => $row) {
                $valDutyCode = get_valDutyCode($row['duty_code'], $sDay, $sRank, $sFleet);
                echo $row['duty_code'] .":". $valDutyCode .",";
            }
            echo "},";
        }
        ?>
        ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.1,
            "gridAlpha": 0.1
        }
        ],
        "graphs": [
        <?php
        foreach ($arrDutyCode as $key => $row) {
        ?>
        {
            "balloonText": "<b>[[title]]</b><span style='font-size:14px'> : <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "lineAlpha": 0,
            "title": "<?php echo $row['duty_code'];?>",
            "type": "column",            
            "valueField": "<?php echo $row['duty_code'];?>",
        },
        <?php } ?>        
        {
            "id": "g1",
            "balloon":
                {
                "drop":true,
                "adjustBorderColor":false,
                "color":"#ffffff"
                },
            "bullet": "round",
            "bulletSize": 3,
            "bulletBorderThickness": 3,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#D91E18",
            "title": "Online Requirement",
            "useLineColorForBulletBorder": true,
            "valueField": "olreq",
            "balloonText": "<b>[[title]]</b><span style='font-size:18px'> : <b>[[value]]</b></span>",
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0.1,
            "position": "left"
        },
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis":false,
            "offset":30,
            "scrollbarHeight": 80,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.1,
            "selectedBackgroundColor": "#888888",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount":true,
            "color":"#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha":1,
            "cursorColor":"#258cbb",
            "limitToGraph":"g1",
            "valueLineAlpha":0.2,
            "valueZoomable":true
        },
        "valueScrollbar":{
            "oppositeAxis":false,
            "offset":50,
            "scrollbarHeight":10
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 10,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "export": {
            "enabled": true
        },
        "allLabels": [
		{
			"text": "Parameter : Fleet[<?php echo $sFleet;?>], StartDate[<?php echo date('d-m-Y',strtotime($sDate));?>], EndDate[<?php echo date('d-m-Y',strtotime($eDate));?>], SSIM[<?php echo ($ssim == null) ? "all" : $ssim;?>], Mode[<?php echo ($mode == 1) ? "Operational" : "Man Power Planning";?>], DO[<?php echo $DO;?>], DataMode[<?php echo ($dataMode == 1 ? "Schedule ODS Only" : ($dataMode == 2 ? "Formula" : ($dataMode == 3 ? "Formula (blend TR & AL)" : ($dataMode == 4 ? "Formula (blend AL Only)" : ""))));?>], IncludeTraining[<?php echo ($incTraining == 1) ? "Yes" : "No";?>], IncludeOffice[<?php echo ($incOffice == 1) ? "Yes" : "No";?>], IncludeOther[<?php echo ($incOther == 1) ? "Yes" : "No";?>]",
			"bold": false,
			"x": 0,
			"y": 0
		}
	    ]
    });     
}); 
</script>