<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> &nbsp;&nbsp;&nbsp;&nbsp; 2018 &copy; <a target="_blank" href="https://www.garuda-indonesia.com">PT Garuda Indonesia (Persero),Tbk</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->