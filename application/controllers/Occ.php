<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Occ extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_occ');

        $i_isLogon = is_login();
        if (!$i_isLogon) redirect('auth', 'refresh');
    }

    /* START OF HOME */

    function index() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ');
        if ($isAuth) {
            redirect('occ/home', 'refresh');
        } else {
            redirect('main', 'refresh');
        }
    }

    function home() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ');
        if ($isAuth) {
            $data = $this->M_occ->home();
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function home_autoref() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ');
        if ($isAuth) {
            $data = $this->M_occ->home();
            $this->load->view('v_occ_home', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function causedelayall() {
        $this->load->view('v_occ_home_cdelayall');
    }

    function causedelaycgk() {
        $this->load->view('v_occ_home_cdelaycgk');
    }

    /* END OF HOME */

    /* START OF DAILY */

    function daily() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/daily');
        if ($isAuth) {
            $data = $this->M_occ->daily();
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function sdata() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');

        //get data
        $data['stn'] = $stn;
        $data['sDate'] = $sDate;
        if ($stn == 'all')
            $stn = NULL;
        $data['nDeparted'] = get_departed('count', $sDate, NULL, $stn);
        $data['nOntime'] = get_onTime('count', $sDate, NULL, $stn);
        $data['nCancel'] = get_cancel('count', $sDate, NULL, $stn);
        $data['nDelay'] = get_delay('count', $sDate, NULL, $stn);
        $data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], 0);
        $data['pDelay'] = get_percentage($data['nDelay'], $data['nDeparted'], 100);

        $this->load->view('v_occ_daily_boxtop', $data);
    }

    function fdata() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');

        //get data
        $data['stn'] = $stn;
        $data['sDate'] = $sDate;
        if ($stn == 'all')
            $stn = NULL;
        $data['nSchedule'] = get_schedule('count', $sDate, NULL, $stn);
        $data['nCancelPlan'] = get_cancelPlan('count', $sDate, NULL, $stn);
        $data['nOnschedule'] = get_onschedule('count', $sDate, NULL, $stn);
        $data['nDeparted'] = get_departed('count', $sDate, NULL, $stn);
        $data['nOntime'] = get_onTime('count', $sDate, NULL, $stn);
        $data['nCancel'] = get_cancel('count', $sDate, NULL, $stn);
        $data['nDelay'] = get_delay('count', $sDate, NULL, $stn);
        $data['pCancelPlan'] = get_percentage($data['nCancelPlan'], $data['nSchedule'], 100);
        $data['pOnschedule'] = get_percentage($data['nOnschedule'], $data['nSchedule'], 100);
        $data['pDeparted'] = get_percentage($data['nDeparted'], $data['nOnschedule'], 100);
        $data['pOTP'] = get_percentOTP($data['nOntime'], $data['nDeparted'], 0);
        $data['pDelay'] = get_percentage($data['nDelay'], $data['nDeparted'], 100);
        $data['pCancel'] = get_percentage($data['nCancel'], $data['nSchedule'], 100);

        $this->load->view('v_occ_daily_boxfdata', $data);
    }

    function otpstat() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');
        $data['stn'] = $stn;
        $data['sDate'] = $sDate;

        $this->load->view('v_occ_daily_otpstats', $data);
    }

    function causedelay() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');
        $data['stn'] = $stn;
        $data['startDate'] = $sDate;

        $this->load->view('v_occ_daily_cdelay', $data);
    }

    function tabledata() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');
        $status = $this->input->get('status');
        if (empty($status))
            $status = 'all';
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;

        $data['stn'] = $stn;
        $data['sDate'] = $sDate;
        $data['status'] = $status;

        if ($status == 'all')
            $data['arrRecords'] = get_schedule('rec', $sDate, NULL, $station);
        if ($status == 'cancelplan')
            $data['arrRecords'] = get_cancelPlan('rec', $sDate, NULL, $station);
        if ($status == 'onschedule')
            $data['arrRecords'] = get_onschedule('rec', $sDate, NULL, $station);
        if ($status == 'departed')
            $data['arrRecords'] = get_departed('rec', $sDate, NULL, $station);
        if ($status == 'delay')
            $data['arrRecords'] = get_delay('rec', $sDate, NULL, $station);
        if ($status == 'ontime')
            $data['arrRecords'] = get_onTime('rec', $sDate, NULL, $station);
        if ($status == 'cancel')
            $data['arrRecords'] = get_cancel('rec', $sDate, NULL, $station);

        $this->load->view('v_occ_daily_tabledata', $data);
    }

    /* END OF DAILY */

    /* START OF FLIGHT RECORD */

    function record() {
        if ($this->session->userdata('logged_in')) {
            $data = $this->M_occ->record();
            $this->load->view('v_occ_main', $data);
        } else {
            $this->session->set_flashdata("not_logged_in", 2);
            redirect('occ/login_page', 'refresh');
        }
    }

    function tablerec() {
        $stn = $this->input->get('stn');
        $stnto = $this->input->get('stnto');
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        $status = $this->input->get('status');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($stnto == 'all')
            $stationto = NULL;
        else
            $stationto = $stnto;
        $data['stn'] = $stn;
        $data['stnto'] = $stnto;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;
        $data['status'] = $status;

        if ($status == 'all')
            $data['arrRecords'] = get_schedule('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'cancelplan')
            $data['arrRecords'] = get_cancelPlan('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'onschedule')
            $data['arrRecords'] = get_onschedule('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'departed')
            $data['arrRecords'] = get_departed('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'delay')
            $data['arrRecords'] = get_delay('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'ontime')
            $data['arrRecords'] = get_onTime('rec', $sDate, $eDate, $station, $stationto);
        if ($status == 'cancel')
            $data['arrRecords'] = get_cancel('rec', $sDate, $eDate, $station, $stationto);

        $this->load->view('v_occ_record_tablerec', $data);
    }

    /* END OF FLIGHT RECORD */

    function viewrec() {
        $idFlight = $this->uri->segment(3);
        $data = $this->M_occ->viewrec($idFlight);
        $this->load->view('v_occ_main_modal', $data);
    }

    /* START OF STATISTICS */

    function stat() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/stat');
        if ($isAuth) {
            $data = $this->M_occ->stat();
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function otplast() {
        $stn = $this->input->get('stn');
        $sDate = $this->input->get('sDate');
        $eDate = $this->input->get('eDate');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;

        $data['stn'] = $station;
        $data['sDate'] = $sDate;
        $data['eDate'] = $eDate;

        $this->load->view('v_occ_stat_otplast', $data);
    }

    /* SEND OF STATISTICS */

    /* START OF FLIGHT OPERATIONAL PER DAY */

    function dataflight() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dataflight');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $data = $this->M_occ->dataflight($datefrom, $dateto, 'all', 'all', 'all', NULL, 'all');
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    //update from method "dataflight"
    function dailyotp_v2() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dataflight');
        if ($isAuth) {
            $data['title'] = 'OTP Dashboard';
            $data['title_small'] = 'OTP per Day';
            $data['arrStation'] = get_station();
            $data['datefrom'] = $this->input->get('datefrom');
            $data['dateto'] = $this->input->get('dateto');
            $data['stn'] = 'all';
            $data['stnto'] = 'all';
            $data['view'] = 'v_occ_record_flightday_v2';
            //$data = $this->M_occ->dailyotp_v2($datefrom, $dateto, 'all', 'all', 'all', NULL, 'all');
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function details_delay() {
        //$type_cod = $this->input->post('type_delay');
        //$cod_date = $this->input->post('cod_date');
        $type_cod = $this->uri->segment(3);
        $cod_date = $this->uri->segment(4);
        $service = $this->uri->segment(5);
        $org = $this->uri->segment(6);
        $dest = $this->uri->segment(7);
        $delay = $this->uri->segment(8);
        $statuspp = $this->uri->segment(9);
        if (is_null($org))
            $org = NULL;
        if (is_null($dest))
            $dest = NULL;
        $data = $this->M_occ->details_delay($type_cod, $cod_date, $service, $org, $dest, $delay, $statuspp);
        $this->load->view('v_occ_main_modal', $data);
    }

    function dataflightbystation() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dataflightbystation');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $data = $this->M_occ->data_flight_by_station($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function dataperflight() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dataperflight');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $data = $this->M_occ->data_per_flight($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function tableperflight() {
        $stn = $this->input->get('stn');
        $stnto = $this->input->get('stnto');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $flighttype = $this->input->get('flighttype');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($stnto == 'all')
            $stationto = NULL;
        else
            $stationto = $stnto;
        if ($flighttype == 'all')
            $flighttype = NULL;
        else
            $flighttype;
        $data = $this->M_occ->data_per_flight($datefrom, $dateto, $station, $stationto, $flighttype);
        $this->load->view('v_occ_record_tableflight', $data);
    }

    function tableperflight_v2() {
        $stn = $this->input->get('stn');
        $stnto = $this->input->get('stnto');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $flighttype = $this->input->get('flighttype');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($stnto == 'all')
            $stationto = NULL;
        else
            $stationto = $stnto;
        if ($flighttype == 'all')
            $flighttype = NULL;
        else
            $flighttype;
        $data = $this->M_occ->data_per_flight($datefrom, $dateto, $station, $stationto, $flighttype);
        $this->load->view('v_occ_record_tableflight', $data);
    }

    function tableflightschedule() {
        $stn = $this->input->get('stn');
        $stnto = $this->input->get('stnto');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $flighttype = $this->input->get('flighttype');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($stnto == 'all')
            $stationto = NULL;
        else
            $stationto = $stnto;
        if ($flighttype == 'all')
            $flighttype = NULL;
        else
            $flighttype;
        $data = $this->M_occ->data_per_flight_schedule($datefrom, $dateto, $station, $stationto, $flighttype);
        $this->load->view('v_occ_schedule_flight_table', $data);
    }

    function tableperday() {
        $stn = $this->input->get('stn');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $org = $this->input->get('org');
        $dest = $this->input->get('dest');
        $statuspp = $this->input->get('statuspp');
        $delay = $this->input->get('delay');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        //if ($org == 'all') $org = NULL;
        //if ($dest == 'all') $dest = NULL;
        $data = $this->M_occ->dataflight($datefrom, $dateto, $station, $org, $dest, $statuspp, $delay);
        $this->load->view('v_occ_record_tableflightday', $data);
    }


    function tableotpperday_v2() {
        $stn = $this->input->get('stn');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $org = $this->input->get('org');
        $dest = $this->input->get('dest');
        $statuspp = $this->input->get('statuspp');
        $delay = $this->input->get('delay');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        //if ($org == 'all') $org = NULL;
        //if ($dest == 'all') $dest = NULL;
        $data = $this->M_occ->dailyotp_v2($datefrom, $dateto, $station, $org, $dest, $statuspp, $delay);
        $this->load->view('v_occ_record_tableflightday_v2', $data);
    }


    function tableperstation() {
        $stn = $this->input->get('stn');
        $apt = $this->input->get('airport');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($apt == 'all')
            $airport = NULL;
        else
            $airport = $apt;
        $data = $this->M_occ->data_flight_by_station($datefrom, $dateto, $station, $airport);
        $this->load->view('v_occ_record_tablestation', $data);
    }

    function tabledephub() {
        $stn = $this->input->get('stn');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        $data = $this->M_occ->data_dephub_report($datefrom, $dateto, $station);
        $this->load->view('v_occ_record_tabledephub', $data);
    }

    function tablepermonth() {
        $stn = $this->input->get('stn');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $org = $this->input->get('org');
        $dest = $this->input->get('dest');
        $statuspp = $this->input->get('statuspp');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($org == 'all')
            $org = NULL;
        if ($dest == 'all')
            $dest = NULL;
        $data = $this->M_occ->data_montly_report($datefrom, $dateto, $station, $org, $dest, $statuspp);
        $this->load->view('v_occ_record_tableflightmonth', $data);
    }

    function dephubreport() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dephubreport');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $data = $this->M_occ->data_dephub_report($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function montlyotpreport() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/montlyotpreport');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            //$sDate = 2018;
            $data = $this->M_occ->data_montly_report($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function delaycodecontributor() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/montlyotpreport');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom'); //$this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            if (is_null($datefrom))
                $datefrom = get_dateotp("-7 days");
            //$sDate = 2018;
            $data = $this->M_occ->data_delaycode_report($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function delaycodecontributor_v2() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/montlyotpreport');
        if ($isAuth) {
            $datefrom = $this->input->get('datefrom'); //$this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            if (is_null($datefrom))
                $datefrom = get_dateotp("-7 days");
            //$sDate = 2018;
            //$data = $this->M_occ->data_delaycode_report($datefrom, $dateto);
            $data['title'] = 'OTP Dashboard';
            $data['title_small'] = 'Daily Delay Contributor';
            $data['fleet'] = list_fleet_MM();
            $data['view'] = 'v_occ_record_delaycode_v2';
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function tabledelaycodecontributor() {
        $delay = $this->input->get('delay');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        //if ($delay == 'all') $delayCont = "all"; else $delayCont = $delay;
        $data = $this->M_occ->data_delaycode_report($datefrom, $dateto, $delay);
        $this->load->view('v_occ_record_tabledelaycode', $data);
    }

    function tabledelaycodecontributor_v2() {
        $delay = $this->input->get('delay');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $fleet = $this->input->get('sFleet');
        //if ($delay == 'all') $delayCont = "all"; else $delayCont = $delay;
        $data = $this->M_occ->data_delaycode_report_v2($datefrom, $dateto, $delay, $fleet);
        $this->load->view('v_occ_record_tabledelaycode', $data);
    }

    function sendOTP() {

        //require 'vendor/autoload.php';
        //require('E://xampp/htdocs/gadsb/vendor/phpmailer/phpmailer/src/phpmailer.php');
        //require('E://xampp/htdocs/gadsb/vendor/phpmailer/phpmailer/src/smtp.php');
        $username = $this->input->post('uname');
        $token = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1204567893";
        $token = str_shuffle($token);
        $token = substr($token, 0, 10);
        $role = "user";
        $station = "";
        $otp = rand(100000, 999999);
        if (strpos($username, '_') !== false) {
            $uname = explode('_', $username);
            $email = $uname[1];
            $station = $uname[0];
        } else {
            $email = $username;
        }
        if (check_registered_email($email) > 0) {
            update_registered_email($email, $station);
            insert_otp_verification($otp);
        } else {
            insert_registered_email($email, $role, $station);
            insert_otp_verification($otp);
        }


        $message_body = "One Time Password for your OTP Dashboard authentication is:<br/><br/>" . $otp;
        $mail = new PHPMailer\PHPMailer\PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = 'tls'; // tls or ssl
        $mail->Port = "587";
        $mail->Username = "donotreply.ga@gmail.com";
        $mail->Password = "adminGa.!24";
        $mail->Host = "smtp.gmail.com";
        $mail->Mailer = "smtp";
        $mail->SetFrom("donotreply.ga@gmail.com", "Administrator GIAA");
        $mail->AddAddress($email);
        $mail->Subject = "<This is automatic generated email> OTP to Login <Please Do Not Reply to this Email>";
        $mail->MsgHTML($message_body);
        $mail->IsHTML(true);
        $mail->Send();
        $mail->clearAddresses();
        exit;
    }

    function login() {
        $username = $this->input->post('uname');
        //$password = $this->input->post('psw');otpverified
        $password = $this->input->post('psw');
        //$station = str_split($username, 3);
        //$data = $this->M_occ->login_page($username, $password);
        //$cekstn = cek_station($station[0]);
        $station = "";
        if (strpos($username, 'KKGA') !== false) {
            $uname = explode('KKGA', $username);
            //$email = $uname[1];
            $station = $uname[0];
        }

        $cekstn = cek_station($station);
        if (($username == "admin" && $password == "adminotp") || (strpos($username, 'KKGA') !== false && $cekstn > 0 && $password == "G4rudaOgl") || ($username == "user" && $password == "userotp") || ($username == "aocc" && $password == "aocc")) {
            echo 'loginsuccess';
            if ($station == "" && $username == "admin") {
                $data = array(
                    'username' => $username,
                    'station' => strtoupper($station),
                    'role' => 'admin',
                    'logged_in' => true
                );
            }else if ($station == "" && $username == "user") {
                $data = array(
                    'username' => $username,
                    'station' => strtoupper($station),
                    'role' => 'user',
                    'logged_in' => true
                );
            }
            else if ($station == "" && $username == "aocc") {
                $data = array(
                    'username' => $username,
                    'station' => strtoupper($station),
                    'role' => 'aocc',
                    'logged_in' => true
                );
            }
            else {
                $data = array(
                    'username' => $username,
                    'station' => strtoupper($station),
                    'role' => 'station',
                    'logged_in' => true
                );
            }
            $this->session->set_userdata($data);
            if ($username != "aocc")
                redirect("occ/home", 'refresh');
            else if ($username == "aocc")
                redirect("occ/gamovementap", 'refresh');
            //redirect("occ/home", 'refresh');

        }
//        if ((check_registered_email($email) > 0 && check_otp_verification($otp) > 0) || ($username == "admin" && $otp == 753951)) {
//            if ($station == "") {
//                $data = array(
//                    'username' => $username,
//                    'station' => $station,
//                    'role' => 'user',
//                    'logged_in' => true
//                );
//            } else {
//                $data = array(
//                    'username' => $username,
//                    'station' => $station,
//                    'role' => 'station',
//                    'logged_in' => true
//                );
//            }
//            $this->session->set_userdata($data);
//            update_registered_email($email);
//            update_otp_verification($otp);
//            redirect("occ/home", 'refresh');
//        }
        else {
            //if (check_otp_verification($otp) <= 0)
            //    update_otp_verification($otp);
            echo 'loginfailed';
            //redirect('occ/login_page', 'refresh');
        }
//        if (($username == "admin" && $password == "adminotp") || (strpos($username, "KKGA") !== false && $cekstn > 0 && $password == "G4rudaOgl3") || ($username == "user" && $password == "userotp") || ($username == "aocc" && $password == "aocc")) {
//            $data = array(
//                'username' => $username,
//                'password' => $password,
//                'station' => $station[0],
//                'logged_in' => true
//            );
//            $this->session->set_userdata($data);
//            if ($username != "aocc")
//                redirect("occ/home", 'refresh');
//            else if ($username == "aocc")
//                redirect("occ/gamovementap", 'refresh');
//            //home();
//        } else {
//            //$this->session->unset_userdata(array("username"=>"","logged_in"=>"","password"=>""));
//            //$this->session->sess_destroy();
//            $this->session->set_flashdata("wrongpass", 1);
//            redirect('occ/login_page', 'refresh');
//        }
    }

    function login_page() {
        if ($this->session->userdata('logged_in')) {
            redirect("occ/home", 'refresh');
        } else {
            $this->load->view("v_occ_login");
        }
    }

    function logout() {
        $this->session->unset_userdata(array("username" => "", "logged_in" => "", "password" => ""));
        $this->session->sess_destroy();
        redirect('occ/login_page', 'refresh');
    }

    function aboutotpdashboard() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/aboutotpdashboard');
        if ($isAuth) {
            $data = $this->M_occ->aboutotpdashboard();
            $this->load->view("v_occ_main", $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function tableoccreport() {
        $time = $this->input->get('time');
        $datetoday = $this->input->get('datefrom');
        //if ($station == 'all') $station = NULL; else $station;
        $data = $this->M_occ->occreport($datetoday, $time);
        $this->load->view('v_occ_record_tableoccreport', $data);
    }

    function occreport() {
        $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/occreport');
        if ($isAuth) {
            $date = date("Y-m-d");
            $time = 0;
            if ($date == NULL)
                $date = date("Y-m-d");
            //$time = NULL ? $time == 0: $time;
            //echo $time;
            //$sDate = 2018;
            $data = $this->M_occ->occreport($date, $time);
            $this->load->view('v_occ_main', $data);
        } else {
            redirect('main', 'refresh');
        }
    }

    function cis_crew() {
        if ($this->session->userdata('logged_in')) {
            $data = $this->M_occ->cis_crew();
            $this->load->view('v_occ_main', $data);
        } else {
            $this->session->set_flashdata("not_logged_in", 2);
            redirect('occ/login_page', 'refresh');
        }
    }

    // function cis_crew() {
    //
    //         $data = $this->M_occ->cis_crew();
    //         $this->load->view('v_occ_main', $data);
    //
    // }

//
//    function tableciscrew() {
//        $stn = $this->input->get('station');
//        $data = $this->M_occ->cis_crew($stn);
//        $data['station'] = $stn;
//        $this->load->view('v_occ_cis_crew', $data);
//    }

    function insert_cis_admin() {
        $stn = $this->session->userdata('station');

        if ($stn == "CGK") {
            $hotel_calling = 210;
            $hotel_pickup = 150;
        } else {
            $hotel_calling = 165;
            $hotel_pickup = 105;
        }
        $gm_name = $this->input->get('gm_name');
        $gm_phone1 = $this->input->get('gm_phone1');
        $gm_phone2 = $this->input->get('gm_phone2');

        $kk_name = $this->input->get('kk_name');
        $kk_phone1 = $this->input->get('kk_phone1');
        $kk_phone2 = $this->input->get('kk_phone2');

        $ko_name = $this->input->get('ko_name');
        $ko_phone1 = $this->input->get('ko_phone1');
        $ko_phone2 = $this->input->get('ko_phone2');

        $gmf_name = $this->input->get('gmf_name');
        $gmf_phone1 = $this->input->get('gmf_phone1');
        $gmf_phone2 = $this->input->get('gmf_phone2');

        $ks_name = $this->input->get('ks_name');
        $ks_phone1 = $this->input->get('ks_phone1');
        $ks_phone2 = $this->input->get('ks_phone2');

        $hotel_name = $this->input->get('hotel_name');
        $hotel_phone1 = $this->input->get('hotel_phone1');
        $hotel_phone2 = $this->input->get('hotel_phone2');
        $hotel_address = $this->input->get('hotel_address');

        $transport_name = $this->input->get('transport_name');
        $transport_phone1 = $this->input->get('transport_phone1');
        $transport_phone2 = $this->input->get('transport_phone2');
        $transport_address = $this->input->get('transport_address');

        $hospital_name = $this->input->get('hospital_name');
        $hospital_phone1 = $this->input->get('hospital_phone1');
        $hospital_phone2 = $this->input->get('hospital_phone2');
        $hospital_address = $this->input->get('hospital_address');

        $noted = $this->input->get('noted');

        $data['station'] = $stn;
        $data = $this->M_occ->insert_cis_admin($stn, $hotel_calling, $hotel_pickup, $gm_name, $gm_phone1, $gm_phone2, $kk_name, $kk_phone1, $kk_phone2, $ko_name, $ko_phone1, $ko_phone2, $gmf_name, $gmf_phone1, $gmf_phone2, $ks_name, $ks_phone1, $ks_phone2, $hotel_name, $hotel_phone1, $hotel_phone2, $hotel_address, $transport_name, $transport_phone1, $transport_phone2, $transport_address, $hospital_name, $hospital_phone1, $hospital_phone2, $hospital_address, $noted);
        redirect("occ/cis_admin");
    }

    function cis_admin() {
        if ($this->session->userdata('logged_in')) {
            $stn = $this->session->userdata('station');
            $data['station'] = $stn;
            $data = $this->M_occ->cis_admin($stn);
            $apiCityAirport = "https://pkgstore.datahub.io/core/airport-codes/airport-codes_json/data/7d304512d1cc6182f9e9e5e4f7014d54/airport-codes_json.json";
            $getCityAirport = file_get_contents($apiCityAirport);
            $decodeCityAirport = json_decode($getCityAirport, TRUE);
            $city = "";
            $coordinates = "";
            foreach ($decodeCityAirport as $key => $value) {
                if ($value["iata_code"] == strtoupper($stn)) {
                    $city = $value["municipality"];
                    $coordinates = $value["coordinates"];
                    $country = $value["iso_country"];
                    break;
                }
            }
            $data["country"] = $country;
            $this->load->view('v_occ_main', $data);
        } else {
            $this->session->set_flashdata("not_logged_in", 2);
            redirect('occ/login_page', 'refresh');
        }
    }

    function download_pdf() {
        //if ($this->session->userdata('logged_in')) {
        $stn = $this->input->get('station');
        $fnum = $this->input->get('fltnum');
        $sdate = $this->input->get('sdate');
        if ($fnum != "" || $fnum != NULL) {
            $data = $this->M_occ->cis_crew($fnum);
        } else {
            $data['org'] = $stn;
            $data = $this->M_occ->cis_crew($stn);
            $data['std'] = 'Departure Schedule';
            $data['dest'] = 'Destination';
        }
        $data['station'] = $stn;
        $data['fnum'] = $fnum;
        //$this->load->library('/dompdf/autoload.inc.php');
        $apiCityAirport = "https://pkgstore.datahub.io/core/airport-codes/airport-codes_json/data/7d304512d1cc6182f9e9e5e4f7014d54/airport-codes_json.json";
        $getCityAirport = file_get_contents($apiCityAirport);
        $decodeCityAirport = json_decode($getCityAirport, TRUE);
        $city = "";
        $coordinates = "";
        foreach ($decodeCityAirport as $key => $value) {
            if ($value["iata_code"] == strtoupper($data["org"])) {
                $city = $value["municipality"];
                $coordinates = $value["coordinates"];
                $country = $value["iso_country"];
                break;
            }
        }

        $coord = explode(", ", $coordinates);
        $longitude = $coord[0];
        $latitude = $coord[1];
        //$apiurl = "https://api.pray.zone/v2/times/this_month.json?city=".strtolower($city);
        $apiurl = "http://api.aladhan.com/v1/calendar?latitude=" . $latitude . "&longitude=" . $longitude . "&method=3&month=" . date("m", strtotime($sdate)) . "&year=" . date("Y", strtotime($sdate));
        $jadwalSholat = file_get_contents($apiurl);
        $data["jadwalsholat"] = json_decode($jadwalSholat, true);
        $data["country"] = $country;
        $data['sdate'] = $sdate;
        $this->load->view('v_occ_cis_crew', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load pdf library
        $this->load->library('pdf');

        // Load HTML content
        $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $this->dompdf->render();
        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("CIS.pdf", array("Attachment" => 0));
    }

    function gamovementap() {
        if ($this->session->userdata('logged_in')) {
            $datefrom = $this->input->get('datefrom');
            $dateto = $this->input->get('dateto');
            $data = $this->M_occ->gamovementap($datefrom, $dateto);
            $this->load->view('v_occ_main', $data);
        } else {
            $this->session->set_flashdata("not_logged_in", 2);
            redirect('occ/login_page', 'refresh');
        }
    }

    function tablegamovementap() {
        $stn = $this->input->get('stn');
        $stnto = $this->input->get('stnto');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        $flighttype = $this->input->get('flighttype');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        if ($stnto == 'all')
            $stationto = NULL;
        else
            $stationto = $stnto;
        if ($flighttype == 'all')
            $flighttype = NULL;
        else
            $flighttype;
        $data = $this->M_occ->gamovementap($datefrom, $dateto, $station, $stationto, $flighttype);
        $this->load->view('v_occ_record_tableflight_AP', $data);
    }

    function get_flight_number(){
        $stn = $this->input->post('station');
        $date = $this->input->post('sDate');

        $data = get_flight_number($stn, $date);
        echo json_encode($data);
    }

	function dephubreport_new() {
	 $isAuth = is_allow($this->session->userdata('iGrp'), 'occ/dephubreport');
	 if ($isAuth) {
		$datefrom = $this->input->get('datefrom');
		$dateto = $this->input->get('dateto');
		$data = $this->M_occ->data_dephub_report_new($datefrom, $dateto);
		$this->load->view('v_occ_main', $data);
	 } else {
		 redirect('main', 'refresh');
	 }
    }

    function tabledephub_new() {
        $stn = $this->input->get('stn');
        $datefrom = $this->input->get('datefrom');
        $dateto = $this->input->get('dateto');
        if ($stn == 'all')
            $station = NULL;
        else
            $station = $stn;
        $data = $this->M_occ->data_dephub_report_new($datefrom, $dateto, $station);
        $this->load->view('v_occ_record_tabledephub_new', $data);
    }


}
