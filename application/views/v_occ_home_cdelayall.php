<?php 
$sDate = get_dateotp('0 days');
$totalDelay = get_delay('count', $sDate);
if ($totalDelay <> 0) {
?>

<script>
jQuery(function(){

  var chart = AmCharts.makeChart( "chartCdelayAll", {
      "type": "pie",
      "theme": "light",
      "dataProvider": [
      <?php
      $sDate = get_dateotp('0 days');
      $nCOD = get_causeOfDelay('count', $sDate);
      $recCOD = get_causeOfDelay('rec', $sDate); 
      $totalDep = get_departed('count', $sDate); 
      $pTotalDelay = get_percentage($totalDelay, $totalDep, 100);
      $nAPTF = 0;
      $nCOMC = 0;
      $nFLOP = 0;
      $nOTHR = 0;
      $nSTNH = 0;
      $nSYST = 0;
      $nTECH = 0;
      $nWTHR = 0;

      if ($pTotalDelay <> 0) {
        foreach ($recCOD as $key => $row) {
          $sCODType = get_causeOfDelayText($row['REASONCODE']);
          if ($sCODType == 'APTF') $nAPTF = $nAPTF + $row['NCD'];
          if ($sCODType == 'COMC') $nCOMC = $nCOMC + $row['NCD'];
          if ($sCODType == 'FLOP') $nFLOP = $nFLOP + $row['NCD'];
          if ($sCODType == 'OTHR') $nOTHR = $nOTHR + $row['NCD'];
          if ($sCODType == 'STNH') $nSTNH = $nSTNH + $row['NCD'];
          if ($sCODType == 'SYST') $nSYST = $nSYST + $row['NCD'];
          if ($sCODType == 'TECH') $nTECH = $nTECH + $row['NCD'];
          if ($sCODType == 'WTHR') $nWTHR = $nWTHR + $row['NCD'];        
        }
        ?>
          {
            "causeofdelay": "APTF",
            "total": <?php echo get_percentage($nAPTF, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "COMC",
            "total": <?php echo get_percentage($nCOMC, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "FLOP",
            "total": <?php echo get_percentage($nFLOP, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "OTHR",
            "total": <?php echo get_percentage($nOTHR, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "STNH",
            "total": <?php echo get_percentage($nSTNH, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "SYST",
            "total": <?php echo get_percentage($nSYST, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "TECH",          
            "total": <?php echo get_percentage($nTECH, $nCOD, $pTotalDelay);?>
          }, {
            "causeofdelay": "WTHR",          
            "total": <?php echo get_percentage($nWTHR, $nCOD, $pTotalDelay);?>
          }
        ],
        "valueField": "total",
        "titleField": "causeofdelay",
        "startDuration": 1,
        "labelRadius": 2,
        "labelText": "[[title]]: [[value]] %",
        "innerRadius": "30%",
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]] %</b></span>",
        "angle": 30,
        "marginBottom": 0,
        "marginTop": -250,
        "pullOutRadius" : "20%",
        "height": 300,
        "outlineAlpha": 0.4,
      <?php } ?>
  });


}); 
</script>

<?php } ?>