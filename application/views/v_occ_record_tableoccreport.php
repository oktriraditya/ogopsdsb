<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />-->
<link href="<?php echo base_url(); ?>assets/global/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url(); ?>assets/global/css/fixedColumns.bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->

<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        vertical-align: middle;
        text-align: center;
        
    }



</style>
<script type="text/javascript">


</script>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> Laporan Operasional Penerbangan Garuda Indonesia digenerate tanggal <?php echo date("d M Y") ?> jam <?php echo $jam; ?> WIB</span>

        </div>  

    </div>
    <div class="portlet-body" id="copyEmail"> 
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >            
                <div class="portlet light">                     
                    <div class="portlet-body">
                        Yth. BOD,<br><br>
                        <?php
                        if (isset($timeFilter) && $timeFilter < 12)
                            echo "Selamat Pagi, ";
                        else if (isset($timeFilter) && ($timeFilter >= "12" && $timeFilter <= "15"))
                            echo "Selamat Siang, ";
                        else if (isset($timeFilter) && ($timeFilter >= "16" && $timeFilter <= "18"))
                            echo "Selamat Sore, ";
                        else if (isset($timeFilter) && ($timeFilter >= "19" && $timeFilter <= "23"))
                            echo "Selamat Malam, ";
                        ?>                                                    
                        Berikut kami sampaikan laporan operasional penerbangan tanggal <?php echo date("d M Y", strtotime($dOtp)) ?> sampai dengan jam <?php if ($timeFilter == 0) echo $jam;
                        else if ($timeFilter == 23) echo $timeFilter . ":59";
                        else echo $timeFilter . ":00"; ?> WIB, sbb:
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >            
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">PENCAPAIAN OTP</span>
                        </div>

                    </div>                        
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column">
                            <thead>
                                <tr>
                                    <th style="width: 30%" class="tengah">Subject</th>
                                    <th  class="tengah">All Station Statistics</th>
                                    <th  class="tengah">CGK Station Statistics</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td  class="tengah">Total Schedules</td>
                                    <td  class="tengah"><?php echo $nOnschedule ?></td>
                                    <td  class="tengah"><?php echo $nOnscheduleCgk ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Total Departures</td>
                                    <td  class="tengah"><?php echo $nDeparted ?></td>
                                    <td  class="tengah"><?php echo $nDepartedCgk ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Total Delay Departures</td>
                                    <td  class="tengah"><?php echo $nDelay ?></td>
                                    <td  class="tengah"><?php echo $nDelayCgk ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Total On-Time</td>
                                    <td  class="tengah"><?php echo $nOntime ?></td>
                                    <td  class="tengah"><?php echo $nOntimeCgk ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">OTP of Departure</td>
                                    <td  class="tengah"><?php
                                        if ($nDeparted > 0)
                                            echo round(($nOntime / $nDeparted) * 100, 1);
                                        else
                                            0;
                                        ?>%</td>
                                    <td  class="tengah"><?php
                                        if ($nDepartedCgk > 0)
                                            echo round(($nOntimeCgk / $nDepartedCgk) * 100, 1);
                                        else
                                            0;
                                        ?>%</td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Total Delay Arrival</td>
                                    <td  class="tengah"><?php echo $nDelayArrival ?></td>
                                    <td  class="tengah"><?php echo $nDelayCgkArrival ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">OTP of Arrival</td>
                                    <td  class="tengah"><?php
                                        if ($nDepartedArr > 0)
                                            echo round(($nOntimeArrival / $nDepartedArr) * 100, 1);
                                        else
                                            0;
                                        ?>%</td>
                                    <td  class="tengah"><?php
                                        if ($nDepartedCgkArr > 0)
                                            echo round(($nOntimeCgkArrival / $nDepartedCgkArr) * 100, 1);
                                        else
                                            0;
                                        ?>%</td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Average of Delay (minutes)</td>
                                    <td  class="tengah"><?php if (isset($get_average_delay)) echo round($get_average_delay, 2);
                                        else echo 0; ?></td>
                                    <td  class="tengah"><?php if (isset($get_average_delay_cgk)) echo round($get_average_delay_cgk, 2);
                                        else echo 0; ?></td>
                                </tr>
                                <tr>
                                    <td  class="tengah">Delay > 4 hours</td>
                                    <td  class="tengah"><?php if (isset($get_delay_four)) echo $get_delay_four;
                                        else echo 0; ?></td>
                                    <td  class="tengah"><?php if (isset($get_delay_four_cgk)) echo $get_delay_four_cgk;
                                        else echo 0; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">  
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">CAUSE OF DELAY</span>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <table class="tengah table table-striped table-bordered table-hover table-condensed table-checkable order-column ">
                            <thead>
                                <tr>
                                    <th class="tengah" width="30%" rowspan="2" >Delay Contributor</th>
                                    <th class="tengah" colspan="2">All Station</th>
                                    <th class="tengah" colspan="2">CGK Station</th>
                                </tr>
                                <tr>
                                    <th class="tengah">#Delay Code</th>
                                    <th class="tengah">Delay (%)</th>
                                    <th class="tengah">#Delay Code</th>
                                    <th class="tengah">Delay (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="tengah">
                                    <td>
                                        Technic
                                    </td>
                                    <td width="15%">
                                        <?php if (isset($ntech)) echo $ntech ?>
                                    </td>
                                    <td width="15%">
                                        <?php if (isset($ntech)) echo $ptech . "%" ?>
                                    </td>
                                    <td width="15%">
<?php if (isset($ntechcgk)) echo $ntechcgk ?>
                                    </td>
                                    <td width="15%">
<?php if (isset($ntechcgk)) echo $ptechcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Station Handling
                                    </td>
                                    <td>
                                        <?php if (isset($nstnh)) echo $nstnh ?>
                                    </td>
                                    <td>
                                        <?php if (isset($nstnh)) echo $pstnh . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($nstnhcgk)) echo $nstnhcgk ?>
                                    </td>
                                    <td>
<?php if (isset($nstnhcgk)) echo $pstnhcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Commercial
                                    </td>
                                    <td>
                                        <?php if (isset($ncomc)) echo $ncomc ?>
                                    </td>
                                    <td>
                                        <?php if (isset($ncomc)) echo $pcomc . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($ncomccgk)) echo $ncomccgk ?>
                                    </td>
                                    <td>
<?php if (isset($ncomccgk)) echo $pcomccgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        System
                                    </td>
                                    <td>
                                        <?php if (isset($nsyst)) echo $nsyst ?>
                                    </td>
                                    <td>
                                        <?php if (isset($nsyst)) echo $psyst . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($nsystcgk)) echo $nsystcgk ?>
                                    </td>
                                    <td>
<?php if (isset($nsystcgk)) echo $psystcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Flight Ops
                                    </td>
                                    <td>
                                        <?php if (isset($nflop)) echo $nflop ?>
                                    </td>
                                    <td>
                                        <?php if (isset($nflop)) echo $pflop . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($nflopcgk)) echo $nflopcgk ?>
                                    </td>
                                    <td>
<?php if (isset($nflopcgk)) echo $pflopcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Airport Facilties
                                    </td>
                                    <td>
                                        <?php if (isset($naptf)) echo $naptf ?>
                                    </td>
                                    <td>
                                        <?php if (isset($naptf)) echo $paptf . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($naptfcgk)) echo $naptfcgk ?>
                                    </td>
                                    <td>
<?php if (isset($naptfcgk)) echo $paptfcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Weather
                                    </td>
                                    <td>
                                        <?php if (isset($nwthr)) echo $nwthr ?>
                                    </td>
                                    <td>
                                        <?php if (isset($nwthr)) echo $pwthr . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($nwthrcgk)) echo $nwthrcgk ?>
                                    </td>
                                    <td>
<?php if (isset($nwthrcgk)) echo $pwthrcgk . "%" ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Misc
                                    </td>
                                    <td>
                                        <?php if (isset($nothr)) echo $nothr ?>
                                    </td>
                                    <td>
                                        <?php if (isset($nothr)) echo $pothr . "%" ?>
                                    </td>
                                    <td>
<?php if (isset($nothrcgk)) echo $nothrcgk ?>
                                    </td>
                                    <td>
<?php if (isset($nothrcgk)) echo $pothrcgk . "%" ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">OTP SPECIAL ROUTE</span>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column ">
                            <thead>
                                <tr >
                                    <th style="width: 15%" class="tengah">Statistics</th>
                                    <th class="tengah">CGK-SIN VV</th>
                                    <th class="tengah">CGK-KNO VV</th>
                                    <th class="tengah">CGK-JOG VV</th>
                                    <th class="tengah">CGK-SUB VV</th>
                                    <th class="tengah" >CGK-DPS VV</th>
                                    <th class="tengah">CGK-UPG VV</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //if (isset($get_otp_special_route)) echo "ada isi"; else echo"kosomng";
                                $schedulesin = 0;
                                $ontimesin = 0;
                                $arrontimesin = 0;
                                $schedarrsin = 0;
                                $schedulekno = 0;
                                $ontimekno = 0;
                                $arrontimekno = 0;
                                $schedarrkno = 0;
                                $schedulejog = 0;
                                $ontimejog = 0;
                                $arrontimejog = 0;
                                $schedarrjog = 0;
                                $schedulesub = 0;
                                $ontimesub = 0;
                                $arrontimesub = 0;
                                $schedarrsub = 0;
                                $scheduledps = 0;
                                $ontimedps = 0;
                                $arrontimedps = 0;
                                $schedarrdps = 0;
                                $scheduleupg = 0;
                                $ontimeupg = 0;
                                $arrontimeupg = 0;
                                $schedarrupg = 0;
                                foreach ($get_otp_special_route as $key => $value) {
                                    if (($value["ORG"] == "CGK" && $value["DEST"] == "SIN") OR ( $value["ORG"] == "SIN" && $value["DEST"] == "CGK")) {
                                        $schedulesin += $value["SCHEDULED"];
                                        $ontimesin += $value["ONTIME"];
                                        $arrontimesin += $value["ARRONTIME"];
                                        $schedarrsin += $value["SCHEDARR"];
                                    } else if (($value["ORG"] == "CGK" && $value["DEST"] == "KNO") OR ( $value["ORG"] == "KNO" && $value["DEST"] == "CGK")) {
                                        $schedulekno += $value["SCHEDULED"];
                                        $ontimekno += $value["ONTIME"];
                                        $arrontimekno += $value["ARRONTIME"];
                                        $schedarrkno += $value["SCHEDARR"];
                                    } else if (($value["ORG"] == "CGK" && $value["DEST"] == "JOG") OR ( $value["ORG"] == "JOG" && $value["DEST"] == "CGK")) {
                                        $schedulejog += $value["SCHEDULED"];
                                        $ontimejog += $value["ONTIME"];
                                        $arrontimejog += $value["ARRONTIME"];
                                        $schedarrjog += $value["SCHEDARR"];
                                    } else if (($value["ORG"] == "CGK" && $value["DEST"] == "SUB") OR ( $value["ORG"] == "SUB" && $value["DEST"] == "CGK")) {
                                        $schedulesub += $value["SCHEDULED"];
                                        $ontimesub += $value["ONTIME"];
                                        $arrontimesub += $value["ARRONTIME"];
                                        $schedarrsub += $value["SCHEDARR"];
                                    } else if (($value["ORG"] == "CGK" && $value["DEST"] == "DPS") OR ( $value["ORG"] == "DPS" && $value["DEST"] == "CGK")) {
                                        $scheduledps += $value["SCHEDULED"];
                                        $ontimedps += $value["ONTIME"];
                                        $arrontimedps += $value["ARRONTIME"];
                                        $schedarrdps += $value["SCHEDARR"];
                                    } else if (($value["ORG"] == "CGK" && $value["DEST"] == "UPG") OR ( $value["ORG"] == "UPG" && $value["DEST"] == "CGK")) {
                                        $scheduleupg += $value["SCHEDULED"];
                                        $ontimeupg += $value["ONTIME"];
                                        $arrontimeupg += $value["ARRONTIME"];
                                        $schedarrupg += $value["SCHEDARR"];
                                    }
                                }
                                ?>  
                                <tr class="tengah">
                                    <td>
                                        Actual Departure
                                    </td>
                                    <td>
                                        <?php echo $schedulesin; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulekno; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulejog; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulesub; ?>
                                    </td>
                                    <td>
<?php echo $scheduledps; ?>
                                    </td>
                                    <td>
<?php echo $scheduleupg; ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        Total Delay
                                    </td>
                                    <td>
                                        <?php echo $schedulesin - $ontimesin; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulekno - $ontimekno; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulejog - $ontimejog; ?>
                                    </td>
                                    <td>
                                        <?php echo $schedulesub - $ontimesub; ?>
                                    </td>
                                    <td>
<?php echo $scheduledps - $ontimedps; ?>
                                    </td>
                                    <td>
<?php echo $scheduleupg - $ontimeupg; ?>
                                    </td>
                                </tr>
                                <tr class="tengah">
                                    <td>
                                        OTP
                                    </td>
                                    <td>
                                        <?php echo $schedulesin > 0 ? round($ontimesin / $schedulesin * 100, 1) : 0; ?>%
                                    </td>
                                    <td>
                                        <?php echo $schedulekno > 0 ? round($ontimekno / $schedulekno * 100, 1) : 0; ?>%
                                    </td>
                                    <td>
                                        <?php echo $schedulejog > 0 ? round($ontimejog / $schedulejog * 100, 1) : 0; ?>%
                                    </td>
                                    <td>
                                        <?php echo $schedulesub > 0 ? round($ontimesub / $schedulesub * 100, 1) : 0; ?>%
                                    </td>
                                    <td>
<?php echo $scheduledps > 0 ? round($ontimedps / $scheduledps * 100, 1) : 0; ?>%
                                    </td>
                                    <td>
<?php echo $scheduleupg > 0 ? round($ontimeupg / $scheduleupg * 100, 1) : 0; ?>%
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >            
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-dark">DELAY INFORMATION</span>
                        </div>
                    </div>                        
                    <div class="portlet-body">
                        <?php
                        $messageTech = "";
                        $messageStnh = "";
                        $messageComc = "";
                        $messageSyst = "";
                        $messageFlop = "";
                        $messageAptf = "";
                        $messageWthr = "";
                        $messageOthr = "";
                        $noTech = 1;
                        $noStnh = 1;
                        $noComc = 1;
                        $noSyst = 1;
                        $noFlop = 1;
                        $noAptf = 1;
                        $noWthr = 1;
                        $noOthr = 1;
                        $menit = 0;
                        $menit2 = 0;
                        $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
                        $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
                        $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
                        $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
                        $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
                        $arrSYST = array('50', '55', '56', '57', '58');
                        $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
                        $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');
                        foreach ($get_info_delay as $key => $value) {
                            $menit = 0;
                            $menit2 = 0;
                            if (strpos(substr($value['DELAYLENGTH1'], 2), "H") !== false && strpos(substr($value['DELAYLENGTH1'], 2), "M") !== false) {
                                $hour = explode("H", substr($value['DELAYLENGTH1'], 2)); //$mins = explode("M", substr($data['delaycd1'][$i],2));
                                $inthour1 = (int) $hour[0];
                                $inthour2 = (int) $hour[1];
                                $menit = ($inthour1 * 60) + $inthour2;
                            } else if (strpos(substr($value['DELAYLENGTH1'], 2), "H") !== false) {
                                $hour = explode("H", substr($value['DELAYLENGTH1'], 2));
                                $inthour1 = (int) $hour[0];
                                $menit = $inthour1 * 60;
                            } else if (strpos(substr($value['DELAYLENGTH1'], 2), "M") !== false) {
                                $mins = explode("M", substr($value['DELAYLENGTH1'], 2));
                                $menit = $mins[0];
                            }
                            if (strpos(substr($value['DELAYLENGTH2'], 2), "H") !== false && strpos(substr($value['DELAYLENGTH2'], 2), "M") !== false) {
                                $hour = explode("H", substr($value['DELAYLENGTH2'], 2)); //$mins = explode("M", substr($data['delaycd1'][$i],2));
                                $inthour1 = (int) $hour[0];
                                $inthour2 = (int) $hour[1];
                                $menit2 = ($inthour1 * 60) + $inthour2;
                            } else if (strpos(substr($value['DELAYLENGTH2'], 2), "H") !== false) {
                                $hour = explode("H", substr($value['DELAYLENGTH2'], 2));
                                $inthour1 = (int) $hour[0];
                                $menit2 = $inthour1 * 60;
                            } else if (strpos(substr($value['DELAYLENGTH2'], 2), "M") !== false) {
                                $mins = explode("M", substr($value['DELAYLENGTH2'], 2));
                                $menit2 = $mins[0];
                            }
                            if (in_array($value["CD1"], $arrTECH)) {

                                $messageTech.= $noTech . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";
                                $noTech++;
                            }
                            if (in_array($value["CD2"], $arrTECH) && !in_array($value["CD1"], $arrTECH)) {
                                $messageTech.=$noTech . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";
                                $noTech++;
                            }
                            if (in_array($value["CD1"], $arrSTNH)) {

                                $messageStnh.= $noStnh . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";
                                $noStnh++;
                            }
                            if (in_array($value["CD2"], $arrSTNH) && !in_array($value["CD1"], $arrSTNH)) {
                                $messageStnh.= $noStnh . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noStnh++;
                            }
                            if (in_array($value["CD1"], $arrCOMC)) {

                                $messageComc.=$noComc . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";
                                $noComc++;
                            }
                            if (in_array($value["CD2"], $arrCOMC) && !in_array($value["CD1"], $arrCOMC)) {
                                $messageComc.= $noComc . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";
                                $noComc++;
                            }
                            if (in_array($value["CD1"], $arrSYST)) {

                                $messageSyst.= $noSyst . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noSyst++;
                            }
                            if (in_array($value["CD2"], $arrSYST) && !in_array($value["CD1"], $arrSYST)) {
                                $messageSyst.= $noSyst . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noSyst++;
                            }
                            if (in_array($value["CD1"], $arrFLOP)) {

                                $messageFlop.=$noFlop . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noFlop++;
                            }
                            if (in_array($value["CD2"], $arrFLOP) && !in_array($value["CD1"], $arrFLOP)) {
                                $messageFlop.= $noFlop . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noFlop++;
                            }
                            if (in_array($value["CD1"], $arrAPTF)) {

                                $messageAptf .=$noAptf . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noAptf++;
                            }
                            if (in_array($value["CD2"], $arrAPTF) && !in_array($value["CD1"], $arrAPTF)) {
                                $messageAptf .= $noAptf . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noAptf++;
                            }
                            if (in_array($value["CD1"], $arrWTHR)) {

                                $messageWthr.= $noWthr . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noWthr++;
                            }
                            if (in_array($value["CD2"], $arrWTHR) && !in_array($value["CD1"], $arrWTHR)) {
                                $messageWthr.= $noWthr . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noWthr++;
                            }
                            if (in_array($value["CD1"], $arrOTHR)) {

                                $messageOthr.= $noOthr . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit" : "") . " " . ($menit2 > 0 ? "dan delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noOthr++;
                            }
                            if (in_array($value["CD2"], $arrOTHR) && !in_array($value["CD1"], $arrOTHR)) {
                                $messageOthr.= $noOthr . ". " . $value["AIRCRAFTREG"] . "/" . $value["AIRCRAFTTYPE"] . " GA" . $value["FLTNUM"] . " rute " . $value["SCHED_DEPARTUREAIRPORT"] . "-" . $value["SCHED_ARRIVALAIRPORT"] . ($menit > 0 ? " delay " . $menit . " menit dan" : "") . " " . ($menit2 > 0 ? "delay " . $menit2 . " menit " : "") . $value["REMARKS"] . "<br>";

                                $noOthr++;
                            }
                        }
                        ?>
<?php if ($messageTech != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">TECHNIC</span>
                            </div><?php echo $messageTech; ?>
                            <br>
<?php } ?>
<?php if ($messageStnh != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">STATION HANDLING</span>
                            </div><?php echo $messageStnh; ?>
                            <br>
<?php } ?>
<?php if ($messageComc != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">COMMERCIAL</span>
                            </div><?php echo $messageComc; ?>
                            <br>
<?php } ?>
<?php if ($messageSyst != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">SYSTEM</span>
                            </div><?php echo $messageSyst; ?>
                            <br>
<?php } ?>
<?php if ($messageFlop != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">FLIGHT OPERATIONS</span>
                            </div><?php echo $messageFlop; ?>
                            <br>
<?php } ?>
<?php if ($messageAptf != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">AIRPORT FACILITIES</span>
                            </div><?php echo $messageAptf; ?>
                            <br>
<?php } ?>
<?php if ($messageWthr != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">WEATHER</span>
                            </div><?php echo $messageWthr; ?>
                            <br>
<?php } ?>
<?php if ($messageOthr != "") { ?>
                            <div class="caption">
                                <span class="caption-subject bold uppercase font-dark">OTHERS</span>
                            </div><?php echo $messageOthr; ?>
                            <br>
<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
