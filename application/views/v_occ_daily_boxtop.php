<script type="text/javascript">
jQuery(document).ready(function() { 
    $('#boxTopOntime').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'ontime';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $('#boxTopDelay').click(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var status = 'delay';

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": status},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);            
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });
});
</script>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">                                                
                        <span data-counter="counterup" data-value="<?php echo $pOTP;?>"><?php echo $pOTP;?></span>
                        <small class="font-green-jungle">% - OTP</small>
                    </h3>
                    <small class="font-green-jungle"><?php echo $stn;?> STATION, <?php echo date('d F Y', strtotime($sDate));?></small>
                </div>
                <div class="icon">
                    <i class="fa fa-line-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?php echo $pOTP;?>%;" class="progress-bar progress-bar-success green-jungle">
                        <span class="sr-only"><?php echo $pOTP;?>% on time performance</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> on time performance </div>
                    <div class="status-number"> <?php echo $pOTP;?>% </div>
                </div>
            </div>
            <br/>
            <div class="progress-info pull-right">
                <a class="btn red btn-sm" href="#boxData" id="boxTopOntime"> OTP Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>    
        </div>
    </div> 
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-yellow-soft">
                        <span data-counter="counterup" data-value="<?php echo $nDelay;?>"><?php echo $nDelay;?></span>
                        <small class="font-yellow-soft">flight delayed</small>
                    </h3>
                    <small class="font-yellow-soft"><?php echo $stn;?> STATION, <?php echo date('d F Y', strtotime($sDate));?></small>
                </div>
                <div class="icon">
                    <i class="fa fa-clock-o"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: <?php echo $pDelay;?>%;" class="progress-bar progress-bar-success yellow-soft">
                        <span class="sr-only"><?php echo $pDelay;?>% delay percentage</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> delay percentage </div>
                    <div class="status-number"> <?php echo $pDelay;?>% </div>
                </div>
            </div>
            <br/>
            <div class="progress-info pull-right">
                <a class="btn red btn-sm" href="#boxData" id="boxTopDelay"> Delayed Flight details
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div> 
        </div>
    </div>      
</div>