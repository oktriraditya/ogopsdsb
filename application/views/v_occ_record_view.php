<!-- BEGIN PAGE BASE CONTENT -->
<?php
foreach ($arrRecords as $key => $row) {                                
?>
<div class="tab-pane" id="tab_3">
    <div class="portlet box blue-sharp">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plane"></i>Ops Flight Data - GA <?php echo $row['FLTNUM'];?></div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->            
            <div class="form-horizontal" role="form">
                <div class="form-body">
                    <h3 class="form-section">Flight Info</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Flight Number:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> GA <?php echo $row['FLTNUM'];?>  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">AC/REG:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['AIRCRAFTREG'];?> </p>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Schedule Departure:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['SCHEDULED_DEPDT'];?> </p>
                                </div>
                            </div>
                        </div>   
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Actual Touchdown:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['ACTUAL_TOUCHDOWN'];?> </p>
                                </div>
                            </div>
                        </div>                         
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">AC/TYPE:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['AIRCRAFTTYPE'];?> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Schedule Arrival:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['SCHEDULED_ARRDT'];?> </p>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Actual Blockon:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['ACTUAL_BLOCKON'];?> </p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Departure Airport:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['DEPAIRPORT'];?> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Actual Blockoff:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['ACTUAL_BLOCKOFF'];?> </p>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Last Update:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['LAST_UPDATE'];?> </p>
                                </div>
                            </div>
                        </div>                     
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Arrival Airport:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['SCHED_ARRIVALAIRPORT'];?> </p>
                                </div>
                            </div>
                        </div>             
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Actual Takeoff:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['ACTUAL_TAKEOFF'];?> </p>
                                </div>
                            </div>
                        </div>                                                           
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Remarks:</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $row['REMARKS'];?> </p>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <?php
                    if (!empty($row['ACTUAL_BLOCKOFF'])) {                    
                        $iDelay = check_delay($row['SCHEDULED_DEPDT'], $row['ACTUAL_BLOCKOFF']);
                        if ($iDelay) {                   
                            $dDelay = get_dDelay($row['FLIGHTLEGREF']);
                    ?>
                    <h3 class="form-section">Delay Info</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Delay Length :</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $iDelay;?> </p>
                                </div>
                            </div>                            
                        </div>          
                        <?php foreach ($dDelay as $key => $rowDelay) {  ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Reason Code :</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo get_causeOfDelayText($rowDelay['REASONCODE']);?> (<?php echo $rowDelay['DURATION'];?>)</p>
                                </div>
                            </div>                                                        
                        </div>                       
                        <?php } ?>                             
                    </div>                    
                    <?php 
                        }
                    } 
                    ?>

                    <h3 class="form-section">Pax Info</h3>
                    <?php foreach($arrPax as $key => $rowPax) { ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Class :</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $rowPax['CLASS'];?> (<?php echo $rowPax['MODE'];?>)</p>
                                </div>
                            </div>                            
                        </div>                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6 bold">Pax :</label>
                                <div class="col-md-6">
                                    <p class="form-control-static"> <?php echo $rowPax['COUNT'];?> </p>
                                </div>
                            </div>                            
                        </div>         
                    </div>  
                    <?php } ?>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php } ?>