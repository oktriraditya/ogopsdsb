<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript">
jQuery(document).ready(function() { 
    $('#statusBox').change(function(){
        blockUI(boxTable);
        var sDate = $("#sDate").val();      
        var stn = $("#stn").val();
        var statusF = $("#statusBox").val();

        $.ajax({
            type    : "GET",
            url     : "<?php echo base_url(); ?>occ/tabledata",
            data    : {"sDate": sDate, "stn": stn, "status": statusF},
            success :
                function(msg) {
                    $("#boxTable").html(msg);
                    unblockUI(boxTable);   
                },
            error   : 
                function(){
                    unblockUI(boxTable);
                    $("#boxTable").html("<div class='alert alert-error'>No Data</div>");
                }
        });
    });

    $("#sample_1").on('click', '.view', function(){
        var id = this.id;
        var target_url = "<?php echo base_url().'occ/viewrec/';?>"+id;
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
    });
});
</script>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase"> <?php echo $stn;?> Station Flight Record, <?php echo date('d-m-Y', strtotime($sDate));?> (<?php echo $status;?>)</span>
        </div>   
        <div class="actions pull-left">
            <div class="table-actions-wrapper">
                <span>&nbsp;</span><span>&nbsp;</span>
                <select class="table-group-action-input form-control input-inline input-small input-sm" name="status" id="statusBox">
                    <option value="all" <?php if ($status == 'all') echo 'selected=selected';?>>All Schedule Plan</option>
                    <option value="cancelplan" <?php if ($status == 'cancelplan') echo 'selected=selected';?>>Cancel Plan</option>
                    <option value="onschedule" <?php if ($status == 'onschedule') echo 'selected=selected';?>>On Schedule</option>
                    <option value="departed" <?php if ($status == 'departed') echo 'selected=selected';?>>Departed</option>
                    <option value="delay" <?php if ($status == 'delay') echo 'selected=selected';?>>Delay</option>
                    <option value="ontime" <?php if ($status == 'ontime') echo 'selected=selected';?>>On Time</option>
                    <option value="cancel" <?php if ($status == 'cancel') echo 'selected=selected';?>>Cancel</option>
                </select>           
            </div> 
        </div>                  
    </div>
    <div class="portlet-body">    
        <div class="table-container">                   
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                <thead>
                    <tr>                                    
                        <th width="1%"> No. </th>
                        <th width="10%"> Flight Number </th>
                        <th width="10%"> Origin </th>
                        <th width="10%"> Destination </th>
                        <th width="10%"> STD </th>
                        <th width="10%"> STA </th>
                        <th width="10%"> ATD </th>
                        <th width="10%"> AC Reg </th>
                        <th width="10%"> AC Type </th>  
                        <th width="10%"> Status </th>
                        <?php if ($status == 'delay') echo '<th width="10%"> Cause Of Delay </th>';?>
                        <th width="10%"> Remarks </th>
                        <th width="10%"> Actions </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <th> </th>
                        <?php if ($status == 'delay') echo '<th> </th>';?>
                        <th> </th>
                        <th> </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php 
                    $i = 1;
                    foreach ($arrRecords as $key => $row) {                                
                    ?>                               
                    <tr class="odd gradeX">     
                        <td> <?php echo $i;?>. </td>
                        <td> GA <?php echo $row['FLTNUM'];?> </td>
                        <td> <?php echo $row['DEPAIRPORT'];?> </td>
                        <td> <?php echo $row['SCHED_ARRIVALAIRPORT'];?> </td>
                        <td> <?php echo $row['DEPTIME'];?> </td>
                        <td> <?php echo $row['ARRTIME'];?> </td>
                        <td> <?php echo $row['OFF_BLOCK'];?> </td>  
                        <td> <?php echo $row['AIRCRAFTREG'];?> </td>
                        <td> <?php echo $row['AIRCRAFTTYPE'];?> </td>
                        <?php 
                        if (!empty($row['OFF_BLOCK'])) { 
                            $diff = date_diff(date_create($row['DEPTIME']), date_create($row['OFF_BLOCK'])); 
                            if (date_create($row['OFF_BLOCK']) > date_create($row['DEPTIME'])) {
                                if (($diff->i > 15) || ($diff->h > 0)) echo '<td>Delay ('.$diff->format("%H:%I").')</td>';
                                else echo '<td>On Time</td>'; 
                                //echo '<td>'.$diff->h.'</td>'; 
                            } else echo '<td>On Time</td>';                      
                        } else
                            echo '<td>'.$row['STATUS'].'</td>';
                        ?>
                        <?php 
                        if ($status == 'delay') { 
                            $dCode = get_delaycode($row['FLIGHTLEGREF'], $row['FLTNUM']); 
                            $rCode = '';
                            foreach($dCode as $key => $cd) {
                                $dCodeText = get_causeOfDelayText($cd['REASONCODE']);
                                $rCode .= $dCodeText."(".$cd['REASONCODE'].')<br/>';
                            }
                        ?>
                        <td> <?php echo $rCode;?> </td>
                        <?php } ?>
                        <td> <?php echo $row['REMARKS'];?> </td>
                        <td>
                            <a class="btn btn-sm btn-outline grey-salsa view" data-target="#view" data-toggle="modal" id="<?php echo $row['FLIGHTLEGREF'];?>"><i class="fa fa-search"></i> View </a>
                        </td>                                    
                    </tr>
                    <?php $i++; } ?>                                
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Flight Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>