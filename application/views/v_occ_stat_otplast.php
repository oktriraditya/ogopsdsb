<script>
jQuery(function(){

  var chart = AmCharts.makeChart("chartOTPlast", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "mouseWheelZoomEnabled":true,
    "dataDateFormat": "YYYY-MM-DD",
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "balloon":{
          "drop":true,
          "adjustBorderColor":false,
          "color":"#ffffff"
        },
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 8,
        "classNameField": "bulletClass",
        "hideBulletsCount": 50,
        "lineThickness": 4,
        "title": "OTP",
        "useLineColorForBulletBorder": true,
        "valueField": "value",
        "balloonText": "<span style='font-size:12px;'>[[title]] on [[category]]:<br><span style='font-size:20px;'>[[value]]%</span> [[additional]]</span>", 
        "showBalloon": true,
        "animationPlayed": true,
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 80,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
    },
    "chartCursor": {
        "pan": true,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha":1,
        "cursorColor":"#258cbb",
        "limitToGraph":"g1",
        "valueLineAlpha":0.2,
        "valueZoomable":true
    },
    "valueScrollbar":{
      "oppositeAxis":false,
      "offset":50,
      "scrollbarHeight":10
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": true
    },
    "dataProvider": [
      <?php
        ini_set('max_execution_time', 300);

        $station = $stn;
        //$lastDate = get_dateotp('-5 days');
        //$nowDate = get_dateotp('0 days');
        $sDate = new DateTime($sDate);
        $eDate = new DateTime($eDate);

        for($i = $sDate; $i <= $eDate; $i->modify('+1 day')) {
          //get otp
          $nDeparted = get_departed('count', $i->format('Y-m-d'), NULL, $station);
          $nDelay = get_delay('count', $i->format('Y-m-d'), NULL, $station);
          $nOnTime = get_onTime('count', $i->format('Y-m-d'), NULL, $station);
          $nCancel = get_cancel('count', $i->format('Y-m-d'), NULL, $station);
          $nOTP = get_percentOTP($nOnTime, $nDeparted, $nCancel);

          echo "{";
          if ($i->format('Y-m-d') == date('Y-m-d')) echo "bulletClass: 'lastBullet',";
          echo "date: '".$i->format('Y-m-d')."',";     
          echo "value:".$nOTP;
          echo "},";
        }
      ?>
    ]
  });

  chart.addListener("rendered", zoomChart);

  zoomChart();

  function zoomChart() {
      chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
  }

}); 
</script>