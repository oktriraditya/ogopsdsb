<!-- BEGIN PAGE LEVEL STYLES -->
<!--<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE LEVEL PLUGINS -->

<!--<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
--><script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
.tengah {
  text-align: center;
  vertical-align: central;
}
.no-wrap{
  white-space: nowrap;
}

</style>
<script type="text/javascript">
jQuery(document).ready(function () {
  $("#viewFrame").on('load', function () {
    $(".loadingIframe").css('display', 'none');
  });
  $("#fltData").on('click', '.view', function () {
    $(".loadingIframe").css('display', 'block');
    var iframe = $("#viewFrame");

    var id = this.id.split(" ");
    //alert(id[3]);
    //var kata2 = id.split(":");
    var target_url = "<?php echo base_url() . 'occ/details_delay/'; ?>" + id[0] + "/" + id[1] + "/" + id[2] + "/" + id[3] + "/" + id[4] + "/" + id[5] + "/" + id[6];


    iframe.attr("src", target_url);
    //ajaxStart: function(){iframe.attr("src", "loading...");},
    //ajaxStop: function(){iframe.attr("src", target_url);}


  });
});

</script>
<style>
.loadingIframe{
  /*border: 16px solid #f3f3f3;
  border-top: 16px solid #3498db;
  border-radius: 50%;
  width: 100px;
  height: 100px;
  animation: spin 2s linear infinite;*/
  position: fixed;
  text-align: center;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-dark">
      <i class="fa fa-th-list font-dark"></i>
      <span class="caption-subject bold uppercase"> OTP per Day <?php //echo $stn.$org.$dest.$delay.$statuspp;  ?>, <?php echo date('d-m-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-m-Y', strtotime($dateto)); ?></span>
    </div>

  </div>

  <div class="portlet-body">
    <div class="tools"> </div>
    <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column no-wrap" id="fltData">
      <thead>
        <tr>
          <th width="100%" class="tengah">No.</th>
          <th width="100%" class="tengah">Departure Date</th>
          <th width="100%" class="tengah">#Departure</th>
          <th width="100%" class="tengah">#On-Time</th>
          <th width="100%" class="tengah">#Delay</th>
          <th width="100%" class="tengah">#Arr. On-Time</th>
          <th width="100%" class="tengah">OTP Departure</th>
          <th width="100%" class="tengah">OTP Arrival</th>
          <th width="100%" class="tengah">OTP First Departure</th>
          <th width="100%" class="tengah">OTP ZMD First Departure</th>
          <th width="100%" class="tengah">Average Delay Dep (mins)</th>
          <th width="100%" class="tengah">Average Delay Arr (mins)</th>
          <?php if (isset($delay) && $delay == "all") { ?>
            <th width="100%" class="tengah">#Delax Pax & Bag. Handling</th>
            <th width="100%" class="tengah">#Delay Cargo & Mail</th>
            <th width="100%" class="tengah">#Delay Ramp Handling</th>
            <th width="100%" class="tengah">#Delay Fueling</th>
            <th width="100%" class="tengah">#Delay Catering</th>
            <th width="100%" class="tengah">#Delay Technical</th>
            <th width="100%" class="tengah">#Delay System</th>
            <th width="100%" class="tengah">#Delay Flight Ops.& Crewing</th>
            <th width="100%" class="tengah">#Delay Weather</th>
            <th width="100%" class="tengah">#Delay Airport, Gov. Authorities & ATFM Restrictions</th>

            <th width="100%" class="tengah">#Delay Misc.</th>
                        <th width="100%" class="tengah">#Delay Reactionary</th>
            <th width="100%" class="tengah">%Delax Pax & Bag. Handling</th>
            <th width="100%" class="tengah">%Delay Cargo & Mail</th>
            <th width="100%" class="tengah">%Delay Ramp Handling</th>
            <th width="100%" class="tengah">%Delay Fueling</th>
            <th width="100%" class="tengah">%Delay Catering</th>
            <th width="100%" class="tengah">%Delay Technical</th>
            <th width="100%" class="tengah">%Delay System</th>
            <th width="100%" class="tengah">%Delay Flight Ops.& Crewing</th>
            <th width="100%" class="tengah">%Delay Weather</th>
            <th width="100%" class="tengah">%Delay Airport, Gov. Authorities & ATFM Restrictions</th>
            <th width="100%" class="tengah">%Delay Misc.</th>
            <th width="100%" class="tengah">%Delay Reactionary</th>
          <?php }if (isset($delay) && $delay == "origin") { ?>
          <?php }if (isset($delay) && $delay == "previous") { ?>
          <?php }?>
        </tr>
      </thead>
      <?php
      $data = array();
      $dataTotal = array();
      $dataTotal['AVGDELAYARR'] = 0;
      $dataTotal['technic'] = 0;
      $dataTotal['RMP'] = 0;
      $dataTotal['PAXBAG'] = 0;
      $dataTotal['SYST'] = 0;
      $dataTotal['FLOPS'] = 0;
      $dataTotal['APTF'] = 0;
      $dataTotal['WEATHER'] = 0;
      $dataTotal['MISC'] = 0;
      $dataTotal['CGM'] = 0;
      $dataTotal['FUEL'] = 0;
      $dataTotal['CATR'] = 0;
            $dataTotal['REAC'] = 0;
      $dataTotal['TOTALCOD'] = 0;
      $dataTotal['AVGDELAY'] = 0;
      $dataTotal['FIRSTFLIGHT'] = 0;
      $dataTotal['OTPFIRSTFLIGHT'] = 0;
      $dataTotal['ZMDFIRSTFLIGHT'] = 0;
      $dataTotal['schedule'] = 0;
      $dataTotal['ontime'] = 0;
      $dataTotal['arrontime'] = 0;
      $dataTotal['delay'] = 0;
      $dataTotal['SCHEDARR'] = 0;
      foreach ($daily_record_flight as $key => $value) {
        $data['schedule'][] = $value['SCHEDULED'];
        $data['tanggal'][] = $value['TANGGAL'];
        $data['ontime'][] = $value['ONTIME'];
        $data['arrontime'][] = $value['ARRONTIME'];
        $data['SCHEDARR'][] = $value['SCHEDARR'];
        $data['FIRSTFLIGHT'][] = $value['FIRSTFLIGHT'];
        $data['OTPFIRSTFLIGHT'][] = $value['OTPFIRSTFLIGHT'];
        $data['ZMDFIRSTFLIGHT'][] = $value['ZMDFIRSTFLIGHT'];
        $data['AVGDELAY'][] = $value['AVGDELAY'];
        $data['AVGDELAYARR'][] = $value['AVGDELAYARR'];
        $data['technic'][] = $value['TECH'];
        $data['RMP'][] = $value['RMP'];
        $data['PAXBAG'][] = $value['PAXBAG'];
        $data['SYST'][] = $value['SYST'];
        $data['FLOPS'][] = $value['FLOPS'];
        $data['APTF'][] = $value['APTF'];
        $data['WEATHER'][] = $value['WEATHER'];
        $data['MISC'][] = $value['MISC'];
        $data['CGM'][] = $value['CGM'];
        $data['FUEL'][] = $value['FUEL'];
        $data['CATR'][] = $value['CATR'];
              $data['REAC'][] = $value['REAC'];
        $data['TOTALCOD'][] = $value['PAXBAG'] + $value['CGM'] + $value['TECH'] + $value['RMP'] +  $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'] + $value['FUEL'] + $value['CATR'] + $value['REAC'];
      }
      /* foreach ($daily_record_cod as $key => $value){
      $data['AVGDELAY'][] = $value['AVGDELAY'];
      $data['AVGDELAYARR'][] = $value['AVGDELAYARR'];
      $data['technic'][] = $value['TECH'];
      $data['RMP'][] = $value['RMP'];
      $data['PAXBAG'][] = $value['PAXBAG'];
      $data['SYST'][] = $value['SYST'];
      $data['FLOPS'][] = $value['FLOPS'];
      $data['APTF'][] = $value['APTF'];
      $data['WEATHER'][] = $value['WEATHER'];
      $data['MISC'][] = $value['MISC'];
      $data['TOTALCOD'][] = $value['TECH']+$value['RMP']+$value['PAXBAG']+$value['SYST']+$value['FLOPS']+$value['APTF']+$value['WEATHER']+$value['MISC'];
      $data['PREVTECH'][] = $value['PREVTECH'];
      $data['PREVRMP'][] = $value['PREVRMP'];
      $data['PREVPAXBAG'][] = $value['PREVPAXBAG'];
      $data['PREVSYST'][] = $value['PREVSYST'];
      $data['PREVFLOPS'][] = $value['PREVFLOPS'];
      $data['PREVAPTF'][] = $value['PREVAPTF'];
      $data['PREVWEATHER'][] = $value['PREVWEATHER'];
      $data['PREVMISC'][] = $value['PREVMISC'];
      $data['TOTALCODPREV'][] = $value['PREVTECH']+$value['PREVRMP']+$value['PREVPAXBAG']+$value['PREVSYST']+$value['PREVFLOPS']+$value['PREVAPTF']+$value['PREVWEATHER']+$value['PREVMISC'];
      $data['ORGTECH'][] = $value['ORGTECH'];
      $data['ORGRMP'][] = $value['ORGRMP'];
      $data['ORGPAXBAG'][] = $value['ORGPAXBAG'];
      $data['ORGSYST'][] = $value['ORGSYST'];
      $data['ORGFLOPS'][] = $value['ORGFLOPS'];
      $data['ORGAPTF'][] = $value['ORGAPTF'];
      $data['ORGWEATHER'][] = $value['ORGWEATHER'];
      $data['ORGMISC'][] = $value['ORGMISC'];
      $data['TOTALCODORG'][] = $value['ORGTECH']+$value['ORGRMP']+$value['ORGPAXBAG']+$value['ORGSYST']+$value['ORGFLOPS']+$value['ORGAPTF']+$value['ORGWEATHER']+$value['ORGMISC'];
      $dataTotal['AVGDELAYARR'] = 0;
      $dataTotal['technic'] = 0;
      $dataTotal['RMP'] = 0;
      $dataTotal['PAXBAG'] = 0;
      $dataTotal['SYST'] = 0;
      $dataTotal['FLOPS'] = 0;
      $dataTotal['APTF'] = 0;
      $dataTotal['WEATHER'] = 0;
      $dataTotal['MISC'] = 0;
      $dataTotal['TOTALCOD'] = 0;
      $dataTotal['PREVTECH'] = 0;
      $dataTotal['PREVRMP'] = 0;
      $dataTotal['PREVPAXBAG'] = 0;
      $dataTotal['PREVSYST'] = 0;
      $dataTotal['PREVFLOPS'] = 0;
      $dataTotal['PREVAPTF'] = 0;
      $dataTotal['PREVWEATHER'] = 0;
      $dataTotal['PREVMISC'] = 0;
      $dataTotal['TOTALCODPREV'] = 0;
      $dataTotal['ORGTECH'] = 0;
      $dataTotal['ORGRMP'] = 0;
      $dataTotal['ORGPAXBAG'] = 0;
      $dataTotal['ORGSYST'] = 0;
      $dataTotal['ORGFLOPS'] = 0;
      $dataTotal['ORGAPTF'] = 0;
      $dataTotal['ORGWEATHER'] = 0;
      $dataTotal['ORGMISC'] = 0;
      $dataTotal['TOTALCODORG'] = 0;
      $dataTotal['AVGDELAY'] = 0;

    } */
    /*
    foreach($total_ontime as $key => $value)$data['ontime'][] = $value['ONTIME'];
    foreach ($total_arr_ontime as $key => $value)$data['arrontime'][] = $value['ARRONTIME'];
    foreach ($total_technic as $key => $value)$data['technic'][] = $value['TECHNIC'];
    foreach ($total_RMP as $key => $value)$data['RMP'][] = $value['RMP'];
    foreach ($total_PAXBAG as $key => $value)$data['PAXBAG'][] = $value['PAXBAG'];
    foreach ($total_syst as $key => $value)$data['SYST'][] = $value['SYST'];
    foreach ($total_flops as $key => $value)$data['FLOPS'][] = $value['FLOPS'];
    foreach ($total_aptf as $key => $value)$data['APTF'][] = $value['APTF'];
    foreach ($total_weather as $key => $value)$data['WEATHER'][] = $value['WEATHER'];
    foreach ($total_misc as $key => $value)$data['MISC'][] = $value['MISC'];
    foreach ($total_cod as $key => $value)$data['TOTALCOD'][] = $value['TOTALCOD']; */
    $count = count($daily_record_flight);
    ?>
    <tbody>
      <?php
      for ($i = 0; $i < $count; $i++) {
        ?>
        <tr class="tengah">
          <td> <?php echo $i + 1; ?> </td>
          <td> <?php echo $data['tanggal'][$i]; ?></td>
          <td> <?php $dataTotal['schedule']+= $data['schedule'][$i];
          echo $data['schedule'][$i]; ?> </td>
          <td> <?php $dataTotal['ontime']+= $data['ontime'][$i];
          echo $data['ontime'][$i]; ?></td>
          <td> <?php $dataTotal['delay']+= ($data['schedule'][$i] - $data['ontime'][$i]);
          echo $data['schedule'][$i] - $data['ontime'][$i]; ?></td>
          <td> <?php $dataTotal['arrontime']+= $data['arrontime'][$i];
          echo $data['arrontime'][$i]; ?></td>
          <td> <?php echo $data['schedule'][$i] > 0 ? round(($data['ontime'][$i] / $data['schedule'][$i]) * 100, 2) : 0; ?>%</td>
          <td> <?php $dataTotal['SCHEDARR']+= $data['SCHEDARR'][$i];
          if ($data['SCHEDARR'][$i] > 0) echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100, 2);
          else echo 0; ?>%</td>
          <td> <?php $dataTotal['OTPFIRSTFLIGHT']+= $data['OTPFIRSTFLIGHT'][$i];
          $dataTotal['FIRSTFLIGHT']+= $data['FIRSTFLIGHT'][$i];
          if ($data['FIRSTFLIGHT'][$i] > 0) echo round(($data['OTPFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2);
          else echo 0; ?>%</td>
          <td> <?php $dataTotal['ZMDFIRSTFLIGHT']+= $data['ZMDFIRSTFLIGHT'][$i];
          if ($data['FIRSTFLIGHT'][$i] > 0) echo round(($data['ZMDFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2);
          else echo 0; ?>%</td>

          <td> <?php if (($data['schedule'][$i] - $data['ontime'][$i]) > 0) {
            $dataTotal['AVGDELAY']+= $data['AVGDELAY'][$i];
            echo round($data['AVGDELAY'][$i], 1);
          } else echo 0; ?></td>
          <td> <?php if (($data['schedule'][$i] - $data['ontime'][$i]) > 0) {
            $dataTotal['AVGDELAYARR']+= $data['AVGDELAYARR'][$i];
            echo round($data['AVGDELAYARR'][$i], 1);
          } else echo 0; ?></td>
          <?php if (isset($delay) && $delay == "all") { ?>
            <td> <?php $dataTotal['PAXBAG']+= $data['PAXBAG'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PAXBAG'][$i]; ?> - Detail </a>  <?php //echo round(($data['PAXBAG'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
            <td> <?php $dataTotal['CGM']+= $data['CGM'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['CGM'][$i]; ?> - Detail </a>  <?php //echo round(($data['CGM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
            <td> <?php $dataTotal['RMP']+= $data['RMP'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['RMP'][$i]; ?> - Detail </a>  <?php // echo round(($data['RMP'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                        <td> <?php $dataTotal['FUEL']+= $data['FUEL'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['FUEL'][$i]; ?> - Detail </a>  <?php // echo round(($data['FUEL'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                                    <td> <?php $dataTotal['CATR']+= $data['CATR'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['CATR'][$i]; ?> - Detail </a>  <?php // echo round(($data['CATR'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
            <td> <?php $dataTotal['technic']+= $data['technic'][$i]; ?> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="1 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['technic'][$i]; ?> - Detail </a>
              <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
              <td> <?php $dataTotal['SYST']+= $data['SYST'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="4 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['SYST'][$i]; ?> - Detail  </a> <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
              <td> <?php $dataTotal['FLOPS']+= $data['FLOPS'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="5 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['FLOPS'][$i]; ?> - Detail </a>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['WEATHER']+= $data['WEATHER'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="7 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['WEATHER'][$i]; ?> - Detail </a>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
              <td> <?php $dataTotal['APTF']+= $data['APTF'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="6 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['APTF'][$i]; ?> - Detail </a>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
              <td> <?php $dataTotal['MISC']+= $data['MISC'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['MISC'][$i]; ?> - Detail </a>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                    <td> <?php $dataTotal['REAC']+= $data['REAC'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['REAC'][$i]; ?> - Detail </a>  <?php //echo round(($data['REAC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>


              <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['PAXBAG'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                else echo 0; ?>%</td>
                <td><?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['CGM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                  else echo 0; ?>%</td>
                  <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['RMP'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                    <td><?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['FUEL'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                      else echo 0; ?>%</td>
                      <td><?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['CATR'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                  <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?> %</td>

                      <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                        <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                          else echo 0; ?>%</td>
                          <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                            else echo 0; ?>%</td>
                          <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                            else echo 0; ?>%</td>

                              <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                                else echo 0; ?>%</td>
                                <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['REAC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                                  else echo 0; ?>%</td>

                                  <?php }if (isset($delay) && $delay == "origin") { ?>
                                  <?php }if (isset($delay) && $delay == "previous") { ?>
                                  <?php }?>
                                </tr>
                              <?php } ?>
                              <tr class="tengah">
                                <?php
                                $dataTotal['TOTALCOD'] = $dataTotal['technic'] + $dataTotal['RMP'] + $dataTotal['PAXBAG'] + $dataTotal['SYST'] + $dataTotal['FLOPS'] + $dataTotal['APTF'] + $dataTotal['WEATHER'] + $dataTotal['MISC'] + $dataTotal['CATR'] + $dataTotal['FUEL'] + $dataTotal['REAC'] + $dataTotal['CGM'];
                                ?>
                                <td> -</td>
                                <td> Total</td>
                                <td> <?php echo $dataTotal['schedule']; ?> </td>
                                <td> <?php echo $dataTotal['ontime']; ?></td>
                                <td> <?php echo $dataTotal['delay']; ?></td>
                                <td> <?php echo $dataTotal['arrontime']; ?></td>
                                <td> <?php echo $dataTotal['schedule'] > 0 ? round($dataTotal['ontime'] / $dataTotal['schedule'] * 100, 2) : 0 ?>% </td>
                                <td> <?php echo $dataTotal['SCHEDARR'] > 0 ? round($dataTotal['arrontime'] / $dataTotal['SCHEDARR'] * 100, 2) : 0 ?>% </td>
                                <td> <?php echo $dataTotal['FIRSTFLIGHT'] > 0 ? round($dataTotal['OTPFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2) : 0 ?>% </td>
                                <td> <?php echo $dataTotal['FIRSTFLIGHT'] > 0 ? round($dataTotal['ZMDFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2) : 0 ?>% </td>
                                <td> <?php echo round($dataTotal['AVGDELAY'] / $count, 1); ?></td>
                                <td> <?php echo round($dataTotal['AVGDELAYARR'] / $count, 1); ?></td>
                                <?php if (isset($delay) && $delay == "all") { ?>
                                  <td> <?php echo $dataTotal['PAXBAG']; ?></td>
                                  <td> <?php echo $dataTotal['CGM']; ?></td>
                                  <td> <?php echo $dataTotal['RMP']; ?></td>
                                  <td> <?php echo $dataTotal['FUEL']; ?></td>
                                  <td> <?php echo $dataTotal['CATR']; ?></td>
                                  <td> <?php echo $dataTotal['technic']; ?></td>
                                  <td> <?php echo $dataTotal['SYST']; ?></td>
                                  <td> <?php echo $dataTotal['FLOPS']; ?></td>
                                                              <td> <?php echo $dataTotal['WEATHER']; ?></td>
                                  <td> <?php echo $dataTotal['APTF']; ?></td>

                                  <td> <?php echo $dataTotal['MISC']; ?></td>
                                      <td> <?php echo $dataTotal['REAC']; ?></td>

                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['PAXBAG'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td><?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['CGM'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['RMP'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td><?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['FUEL'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td><?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['CATR'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['technic'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['SYST'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['FLOPS'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['WEATHER'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['MISC'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>
                                  <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['REAC'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                                  else echo 0; ?>%</td>

                                <?php }if (isset($delay) && $delay == "origin") { ?>
                                <?php }if (isset($delay) && $delay == "previous") { ?>
                                <?php } ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div id="view" class="modal container fade" tabindex="-1">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">View Delay Detail</h4>
                        </div>
                        <div class="modal-body" style="height:450px !important;">
                          <div class="loadingIframe">Please wait...</div>
                          <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
                          </iframe>

                        </div>

                        <div class="modal-footer">
                          <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                        </div>
                      </div>

                      <!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
                      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
                      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
                      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
                      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
                      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
                      <script type="text/javascript">
                      $(document).ready(function () {
                        $('#fltData').DataTable({
                          dom: 'Bfrtip',
                          pagingType: 'full_numbers',
                          bSort: true,
                          lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
                          buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5'],
                          scrollX: true,
                          autoWidth: true
                        });
                      });
                      </script>
