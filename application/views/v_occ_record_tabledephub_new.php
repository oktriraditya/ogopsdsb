<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url();?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
        .tengah {
            text-align: center;
            vertical-align: central;
        }

</style>
<script type="text/javascript">
jQuery(document).ready(function() {
    $("#fltData").on('click', '.view', function(){
        var id = this.id.split(" ");
        //alert(id[1]);
        //var kata2 = id.split(":");
        var target_url = "<?php echo base_url().'occ/details_delay/';?>"+id[0]+"/"+id[1]+"/"+id[2];
        var iframe = $("#viewFrame");
        iframe.attr("src", target_url);
        //ajaxStart: function(){iframe.attr("src", "loading...");},
        //ajaxStop: function(){iframe.attr("src", target_url);}


    });
});
</script>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> OTP Report for Dephub, <?php echo date('d-M-Y', strtotime($datefrom));?> <?php if($dateto) echo 'to '.date('d-M-Y', strtotime($dateto));?></span>
        </div>

    </div>

    <div class="portlet-body">
    <div class="tools"> </div>
    <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column " id="fltData">
        <thead>
            <tr>
                <th width="5%" class="tengah">No.</th>
                <th width="5%" class="tengah">Date (CGK Time)</th>
                <th width="5%" class="tengah">Route</th>
                <th width="5%" class="tengah">Aircraft Type</th>
                <th width="5%" class="tengah">Flight Number</th>
                <th width="5%" class="tengah">Service Area</th>
                <th width="5%" class="tengah">STD (UTC)</th>
                <th width="5%" class="tengah">ATD (UTC)</th>
                <th width="5%" class="tengah">STA (UTC)</th>
                <th width="5%" class="tengah">ATA (UTC)</th>
                <th width="5%" class="tengah">TO CNL</th>
                <th width="5%" class="tengah">NTO CNL</th>
                <th width="5%" class="tengah">CUA CNL</th>
                <th width="5%" class="tengah">LAIN CNL</th>
                <th width="5%" class="tengah">Delay Code</th>
                <th width="5%" class="tengah">Pax Handling of Delay</th>
                <th width="5%" class="tengah">Compensation</th>
                <th width="5%" class="tengah">Remarks</th>
                <th width="5%" class="tengah">Pax Adult</th>
                <th width="5%" class="tengah">Pax Child (N/A)</th>
                <th width="5%" class="tengah">Pax Infant (N/A)</th>
            </tr>
        </thead>
        <?php
        $data = array();
        foreach ($daily_record_per_flight as $key => $value) {
            $data['TANGGAL'][] = $value['TANGGAL'];
            $data['FLTNUM'][] = $value['FLTNUM'];
            $data['AIRCRAFTREG'][] = $value['AIRCRAFTREG'];
            $data['AIRCRAFTTYPE'][] = $value['AIRCRAFTTYPE'];
            $data['SCHED_DEPARTUREAIRPORT'][] = $value['SCHED_DEPARTUREAIRPORT'];
            $data['SCHED_ARRIVALAIRPORT'][] = $value['SCHED_ARRIVALAIRPORT'];
            $data['SCHEDULED_DEPDT_UTC'][] = new DateTime($value['SCHEDULED_DEPDT_UTC']);
            $data['SCHEDULED_ARRDT_UTC'][] = new DateTime($value['SCHEDULED_ARRDT_UTC']);
            $data['ACTUAL_BLOCKOFF_UTC'][] = new DateTime($value['ACTUAL_BLOCKOFF_UTC']);
            $data['ACTUAL_BLOCKON_UTC'][] = new DateTime($value['ACTUAL_BLOCKON_UTC']);
            $data['OTPDEP'][] = $value['OTPDEP'];
            $data['OTPARR'][] = $value['OTPARR'];
            $data['OTPZMD'][] = $value['OTPZMD'];
            $data['delaylength'][] = date_diff(new DateTime($value['SCHEDULED_DEPDT_UTC']),new DateTime($value['ACTUAL_BLOCKOFF_UTC']));
            $data['delaylengtharr'][] = date_diff(new DateTime($value['SCHEDULED_ARRDT_UTC']),new DateTime($value['ACTUAL_BLOCKON_UTC']));
            $data['cd1'][] = $value['CD1'];
            $data['cd2'][] = $value['CD2'];
            $data['delaycd1'][] = $value['DELAYLENGTH1'];
            $data['delaycd2'][] = $value['DELAYLENGTH2'];
            $data['ROUTE'][] = $value['ROUTE'];
            $data['ACT_PAX'][] = $value['ACT_PAX'];
            /*$data['technic'][] = $value['TECH'];
                    $data['STNH'][] = $value['STNH'];
                    $data['COMM'][] = $value['COMM'];
                    $data['SYST'][] = $value['SYST'];
                    $data['FLOPS'][] = $value['FLOPS'];
                    $data['APTF'][] = $value['APTF'];
                    $data['WEATHER'][] = $value['WEATHER'];
                    $data['MISC'][] = $value['MISC'];*/
        }
        $count = count($daily_record_per_flight);
        ?>
        <tbody>
            <?php
            $domstn=array('AMQ','BDO','BDJ','BEJ','BIK','BKS','BMU','BPN','BTH','BTJ','BUW','BWX','CGK','DJB','DJJ','DPS','DTB','ENE'
                ,'FLZ','GNS','GTO','JOG','JBB','KDI','KNG','KNO','KOE','KTG','LBJ','LOP','LSW','LUV','LUW','MDC','MJU','MKW','MKQ'
                ,'MLG','MOF','NBX','PDG','PGK','PKN','PKU','PKY','PLM','PLW','PNK','PSU','SBG','SOC','SOQ','SQG','SRG','SUB','SWQ'
                ,'SXK','TIM','TJQ','TNJ','TRK','TKG','TMC','TTE','UPG','RAQ','BMU','LLO','WNI','KSR');
            $intstn=array('PEK','BKK','CAN','HKG','ICN','JED','KIX','KUL','MEL','NRT','HND','PER','PVG','SIN','AMS','SYD','LHR','MED','BOM','CTU','XIY','CGO');
            $TO= array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89','52','36');
            $NTO = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96','40', '41', '42', '43', '44', '45', '46', '47', '48',
                '10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '37', '38', '39');
            $CUA = array('70', '71', '72', '73', '75', '76', '77');
            $LAIN = array('50', '55', '56', '57', '58','09', '14', '16', '25', '30', '91', '92', '51', '90', '93', '97', '98', '99');

            $arrAPTF = array('80', '81', '82', '83', '84', '85', '86', '87', '88', '89');
            $arrCOMC = array('09', '14', '16', '25', '30', '91', '92');
            $arrFLOP = array('01', '02', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '94', '95', '96');
            $arrOTHR = array('51', '52', '90', '93', '97', '98', '99');
            $arrSTNH = array('10', '11', '12', '13', '15', '17', '18', '20', '21', '22', '23', '24', '26', '27', '28', '29', '31', '32', '33', '34', '35', '36', '37', '38', '39');
            $arrSYST = array('50', '55', '56', '57', '58');
            $arrTECH = array('40', '41', '42', '43', '44', '45', '46', '47', '48');
            $arrWTHR = array('70', '71', '72', '73', '75', '76', '77');

            $sDiff = '+000000015 00:00:00.000000000';
            $firstdep = array();
            $delaylengthmin=0;
            $delaylengthplus=0;
            $uraian= 0;
            $penanganan = 0;
            for ($i = 0; $i < $count; $i++) {
                ?>
                <tr class="tengah">
                    <td> <?php echo $i + 1; ?> </td>
                    <td> <?php echo $data['TANGGAL'][$i]; ?></td>
                    <td> <?php echo $data['SCHED_DEPARTUREAIRPORT'][$i]; ?>-<?php echo $data['SCHED_ARRIVALAIRPORT'][$i]; ?></td>
                   <td> <?php echo $data['AIRCRAFTTYPE'][$i]; ?></td>
                    <td> GA<?php echo $data['FLTNUM'][$i]; ?> </td>
                    <td>  <?php echo $data['ROUTE'][$i];?></td>
                    <td> <?php echo $data['SCHEDULED_DEPDT_UTC'][$i]->format('H:i'); ?></td>
                    <td> <?php if ($data['cd1'][$i] == 'DLRA' && ($data['delaylength'][$i]->h > 0 || $data['delaylength'][$i]->i >15)) {
                        $data['SCHEDULED_DEPDT_UTC'][$i]->add(new DateInterval('PT15M')); echo $data['SCHEDULED_DEPDT_UTC'][$i]->format('H:i');
                    }else echo $data['ACTUAL_BLOCKOFF_UTC'][$i]->format('H:i');?></td>
                    <td> <?php echo $data['SCHEDULED_ARRDT_UTC'][$i]->format('H:i'); ?></td>
                    <td> <?php if ($data['cd1'][$i] == 'DLRA' && ($data['delaylength'][$i]->h > 0 || $data['delaylength'][$i]->i >15)) {
                        $data['SCHEDULED_ARRDT_UTC'][$i]->add(new DateInterval('PT15M')); echo $data['SCHEDULED_ARRDT_UTC'][$i]->format('H:i');
                    }else echo $data['ACTUAL_BLOCKON_UTC'][$i]->format('H:i');?></td>
                    <td> <?php echo 0;?> </td>
                    <td> <?php echo 0;?> </td>
                    <td> <?php echo 0;?> </td>
                    <td> <?php echo 0;?> </td>
                    <td> <?php if ($data['cd1'][$i] != 'DLRA')echo $data['cd1'][$i]; ?></td>

                    <?php if ($data['cd1'][$i] != 'DLRA')if ($data['delaylength'][$i]->invert){$delaylength = -1*$data['delaylength'][$i]->h*60 + -1*$data['delaylength'][$i]->i;}
                    else{$delaylength = $data['delaylength'][$i]->h*60 + $data['delaylength'][$i]->i;}

                    if ((15 <= $delaylength && $delaylength <= 30)) {
                        $uraian = "Penyampaian informasi delay";
                        $penanganan = "-";
                    } else if ((31 <= $delaylength) && ($delaylength <= 105)) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan";

                    } else if ((106 <= $delaylength) && ($delaylength <= 165)) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan & snack box (ditawarkan opsi transfer atau refund)";

                    } else if ((166 <= $delaylength) && ($delaylength <= 225)) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan & makanan berat (ditawarkan opsi transfer atau refund)";

                    } else if ((226 <= $delaylength) && ($delaylength <= 285)) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan, snack box, & makanan berat (ditawarkan opsi transfer atau refund)";

                    } else if ((286 <= $delaylength) && ($delaylength <= 405)) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan, snack box, makanan berat, & delay compensation (ditawarkan opsi transfer atau refund)";
                    } else if (406 <= $delaylength) {
                        $uraian = "Penyampaian informasi delay & pemberian informasi";
                        $penanganan = "Minuman ringan, snack box, makanan berat, delay compensation, hotel, & transportasi (ditawarkan opsi transfer atau refund)";
                    } else {
                      $uraian = "-";
                      $penanganan = "-";
                    }
                    ?>
                    <td> <?php echo $uraian; ?></td>
                    <td> <?php echo $penanganan;?> </td>
                    <td> <?php if (in_array($data['cd1'][$i], $arrAPTF))echo 'Delay due to Airport Facility';
                    else if (in_array($data['cd1'][$i], $arrCOMC))echo 'Delay due to Commercial';
                    else if (in_array($data['cd1'][$i], $arrFLOP))echo 'Delay due to Flight Operation';
                    else if (in_array($data['cd1'][$i], $arrOTHR))echo 'Delay due to others';
                    else if (in_array($data['cd1'][$i], $arrSTNH))echo 'Delay due Station Handling';
                    else if (in_array($data['cd1'][$i], $arrSYST))echo 'Delay due System';
                    else if (in_array($data['cd1'][$i], $arrTECH))echo 'Delay due Technic';
                    else if (in_array($data['cd1'][$i], $arrWTHR))echo 'Delay due Weather';?></td>
                    <td> <?php echo $data['ACT_PAX'][$i]; ?> </td>
                    <td> <?php echo "-";?> </td>
                    <td> <?php echo "-";?> </td>

                <?php } ?>
                </tr>
        </tbody>
    </table>
</div>
</div>
    <div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>
    <div class="modal-body" style="height:450px !important;">
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true,
            autoWidth: true
        });
    });
</script>
