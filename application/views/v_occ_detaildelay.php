<!DOCTYPE html>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/pages/scripts/date-time.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-blockui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVE STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA SCRIPTS BEGIN-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!--TABLE FLIGHT DATA SCRIPTS END-->

<!--TABLE FLIGHT DATA STYLES BEGIN-->
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--TABLE FLIGHT DATA STYLES END-->

<!--css begin-->
    <style type="text/css">
        .kotak {
            margin: auto;
            background-color: white;
            width: 100%;
            padding: 10px 10px 10px 10px;
        }
    
        .tengah {
            text-align: center;
        }
        
        #judul {
            text-align: center;
            font-family: Corbel;
            font-size: 24;
            
        }
    </style>
<!--css end-->

<!--FLIGHT NUMBER-->
<!--table flight number begin-->        
    <div class="kotak">
        <h3 id="judul"> DETAIL DELAY DUE TO <?php if(isset($typeCOD) && $typeCOD == 1)echo 'TECHNIC';
        if(isset($typeCOD) && $typeCOD == 2)echo 'STATION HANDLING';
        if(isset($typeCOD) && $typeCOD == 3)echo 'COMMERCIAL';
        if(isset($typeCOD) && $typeCOD == 4)echo 'SYSTEM';
        if(isset($typeCOD) && $typeCOD == 5)echo 'FLIGHT OPERATIONS';
        if(isset($typeCOD) && $typeCOD == 6)echo 'AIRPORT FACILITY';
        if(isset($typeCOD) && $typeCOD == 7)echo 'WEATHER';
        if(isset($typeCOD) && $typeCOD == 8)echo 'MISCELANOUS';?> </h3>
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column" id="fltData2">
            <thead>
                <tr>
                    <th width="1%" class="tengah">No.</th>
                    <th width="10%" class="tengah">Flight Number</th>
                    <th width="10%" class="tengah">Aircraft Reg</th>
                    <th width="10%" class="tengah">Origin</th>
                    <th width="10%" class="tengah">Destination</th>
                    <th width="10%" class="tengah">Schedule Date</th>
                    <th width="10%" class="tengah">Arrival Date</th>
                    <th width="10%" class="tengah">Actual Departure</th>
                    <th width="10%" class="tengah">Actual Arrival</th>
                    <th width="10%" class="tengah">Delay Length (Total Delay Code 1 dan 2)</th>
                    <th width="10%" class="tengah">Delay Code</th>
                    <th width="19%" class="tengah">Remarks</th>
                    <th width="10%" class="tengah">Impact</th>
                </tr>
            </thead>
        <?php
                $data = array();
                //if(isset($detail_cod))echo count($detail_cod);
                foreach ($detail_cod as $key => $row){
                    $data['origin'][] =  $row['SCHED_DEPARTUREAIRPORT'];
                    $data['destination'][] = $row['SCHED_ARRIVALAIRPORT'];
                    $data['depdate'][] =  $row['SCHEDULED_DEPDT_LC'];
                    $data['arrdate'][] = $row['SCHEDULED_ARRDT_LC'];
                    $data['actualdepdate'][] =  $row['ACTUAL_BLOCKOFF_LC'];
                    $data['actualarrdate'][] = $row['ACTUAL_BLOCKON_LC'];
                    $data['fltnum'][] =  $row['FLTNUM'];
                    $data['aircraftreg'][] = $row['AIRCRAFTREG'];
                    $data['aircrafttype'][] =  $row['SCHED_AIRCRAFTTYPE'];
                    $data['delaylength'][] = date_diff(new DateTime($row['SCHEDULED_DEPDT_LC']),new DateTime($row['ACTUAL_BLOCKOFF_LC']));
                    $data['remark'][] =  $row['REMARKS'];
                    $data['REASONCODE'][] =  $row['REASONCODE'];
                    $data['impact'][] = $row['ONWARD_FLTNUM'];
                }
                $count = count($detail_cod);
                ?>
            <tbody>
                <?php
                
                for ($i=0;$i<$count;$i++){ 
                ?>
                <tr class="tengah">
                    <td> <?php echo $i+1; ?> </td>
                    <td> GA<?php echo $data['fltnum'][$i];?> / <?php echo $data['aircrafttype'][$i];?></td>
                    <td> <?php echo $data['aircraftreg'][$i];?> </td>
                    <td> <?php echo $data['origin'][$i];?></td>
                    <td> <?php echo $data['destination'][$i]?></td>
                    <td> <?php echo $data['depdate'][$i];?></td>
                    <td> <?php echo $data['arrdate'][$i];?></td>
                    <td> <?php echo $data['actualdepdate'][$i];?></td>
                    <td> <?php echo $data['actualarrdate'][$i];?></td>
                    <td> <?php echo $data['delaylength'][$i]->format("%r%H:%I");?></td>
                    <td> <?php echo $data['REASONCODE'][$i];?></td>
                    <td> <?php echo $data['remark'][$i];?> </td>
                    <td> <?php if ($i==0)echo "-";else if($data['aircraftreg'][$i] != $data['aircraftreg'][$i-1])echo "-"; else echo "impact from ".$data['aircraftreg'][$i-1]." / GA".$data['fltnum'][$i-1];?></td>
                    
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData2').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: false,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: [ 'copy', 'excel', 'pdf' ],
            scrollX: true
        });
    });
</script>

    