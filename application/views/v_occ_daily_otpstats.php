<script>
jQuery(function(){

  var chart = AmCharts.makeChart( "chartOTPstats", {
    "type": "serial",
    "hideCredits":true,
    "addClassNames": true,
    "theme": "light",
    "autoMargins": true,
    "precision": 2,
    "balloon": {
      "adjustBorderColor": false,
      "horizontalPadding": 10,
      "verticalPadding": 8,
      "color": "#ffffff"
    },    
    dataProvider: [ 
      <?php
      $dOtp = $sDate;
      
      if ($stn == 'all') $station = NULL;
      else $station = $stn;

      if ($sDate == date('Y-m-d')) {
        if (date('H') == 23) $nowTime = 24;
        else $nowTime = get_hourotp('0 hours');
      } else $nowTime = 24;      
      
      $lastTime = ($nowTime/2)-1;
      
      for($i=$lastTime;$i>=0;$i--) {        
        if ($i!=0) {          
          $limitTime = $nowTime - (2*$i);
          $sTime = $dOtp.'T00:00:00';
          $eTime = $dOtp.'T'.$limitTime.':00:00';        
          
          $nDeparture = get_departByRangeTime('count', $sTime, $eTime, $station);          
          $nDelay = get_delayByRangeTime('count', $sTime, $eTime, $station);
          $nOnTime = get_ontimeByRangeTime('count', $sTime, $eTime, $station); 
          $nCancel = get_cancelByRangeTime('count', $sTime, $eTime, $station);
          $nOTP = get_percentOTP($nOnTime, $nDeparture, $nCancel);

          echo "{";
          //if ($i==1) echo "bulletClass: 'lastBullet',";
          echo "time:'".$limitTime.".00',";
          echo "departure: ".$nDeparture.",";
          echo "ontime: ".$nOnTime.",";          
          echo "delay: ".$nDelay.",";
          echo "cancel: ".$nCancel.",";          
          echo "otp :".$nOTP.",";
          echo "},";
        } else if($i==0) {
          if (date('H') == $nowTime) {
            $limitTime = $nowTime - (2*$i);
            $sTime = $dOtp.'T00:00:00';
            $eTime = $dOtp.'T'.$limitTime.':00:00';        
            
            $nDeparture = get_departByRangeTime('count', $sTime, $eTime, $station);          
            $nDelay = get_delayByRangeTime('count', $sTime, $eTime, $station);
            $nOnTime = get_ontimeByRangeTime('count', $sTime, $eTime, $station); 
            $nCancel = get_cancelByRangeTime('count', $sTime, $eTime, $station);
            $nOTP = get_percentOTP($nOnTime, $nDeparture, $nCancel);

            echo "{";
            echo "bulletClass: 'lastBullet',";
            echo "time:'".$limitTime.".00',";
            echo "departure: ".$nDeparture.",";
            echo "ontime: ".$nOnTime.",";          
            echo "delay: ".$nDelay.",";
            echo "cancel: ".$nCancel.",";          
            echo "otp :".$nOTP.",";
            echo "},";         
          }
        }
      }   
      ?>
    ],
    valueAxes: [ 
      {
        "id" : "v1",
        "axisAlpha": 0,
        "position": "left"
      },
      {
        "id" : "v2",
        "axisAlpha": 0,
        "position": "right"
      }
              
    ],
    startDuration: 1,
    "graphs": [ 
      {
          valueAxis: "v2",
        valueField: "departure",
        title: "Departure",
        type: "column",
        fillColors : "#3598DC",
        fillAlphas: 0.5,
        balloonText: "<span style='font-size:12px;'>[[title]] until [[category]]:<br><span style='font-size:20px;'>[[value]] flight</span> [[additional]]</span>",        
        alphaField: "alpha",
      }, 
      {
        id: "g2",
        valueAxis: "v1",
        title: "On Time Performance",
        valueField: "otp",
        type: "smoothedLine",        
        classNameField: "bulletClass",
        lineAlpha: 0.9,
        lineColor: "#786c56",
        balloonText: "<span style='font-size:12px;'>[[title]] until [[category]]:<br><span style='font-size:20px;'>[[value]]%</span> [[additional]]</span>", 
        lineThickness: 2,
        bullet: "round",
        bulletBorderColor: "#02617a",
        bulletBorderAlpha: 1,
        bulletBorderThickness: 2,
        bulletColor: "#89c4f4",
        showBalloon: true,
        animationPlayed: true,
      },
      {    
          valueAxis: "v2",
        valueField: "ontime",
        title: "On Time",
        type: "column",
        fillColors : "#1BBC9B",
        fillAlphas: 0.5,
        balloonText: "<span style='font-size:12px;'>[[title]] until [[category]]:<br><span style='font-size:20px;'>[[value]] flight</span> [[additional]]</span>",        
        alphaField: "alpha",
      },      
      {
          valueAxis: "v2",
        valueField: "delay",
        title: "Delay",
        type: "column",
        fillColors : "#F7CA18",
        fillAlphas: 0.5,
        balloonText: "<span style='font-size:12px;'>[[title]] until [[category]]:<br><span style='font-size:20px;'>[[value]] flight</span> [[additional]]</span>",        
        alphaField: "alpha",
      },      
      {    
          valueAxis: "v2",
        valueField: "cancel",
        title: "Cancel",
        type: "column",
        fillColors : "#D91E18",
        fillAlphas: 0.5,
        balloonText: "<span style='font-size:12px;'>[[title]] until [[category]]:<br><span style='font-size:20px;'>[[value]] flight</span> [[additional]]</span>",        
        alphaField: "alpha",
      },         
    ],
    "categoryField": "time",
    "categoryAxis": {
      "gridPosition": "start",
      "axisAlpha": 0,
      "tickLength": 0
    },
    chartCursor: {
        zoomable: true,
        categoryBalloonDateFormat: "H",
        cursorAlpha: 0,
        categoryBalloonColor: "#e26a6a",
        categoryBalloonAlpha: 0.8,
        valueBalloonsEnabled: true
    },
    legend: {
        bulletType: "round",
        equalWidths: true,
        valueWidth: 50,
        useGraphSettings: true,
        color: "#6c7b88"
    },
    export: {
      "enabled": true
    }
  });

}); 
</script>