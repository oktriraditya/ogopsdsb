<!-- BEGIN PAGE LEVEL STYLES -->
<!--<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE LEVEL PLUGINS -->

<!--<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
--><script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/global/scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
<style type="text/css">
    .tengah {
        text-align: center;
        vertical-align: central;
    }
    .no-wrap{
        white-space: nowrap;
    }

</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#viewFrame").on('load', function () {
            $(".loadingIframe").css('display', 'none');
        });
        $("#fltData").on('click', '.view', function () {
            $(".loadingIframe").css('display', 'block');
            var iframe = $("#viewFrame");

            var id = this.id.split(" ");
            //alert(id[3]);
            //var kata2 = id.split(":");
            var target_url = "<?php echo base_url() . 'occ/details_delay/'; ?>" + id[0] + "/" + id[1] + "/" + id[2] + "/" + id[3] + "/" + id[4] + "/" + id[5] + "/" + id[6];


            iframe.attr("src", target_url);
            //ajaxStart: function(){iframe.attr("src", "loading...");},
            //ajaxStop: function(){iframe.attr("src", target_url);}


        });
    });

</script>
<style>
    .loadingIframe{
        /*border: 16px solid #f3f3f3;
        border-top: 16px solid #3498db;
        border-radius: 50%;
        width: 100px;
        height: 100px;
        animation: spin 2s linear infinite;*/
        position: fixed;
        text-align: center;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <i class="fa fa-th-list font-dark"></i>
            <span class="caption-subject bold uppercase"> OTP per Day <?php //echo $stn.$org.$dest.$delay.$statuspp;  ?>, <?php echo date('d-m-Y', strtotime($datefrom)); ?> <?php if ($dateto) echo 'to ' . date('d-m-Y', strtotime($dateto)); ?></span>
        </div>

    </div>

    <div class="portlet-body">
        <div class="tools"> </div>
        <table class="table table-striped table-bordered table-hover table-condensed table-checkable order-column no-wrap" id="fltData">
            <thead>
                <tr>
                    <th width="100%" class="tengah">No.</th>
                    <th width="100%" class="tengah">Departure Date</th>
                    <th width="100%" class="tengah">#Departure</th>
                    <th width="100%" class="tengah">#On-Time</th>
                    <th width="100%" class="tengah">#Delay</th>
                    <th width="100%" class="tengah">#Arr. On-Time</th>
                    <th width="100%" class="tengah">OTP Departure</th>
                    <th width="100%" class="tengah">OTP Arrival</th>
                    <th width="100%" class="tengah">OTP First Departure</th>
                    <th width="100%" class="tengah">OTP ZMD First Departure</th>
                    <th width="100%" class="tengah">Average Delay Dep (mins)</th>
                    <th width="100%" class="tengah">Average Delay Arr (mins)</th>
                    <?php if (isset($delay) && $delay == "all") { ?>
                        <th width="100%" class="tengah">#Delay Technic</th>
                        <th width="100%" class="tengah">#Delay Station Handling</th>
                        <th width="100%" class="tengah">#Delay Commercial</th>
                        <th width="100%" class="tengah">#Delay System</th>
                        <th width="100%" class="tengah">#Delay Flight Ops</th>
                        <th width="100%" class="tengah">#Delay Airport Fac</th>
                        <th width="100%" class="tengah">#Delay Weather</th>
                        <th width="100%" class="tengah">#Delay Misc</th>
                        <th width="100%" class="tengah">%Delay Technic</th>
                        <th width="100%" class="tengah">%Delay Station Handling</th>
                        <th width="100%" class="tengah">%Delay Commercial</th>
                        <th width="100%" class="tengah">%Delay System</th>
                        <th width="100%" class="tengah">%Delay Flight Ops</th>
                        <th width="100%" class="tengah">%Delay Airport Fac</th>
                        <th width="100%" class="tengah">%Delay Weather</th>
                        <th width="100%" class="tengah">%Delay Misc</th>
                    <?php }if (isset($delay) && $delay == "origin") { ?>
                        <th width="100%" class="tengah">Delay Origin Technic</th>
                        <th width="100%" class="tengah">Delay Origin Station Handling</th>
                        <th width="100%" class="tengah">Delay Origin Commercial</th>
                        <th width="100%" class="tengah">Delay Origin System</th>
                        <th width="100%" class="tengah">Delay Origin Flight Ops</th>
                        <th width="100%" class="tengah">Delay Origin Airport Fac</th>
                        <th width="100%" class="tengah">Delay Origin Weather</th>
                        <th width="100%" class="tengah">Delay Origin Misc</th>
                        <th width="100%" class="tengah">%Delay Origin Technic</th>
                        <th width="100%" class="tengah">%Delay Origin Station Handling</th>
                        <th width="100%" class="tengah">%Delay Origin Commercial</th>
                        <th width="100%" class="tengah">%Delay Origin System</th>
                        <th width="100%" class="tengah">%Delay Origin Flight Ops</th>
                        <th width="100%" class="tengah">%Delay Origin Airport Fac</th>
                        <th width="100%" class="tengah">%Delay Origin Weather</th>
                        <th width="100%" class="tengah">%Delay Origin Misc</th>
                    <?php }if (isset($delay) && $delay == "previous") { ?>
                        <th width="100%" class="tengah">Delay Previous Technic</th>
                        <th width="100%" class="tengah">Delay Previous Station Handling</th>
                        <th width="100%" class="tengah">Delay Previous Commercial</th>
                        <th width="100%" class="tengah">Delay Previous System</th>
                        <th width="100%" class="tengah">Delay Previous Flight Ops</th>
                        <th width="100%" class="tengah">Delay Previous Airport Fac</th>
                        <th width="100%" class="tengah">Delay Previous Weather</th>
                        <th width="100%" class="tengah">Delay Previous Misc</th>
                        <th width="100%" class="tengah">%Delay Previous Technic</th>
                        <th width="100%" class="tengah">%Delay Previous Station Handling</th>
                        <th width="100%" class="tengah">%Delay Previous Commercial</th>
                        <th width="100%" class="tengah">%Delay Previous System</th>
                        <th width="100%" class="tengah">%Delay Previous Flight Ops</th>
                        <th width="100%" class="tengah">%Delay Previous Airport Fac</th>
                        <th width="100%" class="tengah">%Delay Previous Weather</th>
                        <th width="100%" class="tengah">%Delay Previous Misc</th>
                    <?php } ?>
                </tr>
            </thead>
            <?php
            $data = array();
            $dataTotal = array();
            $dataTotal['AVGDELAYARR'] = 0;
            $dataTotal['technic'] = 0;
            $dataTotal['STNH'] = 0;
            $dataTotal['COMM'] = 0;
            $dataTotal['SYST'] = 0;
            $dataTotal['FLOPS'] = 0;
            $dataTotal['APTF'] = 0;
            $dataTotal['WEATHER'] = 0;
            $dataTotal['MISC'] = 0;
            $dataTotal['TOTALCOD'] = 0;
            $dataTotal['PREVTECH'] = 0;
            $dataTotal['PREVSTNH'] = 0;
            $dataTotal['PREVCOMM'] = 0;
            $dataTotal['PREVSYST'] = 0;
            $dataTotal['PREVFLOPS'] = 0;
            $dataTotal['PREVAPTF'] = 0;
            $dataTotal['PREVWEATHER'] = 0;
            $dataTotal['PREVMISC'] = 0;
            $dataTotal['TOTALCODPREV'] = 0;
            $dataTotal['ORGTECH'] = 0;
            $dataTotal['ORGSTNH'] = 0;
            $dataTotal['ORGCOMM'] = 0;
            $dataTotal['ORGSYST'] = 0;
            $dataTotal['ORGFLOPS'] = 0;
            $dataTotal['ORGAPTF'] = 0;
            $dataTotal['ORGWEATHER'] = 0;
            $dataTotal['ORGMISC'] = 0;
            $dataTotal['TOTALCODORG'] = 0;
            $dataTotal['AVGDELAY'] = 0;
            $dataTotal['FIRSTFLIGHT'] = 0;
            $dataTotal['OTPFIRSTFLIGHT'] = 0;
            $dataTotal['ZMDFIRSTFLIGHT'] = 0;


            $dataTotal['schedule'] = 0;
            $dataTotal['ontime'] = 0;
            $dataTotal['arrontime'] = 0;
            $dataTotal['delay'] = 0;
            $dataTotal['SCHEDARR'] = 0;
            foreach ($daily_record_flight as $key => $value) {
                $data['schedule'][] = $value['SCHEDULED'];
                $data['tanggal'][] = $value['TANGGAL'];
                $data['ontime'][] = $value['ONTIME'];
                $data['arrontime'][] = $value['ARRONTIME'];
                $data['SCHEDARR'][] = $value['SCHEDARR'];
                $data['FIRSTFLIGHT'][] = $value['FIRSTFLIGHT'];
                $data['OTPFIRSTFLIGHT'][] = $value['OTPFIRSTFLIGHT'];
                $data['ZMDFIRSTFLIGHT'][] = $value['ZMDFIRSTFLIGHT'];



                $data['AVGDELAY'][] = $value['AVGDELAY'];
                $data['AVGDELAYARR'][] = $value['AVGDELAYARR'];
                $data['technic'][] = $value['TECH'];
                $data['STNH'][] = $value['STNH'];
                $data['COMM'][] = $value['COMM'];
                $data['SYST'][] = $value['SYST'];
                $data['FLOPS'][] = $value['FLOPS'];
                $data['APTF'][] = $value['APTF'];
                $data['WEATHER'][] = $value['WEATHER'];
                $data['MISC'][] = $value['MISC'];
                $data['TOTALCOD'][] = $value['TECH'] + $value['STNH'] + $value['COMM'] + $value['SYST'] + $value['FLOPS'] + $value['APTF'] + $value['WEATHER'] + $value['MISC'];
                $data['PREVTECH'][] = $value['PREVTECH'];
                $data['PREVSTNH'][] = $value['PREVSTNH'];
                $data['PREVCOMM'][] = $value['PREVCOMM'];
                $data['PREVSYST'][] = $value['PREVSYST'];
                $data['PREVFLOPS'][] = $value['PREVFLOPS'];
                $data['PREVAPTF'][] = $value['PREVAPTF'];
                $data['PREVWEATHER'][] = $value['PREVWEATHER'];
                $data['PREVMISC'][] = $value['PREVMISC'];
                $data['TOTALCODPREV'][] = $value['PREVTECH'] + $value['PREVSTNH'] + $value['PREVCOMM'] + $value['PREVSYST'] + $value['PREVFLOPS'] + $value['PREVAPTF'] + $value['PREVWEATHER'] + $value['PREVMISC'];
                $data['ORGTECH'][] = $value['ORGTECH'];
                $data['ORGSTNH'][] = $value['ORGSTNH'];
                $data['ORGCOMM'][] = $value['ORGCOMM'];
                $data['ORGSYST'][] = $value['ORGSYST'];
                $data['ORGFLOPS'][] = $value['ORGFLOPS'];
                $data['ORGAPTF'][] = $value['ORGAPTF'];
                $data['ORGWEATHER'][] = $value['ORGWEATHER'];
                $data['ORGMISC'][] = $value['ORGMISC'];
                $data['TOTALCODORG'][] = $value['ORGTECH'] + $value['ORGSTNH'] + $value['ORGCOMM'] + $value['ORGSYST'] + $value['ORGFLOPS'] + $value['ORGAPTF'] + $value['ORGWEATHER'] + $value['ORGMISC'];
            }
            /* foreach ($daily_record_cod as $key => $value){
              $data['AVGDELAY'][] = $value['AVGDELAY'];
              $data['AVGDELAYARR'][] = $value['AVGDELAYARR'];
              $data['technic'][] = $value['TECH'];
              $data['STNH'][] = $value['STNH'];
              $data['COMM'][] = $value['COMM'];
              $data['SYST'][] = $value['SYST'];
              $data['FLOPS'][] = $value['FLOPS'];
              $data['APTF'][] = $value['APTF'];
              $data['WEATHER'][] = $value['WEATHER'];
              $data['MISC'][] = $value['MISC'];
              $data['TOTALCOD'][] = $value['TECH']+$value['STNH']+$value['COMM']+$value['SYST']+$value['FLOPS']+$value['APTF']+$value['WEATHER']+$value['MISC'];
              $data['PREVTECH'][] = $value['PREVTECH'];
              $data['PREVSTNH'][] = $value['PREVSTNH'];
              $data['PREVCOMM'][] = $value['PREVCOMM'];
              $data['PREVSYST'][] = $value['PREVSYST'];
              $data['PREVFLOPS'][] = $value['PREVFLOPS'];
              $data['PREVAPTF'][] = $value['PREVAPTF'];
              $data['PREVWEATHER'][] = $value['PREVWEATHER'];
              $data['PREVMISC'][] = $value['PREVMISC'];
              $data['TOTALCODPREV'][] = $value['PREVTECH']+$value['PREVSTNH']+$value['PREVCOMM']+$value['PREVSYST']+$value['PREVFLOPS']+$value['PREVAPTF']+$value['PREVWEATHER']+$value['PREVMISC'];
              $data['ORGTECH'][] = $value['ORGTECH'];
              $data['ORGSTNH'][] = $value['ORGSTNH'];
              $data['ORGCOMM'][] = $value['ORGCOMM'];
              $data['ORGSYST'][] = $value['ORGSYST'];
              $data['ORGFLOPS'][] = $value['ORGFLOPS'];
              $data['ORGAPTF'][] = $value['ORGAPTF'];
              $data['ORGWEATHER'][] = $value['ORGWEATHER'];
              $data['ORGMISC'][] = $value['ORGMISC'];
              $data['TOTALCODORG'][] = $value['ORGTECH']+$value['ORGSTNH']+$value['ORGCOMM']+$value['ORGSYST']+$value['ORGFLOPS']+$value['ORGAPTF']+$value['ORGWEATHER']+$value['ORGMISC'];

              $dataTotal['AVGDELAYARR'] = 0;
              $dataTotal['technic'] = 0;
              $dataTotal['STNH'] = 0;
              $dataTotal['COMM'] = 0;
              $dataTotal['SYST'] = 0;
              $dataTotal['FLOPS'] = 0;
              $dataTotal['APTF'] = 0;
              $dataTotal['WEATHER'] = 0;
              $dataTotal['MISC'] = 0;
              $dataTotal['TOTALCOD'] = 0;
              $dataTotal['PREVTECH'] = 0;
              $dataTotal['PREVSTNH'] = 0;
              $dataTotal['PREVCOMM'] = 0;
              $dataTotal['PREVSYST'] = 0;
              $dataTotal['PREVFLOPS'] = 0;
              $dataTotal['PREVAPTF'] = 0;
              $dataTotal['PREVWEATHER'] = 0;
              $dataTotal['PREVMISC'] = 0;
              $dataTotal['TOTALCODPREV'] = 0;
              $dataTotal['ORGTECH'] = 0;
              $dataTotal['ORGSTNH'] = 0;
              $dataTotal['ORGCOMM'] = 0;
              $dataTotal['ORGSYST'] = 0;
              $dataTotal['ORGFLOPS'] = 0;
              $dataTotal['ORGAPTF'] = 0;
              $dataTotal['ORGWEATHER'] = 0;
              $dataTotal['ORGMISC'] = 0;
              $dataTotal['TOTALCODORG'] = 0;
              $dataTotal['AVGDELAY'] = 0;

              } */
            /*
              foreach($total_ontime as $key => $value)$data['ontime'][] = $value['ONTIME'];
              foreach ($total_arr_ontime as $key => $value)$data['arrontime'][] = $value['ARRONTIME'];
              foreach ($total_technic as $key => $value)$data['technic'][] = $value['TECHNIC'];
              foreach ($total_stnh as $key => $value)$data['STNH'][] = $value['STNH'];
              foreach ($total_comm as $key => $value)$data['COMM'][] = $value['COMM'];
              foreach ($total_syst as $key => $value)$data['SYST'][] = $value['SYST'];
              foreach ($total_flops as $key => $value)$data['FLOPS'][] = $value['FLOPS'];
              foreach ($total_aptf as $key => $value)$data['APTF'][] = $value['APTF'];
              foreach ($total_weather as $key => $value)$data['WEATHER'][] = $value['WEATHER'];
              foreach ($total_misc as $key => $value)$data['MISC'][] = $value['MISC'];
              foreach ($total_cod as $key => $value)$data['TOTALCOD'][] = $value['TOTALCOD']; */


            $count = count($daily_record_flight);
            ?>
            <tbody>
            <?php
            for ($i = 0; $i < $count; $i++) {
                ?>
                    <tr class="tengah">
                        <td> <?php echo $i + 1; ?> </td>
                        <td> <?php echo $data['tanggal'][$i]; ?></td>
                        <td> <?php $dataTotal['schedule']+= $data['schedule'][$i];
                echo $data['schedule'][$i]; ?> </td>
                        <td> <?php $dataTotal['ontime']+= $data['ontime'][$i];
                echo $data['ontime'][$i]; ?></td>
                        <td> <?php $dataTotal['delay']+= ($data['schedule'][$i] - $data['ontime'][$i]);
                echo $data['schedule'][$i] - $data['ontime'][$i]; ?></td>
                        <td> <?php $dataTotal['arrontime']+= $data['arrontime'][$i];
                echo $data['arrontime'][$i]; ?></td>
                        <td> <?php echo $data['schedule'][$i] > 0 ? round(($data['ontime'][$i] / $data['schedule'][$i]) * 100, 2) : 0; ?>%</td>
                        <td> <?php $dataTotal['SCHEDARR']+= $data['SCHEDARR'][$i];
                if ($data['SCHEDARR'][$i] > 0) echo round(($data['arrontime'][$i] / $data['SCHEDARR'][$i]) * 100, 2);
                else echo 0; ?>%</td>

                        <td> <?php $dataTotal['OTPFIRSTFLIGHT']+= $data['OTPFIRSTFLIGHT'][$i];
                $dataTotal['FIRSTFLIGHT']+= $data['FIRSTFLIGHT'][$i];
                if ($data['FIRSTFLIGHT'][$i] > 0) echo round(($data['OTPFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2);
                else echo 0; ?>%</td>
                        <td> <?php $dataTotal['ZMDFIRSTFLIGHT']+= $data['ZMDFIRSTFLIGHT'][$i];
                    if ($data['FIRSTFLIGHT'][$i] > 0) echo round(($data['ZMDFIRSTFLIGHT'][$i] / $data['FIRSTFLIGHT'][$i]) * 100, 2);
                    else echo 0; ?>%</td>

                        <td> <?php if (($data['schedule'][$i] - $data['ontime'][$i]) > 0) {
                        $dataTotal['AVGDELAY']+= $data['AVGDELAY'][$i];
                        echo round($data['AVGDELAY'][$i], 1);
                    } else echo 0; ?></td>
                        <td> <?php if (($data['schedule'][$i] - $data['ontime'][$i]) > 0) {
                        $dataTotal['AVGDELAYARR']+= $data['AVGDELAYARR'][$i];
                        echo round($data['AVGDELAYARR'][$i], 1);
                    } else echo 0; ?></td>
    <?php if (isset($delay) && $delay == "all") { ?>
                            <td> <?php $dataTotal['technic']+= $data['technic'][$i]; ?> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="1 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['technic'][$i]; ?> - Detail </a>
        <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['STNH']+= $data['STNH'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['STNH'][$i]; ?> - Detail </a>  <?php // echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['COMM']+= $data['COMM'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['COMM'][$i]; ?> - Detail </a>  <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['SYST']+= $data['SYST'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="4 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['SYST'][$i]; ?> - Detail  </a> <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                            <td> <?php $dataTotal['FLOPS']+= $data['FLOPS'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="5 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['FLOPS'][$i]; ?> - Detail </a>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['APTF']+= $data['APTF'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="6 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['APTF'][$i]; ?> - Detail </a>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                            <td> <?php $dataTotal['WEATHER']+= $data['WEATHER'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="7 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['WEATHER'][$i]; ?> - Detail </a>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['MISC']+= $data['MISC'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['MISC'][$i]; ?> - Detail </a>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?> %</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCOD'][$i] > 0) echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
    <?php }if (isset($delay) && $delay == "origin") { ?>
                            <td> <?php $dataTotal['ORGTECH']+= $data['ORGTECH'][$i]; ?> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="1 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGTECH'][$i]; ?> - Detail </a>
        <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGSTNH']+= $data['ORGSTNH'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGSTNH'][$i]; ?> - Detail </a>  <?php // echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGCOMM']+= $data['ORGCOMM'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGCOMM'][$i]; ?> - Detail </a>  <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGSYST']+= $data['ORGSYST'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="4 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGSYST'][$i]; ?> - Detail  </a> <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGFLOPS']+= $data['ORGFLOPS'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="5 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGFLOPS'][$i]; ?> - Detail </a>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGAPTF']+= $data['ORGAPTF'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="6 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGAPTF'][$i]; ?> - Detail </a>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGWEATHER']+= $data['ORGWEATHER'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="7 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGWEATHER'][$i]; ?> - Detail </a>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['ORGMISC']+= $data['ORGMISC'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['ORGMISC'][$i]; ?> - Detail </a>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGTECH'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
        else echo 0; ?> %</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGSTNH'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGCOMM'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
        else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGSYST'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGFLOPS'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGAPTF'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGWEATHER'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODORG'][$i] > 0) echo round(($data['ORGMISC'][$i] / $data['TOTALCODORG'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
    <?php }if (isset($delay) && $delay == "previous") { ?>
                            <td> <?php $dataTotal['PREVTECH']+= $data['PREVTECH'][$i]; ?> <a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="1 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVTECH'][$i]; ?> - Detail </a>
        <?php //echo round(($data['technic'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVSTNH']+= $data['PREVSTNH'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="2 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVSTNH'][$i]; ?> - Detail </a>  <?php // echo round(($data['STNH'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVCOMM']+= $data['PREVCOMM'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="3 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVCOMM'][$i]; ?> - Detail </a>  <?php //echo round(($data['COMM'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVSYST']+= $data['PREVSYST'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="4 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVSYST'][$i]; ?> - Detail  </a> <?php //echo round(($data['SYST'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVFLOPS']+= $data['PREVFLOPS'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="5 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVFLOPS'][$i]; ?> - Detail </a>  <?php //echo round(($data['FLOPS'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVAPTF']+= $data['PREVAPTF'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="6 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVAPTF'][$i]; ?> - Detail </a>  <?php //echo round(($data['APTF'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php $dataTotal['PREVWEATHER']+= $data['PREVWEATHER'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="7 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVWEATHER'][$i]; ?> - Detail </a>  <?php //echo round(($data['WEATHER'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2); ?></td>
                            <td> <?php $dataTotal['PREVMISC']+= $data['PREVMISC'][$i]; ?><a class="btn btn-sm btn-outline blue-sharp view" data-target="#view" data-toggle="modal" id="8 <?php echo $data['tanggal'][$i]; ?> <?php echo $stn; ?> <?php echo $org; ?> <?php echo $dest; ?> <?php echo $delay; ?> <?php echo $statuspp; ?>"><i class="fa fa-search"></i> <?php echo $data['PREVMISC'][$i]; ?> - Detail </a>  <?php //echo round(($data['MISC'][$i] / $data['TOTALCOD'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100,2);  ?></td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVTECH'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?> %</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVSTNH'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVCOMM'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVSYST'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVFLOPS'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVAPTF'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVWEATHER'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
                            <td> <?php if ($data['TOTALCODPREV'][$i] > 0) echo round(($data['PREVMISC'][$i] / $data['TOTALCODPREV'][$i]) * (($data['schedule'][$i] - $data['ontime'][$i]) / $data['schedule'][$i]) * 100, 2);
                    else echo 0; ?>%</td>
    <?php } ?>
                    </tr>
<?php } ?>
                <tr class="tengah">
<?php
$dataTotal['TOTALCOD'] = $dataTotal['technic'] + $dataTotal['STNH'] + $dataTotal['COMM'] + $dataTotal['SYST'] + $dataTotal['FLOPS'] + $dataTotal['APTF'] + $dataTotal['WEATHER'] + $dataTotal['MISC'];
$dataTotal['TOTALCODORG'] = $dataTotal['ORGTECH'] + $dataTotal['ORGSTNH'] + $dataTotal['ORGCOMM'] + $dataTotal['ORGSYST'] + $dataTotal['ORGFLOPS'] + $dataTotal['ORGAPTF'] + $dataTotal['ORGWEATHER'] + $dataTotal['ORGMISC'];
$dataTotal['TOTALCODPREV'] = $dataTotal['PREVTECH'] + $dataTotal['PREVSTNH'] + $dataTotal['PREVCOMM'] + $dataTotal['PREVSYST'] + $dataTotal['PREVFLOPS'] + $dataTotal['PREVAPTF'] + $dataTotal['PREVWEATHER'] + $dataTotal['PREVMISC'];
?>
                    <td> -</td>
                    <td> Total</td>
                    <td> <?php echo $dataTotal['schedule']; ?> </td>
                    <td> <?php echo $dataTotal['ontime']; ?></td>
                    <td> <?php echo $dataTotal['delay']; ?></td>
                    <td> <?php echo $dataTotal['arrontime']; ?></td>
                    <td> <?php echo $dataTotal['schedule'] > 0 ? round($dataTotal['ontime'] / $dataTotal['schedule'] * 100, 2) : 0 ?>% </td>
                    <td> <?php echo $dataTotal['SCHEDARR'] > 0 ? round($dataTotal['arrontime'] / $dataTotal['SCHEDARR'] * 100, 2) : 0 ?>% </td>
                    <td> <?php echo $dataTotal['FIRSTFLIGHT'] > 0 ? round($dataTotal['OTPFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2) : 0 ?>% </td>
                    <td> <?php echo $dataTotal['FIRSTFLIGHT'] > 0 ? round($dataTotal['ZMDFIRSTFLIGHT'] / $dataTotal['FIRSTFLIGHT'] * 100, 2) : 0 ?>% </td>
                    <td> <?php echo round($dataTotal['AVGDELAY'] / $count, 1); ?></td>
                    <td> <?php echo round($dataTotal['AVGDELAYARR'] / $count, 1); ?></td>
<?php if (isset($delay) && $delay == "all") { ?>
                        <td> <?php echo $dataTotal['technic']; ?></td>
                        <td> <?php echo $dataTotal['STNH']; ?></td>
                        <td> <?php echo $dataTotal['COMM']; ?></td>
                        <td> <?php echo $dataTotal['SYST']; ?></td>
                        <td> <?php echo $dataTotal['FLOPS']; ?></td>
                        <td> <?php echo $dataTotal['APTF']; ?></td>
                        <td> <?php echo $dataTotal['WEATHER']; ?></td>
                        <td> <?php echo $dataTotal['MISC']; ?></td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['technic'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['STNH'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['COMM'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['SYST'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['FLOPS'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['APTF'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['WEATHER'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCOD'] > 0) echo round(($dataTotal['MISC'] / $dataTotal['TOTALCOD']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
                    else echo 0; ?>%</td>
<?php }if (isset($delay) && $delay == "origin") { ?>
                        <td> <?php echo $dataTotal['ORGTECH']; ?></td>
                        <td> <?php echo $dataTotal['ORGSTNH']; ?></td>
                        <td> <?php echo $dataTotal['ORGCOMM']; ?></td>
                        <td> <?php echo $dataTotal['ORGSYST']; ?></td>
                        <td> <?php echo $dataTotal['ORGFLOPS']; ?></td>
                        <td> <?php echo $dataTotal['ORGAPTF']; ?></td>
                        <td> <?php echo $dataTotal['ORGWEATHER']; ?></td>
                        <td> <?php echo $dataTotal['ORGMISC']; ?></td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGTECH'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGSTNH'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGCOMM'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGSYST'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGFLOPS'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGAPTF'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGWEATHER'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODORG'] > 0) echo round(($dataTotal['ORGMISC'] / $dataTotal['TOTALCODORG']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>

<?php }if (isset($delay) && $delay == "previous") { ?>
                        <td> <?php echo $dataTotal['PREVTECH']; ?></td>
                        <td> <?php echo $dataTotal['PREVSTNH']; ?></td>
                        <td> <?php echo $dataTotal['PREVCOMM']; ?></td>
                        <td> <?php echo $dataTotal['PREVSYST']; ?></td>
                        <td> <?php echo $dataTotal['PREVFLOPS']; ?></td>
                        <td> <?php echo $dataTotal['PREVAPTF']; ?></td>
                        <td> <?php echo $dataTotal['PREVWEATHER']; ?></td>
                        <td> <?php echo $dataTotal['PREVMISC']; ?></td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVTECH'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVSTNH'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVCOMM'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVSYST'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVFLOPS'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVAPTF'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVWEATHER'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>
                        <td> <?php if ($dataTotal['TOTALCODPREV'] > 0) echo round(($dataTotal['PREVMISC'] / $dataTotal['TOTALCODPREV']) * ($dataTotal['delay'] / $dataTotal['schedule']) * 100, 2);
    else echo 0; ?>%</td>

<?php } ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="view" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">View Delay Detail</h4>
    </div>


    <div class="modal-body" style="height:450px !important;">
        <div class="loadingIframe">Please wait...</div>
        <iframe id="viewFrame" src="about:blank" width="100%" height="100%" frameborder="0">
        </iframe>

    </div>

    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#fltData').DataTable({
            dom: 'Bfrtip',
            pagingType: 'full_numbers',
            bSort: true,
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
            buttons: ['copyHtml5', 'excelHtml5', 'pdfHtml5'],
            scrollX: true,
            autoWidth: true
        });
    });
</script>
