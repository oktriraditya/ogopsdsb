function date_time(id) {    
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    //months = new Array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    months = new Array('January', 'February', 'March', 'April', 'Mey', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    d = date.getDate();
    day = date.getDay();
    //days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    h = date.getHours();
    if(h<10)
    {
            h = "0"+h;
    }
    m = date.getMinutes();
    if(m<10)
    {
            m = "0"+m;
    }
    s = date.getSeconds();
    if(s<10)
    {
            s = "0"+s;
    }
    result = '<strong>Local Time:</strong> '+days[day]+', '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
    document.getElementById(id).innerHTML = result;
    setTimeout('date_time("'+id+'");','1000');
    

    return true;
}

function date_time_utc(id) {  
    date = new Date;
    year = date.getUTCFullYear();
    month = date.getUTCMonth();
    //months = new Array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    months = new Array('January', 'February', 'March', 'April', 'Mey', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    d = date.getUTCDate();
    day = date.getUTCDay();
    //days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    h = date.getUTCHours();
    if(h<10)
    {
            h = "0"+h;
    }
    m = date.getUTCMinutes();
    if(m<10)
    {
            m = "0"+m;
    }
    s = date.getUTCSeconds();
    if(s<10)
    {
            s = "0"+s;
    }
    result = '<strong>UTC Time:</strong> '+days[day]+', '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
    document.getElementById(id).innerHTML = result;
    setTimeout('date_time_utc("'+id+'");','1000');

    return true;
}